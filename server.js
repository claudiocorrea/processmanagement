var port = 80,		
    compression = require('compression'),
	express = require('express'),
	app = express(),
	session = require('express-session'),
	bodyParser = require('body-parser'),
	path = require('path'),
	router = express.Router(),
	routerSession = require('./nodejs/routes/session'),
	routerUser = require('./nodejs/routes/user'),
	routerForm = require('./nodejs/routes/sgpForm'),
	routerSoap = require('./nodejs/routes/soapCalls'),
	routerPasosWkf = require('./nodejs/routes/pasosWorkflow'),
    routerComboAnidado = require('./nodejs/routes/comboAnidado'),
    routerTareasPersonales = require('./nodejs/routes/tareasPersonales'),
    routerWorkflowRules = require('./nodejs/routes/workflowRules'),
    routerAlerts = require('./nodejs/routes/alerts'),
    routerNotificaciones = require('./nodejs/routes/notificaciones'),
    routerTrackerMail = require('./nodejs/routes/emailJob');

	
global.DB = 'SPM';
	
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    res.header('Cache-Control', 'no-cache');

    if ( req.method == 'OPTIONS') { res.sendStatus(200); res.end(); } else { next(); }
})

app.use(compression());
app.use(express.static('./'));

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json({limit: '50mb'}));

app.use(session({
    secret: 'SGPProcessManagement2017',
    resave: false,
    saveUninitialized: true,
    user: null,
    token: null,
    name: 'SGPProcessManagementSession'
}));

/*router.get('/', function (req, res, next) {
    console.log("URL Params: " + JSON.stringify(req.query));
    var esFormIntegrado = req.query.verForm;

    if (esFormIntegrado != null && typeof esFormIntegrado != undefined) {
        console.log("Ha entrado al form");
        res.sendFile(path.join(__dirname + '/home.html'));
    } else {
        console.log("Ha entrado solo al home");
		res.sendFile(path.join(__dirname + '/login.html'));
    }
});*/


app.use(router);
app.use('/session', routerSession);
app.use('/user', routerUser);
app.use('/form', routerForm);
app.use('/soap', routerSoap);
app.use('/pasosWorkflow', routerPasosWkf);
app.use('/comboAnidado', routerComboAnidado);
app.use('/tareasPersonales', routerTareasPersonales);
app.use('/workflowRules', routerWorkflowRules);
app.use('/alerts', routerAlerts);
app.use('/notification', routerNotificaciones);
app.use('/images', routerTrackerMail);

app.listen(port, function () {
    console.log('Servidor iniciado ( port: ' + port + ') - ' + new Date() + '... ');
});
