/**
 * Created by Antonio on 7/31/2017.
 */
var js2xmlparser = require('js2xmlparser2'),
    fs = require('fs');

exports.exportJSON = function (jsonData, config) {
    var result = {},
        config = config || {useCDATA: true, declaration: {include: false, encoding: "UTF-8"}},
        root = (config.root && config.root != "")?config.root:"root",
        useCDATA = (config.useCDATA != null && typeof config.useCDATA != undefined)?config.useCDATA:true,
        includeDeclaration = (config.declaration)?config.declaration.include:false,
        xml = null;

    return new Promise(function (resolve, reject) {
        try {
            xml = js2xmlparser(root, jsonData, {
                useCDATA: useCDATA,
                declaration: {
                    include: includeDeclaration,
                    encoding: "UTF-8"
                }
            });
            result = {success: true, xml: xml};
            resolve(result);
        } catch (e) {
            result = {success: false, error: e};
            return reject(result);
        }

    });

}

exports.exportAndSaveJSON = function (jsonData, config) {
    var result = {},
        config = config || {useCDATA: true, declaration: {include: false, encoding: "UTF-8"}},
        destFile = (config.destFile)?config.destFile:"c:\\temp\\jsonExported2Xml.xml",
        root = (config.root && config.root != "")?config.root:"root",
        useCDATA = (config.useCDATA != null && typeof config.useCDATA != undefined)?config.useCDATA:true,
        includeDeclaration = (config.declaration)?config.declaration.include:false,
        xml = null;

    return new Promise(function (resolve, reject) {
        try {
            xml = js2xmlparser(root, jsonData, {
                useCDATA: useCDATA,
                declaration: {
                    include: includeDeclaration,
                    encoding: "UTF-8"
                }
            });

            fs.writeFile(destFile, xml, function (err) {
                if (err) return reject({success: false, error: err});
                resolve({success: true, xml: xml});
            })
        } catch (e) {
            console.log("Se produjo el error: " + e);
            result = {success: false, error: e};
            return reject({success: false, error: e});
        }

    });

}