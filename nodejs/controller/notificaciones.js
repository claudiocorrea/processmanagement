var DBUtil = require('../util/DBUtil'),
    Promise = require('bluebird'),
    moment = require('moment');

exports.getTotalNotificationsByUser = function (data) {
    var config = {
            spName: '[dbo].[Mensajes_TX_IdUser_Cantidad]',
            params: {
                input: [
                    {name: 'IdUser', type: 'Int', value: data.IdUser}
                ]
            }
        };

    return new Promise(function (resolve, reject) {
        DBUtil.executeSp(config)
            .then(function (result) {
                if (Array.isArray(result)) {
                    result = result[0];
                }
                resolve({success: true, cantidad: result[0].Cantidad});
            })
            .catch(function (error) {
                reject({success: false, error: error});
            });
    });
};

exports.getPendingsByUser = function (data) {
    var config = {
        spName: '[dbo].[Mensajes_TX_IdUser]',
        params: {
            input: [
                {name: 'user', type: 'Int', value: data.IdUser}
            ]
        }
    };

    return new Promise(function (resolve, reject) {
        DBUtil.executeSp(config)
            .then(function (result) {
                if (Array.isArray(result)) {
                    result = result[0];
                }
                resolve({success: true, data: result});
            })
            .catch(function (error) {
                reject({success: false, error: error});
            });
    });
};

exports.markNotificationAsRead = function (data) {
    var config = {
        spName: '[dbo].[Mensajes_MarkAsRead]',
        params: {
            input: [
                {name: 'IdMensaje', type: 'Int', value: data.IdMensaje},
                {name: 'FechaLeido', type: 'VarChar', value: moment().format('DD/MM/YYYY')},
                {name: 'HoraLeido', type: 'VarChar', value: moment().format('HH:mm:ss')}

            ]
        }
    };

    return new Promise(function (resolve, reject) {
        DBUtil.executeSp(config)
            .then(function (result) {
                if (Array.isArray(result)) {
                    result = result[0];
                }
                resolve({success: true});
            })
            .catch(function (error) {
                reject({success: false, error: error, notificationId: data.IdMensaje});
            });
    });
}