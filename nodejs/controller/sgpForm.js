var DBUtil = require('../util/DBUtil'),
    js2xmlparser = require("js2xmlparser2"),
    json2xmlExporter = require('../util/json2xml'),
    fs = require('fs'),
    Promise = require('bluebird'),
    workflowCommentsController = require('./workflowComments'),
    workflowAttachmentsController = require('./workflowAttachments'),
    moment = require('moment');


exports.getFormFieldsJson = function (req, res) {
    var config = {
        spName: '[dbo].[Queries_asmx.GetJsonForm]',
        params: {
            input: [
                {name: 'IdForm', type: 'Int', value: req.query.idForm},
                {name: 'WorkFlowId', type: 'Int', value: req.query.workflowId}
            ]
        }
    };

    DBUtil.executeSp(config)
        .then (function (result) {

            res.status(200);
            try {
                var form = [],
                    result2 = JSON.stringify(result).replace(/\\t/g, "").replace(/\\"/g, '"').replace(/\\\\/g, "\\");

                result2 = result2.replace("[[{\"JsonForm\":\"", "");

                result2 = result2.substring(0, result2.length - 4);

                var response = JSON.parse(result2);

                if (response.secciones) {
                    response.secciones.forEach(function (seccion) {
                        if (seccion.items) {
                            var items = [];
                            seccion.items.forEach(function(item) {
                                if (item.ValorActual && item.ValorActual.indexOf("||") >= 0) {
                                    item.ValorActual = parseValues(item.ValorActual);
                                }
                                if (item.ValoresPosibles && item.ValoresPosibles.indexOf("||") >= 0) {
                                    item.ValoresPosibles = parseValues(item.ValoresPosibles	);
                                }
                                items.push(item);
                            });
                        }
                        seccion.items = items;
                        form.push(seccion);
                    });
                }

                secciones = {
                    id: response.id,
                    secciones: form
                };
                res.json(secciones);

            } catch (e) {
                console.log(e)
                res.json(result);
            }
        })
        .catch (function (error) {
            res.status(500);
            res.json(error);
        });
}

parseValues = function (listaPV) {
    var retList = [],
        arr = listaPV.split("|");

    if (Array.isArray(arr)) {
        arr.forEach(function(str) {
            var a = str.split("\\");
            if (Array.isArray(a)) {
                retList.push(a[1]);
            }
        });
    }
    return retList;
}

parsePossibleValues = function (listaPV) {
    var retList = [],
        arr = listaPV.replace(/\|\|/g, "\\").split("|");

    if (Array.isArray(arr)) {
        arr.forEach(function(str) {
            var a = str.split("\\");
            if (Array.isArray(a)) {
                retList.push({
                    id: a[1],
                    desc: a[0]
                });
            }
        });
    }
    return retList;
}



exports.captureForm = function (req, res) {
    var jsonData = req.body,
        userId = jsonData.idUser,
        languageId = req.body.languageId,
        enabledMessages = req.body.enabledMessages,
        enabedEmails = req.body.enabledEmails,

        childFormsData = (jsonData.childForms != null && jsonData.childForms.length > 0)?jsonData.childForms:null,
        xmlFormsToSave = [],//Array que contiene todos los xml que representan todos los forms que se deben guardar en esta peticion
        promisesArray = [],//Aca se guardan las promesas que corresponden a cada form embebido
        fileNameFormPpal = "form_ppal" + jsonData.SystemIdStep + "_" + jsonData.SystemForm + ".xml";

    delete jsonData.childForms;

    //Siempre el primer form es el principal
    xmlFormsToSave.push(js2xmlparser("root", jsonData, {useCDATA: true, declaration: {include: false, encoding: "UTF-8"}}));

    //Exporto el xml del form padre
    if (process.env.NODEJS_EXPORT_JSON_PARAMS) {
        fileNameFormPpal = process.env.NODEJS_EXPORT_JSON_PARAMS_DIR + "/" + fileNameFormPpal;

        json2xmlExporter.exportAndSaveJSON(jsonData, {destFile: fileNameFormPpal});
    }


    //Se generan los XML de los forms hijos
    if (childFormsData != null && typeof childFormsData != undefined) {
        childFormsData.forEach(function (row) {
            row.campos.forEach(function (linea) {
                var arrObj = [],
                    fileNameFormChild = "form",
                    seq = null;
                Object.keys(linea).forEach(function (key) {
                    if (key != "id" && key != "sequence") {
                        arrObj.push({nombre: key, valor: linea[key]});
                    }
                    if (key == "System_Sequence") {
                        seq = linea[key];
                    }
                    if (key == "SYSTEMFORM") {
                        formChildId = linea[key];
                    }

                });
                //Agrego campos extra que vienen en la cabecera de la peticion
                arrObj.push({nombre: 'IdStep', valor: jsonData.SystemIdStep});
                arrObj.push({nombre: 'Apruebo', valor: jsonData.Apruebo});
                arrObj.push({nombre: 'IdUser', valor: jsonData.SystemIdUser});
                arrObj.push({nombre: 'SoloGrabo', valor: jsonData.SoloGrabo});
                arrObj.push({nombre: 'Diseno2', valor: jsonData.Diseno2});

                arrObj = {campo: [arrObj]};

                //Guardo este form generado
                xmlFormsToSave.push(js2xmlparser("root", arrObj, {useCDATA: true, declaration: {include: false, encoding: "UTF-8"}}));

                //Exporto el xml
                if (process.env.NODEJS_EXPORT_JSON_PARAMS) {
                    fileNameFormChild = process.env.NODEJS_EXPORT_JSON_PARAMS_DIR + "/" + fileNameFormChild + "_step" + jsonData.SystemIdStep + "_" + formChildId + "_" + seq + ".xml";

                    json2xmlExporter.exportAndSaveJSON(arrObj, {destFile: fileNameFormChild});
                }
            });

        });

    }

    var index = 0,
        p_seq = 1,
        resultsArray = [],
        comment = '';

    function processData () {
        if (index < xmlFormsToSave.length) {
            console.log("*******************  Comenzado [dbo].[Queries_asmx.CaptureFormFromXml] - " + new Date());
            var config = {
                spName: '[dbo].[Queries_asmx.CaptureFormFromXml]',
                params: {
                    input: [
                        {name: 'IdUser', type: 'Int', value: userId},
                        {name: 'IdLanguage', type: 'Int', value: languageId},
                        {name: 'xmlData', type: 'NVarchar', value: xmlFormsToSave[index]},
                        {name: 'EnabledMessages', type: 'Int', value: enabledMessages},
                        {name: 'EnabledEmails', type: 'Int', value: enabedEmails}
                    ],
                    output: [
                        {name: 'CaptureForm', type: 'NVarchar', value: null}
                    ]
                }
            };

            index += 1;
            p_seq += 1;

            console.log(config.params.input)

            DBUtil.executeSp(config)
                .then(function (result) {
                    console.log('*** Respuesta capture form: ', result)
                    try {
                        if (Array.isArray(result)) {
                            if (result.length > 1) {
                                result = (result[1])[0];
                            } else {
                                result = (result[0])[0];
                            }
                        } else {
                            result = {Result: 'Error'}
                        }

                        if (result.Result.startsWith('OK')) {
                            resultsArray.push(true);
                            if(result.Result.includes('|')) {
                                var arr = result.Result.split('|')
                                if(arr.length > 0 && arr[1].length > 1)
                                    comment = result.Result.split('|')[1]
                            }
                        } else {
                            result["success"] = false;
                            resultsArray.push(false);
                        }
                        processData();
                    } catch(error) { console.log('ERROR!: ', error)}
                })
                .catch(function (error) {
                    console.log(p_seq + ") --- Catch Queries_asmx.CaptureForm (" + new Date() + "): " + JSON.stringify(error));
                    resultsArray.push(false);
                });


        } else {
            var flag = true;
            resultsArray.forEach(function (r) {
                if (!r) {
                    flag = false;
                }
            });

            if (!flag) {
                res.status(500);
                res.json({success: false});
            } else {
                res.status(200);
                res.json({success: true, comment: comment});
            }
        }
    }

    processData();
}

processArray = function (array, otherConfig, fn) {
    var index = 0,
        p_seq = 1;

    function next() {
        if (index < array.length) {
            fn(otherConfig.userId, otherConfig.languageId, otherConfig.enabledMessages, otherConfig.enabedEmails, array[index++], p_seq).then(next());
            p_seq += 1;
        }
    }

    next();
}

invokeCaptureForm = function (userId, languageId, enabledMessages, enabedEmails, xmlForm, p_seq, p_delay) {
    console.log("\n Comenzando executeSp CaptureFormFromXml (" + new Date() + ")");

    var config = {
        //spName: '[dbo].[Queries_asmx.CaptureForm]',
        spName: '[dbo].[Queries_asmx.CaptureFormFromXml]',
        params: {
            input: [
                {name: 'IdUser', type: 'Int', value: userId},
                {name: 'IdLanguage', type: 'Int', value: languageId},
                //{name: 'Fields', type: 'NVarchar', value: req.body.fields},
                {name: 'xmlData', type: 'NVarchar', value: xmlForm},
                {name: 'EnabledMessages', type: 'Int', value: enabledMessages},
                {name: 'EnabledEmails', type: 'Int', value: enabedEmails}
            ],
            output: [
                {name: 'CaptureForm', type: 'NVarchar', value: null}
            ]
        }
    };

    return DBUtil.executeSp(config)
        .then(function (result) {
            if (Array.isArray(result)) {
                if (result.length > 1) {
                    result = (result[1])[0];
                } else {
                    result = (result[0])[0];
                }
            } else {
                result = {Result: 'Error'}
            }

            if (result.Result.startsWith('OK')) {

                return true;
            } else {
                result["success"] = false;
                return false;
            }
        })
        .catch(function (error) {
            console.log(p_seq + ") --- Catch Queries_asmx.CaptureForm (" + new Date() + "): " + JSON.stringify(error));
            return false;
            //reject(false);
        });

}

createExtJSField = function(item) {
    var field = (item.Tipo == "Radio Button") ? {} : {
        idField: item.IdField,
        name: item.Id,
        fieldLabel: item.TextoFrontEnd,
        labelAlign: (item.TextoFrontEnd.length > 15) ? 'top' : 'left',
        editable: false,
        posicion: item.Posicion,
        mandatory: eval(item.Obligatorio), readonly: eval(item.SoloLectura),
        originDB: item.IdOrigen, tableDB: item.Tablename,
        spDB: item.Procedimiento, input: item.Parametros,
        fieldValue: item.CampoValueCombo, comboText: item.CampoTextCombo,
        hidden: (item.Visible == 1) ? true : false,
    };

    field.listeners = { focusenter: 'fieldSelection' }
    if (item.Style != "") { field.fieldStyle = item.Style }

    try {
        switch (item.Tipo) {
            case "FieldContainer":
                field = {
                    xtype: 'panel', idField: item.IdField,
                    name: item.Id.toLowerCase(),
                    posicion: item.Posicion,
                    layout: 'hbox',
                    defaults: { flex: 1, margin: '0 20 10 0' },
                    items: [ ],
                    listeners: { focusleave: 'outSelectionColumns', add: 'addItemToFieldContainer' }
                }
                break;
            case "TextBox":
                field.xtype = 'textfield';
                break;
            case "TextArea":
                field.xtype = 'htmleditor';
                field.labelAlign = 'top'
                field.listeners = { focusenter: 'fieldSelection', sync: 'fieldSelection' }
                break;
            case "DateField":
                field.xtype = 'datefield'
                field.dategreater = eval(item.FechaMayorOIgualaHoy)
                break;
            case "NumberField":
                field.xtype = 'numberfield'
                break;
            case "Combo":
                field.xtype = 'combobox'
                field.editable = false
                field.typeAhead = true
                field.displayField = 'value'
                field.valueField = 'id'
                field.publishes = 'value',
                    field.allowEmpty = item.PermiteValorVacio
                field.optionNew = item.OpcionNuevo
                break;
            case "CheckBox":
                field.xtype = 'checkbox';
                field.fieldLabel = null;
                field.boxLabel = item.TextoFrontEnd;
                break;
            case "Radio Button":
                field = {
                    xtype: 'radiogroup', idField: item.IdField,
                    name: item.Id.toLowerCase(),
                    fieldLabel: item.TextoFrontEnd,
                    posicion: item.Posicion,
                    items:[
                        {	name:'opcion',
                            bind:{ boxLabel:'{txtFields.words.yes}'},
                            inputValue:'si', labelClsExtra:'labelField'
                        },{
                            name:'opcion',
                            boxLabel:'No', inputValue:'no',
                            padding:'0 0 0 15', labelClsExtra:'labelField'
                        }
                    ],
                    mandatory: !eval(item.Obligatorio), readonly: eval(item.SoloLectura),
                    listeners: { focusenter: 'fieldSelection' }
                }
                break;
            case "Lista a Buscar Multiple":
                field.xtype = 'tagfield';
                break;
            case "Form":
                field = {
                    xtype: 'fieldcontainer', idField: item.IdField,
                    name: 'form_' + item.Id,
                    fieldLabel: item.TextoFrontEnd,
                    labelClsExtra:'labelField', posicion: item.Posicion,
                    IdForm: item.IdFormularioHijo,
                    items: [
                        {	xtype:'button', userCls:'circular',
                            iconCls:'x-fa fa-file-text-o',
                            tooltip: item.TextoFrontEnd
                        }
                    ],
                    mandatory: !eval(item.Obligatorio), readonly: eval(item.SoloLectura),
                    listeners: { focusenter: 'fieldSelection' }
                }
                break;
        }
    } catch (e) {
        console.log("*** ERROR al parsear EXTJS Item: " + e)
    }

    return field
}

/**
 * Devuelve un pares clave/valor donde la clave es el id del FC y el valor es la seccion a la cual pertenece el FC
 * @param dataset Conjunto de campos del form
 * @return Mapa de los FC encontrados
 */
function getFieldContainersFromDataset(dataset) {
    var map = new Map();

    dataset.forEach(function (d) {
        if (d.Tipo == "FieldContainer") {
            map.set(d.Id, d.Seccion);
        }
    });

    return map;
}

/**
 * Los campos que pertenecen a un fieldContainer tienen como nombre de seccion el ID de dicho FC. Esta funcion corrige eso
 * @param dataset
 * @param mapFieldContainers
 * @return {Array}
 */
function fixSectionsFromDataSet(dataset, mapFieldContainers) {

    dataset.forEach(function (d) {
        var seccionFc = null;

        seccionFc = mapFieldContainers.get(d.Seccion);
        //El campo tiene como nombre de Seccion el Id de un FieldContainer?
        if (seccionFc != undefined) {
            var fcId = d.Seccion;

            d.Seccion = seccionFc; //Corrige el nombre de Seccion
            d.fieldContainerPadre = fcId; //Marca este campo como hijo de un FieldContainer
        }

    });

    return dataset;
}

/**
 * Devuelve un Map con todos los nombres de Secciones
 * @param dataset   Datos donde buscar
 * @return Map con nombres de secciones
 *
 */
function getSectionsFromDataSet(dataset) {
    var set = new Set();

    dataset.forEach(function (d) {
        set.add(d.Seccion);
    });

    return set;
}

/**
 * Asegura que todos los campos que pertenecen a una misma seccion se encuentren agrupados correctamente
 * @param dataset   Datos originales
 * @return  Datos agrupados por seccion
 */
function fixFieldsInSections(dataset) {
    var secciones = getSectionsFromDataSet(dataset),
        tempDataset = [];

    secciones.forEach(function (seccion) {
        dataset.forEach(function (item) {
            if (item.Seccion !== seccion) return;

            tempDataset.push(item);
        })
    });

    return tempDataset;
}

exports.getFormFields = function(req, res) {
    var config = {
        spName: '[dbo].[getFormFields]',
        params: {
            input: [
                {name: 'IdForm', type: 'Int', value: req.query.idForm},
                {name: 'WorkFlowId', type: 'Int', value: (req.query.workflowId != null && req.query.workflowId != undefined && req.query.workflowId != "")?req.query.workflowId:null},
                {name: 'User', type: 'Int', value: req.query.userId}
            ]
        }
    }

    DBUtil.executeSp(config)
        .then(function (result) {
            //console.log("----- Then getFormFields: " + JSON.stringify(result));
            var ignorarHabilitado = (req.query.workflowId != null && req.query.workflowId != undefined && req.query.workflowId != "")?false:true,
                form = {
                    id: '',
                    secciones: []
                },
                hayCampoCheckbox = false;

            form.ocultaBotonGrabarEnviar = false;

            if (req.query.ignoraCampoHabilitados != null && req.query.ignoraCampoHabilitados != undefined) {
                ignorarHabilitado = true;
            }

            if (Array.isArray(result)) {
                if (Array.isArray(result[0])) {
                    result = result[0];
                }

                var cambioSeccion = false,
                    seccionActual = null,
                    formName = "",
                    rechazoHabilitado = false,
                    seccionFieldContainer = null,
                    fieldContainersAvail = getFieldContainersFromDataset(result);


                result = fixSectionsFromDataSet(result, fieldContainersAvail);
                secciones = getSectionsFromDataSet(result);

                secciones.forEach(function (seccion) {
                    var tempSeccion = {
                            texto: seccion,
                            items: []
                        },
                        tieneCampos = false;

                    result.forEach(function (r2) {

                        formName = r2.FormName;

                        if (r2.Seccion !== seccion) return;

                        if (r2.RechazoEnFormularios != null && r2.RechazoEnFormularios != undefined ) {
                            if ((isNaN(r2.RechazoEnFormularios) && r2.RechazoEnFormularios.toLowerCase() == "true") || r2.RechazoEnFormularios == "1"  || r2.RechazoEnFormularios == 1) {
                                rechazoHabilitado = true;
                            }
                        }
                        if (ignorarHabilitado) {
                            if (r2.Seccion == seccion){
                                if (r2.Tipo == "Combo" || r2.Tipo == "Lista a Buscar Multiple") {
                                    try {
                                        r2.ValorActual = JSON.parse(r2.ValorActual);
                                    }catch (e) {
                                        if (r2.Tipo == "Lista a Buscar Multiple") {
                                            r2.ValorActual = r2.ValorActual.replace(/\|\|\|\|/g, "\\").replace(/\|\|/g, '|');
                                            if (r2.ValorActual.endsWith("|")) r2.ValorActual = r2.ValorActual.substring(0, r2.ValorActual.lastIndexOf("|"));
                                            if (r2.ValorActual && r2.ValorActual.indexOf("|") >= 0) {
                                                r2.ValorActual = parseValues(r2.ValorActual);
                                            }else {
                                                if (r2.ValorActual != null && r2.ValorActual != "") {
                                                    var arr = r2.ValorActual.split("\\");
                                                    r2.ValorActual = [arr[1]];
                                                }
                                            }
                                        }

                                    }
                                }
                                r2.Visible = 1;
                                tempSeccion.items.push(r2);
                                tieneCampos = true;
                            }

                        } else { // Hay que considerar el campo Habilitado
                            if (r2.Habilitado == 1 || (r2.Habilitado == 0 && r2.Visible == 1)) {
                                if (r2.Seccion == seccion){
                                    if (r2.Tipo == "Combo" || r2.Tipo == "Lista a Buscar Multiple") {
                                        try {
                                            r2.ValorActual = JSON.parse(r2.ValorActual);
                                        }catch (e) {
                                            if (r2.Tipo == "Lista a Buscar Multiple") {
                                                r2.ValorActual = r2.ValorActual.replace(/\|\|\|\|/g, "\\").replace(/\|\|/g, '|');
                                                if (r2.ValorActual.endsWith("|")) r2.ValorActual = r2.ValorActual.substring(0, r2.ValorActual.lastIndexOf("|"));
                                                if (r2.ValorActual && r2.ValorActual.indexOf("|") >= 0) {
                                                    r2.ValorActual = parseValues(r2.ValorActual);
                                                }else {
                                                    if (r2.ValorActual != null && r2.ValorActual != "") {
                                                        var arr = r2.ValorActual.split("\\");
                                                        r2.ValorActual = [arr[1]];
                                                    }
                                                }
                                            }

                                        }
                                    }
                                    tempSeccion.items.push(r2);
                                    tieneCampos = true;
                                }

                            }

                        }

                        if (r2.Tipo == "CheckBox") {
                            hayCampoCheckbox = true;
                            form.ocultaBotonGrabarEnviar = true;
                            if (r2.OcultaAprobarSiEsFalso != null && r2.OcultaAprobarSiEsFalso != "") {
                                form.ocultaBotonGrabarEnviar = (r2.OcultaAprobarSiEsFalso == "0" || r2.OcultaAprobarSiEsFalso.toLowerCase() == "true")?false:true;
                            }
                        }

                    });
                    if (tieneCampos) form.secciones.push(tempSeccion);

                });

                /*result.forEach(function(r) {
                    var seccion,
                        esFieldContainer = false,
                        tieneCampos = false;

                    formName = r.FormName;

                    if (r.Seccion != seccionActual) {
                        cambioSeccion = true;
                        //Guardo la seccion anterior
                        seccionActual = r.Seccion;
                        seccion = {
                            texto: seccionActual,
                            items: []
                        }
                        //Genero una nueva seccion

                        console.log("Seccion: " + seccionActual);

                        result.forEach(function(r2){


                        });
                    } else cambioSeccion = false;


                    if (cambioSeccion) {
                        if (tieneCampos) form.secciones.push(seccion);
                    }
                });*/
            }

            res.status(200);
            form.id = formName;
            form.rechazoHabilitado = rechazoHabilitado;
            res.json(form);
        })
        .catch(function (error) {
            console.log("----- Catch getFormFields: " + JSON.stringify(error));
            console.log(error);
            res.status(500);
            res.json(error);
        });
}



exports.getFormFieldValues = function (req, res) {
    console.log('INICIANDO [dbo].[FormsFields_TX_ValoresPosiblesDeCampo]');

    var config = {
            spName: '[dbo].[FormsFields_TX_ValoresPosiblesDeCampo]',
            params: {
                input: [
                    {name: 'IdField', type: 'Int', value: req.query.fieldId},
                    {name: 'StepId', type: 'Int', value: req.query.stepId},
                    {name: 'IdUser', type: 'Int', value: req.query.userId}
                ]
            }
        },
        fieldValue = req.query.fieldValue,
        fieldDescription = req.query.fieldDescription;


    DBUtil.executeSp(config)
        .then(function (result) {
            result = result[0];

            if (result.length > 0) {
                var temp = [];
                result.forEach(function (r) {
                    var obj = {
                        id: r[fieldValue],
                        descripcion: r[fieldDescription]
                    }

                    temp.push(obj);
                });

                result = temp;
            }
            res.status(200);
            res.json({success: true, result: result});
        })
        .catch(function (error) {
            console.log("Catch FormsFields_TX_ValoresPosiblesDeCampo: " + JSON.stringify(error));
            res.status(500);
            res.json({success: false, error: error});
        })
}

exports.getChildFormFieldValues = function (req, res) {
    console.log("INICIANDO FormsChild_TX_DetailByFormWorkFlow");

    var config = {
        spName: '[dbo].[FormsChild_TX_DetailByFormWorkFlow]',
        params: {
            input: [
                {name: 'WorkFlowId', type: 'Int', value: req.query.workflowId},
                {name: 'IdChildForm', type: 'Int', value: req.query.idForm}
            ]
        }
    };

    DBUtil.executeSp(config)
        .then(function (result) {
            var seq = 0,
                tempRes = [];

            result = result[0];

            result.forEach(function (res1) {

                if (res1.Line != seq) {
                    var objTemp = {};
                    seq = res1.Line;

                    objTemp.sequence = seq;

                    result.forEach(function (rest) {
                        if (rest.Line == seq) {
                            objTemp[rest.Fieldname] = rest.Value;
                        }
                    });

                    tempRes.push(objTemp);
                }
            });
            res.status(200);
            res.json({success: true, result: tempRes});
        })
        .catch(function (error) {
            res.status(500);
            res.json({success: false, error: error});
        })
}

invokeGetFormFields = function (idForm, workflowId, userId) {
    var config = {
        spName: '[dbo].[getFormFields]',
        params: {
            input: [
                {name: 'IdForm', type: 'Int', value: idForm},
                {name: 'WorkFlowId', type: 'Int', value: workflowId},
                {name: 'User', type: 'Int', value: userId}
            ]
        }
    }

    return new Promise(function (resolve, reject) {
        DBUtil.executeSp(config)
            .then(function (result) {
                var itemValue = '';

                console.log("\t *************** Then de la promise del child form: " + JSON.stringify(config, undefined, 1))
                if (Array.isArray(result[0])) {
                    result = result[0];
                }
                result.forEach(function (r) {
                    itemValue += '<tr><td>' + r.TextoFrontEnd + '</td></tr>';
                })

                resolve({success: true, response: itemValue});
            })
            .catch(function (error) {
                console.log("\t *************** Catch de la promise del child form: " + JSON.stringify(config, undefined, 1))
                reject({success: false, error: error});
            })
    });
}

formHasChildForms = function (formData) {
    var flag = false,
        arrFormIds = [];

    formData.forEach(function (formField) {
        if (formField.Tipo === "Form") {
            flag = true;
            arrFormIds.push({idField: formField.IdField, idForm: formField.IdFormularioHijo});
        }
    });

    return {result: flag, forms: arrFormIds};
};

exports.generatePDF = function (req, res) {
    var formFieldsMap = new Map(),
        config = {
            spName: '[dbo].[getFormFields]',
            params: {
                input: [
                    {name: 'IdForm', type: 'Int', value: req.query.idForm},
                    {name: 'WorkFlowId', type: 'Int', value: (req.query.workflowId != null && req.query.workflowId != undefined && req.query.workflowId != "")?req.query.workflowId:null},
                    {name: 'User', type: 'Int', value: req.query.userId}
                ]
            }
        },
        commentsAndAttachmentsArray = [],
        childFormsArray = [],
        htmlComments = '',
        htmlAttachments = '',
        workflowData = {
            workflow: req.query.workflow,
            proceso: req.query.proceso,
            stepTxt: req.query.stepTxt
        };

    commentsAndAttachmentsArray.push(workflowCommentsController.getWorkflowComments({
        workflowId: req.query.workflowId,
        onlyPending: null,
        userId: req.query.userId,
        idLanguage: 1
    }));

    commentsAndAttachmentsArray.push(workflowAttachmentsController.getWorkflowAttachments({workflowId: req.query.workflowId}));

    Promise.all(commentsAndAttachmentsArray)
        .then(function (result) {
            // console.log("\n\nEn el Then de Promise.all de Comments & Attachments: " + JSON.stringify(result));
            result.forEach(function (res) {
                if (res.comments) {
                    htmlComments = this.getHtmlFromComments(res.comments);
                }
                if (res.attachments) {
                    htmlAttachments = this.getHtmlFromAttachments(res.attachments);
                }
            });

            DBUtil.executeSp(config)
                .then(function (result) {
                    var options = {
                        html: '',
                        paperSize: {
                            format: 'A4',
                            orientation: 'portrait',
                            border: '1cm'
                        }
                    };

                    if (Array.isArray(result)) {
                        if (Array.isArray(result[0])) {
                            result = result[0];
                        }

                        var fieldContainersAvail = getFieldContainersFromDataset(result);


                        result = fixSectionsFromDataSet(result, fieldContainersAvail);

                        result = fixFieldsInSections(result);

                        if (formHasChildForms(result).result == true) {
                            var childForms = formHasChildForms(result).forms;

                            childForms.forEach(function (cf) {
                                childFormsArray.push({
                                    idField: cf.idField, //Solo necesario en el caso que hayan varios forms child del mismo tipo
                                    idForm: cf.idForm,
                                    workflowId: req.query.workflowId,
                                    userId: req.query.userId
                                });

                            });

                        }

                        // console.log("ChildForms: " + JSON.stringify(childForms, undefined, 2));

                    }

                    if (childFormsArray.length > 0) {
                        var promisesArray = [];

                        childFormsArray.forEach(function (cf) {
                            promisesArray.push(this.invoqueGetFormChildData(cf.idForm, cf.workflowId, cf.userId, cf.idField, formFieldsMap));
                        });

                        Promise.all(promisesArray)
                            .then(function () {
                                // console.log("\t ******************************************** Then de Promise.all");

                                var html = this.getHtmlFromFormFields(result, workflowData, htmlComments, htmlAttachments, formFieldsMap);
                                this.convertPDF(html, req, res);
                            })
                            .catch(function (error) {
                                // console.log("\t ******************************************** Catch de Promise.all")
                                res.status(500);
                                res.json({success: false, error: "", fileId: -1});
                            })
                    } else {
                        var html = this.getHtmlFromFormFields(result, workflowData, htmlComments, htmlAttachments, null);
                        this.convertPDF(html, req, res);
                    }


                })
                .catch(function (error) {
                    res.status(500);
                    res.json({success: false, error: error, fileId: -1});
                });

        })
        .catch(function (error) {
            console.log("\n\nEn el Catch de Promise.all de Comments & Attachments: " + JSON.stringify(error));
        });


}

getHtmlFromComments = function (commentsData) {
    var str = '',
        html = '<br><span style="font-weight: bold; font-family: Tahoma; font-size: 16px; color: white; background-color:#19481a">Comentarios</span>';

    html += '<br><table width="100%" border="0" cellpadding="5">';

    html += '<tr><td style="background-color: #19481a;color: white; font-family: Helvetica; font-size: 9px; font-weight: bold">Usuario</td>'
        + '<td style="background-color: #19481a;color: white; font-family: Helvetica; font-size: 9px; font-weight: bold">Fecha/Hora</td>'
        + '<td style="background-color: #19481a;color: white; font-family: Helvetica; font-size: 9px; font-weight: bold">Comentario</td></tr>';

    commentsData.forEach(function (comment) {
        html += '<tr><td style="font-family: Helvetica; font-size: 9px; font-weight: bold">' + comment.UserName + '</td>';
        html += '<td style="font-family: Helvetica; font-size: 9px; font-weight: normal">' + comment.Cdate + ' - ' + comment.Ctime + '</td>';
        html += '<td style="font-family: Helvetica; font-size: 9px; font-weight: normal;"><div style="width: 360px;overflow: hidden; word-wrap: break-word;">' + comment.Description + '</div></td></tr>';
    });


    html += '</table>';

    return html;
}

getHtmlFromAttachments = function (attachmentsData) {
    var str = '',
        html = '<br><span style="font-weight: bold; font-family: Tahoma; font-size: 16px; color: white; background-color:#19481a">Adjuntos</span>';

    html += '<br><table width="100%" border="0" cellpadding="5">';

    html += '<tr><td style="background-color: #19481a;color: white; font-family: Helvetica; font-size: 9px; font-weight: bold">Nombre Archivo</td>'
        + '<td style="background-color: #19481a;color: white; font-family: Helvetica; font-size: 9px; font-weight: bold">Fecha/Hora</td>'
        + '<td style="background-color: #19481a;color: white; font-family: Helvetica; font-size: 9px; font-weight: bold">Subido por</td></tr>';

    attachmentsData.forEach(function (attachment) {
        html += '<tr><td style="font-family: Helvetica; font-size: 9px; font-weight: bold">' + attachment.Archivo + '</td>';
        html += '<td style="font-family: Helvetica; font-size: 9px; font-weight: normal">' + attachment.Date + ' - ' + attachment.Time + '</td>';
        html += '<td style="font-family: Helvetica; font-size: 9px; font-weight: normal">' + attachment.FullName + '</td></tr>';
    });


    html += '</table>';

    return html;
}

getHtmlFromFormFields = function (formPpalData, workflowData, htmlComments, htmlAttachments, formFieldsMap) {
    var str = '',
        html = '<html><head></head>',
        wkfDesc = workflowData.workflow + ' - ' + workflowData.proceso + ' - ' + workflowData.stepTxt;

    html += '<body>';

    var cambioSeccion = false,
        seccionActual = null;

    //Agrega la informacion del wkf
    html += '<br><span style="font-family: Tahoma; font-size: 16px; font-style: bold; color: black">' + wkfDesc + "</span></br>";

    formPpalData.forEach(function(r) {
        if (r.Seccion != seccionActual) {
            cambioSeccion = true;
            //Guardo la seccion anterior
            seccionActual = r.Seccion;

            //Genero una nueva seccion
            html += '<br><span style="font-family: Tahoma; font-size: 16px; font-style: bold; color: white; background-color:#19481a">' + r.Seccion + "</span></br>";

            formPpalData.forEach(function(r2){

                if (r2.Seccion != seccionActual) return;
                var itemValue = '',
                    str = "<table border='0' width='100%'><tbody>";

                if (r2.Tipo !== "Lista a Buscar Multiple" && r2.Tipo !== "Form") {
                    if (r2.Tipo !== "FieldContainer") {
                        str += '<tr><td align="top" style="width: 15%; font-family: Tahoma; font-size: 12px; font-style: bold"><h4>' + r2.TextoFrontEnd + ':</h4></td><td ';
                    }
                } else {
                    str += '<tr><td align="top" style="width: 15%; font-family: Tahoma; font-size: 12px; font-style: bold" colspan=2><h4>' + r2.TextoFrontEnd + ':</h4></td></tr><tr><td colspan=2 ';
                }

                if (r2.Tipo !== "FieldContainer") {
                    if (r2.Style != null && r2.Style != "") {
                        str += 'style="' + r2.Style + '; width=auto">';
                    } else {
                        str += ' style="font-family: Tahoma; font-size: 12px; font-style: normal; width: auto" >';
                    }
                }

                switch (r2.Tipo) {
                    case "FieldContainer":
                        //itemValue = '&nbsp;';
                        break;
                    case "Combo":
                        itemValue = r2.ValorActual2;
                        break;
                    case "Lista a Buscar Multiple":

                        itemValue = '<ul>';
                        if (r2.ValorActual2 != null) {
                            var arrVa2;


                            //arrVa2 = r2.ValorActual2.substring(0, r2.ValorActual2.length - 1).split(",");
                            if (r2.ValorActual2.endsWith(",")) {
                                r2.ValorActual2 = r2.ValorActual2.substring(0, r2.ValorActual2.length - 1);
                            }
                            arrVa2 = r2.ValorActual2.split(",");

                            arrVa2.forEach(function (va) {
                                itemValue += '<li>' + va + '</li>';
                            });
                        }
                        itemValue += '</ul>';
                        break;
                    case "Form":
                        var key = r2.IdField + '' + r2.IdFormularioHijo,
                            fcdata = formFieldsMap.get(key),
                            columnsArray = [];

                        if (Array.isArray(fcdata)) fcdata = fcdata[0];

                        if (fcdata != undefined && fcdata != null) {
                            //Obtengo las columnas
                            if (fcdata[0] != undefined && fcdata[0] != null) {
                                Object.keys(fcdata[0]).forEach(function (key) {
                                    if (key !== "Linea") {
                                        columnsArray.push(key);
                                    }
                                });
                            }

                            itemValue = '<table style="border-collapse: collapse;width: 90%;margin: 0px auto;"><tr>';
                            columnsArray.forEach(function (column) {
                                itemValue += '<td style="background-color: #19481a;color: white; font-family: Tahoma; font-size: 9px; font-style: normal">' + column + '</td>';
                            });
                            itemValue += '</tr>';
                            //Terminada generacion de columnas

                            //Se muestran ahora los datos
                            fcdata.forEach(function (data) {
                                itemValue += '<tr>';
                                Object.keys(data).forEach(function (key) {
                                    if (key !== "Linea") {
                                        var vv = data[key];
                                        if (vv.endsWith("__isDate__")) { //Es una fecha
                                            vv = vv.substring(0, vv.indexOf("__isDate__"));
                                            var fecha = new Date(vv);
                                            vv = fecha.getDate() + '/' + (fecha.getMonth() + 1) + '/' + fecha.getFullYear();
                                        }
                                        itemValue += '<td style="font-family: Tahoma; font-size: 9px; font-style: normal">' + vv + '</td>';
                                    }
                                })
                                itemValue += '</tr>'
                            });

                            itemValue += '</table>';

                        } else {
                            itemValue = '&nbsp;'
                        }

                        break;
                    default:
                        itemValue = r2.ValorActual;
                }

                if (r2.Tipo !== "FieldContainer") {
                    str += ((r2.Tipo == "Form")?'':'') + itemValue + '</td></tr>';
                }

                str += '</tbody></table>';


                html += str;

            });
        } else cambioSeccion = false;


    });

    html += htmlComments;

    html += htmlAttachments;

    html += '</body></html>';

    return html;
};

convertPDF = function (html, req, res) {
    var pdf = require('phantom-html2pdf'),
        options = {
            html: html,
            paperSize: {
                format: 'A4',
                orientation: 'portrait',
                margin: {
                    top: '1.5cm',
                    bottom: '1.5cm'
                }
            },
            runnings: './nodejs/config/runningsFilePhantomPDF.js'
        };

    pdf.convert(options, function (error, result) {
        var d = new Date(),
            fileId = d.getTime(),
            fName = "nodejs/temp/pdf/form_" + fileId + ".pdf";

        result.toFile(fName, function () {
            res.status(200);
            res.json({success: true, fileId: fileId});
        });
    });

}

exports.getPDF = function (req, res) {
    var path = "nodejs/temp/pdf/", options = {
        root: path,
        dotfiles: 'deny',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true
        }
    };

    var fileId = req.query.fileId,
        fileName = "form_" + fileId + ".pdf";

    res.sendFile(fileName, options, function (err) {
        if (err) {
            console.log(err);
        } else {
            console.log('Sent:', fileName);
            fs.unlink(path + fileName, function(err){console.log("Resultado unlink: " + JSON.stringify(err));});
        }
    });
}

invoqueGetFormChildData = function (idForm, workflowId, userId, idField, formFieldsMap) {
    console.log("Consultando el cf: " + idForm + " - " + workflowId + " - " + userId + " - " + idField);
    var config = {
        spName: '[dbo].[FormsChild_TX_GetData]',
        params: {
            input: [
                {name: 'IdFormChild', type: 'Int', value: idForm},
                {name: 'WorkFlowId', type: 'Int', value: workflowId},
                {name: 'IdUser', type: 'Int', value: userId}
            ]
        }
    }

    return new Promise(function (resolve, reject) {
        DBUtil.executeSp(config)
            .then(function (result) {
                var key = idField + '' + idForm;

                formFieldsMap.set(key, result);

                resolve({success: true});
            })
            .catch(function (error) {
                reject({success: false, error: error});
            })
    });
};

exports.getExtJSFormFields = function (req, res) {
    var config = {
        spName: '[dbo].[getFormFields]',
        params: {
            input: [
                {name: 'IdForm', type: 'Int', value: req.query.idForm},
                {name: 'WorkFlowId', type: 'Int', value: (req.query.workflowId != null && req.query.workflowId != undefined && req.query.workflowId != "")?req.query.workflowId:null},
                {name: 'User', type: 'Int', value: req.query.userId}
            ]
        }
    }

    DBUtil.executeSp(config)
        .then(function (result) {
            var formExtJS = getFormFields(result)
            res.status(200).json(formExtJS);
        })
        .catch(function (error) {
            console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

getExtJSItem = function(item) {
    var i = (item.Tipo == "Radio Button")? {}: {
        bind: {value: '{formFields.' + item.Id + '}'},
        value: item.ValorActual,
        flex: 1,
        itemId: item.Id,
        name: item.Id,
        readOnly: eval(item.SoloLectura),
        allowBlank: !eval(item.Obligatorio),
        fieldLabel: item.TextoFrontEnd,
        posicion: item.Posicion,
        afterSubTpl: item.CampoHtml,
        idField: item.IdField,
        campoValueCombo: item.CampoValueCombo,
        campoTextCombo: item.CampoTextCombo,
        //labelWidth: (item.TextoFrontEnd.length > 15)?150:100
        labelAlign: (item.TextoFrontEnd.length > 15)?'top':'left',
        hidden: (item.Visible == 1)?false:true,
        labelClsExtra: 'labelField',
        seccion: item.Seccion
    };

    if (item.Style != "") {
        i.fieldStyle = item.Style;
    }

    try {
        switch (item.Tipo) {
            case "TextBox":
                i.xtype = 'textfield';
                if (i.itemId.startsWith('IP')) {
                    i.vtype = 'IPAddress';
                };
                break;
            case "DateField":
                var mayorHoy = eval(item.FechaMayorOIgualaHoy),
                    valor = new moment(item.ValorActual, "DD/MM/YYYY");
                i.xtype = 'datefield';
                i.value = valor.toJSON();
                i.campoMenorQue = item.MenorQueCampo;

                if (mayorHoy) {
                    i.minValue = new Date();
                }
                if (true /*form*/) {
                    if (item.MenorQueCampo && item.MenorQueCampo != "") { // Si es un form, el chequeo se hace de otra manera
                        i.listeners = {
                            change: function (field) {
                                var parentObj = form.down('#' + item.MenorQueCampo);

                                console.log("item.MenorQueCampo: ", parentObj);
                                if (parentObj) {
                                    me.__onCheckValorMenorQue(field, parentObj);
                                }
                            }
                        }
                    }

                } else { //No es un form, sino una grilla

                }
                break;
            case "Combo":
                i.xtype = 'combobox';
                i.editable = false;
                i.typeAhead = true;
                i.dependsOn = (item.ComboPadre == null || item.ComboPadre == undefined)?"":item.ComboPadre;

                /*if (i.dependsOn == "" || i.dependsOn == null || i.dependsOn == undefined) {
                    var store = Ext.create('Ext.data.Store', {
                        model: 'ProcessManagement.model.ComboIdDescripcion',
                        proxy: {
                            type: 'ajax',
                            //url: 'soap/valoresPosibles', //SOLO util si se usan webservices de .NET
                            url: 'form/getFieldValues',
                            //url: 'form/getFieldValues',
                            reader: {
                                type: 'json',
                                rootProperty: 'result'
                            },
                            timeout: 500000
                        }
                    });
                    i.store = store;

                    if (!form) me.getConfig('storesToLoad').push(store);

                    me.getConfig('idFieldsToLoad').push({id: item.Id, xtype: i.xtype});

                    store.load({
                        params: {
                            userId: loginUser.idUser,
                            stepId: me.getView().getViewModel().get('StepId'),
                            token: loginUser.token,
                            fieldId: item.IdField,
                            fieldValue: item.CampoValueCombo,
                            fieldDescription: item.CampoTextCombo
                        },
                        callback: function () {
                            var query = Ext.String.format('{0}[name={1}]', i.xtype, item.Id),
                                component = Ext.ComponentQuery.query(query)[0];

                            if (component) component.unmask();

                            if (!form) {
                                me.getConfig('storesToLoad').pop(store);

                                if (me.getConfig('storesToLoad').length <= 0) {
                                    me.abrirFormHijo(eval(item.SoloLectura));
                                }
                            }
                        }
                    });
                };*/
                i.valueField = 'id';
                i.displayField = 'descripcion';
                i.queryMode = 'local';
                break;
            case "NumberField":
                i.xtype = 'numberfield';
                i.campoMenorQue = item.MenorQueCampo;

                if (true/*form*/) {
                    if (item.MenorQueCampo && item.MenorQueCampo != "") {
                        i.listeners = {
                            change: function (field) {
                                var parentObj = form.down('#' + item.MenorQueCampo);

                                console.log("item.MenorQueCampo: ", parentObj);
                                if (parentObj) {
                                    me.__onCheckValorMenorQue(field, parentObj);
                                }
                            }
                        }
                    }
                }

                break;
            case "Lista a Buscar Multiple":
                var store = Ext.create('Ext.data.Store', {
                    model: 'ProcessManagement.model.ComboIdDescripcion',
                    proxy: {
                        type: 'ajax',
                        //url: 'soap/valoresPosibles',
                        url: 'form/getFieldValues',
                        //url: 'form/getFieldValues',
                        reader: {
                            type: 'json',
                            rootProperty: 'result'
                        },
                        timeout: 500000
                    }
                });
                i.queryMode = 'local';
                i.xtype = 'tagfield';
                i.valueField = 'id';
                i.displayField = 'idDescripcion';
                i.store = store;

                /*if (!form) me.getConfig('storesToLoad').push(store);

                me.getConfig('idFieldsToLoad').push({id: item.Id, xtype: i.xtype});

                store.load({
                    params: {
                        userId: loginUser.idUser,
                        stepId: me.getView().getViewModel().get('StepId'),
                        token: loginUser.token,
                        fieldId: item.IdField,
                        fieldValue: item.CampoValueCombo,
                        fieldDescription: item.CampoTextCombo
                    },
                    callback: function () {
                        var query = Ext.String.format('{0}[name={1}]', i.xtype, item.Id),
                            component = Ext.ComponentQuery.query(query)[0];

                        if (component) component.unmask();

                        if (!form) {
                            me.getConfig('storesToLoad').pop(store);

                            if (me.getConfig('storesToLoad').length <= 0) {
                                me.abrirFormHijo(eval(item.SoloLectura));
                            }
                        }
                    }
                });*/

                break;
            case "TextArea":
                i.xtype = 'htmleditor';
                break;
            case "CheckBox":
                i.xtype = 'checkbox';
                i.fieldLabel = null;
                i.boxLabel = item.TextoFrontEnd;
                break;
            case "Form":
                var formHijo = {};

                i = {
                    xtype: 'fieldcontainer',
                    fieldLabel: (item.TextoFrontEnd != null && item.TextoFrontEnd != "")?item.TextoFrontEnd:null,
                    labelClsExtra: 'labelField',
                    items: [{
                        xtype: 'button',
                        iconCls: 'x-fa fa-file-text-o',
                        userCls: 'circular',
                        xtype: 'button',
                        tooltip: item.TextoFrontEnd,
                        handler: function () {
                            me.cargarDatosFormHijo(item.IdFormularioHijo, item.IdField, eval(item.SoloLectura));
                        }
                    },
                        {
                            xtype: 'hiddenfield',
                            itemId: item.Id,
                            name: item.Id,
                            value: item.IdFormularioHijo,
                            isFormField: true
                        }
                    ],
                    seccion: item.Seccion
                }

                formHijo = {
                    id: item.IdFormularioHijo,
                    readOnly: eval(item.SoloLectura)
                }
                //me.getConfig('formsHijo').push(formHijo);
                break;
            case "Radio Button":
                var value = {};
                value[item.Id] = item.ValorActual;

                i = {
                    xtype: 'radiogroup',
                    allowBlank: false, //!eval(item.Obligatorio),
                    fieldLabel: item.TextoFrontEnd,
                    posicion: item.Posicion,
                    columns: 2,
                    layout: {
                        type: 'column'
                    },
                    columnWidth: 1,
                    idField: item.Id,
                    items: [
                        {
                            bind: {boxLabel: '{txtFields.words.yes}'},
                            inputValue: 'si',
                            name: item.Id,
                            labelClsExtra: 'labelField'
                        },
                        {
                            boxLabel: 'No',
                            inputValue: 'no',
                            name: item.Id,
                            padding: '0 0 0 15',
                            labelClsExtra: 'labelField'
                        }
                    ],
                    value: value,
                    labelClsExtra: 'labelField',
                    seccion: item.Seccion
                }
                break;
            default:
                i.xtype = 'textfield';
        }
        if (i.xtype == "multiselect" && i.data == null) {
            i.height = 100;
        }

    } catch (e) {
        console.log("********************************************* ERROR al parsear EXTJS Item: " + e)
    }
    return i;
}