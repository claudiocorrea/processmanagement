var DBUtil = require('../util/DBUtil'),
    Promise = require('bluebird');

exports.getWorkflowComments = function (data) {
    var config = {
        spName: '[dbo].[Queries_asmx.Comments_SPM]',
        params: {
            input: [
                {name: 'WorkFlowid', type: 'Int', value: data.workflowId},
                {name: 'OnlyPending', type: 'Bit', value: data.onlyPending},
                {name: 'IdUser', type: 'Int', value: data.userId},
                {name: 'IdLanguage', type: 'Int', value: data.idLanguage}
            ]
        }
    };

    return new Promise(function (resolve, reject) {
        DBUtil.executeSp(config)
            .then(function (result) {
                if (Array.isArray(result)) {
                    result = result[0];
                }
                console.log("En el Then de Comments: " + JSON.stringify(result));
                resolve({success: true, comments: result});
            })
            .catch(function (error) {
                reject({success: false, error: error});
            });

    });

};