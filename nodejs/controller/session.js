/**
 * Created by Antonio on 5/28/2017.
 */

exports.validate = function (req, res) {
    if (req.session && req.session.user) {
        res.status(200);
        res.send({success: true, user: req.session.user[0]});
    } else {
        res.status(401);
        res.send({success: false});
    }
}

exports.invalidate = function (req, res) {
    req.session.user = null;
    req.session.destroy();
    res.status(200);
    res.send({success: true});
}
