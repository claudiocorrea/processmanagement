/**
 * Created by Antonio on 8/29/2017.
 */
var DBUtil = require('../util/DBUtil');

exports.obtenerValoresComboAnidado = function (req, res) {
    var config = {
        spName: '[dbo].[Queries_asmx.ComboAnidado]',
        params: {
            input: [
                {name: 'CampoComboHijo', type: 'NVarchar', value: req.query.comboHijo},
                {name: 'Parameter', type: 'NVarchar', value: req.query.parameter},
                {name: 'IdForm', type: 'Int', value: req.query.idForm},
                {name: 'IdStep', type: 'Int', value: req.query.idStep},
                {name: 'WorkFlowId', type: 'Int', value: req.query.workFlowId},
                {name: 'User', type: 'Int', value: req.query.idUser},
                {name: 'CampoPadre', type: 'NVarchar', value: req.query.comboPadre},
                {name: 'Token', type: 'NVarchar', value: req.query.token}
            ],
            output: [
                {name: 'Return', type: 'NVarchar', value: null}
            ]
        }
    };

    console.log("\nEjecutando:\n\t\t" + JSON.stringify(config));

    DBUtil.executeSp(config)
        .then(function (result) {
            console.log("RESPUESTA: ", result)
            if (Array.isArray(result)) {
                result = (result[0])[0];
            }
            result = result.Result;

            if (result.endsWith(",")) {
                result = result.substring(0, result.length - 1);
            }

            console.log("***********************Respuesta");
            console.log("Then [dbo].[Queries_asmx.ComboAnidado]: " + JSON.stringify(result));

            var arr = result.split( /,\[/g);

            if (Array.isArray(arr)) {
                result = [];
                console.log("Arr: " + JSON.stringify(arr));
                arr.forEach(function (tempItem) {
                    console.log("tempItem antes: ", tempItem);
                    tempItem = tempItem.replace("[", "").replace("]", "");
                    console.log("tempItem despues: ", tempItem);
                    var tempArr = tempItem.split(/,|;/g);

                    result.push({clave: tempArr[0], valor: tempArr[1], id: tempArr[0], descripcion: tempArr[1]});
                })
            }

            res.status(200);
            res.json({success: true, values: result});
        })
        .catch(function (error) {
            console.log("Catch [dbo].[Queries_asmx.ComboAnidado]: " + JSON.stringify(error));
            res.status(500);
            res.json({success: false, error: error});
        });

}