var DBUtil = require('../util/DBUtil'),
    Promise = require('bluebird');

exports.getWorkflowAttachments = function (data) {
    var config = {
        spName: '[dbo].[Workflows_Archivos_TX_WorkFlowId_SPM]',
        params: {
            input: [
                {name: 'WorkFlowId', type: 'Int', value: data.workflowId}
            ]
        }
    };

    return new Promise(function (resolve, reject) {
        DBUtil.executeSp(config)
            .then(function (result) {
                if (Array.isArray(result)) {
                    result = result[0];
                }
                resolve({success: true, attachments: result});
            })
            .catch(function (error) {
                reject({success: false, error: error});
            });
    });
}