/**
 * Created by Antonio on 9/5/2017.
 */
var DBUtil = require('../util/DBUtil'),
    http = require('http'),
    fs = require('fs'),
    path = require('path');

exports.tareasPersonales_V2 = function (req, res) {
    var config = {
        spName: '[dbo].[Workflows_TX_TareasPersonales_V2]',
        params: {
            input: [
                {name: 'p_userId', type: 'Int', value: req.query.userId},
                {name: 'p_stepId', type: 'Int', value: req.query.stepId}
            ]
        }
    };

    DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Result: " + JSON.stringify(result))
            if (Array.isArray(result)) {
                if (result.length > 0) {
                    if (result[0].length > 0) { result = (result[0])[0];
                    } else { result = null }
                } else { result = null }
            }
            res.status(200).json({success: true, result: result});
        })
        .catch(function (error) {
            res.status(500).json({success: false, error: error});
        })
}

exports.getStepsByWorkflowId = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.GetStepsByWorkflowId]',
        params: {
            input: [
				{name: 'IdUser', type: 'Int', value: req.query.idUser},
				{name: 'IdLanguage', type: 'Int', value: 1},
                {name: 'WorkflowID', type: 'Int', value: req.query.workflowId}
            ]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.getFileFromURL = function(req, res) {
    console.log("Descargando el archivo: " + req.query.fileName + " from " + req.query.fileUrl)

    var folder = ".\\temp\\",
        dest = folder + req.query.fileName,
        url = req.query.fileUrl;  

    !fs.existsSync(folder) && fs.mkdirSync(folder);

    fs.access(dest, (err) => {
        if (!err) {
            res.status(200).json({ success: true, path: dest });
        } else {
            downloadFile(url, dest, (err) => {
                if (err) {
                    console.log(err);
                    res.status(500).json({ success: false, error: err });
                } else {
                    console.log('File is downloaded');
                    res.status(200);
                    res.json({ success: true, path: dest });
                }
                
            });
        }
    });
}

const downloadFile = (url, dest, callback) => {
    return new Promise((resolve, reject) => {        
        const req = http.get(url, (res) => {

            if (res.statusCode !== 200) {
                return callback('File is not found');
            }
            const len = parseInt(res.headers['content-length'], 10);
            let dowloaded = 0;
            var file = fs.createWriteStream(dest, { encoding: 'utf8' });
            res.pipe(file);
            res.on('data', (chunk) => {
                dowloaded += chunk.length;
                //console.log("Downloading " + (100.0 * dowloaded / len).toFixed(2) + "% " + dowloaded + " bytes" + "\r");
            }).on('end', () => {
                console.log('END')
                file.end();
                callback(null);
            }).on('error', (err) => {
                console.log('ERROR: ', err)
                callback(err.message);
            })

            file.on('error', function (err) {
                console.log('FILE ERROR: ', err);
                callback(err.message);
            });

        }).on('error', (err) => {
            console.log('ERROR!: ', err)
            fs.unlink(dest);
            callback(err.message);
        });
    })

}
