/**
 * Created by Antonio on 7/19/2017.
 */
var soap = require('soap'),
    fconfig = require('../../../FileConfig');

exports.obtenerValoresPosiblesCampo = function (req, res) {
    var params = {
        IdUser: req.query.userId,
        StepId: req.query.stepId,
        Token: req.query.token,
        Id: req.query.fieldId
    }

    soap.createClient(fconfig.urlEndPointQueriesAsmx, function (error, client) {
        if (error) {
            res.status(500);
            res.json({success: false, error: error});
        } else {
            client.ValoresPosiblesCampo(params, function (err, result) {
                if (err) {
                    res.status(500);
                    res.json({success: false, error: err});
                } else {
                    if (result) {
                        result = result.ValoresPosiblesCampoResult.replace("<![CDATA[", "").replace("]]>", "");

                        res.status(200);
                        res.json({success: true, values: parseValoresPosibles(result)});
                    }
                }
            })
        }
    });
}

parseValoresPosibles = function (strVP) {
    var result = [];
    strVP = strVP.split("||");

    if (Array.isArray(strVP)) {
        strVP.forEach(function (str) {
            if (str != "") {
                var tempArr = str.split("|"),
                    tempItem = {};
                if (tempArr.length <= 1) { //En algunos casos, para la descripcion Seleccionar no viene ID
                    tempItem = {
                        id: null,
                        descripcion: tempArr[0]
                    }
                } else {
                    tempItem = {
                        id: tempArr[1],
                        descripcion: tempArr[0]
                    }
                }

                result.push(tempItem);
            }
        })
    }

    return result;
}