/**
 * Created by Antonio on 7/21/2017.
 */
var soap = require('soap'),
    fconfig = require('../../../FileConfig');

exports.obtenerValoresComboAnidado = function (req, res) {
    var params = {
        CampoComboHijo: req.query.comboHijo,
        Parameter: req.query.parameter,
        IdForm: req.query.idForm,
        IdStep: req.query.idStep,
        WorkFlowId: req.query.workFlowId,
        User: req.query.idUser,
        CampoPadre: req.query.comboPadre,
        Token: req.query.token
    };

    soap.createClient(fconfig.urlEndPointQueriesAsmx, function (error, client) {
        if (error) {
            res.status(500);
            res.json({success: false, error: error});
        } else {
            client.ComboAnidado(params, function (err, result) {
                if (err) {
                    res.status(500);
                    res.json({success: false, error: err});
                } else {
                    if (result) {
                        result = result.ComboAnidadoResult.replace("<![CDATA[", "").replace("]]>", "");

                        res.status(200);
                        res.json({success: true, values: parseValoresComboHijo(result)});
                    }
                }
            })
        }

    })
}

parseValoresComboHijo = function (strVP) {
    var result = [];
    strVP = strVP.split("|");

    if (Array.isArray(strVP)) {
        strVP.forEach(function (str) {
            if (str != "") {
                var tempArr = str.split(";"),
                    tempItem = {};
                if (tempArr.length <= 1) { //En algunos casos, para la descripcion Seleccionar no viene ID
                    tempItem = {
                        id: null,
                        descripcion: tempArr[1]
                    }
                } else {
                    tempItem = {
                        id: tempArr[0],
                        descripcion: tempArr[1]
                    }
                }

                result.push(tempItem);
            }
        })
    }

    return result;
}