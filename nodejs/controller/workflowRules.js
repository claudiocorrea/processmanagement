/**
 * Created by Antonio on 23/09/2017.
 */
var DBUtil = require('../util/DBUtil');

exports.workflow_Forms = function (req, res) {
    var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.Forms]',
        params: {
            input: [
                {name: 'WorkFlowId', type: 'Int', value: req.query.workflowId/*4982*/}
            ]
        }
    };

    DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Result WorkFlowRules.WorkFlow.Forms: " + JSON.stringify(result));
            if (Array.isArray(result)) {
                result = (result[0])[0];
            }
            res.status(200);
            res.json({success: true, result: result});
        })
        .catch(function (error) {
            console.log("Error WorkFlowRules.WorkFlow.Forms: " + JSON.stringify(error));
            res.status(500);
            res.json({success: false, error: error});
        })
}