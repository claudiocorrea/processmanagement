var DBUtil = require('../util/DBUtil'),
    Promise = require('bluebird');

exports.getAllPendingAlerts = function (callback) {
    var config = {
            spName: '[dbo].[EmailJobs_TX_Pending]'
        },
        response = {succes: true, data: [], error: null};

    return new Promise(function (resolve, reject) {
        DBUtil.executeSp(config)
            .then(function (result) {
                //console.log("Result EmailJobs_TX_Pending: " + result + "\n\n" + JSON.stringify(result));

                if (Array.isArray(result)) {
                    response.data = result[0];
                }

                resolve(response);
            })
            .catch(function (error) {
                console.log("Error EmailJobs_TX_Pending: " + error + "\n\n" + JSON.stringify(error));
                response.succes = false;
                response.error = error;
                reject(error);
            });
    });

};

exports.markAlertAsSent = function (data) {
    var config = {
        spName: '[dbo].[EmailJobs_MarkJobAsRead]',
        params: {
            input: [
                {name: 'IdJob', type: 'Int', value: data.alertId},
                {name: 'DateOut', type: 'VarChar', value: data.dateOut},
                {name: 'TimeOut', type: 'VarChar', value: data.timeOut},
                {name: 'Result', type: 'VarChar', value: data.result}
            ]
        }
    };

    return new Promise(function (resolve, reject) {
        DBUtil.executeSp(config)
            .then(function (result) {
                resolve({success: true, result: result});
            })
            .catch(function (error) {
                reject({success: false, error: error});
            });
    });
};