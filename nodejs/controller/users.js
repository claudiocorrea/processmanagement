/**
 * Created by Antonio on 5/30/2017.
 */
var config = require('../../FileConfig'),
    request = require('request');

exports.login = function (req, res) {
    var url = config.urlServices + 'queries/users/login?username=' + req.query.username + '&password=' + req.query.password;
    request.post(url, function (error, response, body) {

        req.session.user = null;
        console.log("Respuesta Login: ");
        //console.log(error);
        //console.log(response);
        console.log(body);

        if (error) {
            res.status(500);
            res.json({success: false});
        } else {
            body = JSON.parse(body);
            //console.log(body[0]);
            //if (body[0].LoginResult == -1) {
            if (!Array.isArray(body)) {
                console.log("NO logueado");
                res.status(401);
                res.setHeader("Content-Type", "text/json");
                res.send(body);
            } else if (body[0].LoginResult == -1) {
                console.log("NO logueado");
                res.status(401);
                res.setHeader("Content-Type", "text/json");
                res.send(body);
            } else {
                //req.session.user = body[0];
				body[0].UserName = req.query.username
                req.session.user = body;
                console.log("Logueado")
                //console.log("error: ", error);
                //console.log("response: ", response);
                console.log("body: ", body);
                res.status(200);
                res.setHeader("Content-Type", "text/json");
                res.send(body);
            }
        }
    });
}

exports.userLogin = function (req, res) {
	res.status(200);
    res.setHeader("Content-Type", "text/json");
    res.send(req.session.user);
}

exports.loginDummy = function (req, res) {
    var response = {
        success: true,
        _resultSet: {
            records: [{
                LoginResult: 1,
                username: 'admin',
                Name: 'User Admin',
                Legajo: 1,
                Token: 'lalalala',
                IdProfile: 1
            }]
        }
    }
    req.session.user = null;

    req.session.user = response;

    res.status(200);
    res.setHeader("Content-Type", "text/json");
    res.send(response);
}