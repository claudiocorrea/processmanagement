/**
 * Created by Antonio on 7/28/2017.
 */
var DBUtil = require('../util/DBUtil');

exports.getPasosAnteriores = function (req, res) {
    var config = {
        spName: '[dbo].[Queries_asmx.PreviousSteps]',
        params: {
            input: [
                {name: 'WorkFlowid', type: 'Int', value: req.query.workflowId}
            ]
        }
    };

    DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then [dbo].[Queries_asmx.PreviousSteps]: " + JSON.stringify(result));
            if (Array.isArray(result)) {
                result = {success: true, listaPasosAnteriores: result[0]};
            } else {
                result = {success:true, listaPasosAnteriores: []}
            }
            res.status(200);
            res.json(result);
        })
        .catch(function (error) {
            res.status(500);
            res.json({success: false, error: error});
        });
}

exports.rechazarPaso = function (req, res) {
    var config = {
        spName: '[dbo].[Queries_asmx.RejectStep]',
        params: {
            input: [
                {name: 'IdUser', type: 'Int', value: req.body.userId},
                {name: 'IdLanguage', type: 'Int', value: 1},
                {name: 'StepId', type: 'Int', value: req.body.stepId},
                {name: 'TipoDeRechazo', type: 'Int', value: req.body.tipoRechazo},
                {name: 'PasoAVolver', type: 'NVarchar', value: req.body.pasoAnterior},
                {name: 'Comments', type: 'NVarchar', value: req.body.motivoRechazo},
                {name: 'EnabledMessages', type: 'Int', value: 1},
                {name: 'EnabledEmails', type: 'Int', value: 1}
            ],
            output: [
                {name: 'RejectStep', type: 'NVarchar', value: req.body.userId}
            ]
        }
    };

    console.log("Params [dbo].[Queries_asmx.RejectStep]: " + JSON.stringify(config));

    DBUtil.executeSp(config)
        .then(function (result) {
            console.log("---- Then [dbo].[Queries_asmx.RejectStep]: " + JSON.stringify(result));
            try{
                if (Array.isArray(result)) result = result[0];
                if (result[0].Result.startsWith("OK")) {
                    result = {success: true};
                } else {
                    result = {success: false, error: result[0].Result};
                }
            } catch (e) {console.log(e)}
            res.status(200);
            res.json(result);
        })
        .catch(function (error) {
            console.log("---- Catch [dbo].[Queries_asmx.RejectStep]: " + JSON.stringify(error));
            res.status(500);
            res.json(error);
        })
}


exports.aprobarPaso = function (req, res) {
	console.log('Query: ', req.query)
    var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.Step_Approve]',
        params: {
            input: [
                {name: 'IdUser', type: 'Int', value: req.query.userId},
                {name: 'IdLanguage', type: 'Int', value: 1},
                {name: 'StepId', type: 'Int', value: req.query.stepId},				
                {name: 'Approved', type: 'Int', value: req.query.approveOrReject},
				{name: 'TipoDeRechazo', type: 'NVarchar', value: req.query.tipoRechazo},
                {name: 'PasoAVolver', type: 'NVarchar', value: req.query.pasoAnterior},
                {name: 'Comments', type: 'NVarchar', value: req.query.motivoRechazo},
                {name: 'EnabledMessages', type: 'Int', value: 1},
                {name: 'EnabledEmails', type: 'Int', value: 1},
				{name: 'LocalTransaction', type: 'Int', value: 0},
            ],
            output: [
                {name: 'Step_Approve', type: 'NVarchar', value: '' }
            ]
        }
    };

    console.log("Params [dbo].[WorkFlowRules.WorkFlow.Step_Approve]: " + JSON.stringify(config));

    DBUtil.executeSp(config)
        .then(function (result) {
            console.log("---- Then [dbo].[WorkFlowRules.WorkFlow.Step_Approve]: " + JSON.stringify(result));
			result = 
            res.status(200);
            res.json({success: true});
        })
        .catch(function (error) {
            console.log("---- Catch [dbo].[WorkFlowRules.WorkFlow.Step_Approve]: " + JSON.stringify(error));
            res.status(500);
            res.json(error);
        })
}

