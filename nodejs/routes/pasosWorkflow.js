/**
 * Created by Antonio on 7/28/2017.
 */
var express = require('express'),
    routerPasosWkf = express.Router(),
    pasosWkfController = require('../controller/pasosWorkflow');

routerPasosWkf.get('/pasosAnteriores', pasosWkfController.getPasosAnteriores);
routerPasosWkf.post('/rechazarPaso', pasosWkfController.rechazarPaso);

//METODOS PARA DEMO CMPC

routerPasosWkf.get('/aprobarPaso', pasosWkfController.aprobarPaso);

module.exports = routerPasosWkf;