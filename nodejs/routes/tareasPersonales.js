/**
 * Created by Antonio on 9/5/2017.
 */
var express = require('express'),
    routerTareasPersonales = express.Router(),
    tareasPersonalesController = require('../controller/tareasPersonales');

routerTareasPersonales.get('/V2', tareasPersonalesController.tareasPersonales_V2);
routerTareasPersonales.get('/getStepsByWorkflowId', tareasPersonalesController.getStepsByWorkflowId);

routerTareasPersonales.get('/downloadFileFromURL', tareasPersonalesController.getFileFromURL);
module.exports = routerTareasPersonales;