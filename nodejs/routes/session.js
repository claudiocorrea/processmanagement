/**
 * Created by Antonio on 5/28/2017.
 */
var express = require('express'),
    routerSession = express.Router(),
    sessionController = require('../controller/session');

routerSession.get('/validate', sessionController.validate);
routerSession.get('/invalidate', sessionController.invalidate);

module.exports = routerSession;