var express = require('express'),
    notificacionesController = require('../controller/notificaciones'),
    notificacionesRouter = express.Router();

notificacionesRouter.get('/qtyByUser', function (req, res) {
    notificacionesController.getTotalNotificationsByUser({IdUser: req.query.IdUser})
        .then(function (result) {
            res.status(200);
            res.json(result);
        })
        .catch(function (error) {
            res.status(500);
            res.json(error);
        });
});

notificacionesRouter.get('/getPendingsByUser', function (req, res) {
    notificacionesController.getPendingsByUser({IdUser: req.query.idUser})
        .then(function (result) {
            res.status(200);
            res.json(result);
        })
        .catch(function (error) {
            res.status(500);
            res.json(error);
        });
});

notificacionesRouter.post('/markAsRead', function (req, res) {
    var arrayNotificaciones = [];

    console.log("Req.body: " + JSON.stringify(req.body));
    req.body.notifications.forEach(function (notificationId) {
        console.log("Procesando notification: " + notificationId);
        arrayNotificaciones.push(notificacionesController.markNotificationAsRead({IdMensaje: notificationId}));
    });

    Promise.all(arrayNotificaciones)
        .then(function (result) {
            var success = true,
                missed = [];
            console.log("Then de markAsRead: " + JSON.stringify(result));
            result.forEach(function (res) {
                if (res.success !== true) {
                    success = false;
                    missed.push(res.notificationId);
                }
            });
            res.status(200);
            if (success) {
                res.json({success: success});
            } else {
                res.json({success: success, missed: missed, error: null});
            }
        })
        .catch(function (error) {
            console.log("Catch de markAsRead: " + JSON.stringify(error));
            res.status(500);
            res.json({success: false, error: error});
        });
})

module.exports = notificacionesRouter;