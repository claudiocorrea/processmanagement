/**
 * Created by Antonio on 8/29/2017.
 */
var express = require('express'),
    routerComboAnidado = express.Router(),
    controllerComboAnidado = require('../controller/comboAnidado');

routerComboAnidado.get('/', controllerComboAnidado.obtenerValoresComboAnidado);

module.exports = routerComboAnidado;