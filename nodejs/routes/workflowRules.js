/**
 * Created by Antonio on 23/09/2017.
 */
var express = require('express'),
    routerWorkflowRules = express.Router(),
    workflowRulesController = require('../controller/workflowRules');

routerWorkflowRules.get('/workflowForms', workflowRulesController.workflow_Forms);

module.exports = routerWorkflowRules;