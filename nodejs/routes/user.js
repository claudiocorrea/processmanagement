/**
 * Created by Antonio on 6/1/2017.
 */
var express = require('express'),
    routerUser = express.Router(),
    userController = require('../controller/users');

routerUser.post('/login', userController.login);
routerUser.get('/userLogin', userController.userLogin);
routerUser.post('/loginDummy', userController.loginDummy);

module.exports = routerUser;