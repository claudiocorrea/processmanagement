var express = require ('express'),
    alertsController = require('../controller/alerts'),
    alertsRouter = express.Router(),
    smtpConfig = require('../config/mailConfig').getSMTPConfig(),
    nodemailer = require('nodemailer'),
    transporter = nodemailer.createTransport(smtpConfig),
    moment = require('moment');


alertsRouter.get('/pendings', function (req, res) {
    alertsController.getAllPendingAlerts()
        .then(function (result) {
            res.status(200);
            res.json(result);
        })
        .catch(function (error) {
            res.status(500);
            res.json(error);
        });
});

alertsRouter.get('/sendPendings', function (req, res) {
    alertsController.getAllPendingAlerts()
        .then(function (result) {

            if (result.success !== true) {
                result.data.forEach(function (alert) {
                    var message = {
                        from: smtpConfig.auth.user,
                        to: alert.ToEmail,
                        subject: alert.Subject,
                        text: alert.Body,
                        html: alert.Body
                    };

                    transporter.sendMail(message, function (error, info) {
                        var fechaStr = moment().format('DD/MM/YYYY'),
                            horaStr = moment().format('HH:mm:ss'),
                            res = {
                                alertId: alert.IdJob,
                                dateOut: fechaStr,
                                timeOut: horaStr,
                                result: 'ok'
                            };
                        if (error) {
                            console.log("Error al enviar el mensaje " + JSON.stringify(message) + "\n\t\t" + error);
                            res.result = error;

                        } else {
                            console.log("Mensaje enviado a las " + fechaStr + " " + horaStr);
                        }

                        alertsController.markAlertAsSent(res);

                    });
                });
            }
            res.status(200);
            res.json({success: true});
        })
        .catch(function (error) {
            res.status(500);
            res.json(error);
        });
})

module.exports = alertsRouter;