/**
 * Created by Antonio on 7/7/2017.
 */
var express = require('express'),
    routerForm = express.Router(),
    formController = require('../controller/sgpForm');

routerForm.get('/getFormFieldsJson', formController.getFormFieldsJson);
routerForm.post('/', formController.captureForm);
routerForm.get('/', formController.getFormFields);
routerForm.get('/getFieldValues', formController.getFormFieldValues);
routerForm.get('/getChildFormFieldValues', formController.getChildFormFieldValues);
routerForm.get('/generatePDF', formController.generatePDF);
routerForm.get('/PDF', formController.getPDF);
routerForm.get('/extjsFormFields', formController.getExtJSFormFields);

module.exports = routerForm;
