/**
 * Created by Antonio on 7/19/2017.
 */
var express = require('express'),
    routerSoap = express.Router(),
    soapValoresPosiblesController = require('../controller/soap/valoresPosiblesCampo'),
    soapValoresComboHijo = require('../controller/soap/comboAnidado');

routerSoap.get('/valoresPosibles', soapValoresPosiblesController.obtenerValoresPosiblesCampo);
routerSoap.get('/valoresComboHijo', soapValoresComboHijo.obtenerValoresComboAnidado);

module.exports = routerSoap;