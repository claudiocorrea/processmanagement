var util = require('util');
/**
 * Ejemplos de configuracion:
 *
 * {minute: string, hour: string, dayOfMonth: string, month: string, dayOfWeek: string}
 *
 * minute = '1,2,4,5'   //Se ejecuta al minuto 1,2,4,5
 * minute = '1-5'       //Se ejecuta en los minutos 1 a 5
 * minute = '1-10/2'       //Se ejecuta cada minuto par entre 1 y 10 (2, 4, 6 y 8)
 */

var config = {
    minute: '*/5', //Cada 5 minutos
    hour: '*',
    dayOfMonth: '*',
    month: '*',
    dayOfWeek: '*'
};

//module.exports = config;

exports.getCronConfigEmailJobs = function () {
    return util.format("%s %s %s %s %s", config.minute, config.hour, config.dayOfMonth, config.month, config.dayOfWeek);
}