module.exports = {
/*
    header: {
        height: '1.5cm', contents: function (page) {
            return '<header class="pdf-header" style=" overflow:hidden; font-size: 10px; padding: 10px; margin: -20 -15px; color: #fff; background: none repeat scroll 0 0 #00396f;"><p> XYZ </p></header>'
        }
    },
*/

    footer: {
        height: '1cm', contents: function (page, numPages) {
            return '<span style="float:right; font-size: 8px">P&aacute;gina ' + page + ' de ' + numPages + '</span>';
        }
    }

}
