Ext.define('ProcessManagement.grid.cmpc.Sesiones', {
    extend: 'Ext.grid.Panel',
    xtype: 'gridSesiones',

    requires: [
        'ProcessManagement.store.cmpc.KPIs'
    ],

    margin: 5,

    collapsible: true,
    
    border: true,

    columns: [
        {   xtype: 'templatecolumn', text: 'Nombre del proyecto', dataIndex: 'proyect', 
            width: 190, tpl: [ '<p/><div class="tplText">{proyect}</div>' ] 
        }, { 
            xtype: 'templatecolumn', text: 'Tipo actividad', dataIndex: 'actividad', 
            flex: 1, tpl: [ '<p/><div class="tplText">{actividad}</div>' ]  
        }, { 
            xtype: 'templatecolumn', text: 'Año', dataIndex: 'ano', 
            width: 60, tpl: [ '<p/><div class="tplText">{ano}</div>' ]  
        }, { 
            xtype: 'templatecolumn', text: 'Enero', dataIndex: 'enero', 
            width: 60, tpl: [ '<p/><div class="tplText">{enero}</div>' ]  
        }, { 
            xtype: 'templatecolumn', text: 'Febrero', dataIndex: 'febrero', 
            width: 60, tpl: [ '<p/><div class="tplText">{febrero}</div>' ]  
        }, { 
            xtype: 'templatecolumn', text: 'Marzo', dataIndex: 'marzo', 
            width: 60, tpl: [ '<p/><div class="tplText">{marzo}</div>' ]  
        }, { 
            xtype: 'templatecolumn', text: 'Abril', dataIndex: 'abril', 
            width: 60, tpl: [ '<p/><div class="tplText">{abril}</div>' ]  
        }, { 
            xtype: 'templatecolumn', text: 'Mayo', dataIndex: 'mayo', 
            width: 60, tpl: [ '<p/><div class="tplText">{mayo}</div>' ]  
        }, { 
            xtype: 'templatecolumn', text: 'Junio', dataIndex: 'junio', 
            width: 60, tpl: [ '<p/><div class="tplText">{junio}</div>' ]  
        }, { 
            xtype: 'templatecolumn', text: 'Julio', dataIndex: 'julio', 
            width: 60, tpl: [ '<p/><div class="tplText">{julio}</div>' ]  
        }, { 
            xtype: 'templatecolumn', text: 'Agosto', dataIndex: 'agosto', 
            width: 60, tpl: [ '<p/><div class="tplText">{agosto}</div>' ]  
        }, { 
            xtype: 'templatecolumn', text: 'Septiembre', dataIndex: 'septiembre', 
            width: 60, tpl: [ '<p/><div class="tplText">{septiembre}</div>' ]  
        }, { 
            xtype: 'templatecolumn', text: 'Octubre', dataIndex: 'octubre', 
            width: 60, tpl: [ '<p/><div class="tplText">{octubre}</div>' ]  
        }, { 
            xtype: 'templatecolumn', text: 'Noviembre', dataIndex: 'noviembre', 
            width: 60, tpl: [ '<p/><div class="tplText">{noviembre}</div>' ]  
        }, { 
            xtype: 'templatecolumn', text: 'Diciembre', dataIndex: 'diciembre', 
            width: 60, tpl: [ '<p/><div class="tplText">{diciembre}</div>' ]  
        }, { 
            xtype: 'templatecolumn', text: 'Total', dataIndex: 'total', 
            width: 65, tpl: [ '<p/><div class="tplText">{total}</div>' ]  
        } 
    ]

})