Ext.define('ProcessManagement.grid.Notifications', {
    extend: 'Ext.grid.Panel',
    xtype: 'gridNotifications',

    requires: [
        'Ext.grid.plugin.RowExpander',
        'ProcessManagement.store.Notifications'
    ],

    reference: 'gridNotifications',

    store: { type: 'notifications'},

    flex: 1,

    headerBorders: false,
    border: false,

    columns: [
        {   xtype: 'templatecolumn',
            flex: 1,
            bind: {text: '{txtFields.words.document}'},
            width: 100, sortable : true, dataIndex: 'WorkflowProceso',
            tpl: [ '<p/><div class="tplText">{WorkflowProceso}</div>' ]
        },
        {   xtype: 'templatecolumn',
            flex: 1,
            bind: {text: '{txtFields.words.username}'},
            width: 100, sortable : true, dataIndex: 'UserName',
            tpl: [ '<p/><div class="tplText">{UserName}</div>' ]
        },
        {   xtype: 'datecolumn',
            bind: {text: '{txtFields.words.date}'},
            format: 'd/m/Y H:i:s',
            width: 150, sortable : true, dataIndex: 'FechaHora',
            tpl: [ '<p/><div class="tplText">{FechaHora}</div>' ]
        },
        {   xtype: 'templatecolumn',
            flex: 1,
            bind: {text: '{txtFields.words.message}'},
            width: 100, sortable : true, dataIndex: 'Mensaje',
            tpl: [ '<p/><div class="tplText">{Mensaje}</div>' ]
        },
        {   xtype: 'templatecolumn',
            flex: 1,
            bind: {text: '{txtFields.words.reference}'},
            width: 100, sortable : true, dataIndex: 'Referencia',
            tpl: [ '<p/><div class="tplText">{Referencia}</div>' ]
        }
    ],

    bind: {
        selection: '{theRow}'
    },

    selModel: {
        selType: 'checkboxmodel',
        injectCheckbox: 'first'
    },

    buttons: [
        {
            bind: {
                text: '{txtFields.words.markSelectionAsRead}',
                disabled: '{!theRow}'
            },
            disabled: true,
            handler: 'markSelectionAsRead',
            reference: 'markSelectedNotificationsAsRead'
        },
        {
            bind: {
                text: '{txtFields.words.markAllAsRead}'
            },
            disabled: false,
            handler: 'markAllAsRead',
            reference: 'markAllNotificationsAsRead'
        },
        {
            bind: {
                text: '{txtFields.words.close}'
            },
            disabled: false,
            handler: 'closeWindow'
        }
    ]

})