Ext.define('ProcessManagement.grid.Workflows', {
    extend: 'Ext.grid.Panel',
    xtype: 'gridWorkflows',
	
	requires: [ 
		'Ext.grid.plugin.RowExpander',
		'ProcessManagement.store.Workflows'		
	],

    reference: 'gridWorkflows',

    store: { type: 'workflows'},

    flex: 1,

    headerBorders: false,
    border: false,

    columns: [
        {   xtype: 'templatecolumn',
            flex: 1,
            tpl: [
                '<div class="tplTitle2"><span class="{icon}">' +
                '</span>&ensp;&ensp;{Workflow}</div>'
            ]
        }
    ],

    plugins: {
        rowexpander: {
            rowBodyTpl: new Ext.XTemplate(
                '<div class="tplDescription">{description}</div>'
            )
        }
    },

    bind: {
        selection: '{theRow}'
    },

    buttons: [
        {
            bind: {
                text: '{txtFields.words.new}',
                disabled: '{!theRow}'
            },
            disabled: true,
            handler: 'openRequest'
        }
    ],

    listeners: {
        selectionchange: 'selectionWorkflow'
    }

})