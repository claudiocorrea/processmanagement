Ext.define('ProcessManagement.grid.Steps', {
    extend: 'Ext.grid.Panel',
    xtype: 'gridSteps',
	
	requires: [ 
		'Ext.grid.plugin.RowExpander',
		'ProcessManagement.store.Steps'		
	],

    store: { type: 'steps' },

    headerBorders: false,
    border: false,

    padding: '10 15 0 15',

    hideHeaders: false,
    scrollable: 'vertical', 

    columns: [
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.workflowId}'},
            width: 80, sortable : true, dataIndex: 'WorkflowId',
            tpl: [ 
				'<div class="toggle {Estado}"></div>',
				'<p/><div class="tplText">{WorkflowId}</div><p/>'
			],
			hidden: false
        },
        {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.detail}'},
            flex: 1, sortable : true, dataIndex: 'proceso',
            tpl: [
                
                '<div class="tplText">{proceso} - {Referencia}</div>',
                '<div class="tplTitle">{WorkFlow}</div>'
            ]
        },
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.code}'},
            width: 100, sortable : true, dataIndex: 'proceso',
            tpl: [ '<p/><div class="tplText">{proceso}</div>' ],
			hidden: true
        },
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.title}'},
            width: 100, sortable : true, dataIndex: 'Referencia',
            tpl: [ '<p/><div class="tplText">{Referencia}</div>' ],
			hidden: true
        },
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.request}'},
            width: 100, sortable : true, dataIndex: 'WorkFlow',
            tpl: [ '<p/><div class="tplText">{WorkFlow}</div>' ],
			hidden: true
        },
		
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.stepId}'},
            width: 100, sortable : true, dataIndex: 'StepId',
            tpl: [ '<p/><div class="tplText">{StepId}</div>' ],
			hidden: true
        },
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.task}'},
            flex: 1, sortable : true, dataIndex: 'StepTxt',
            tpl: [ '<p/><div class="tplText">{StepTxt}</div>' ]
        },
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.date}s'},
            width: 210, sortable : true, dataIndex: 'fechas',
            tpl: [ '<p/><div class="tplText">{fechas}</div>' ]
        },
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.allowDocs}'},
            width: 130, sortable : true, dataIndex: 'AllowDocs',
            tpl: [ '<p/><div class="tplText">{AllowDocs}</div>' ],
			hidden: true
        },
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.personInCharge}'},
            flex: 1, sortable : true, dataIndex: 'responsable',
            tpl: [ '<p/><div class="tplText">{responsable}</div>' ],
			hidden: true
        },
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.state}'},
            width: 150, sortable : true, dataIndex: 'Estado',
            tpl: [ '<p/><div class="tplText">{Estado}</div>' ],
			hidden: true
        },	
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.originalWorkflowId}'},
            width: 150, sortable : true, dataIndex: 'WorkflowIDoriginal',
            tpl: [ '<p/><div class="tplText">{WorkflowIDoriginal}</div>' ],
			hidden: true
        },
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.companyId}'},
            width: 150, sortable : true, dataIndex: 'IdEmpresa',
            tpl: [ '<p/><div class="tplText">{IdEmpresa}</div>' ],
			hidden: true
        },
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.form}'},
            width: 150, sortable : true, dataIndex: 'IdForm',
            tpl: [ '<p/><div class="tplText">{IdForm}</div>' ],
			hidden: true
        },		
        {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-sitemap',
                bind: {tooltip: '{txtFields.words.model}'},
                handler: 'openModel'
            }]
        },
        {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-file',
                bind: {tooltip: '{txtFields.words.show}'},
                handler: 'openRequest'
            }]
        }
    ],

    plugins: {
        rowexpander: {
            rowBodyTpl: new Ext.XTemplate(
				'<p>{description}</p>'
            )
        }
    },

    listeners: {
        select: 'selectionChange'
    }
});