Ext.define('ProcessManagement.grid.Files', {
    extend: 'Ext.grid.Panel',
    xtype: 'gridFiles',
	
	requires: [ 
		'Ext.grid.plugin.RowExpander',
		'ProcessManagement.store.Files'		
	],

    reference: 'gridFiles',

    store: { type: 'files' },

    headerBorders: false,
    border: false,

    scrollable: 'vertical',

    columns: [
        {   xtype: 'templatecolumn',
            flex: 1,
            tpl: [
                '<div class="tplTitle3" style="white-space: normal;"><span class="{icon}">',
                '</span>&ensp;&ensp;{Archivo}</div>',
                '<div class="tplText" style="white-space: normal;">',
                '&ensp;&ensp;&ensp;&ensp;{Date} {Time} ',
                '<span style="padding-left: 20%;">{FullName}</span></div>'
            ]
        }
    ],

    listeners: {
        selectionchange: 'selectionFile'
    }

})