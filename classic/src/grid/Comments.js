Ext.define('ProcessManagement.grid.Comments', {
    extend: 'Ext.grid.Panel',
    xtype: 'gridComments',
	
	requires: [ 
		'Ext.grid.plugin.RowExpander',
		'ProcessManagement.store.Comments'		
	],

    reference: 'gridComments',

    store: { type: 'comments' },

    headerBorders: false,
    border: false,

    scrollable: 'vertical',

    columns: [
        {   xtype: 'templatecolumn',
            flex: 1,
            tpl: [
                '<div class="tplTitle4" style="white-space: normal;">',
                '<img class="tplImage" src="resources/images/users/IMG{IdUser}.png" ' +
                ' onerror="this.src = \'resources/images/users/HSA0.png\'"/>&ensp;&ensp;{UserName}</div>',
                '<div class="tplText3" style="white-space: normal;word-wrap: break-word;">{Description}</div>',
                '<div class="tplText">',
                '&ensp;&ensp;&ensp;&ensp;{Cdate} {Ctime}</div>'
            ]
        }
    ],

    listeners: {
        selectionchange: 'selectionComment'
    }

})