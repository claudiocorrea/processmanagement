Ext.define('ProcessManagement.view.main.Main', {
    extend: 'Ext.container.Container',
    xtype: 'main',

    id: 'main',

    requires: [
        'Ext.plugin.Viewport',
		'ProcessManagement.view.screen.Dashboard',
		'ProcessManagement.view.screen.Request',
        'ProcessManagement.view.screen.Search',
        'ProcessManagement.view.screen.Files',
        'ProcessManagement.view.cmpc.screen.DashboardCMPC',
        'ProcessManagement.view.cmpc.screen.*'
    ],

    controller: 'main',
    viewModel: 'main',

    plugins: 'viewport',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        { xtype: 'mainToolbar'},
        { xtype: 'mainMenu', flex: 1 }
    ],

    listeners: {
        afterrender: 'initMain'
    }

})