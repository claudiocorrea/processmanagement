Ext.define('ProcessManagement.view.component.Toolbar', {
    extend: 'Ext.Toolbar',
    xtype: 'mainToolbar',
	
	id: 'toolbar',

    height: 60,
    padding: 0,
    border: false,

    items: [
        {   xtype: 'component', cls: 'sgplogo', reference: 'imageLogo',
            html: '<a href="."><div><img src="resources/images/logo.png"></div></a>'
        }, {
            xtype: 'label',
            text: globalParams.appName,
            cls: 'sgptitle'
        }, {
            xtype:'button',
            reference: 'btnNewRequest',
            userCls: 'circular btnPlus',
            iconCls:'x-fa fa-plus',
            bind: { tooltip: '{txtFields.words.new} {txtFields.words.document}' },
            margin: '0 0 0 30', style: 'left: 281px !important;',
            handler: 'openNewRequest'
        }, '->' , {
            xtype: 'button',
            iconCls: 'x-fa fa-bell-o',
            style: 'border: none; background-color: white;',
            reference: 'btnNotifications',
            text: ' ',
            hidden: true,
            handler: 'showNotificationsPopup',
            plugins: [
                {
                    ptype: 'badgetext',
                    defaultText: 0,
                    disableOpacity: 1,
                    disableBg: 'red',
                    align: 'center'
                }
            ],
            listeners: {
                'badgeclick': {
                    fn: function (button) {
                        button.click();
                    }
                }
            }
        }, {
            xtype:'button',
			reference: 'btnUser',
			userCls: 'circular',
            style: 'border: none; background: transparent;'
        }, {
            xtype: 'image',
            reference: 'imageUser',
            width: 40, height: 40, margin: '0 10 0 0',
            userCls: 'circular',
            src: 'resources/images/users/HSA0.png',
			hidden: true
        }, {
            xtype:'button',
			userCls: 'circular',
            style: 'border: none; background-color: white;',			
            iconCls:'x-fa fa-cog', arrowVisible: false,
            bind: { tooltip: '{txtFields.words.config}' },
			menu: [
				{ 	bind: { text: '{txtFields.words.lang}' },
                    menu: {
                        items: [
                            {	id: 'es',
                                bind: { text: '{txtFields.words.spanish}' },
                                checked: false,
                                group: 'lang',
                                checkHandler: 'changeLanguage'
                            }, {
								id: 'en',
                                bind: { text: '{txtFields.words.english}' },
                                checked: false,
                                group: 'lang',
                                checkHandler: 'changeLanguage'
                            }
						]
					}
				},	
				{ 	bind: {text: '{txtFields.words.colors}'}, handler: 'openTheme' }
			]
        }, {
            xtype:'button',
            reference: 'btnLogout',
			userCls: 'circular',
            style: 'border: none; background-color: white;',
            iconCls:'x-fa fa-sign-out',
            bind: { tooltip: '{txtFields.words.logout}' },
            handler: 'logout'
        }
    ]

})