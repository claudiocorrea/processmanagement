Ext.define('ProcessManagement.view.main.Menu', {
	extend: 'Ext.Panel',
	xtype: 'mainMenu',
	
	id: 'mainMenu',

	requires: [	],
	
	viewModel: 'menu',
	
	layout: 'border',	
	header: false,
	
	items: [
		{	region: 'west',
			width: 60, reference: 'main-menu',
			split: false,
			cls: 'treelist-with-nav',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			border: false,
			scrollable: 'y',
			items: [
				{
					xtype: 'treelist', id: 'menu-treelist' ,
					reference: 'navTreeList',
					ui: 'navigator',				
					micro: true, expanderOnly: false,
					bind: '{navItems}', hidden: true,
					listeners: { selectionchange: 'onNavTreeSelectionChange' }
				}, {
					xtype: 'treelist', id: 'cmpc-menu-treelist' ,
					ui: 'navigator',				
					micro: true, expanderOnly: false,
					bind: '{navCmpc}', hidden: true,
					listeners: { selectionchange: 'onNavTreeSelectionChange' }
				}
			]
		}, {
			region: 'center',
			reference: 'mainMenuPanel',
			items: []
		}
	]

})
