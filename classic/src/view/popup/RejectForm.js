Ext.define('ProcessManagement.view.popup.RejectForm', {
    extend: 'Ext.window.Window',
	
	reference: 'rejectPopup',
	
	controller: 'rejectForm',
	
	width: 460,
    
    layout: {
        type: 'fit'
    },
	
	modal: true,
	
	viewModel: {
        data: {
            comentario: '',
            tipoRechazo: '',
            pasoAnterior: ''
        }
    },
	
	dockedItems: [
		{	xtype: 'toolbar',
			dock: 'bottom',
			ui: 'footer',
			border: false,
			items: [ '->',
				{
					xtype: 'button',
					formBind: true,
					bind: {text: "{txtFields.words.accept}"},
					handler: 'onRechazar'
				},
				{
					xtype: 'button',
					bind: {text: '{txtFields.words.cancel}'},
					handler: 'close'
				}
			]
		}
	],
	
	items: [
		{ 	xtype: 'form',
			bodyPadding: 15,
			defaults: {
				anchor: '100%',
				allowBlank: false,
				labelWidth: 60
			},
			items: [
				{	xtype: 'textfield',
					name: 'comentario',
					labelAlign: 'top',
					bind: {fieldLabel: '{txtFields.words.comment}', value: '{comentario}'}							
				}, 
				{   xtype: 'radiogroup',
					layout: {type: 'table', columns: 2},
					labelAlign: 'top',
					bind: {fieldLabel: '{txtFields.titles.rejectType}', value: '{tipoRechazo}'},
					items: [
						{ bind: {boxLabel: '{txtFields.titles.rejectAndFinish}'}, inputValue: '0', name: 'RtipoRechazo', colspan: 2 },
						{ bind: {boxLabel: '{txtFields.titles.rejectAndGoToPreviousStep}'}, inputValue: '1', name: 'RtipoRechazo', colspan: 2 },
						{ bind: {boxLabel: '{txtFields.titles.rejectAndRestart}'}, inputValue: '2', name: 'RtipoRechazo', colspan: 2 },
						{ bind: {boxLabel: '{txtFields.titles.rejectAndGoToStep}' + ': &nbsp;&nbsp;'}, inputValue: '3', name: 'RtipoRechazo' },
						{ 
							xtype: 'combobox', reference: 'cbPreviousSteps',
							hideLabel: true, editable: false, 
							store: Ext.create('ProcessManagement.store.PreviousSteps'), 
							displayField: 'Title', valueField: 'StepId', queryMode: 'local', 
							bind: {value: '{pasoAnterior}'}
						}
					],
					listeners: {
						change: function (_this, newValue, oldValue, eOpts) {
							var combo = _this.up('window').down('combobox');
							if (newValue.RtipoRechazo == "3") {
								combo.setDisabled(false);
							} else {
								combo.setDisabled(true);
								combo.clearValue();
							}

						}
					}
				}
			],
			listeners: {
				afterlayout: function (_this, layout, eOpts) {
					this.down('textfield').clearInvalid();
					this.down('textfield').focus(true);
				},
				afterrender: function (_this, eOpts) {
					this.down('textfield').clearInvalid();
					this.down('textfield').focus(true);
				}
			}
		}
	],

    listeners: {
        afterrender: 'initScreen'
    }

	
})