Ext.define('ProcessManagement.view.popup.Upload', {
    extend: 'Ext.window.Window',

    reference: 'uploadPopup',

    controller: 'upload',

    y: '3%',

    width: '74%',
    height: '94%',

    layout : 'fit',

    bind: {
        title: '{txtFields.messages.selectFile}'
    },
    //header: false,
    resizable: true,
    modal: true,

    viewModel: {
        data: {
            file: ''
        }
    },

    items: [
        {
            xtype : "component",
            reference: 'componentURL',
            autoEl : {
                tag : "iframe",
                src : ""
            }
        }
        /*{
            xtype: 'form',
            reference: 'uploadForm',
            bodyPadding: 15,
            layout: 'hbox',
            items: [
                /*{
                    xtype: 'filefield', width: '100%',
                    bind: {
                        value: '{file}'
                    },
                    msgTarget: 'side', allowBlank: false,
                    listeners: {
                        change: 'upload'
                    }
                }                
            ]
        }*/
    ]

})