Ext.define('ProcessManagement.view.popup.Modal', {
    extend: 'Ext.window.Window',
	
	reference: 'modalPopup',
	
	width: '50%',
    height: '75%',
	
	resizable: true,
    layout : 'fit',
    modal: true,
	
	maximized: true,

    items : [{
        xtype : "component",
		reference: 'componentURL',
        autoEl : {
            tag : "iframe",
            src : ""
        }
    }]
})
