Ext.define('ProcessManagement.view.popup.Enterprise', {
    extend: 'Ext.window.Window',

    controller: 'lock',
    viewModel: 'main',
	
    reference: 'enterprisePopup',

    width: 400,
    //height: '40%',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    bind: { 
        title: '{txtFields.titles.selectEnterprise}', 
    },
    modal: true,
    closable: false,

    items: [
        {   xtype: 'panel', reference: 'radioEnterprise', 
            layout: { type: 'vbox', align: 'stretch' }, padding: 10 
        }
    ],

    listeners: {
        afterrender: 'initEnterprisePopup'
    }
})