/**
 * Created by Antonio on 8/10/2017.
 */
Ext.define('ProcessManagement.view.popup.ChildForm', {
    extend: 'Ext.window.Window',

    reference: 'childForm',

    controller: 'childForm',

    width: '50%',
    height: '75%',

    layout: {
        type: 'fit'
    },

    modal: true,

    viewModel: {
        data: {
            idForm: null,
            idField: null
        }
    },

    tools: [
        { reference: 'btnNewDetail', iconCls: 'x-fa fa-plus', handler: 'onAddNewDetailRow', bind: {tooltip: '{txtFields.words.add}'} }
    ],


    padding: '0 5 0 5',

    dockedItems: [
        {	xtype: 'toolbar',
            dock: 'bottom',
            ui: 'footer',
            border: false,
            defaults: {
                //userCls: 'circular',
                margin: 5
            },
            items: [ '->',
                {
                    xtype: 'button',
                    formBind: true,
                    bind: {tooltip: "{txtFields.words.accept}", text: "{txtFields.words.accept}"},
                    //iconCls: 'x-fa fa-floppy-o',
                    handler: 'onAcceptChildForm',
                    reference: 'btnAcceptChildForm'
                },
                {
                    xtype: 'button',
                    bind: {tooltip: '{txtFields.words.cancel}', text: '{txtFields.words.cancel}'},
                    //iconCls: 'x-fa fa-window-close',
                    handler: 'onCancelChildForm'
                }
            ]
        }
    ],

    items: [
        {
            xtype: 'gridpanel',
            reference: 'gridCamposFormHijo',
            selModel: {
                mode: 'MULTI'
            },
            layout: {
                type: 'fit',
                align: 'stretch'
            },
            plugins: [{
                ptype: 'cellediting',
                clicksToEdit: 1
            }]/*,
            tbar: [
                '->',
                {userCls: 'circular', iconCls: 'x-fa fa-plus-circle', handler: 'onAddNewDetailRow'}
            ]*/
        }
            ]


})