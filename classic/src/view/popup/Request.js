Ext.define('ProcessManagement.view.popup.Request', {
    extend: 'Ext.window.Window',

    controller: 'workflow',
	
    reference: 'newRequestPopup',

    width: 600,
    height: '75%',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    bind: { title: '{txtFields.words.new} {txtFields.words.document}' },
    modal: true,

    tbar: {
        items: [
            {
                xtype: 'textfield',
                reference: 'filterText',
                width: '99%',
                bind: {emptyText: '{txtFields.words.search}' + '...'},
                listeners: {
                    change: 'filterList'
                }
            }
        ]
    },

    items: [
        {   xtype: 'gridWorkflows' }
    ]
})