Ext.define('ProcessManagement.view.popup.Theme', {
    extend: 'Ext.window.Window',
	
	requires: [
		'Ext.ux.colorpick.Button'
	],
	
	reference: 'themePopup',
	
	controller: 'theme',

    width: 200,
    height: 250,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    bind: { title: '{txtFields.titles.theme}' },	
	
	bodyCls: 'screenBackground',
	
	viewModel: {
        data: {
			colorTbar: 'white',
            colorMenu: '#1E90FF',
			colorHeader: '#1E90FF',
            full: false
        }
    },
	
	defaults: {
		xtype: 'fieldcontainer',
		layout: 'hbox',
		margin: '10 10 0 10'
	},
	
	items: [
		{ 	
			items: [
				{	xtype: 'colorbutton',
					bind: '{colorTbar}',
					width: 25, height: 25, margin: 5,
					listeners: {
						change: 'onChangeTbar'
					}
				}, { xtype: 'component', html: ' Cabecera', cls: 'labelDefault' }
				
			]
		},
		{ 	items: [
				{	xtype: 'colorbutton',
					bind: '{colorMenu}',
					width: 25, height: 25, margin: 5,
					listeners: {
						change: 'onChangeMenu'
					}
				}, { 	xtype: 'component', html: ' Menu', cls: 'labelDefault' }
				
			]
		},
		{ 	items: [
				{	xtype: 'colorbutton',
					bind: '{colorHeader}',
					width: 25, height: 25, margin: 5,
					listeners: {
						change: 'onChangeHeader'
					}
				}, { 	xtype: 'component', html: ' Titulos', cls: 'labelDefault' }
				
			]
		}		
	],
	
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'bottom',
		ui: 'footer',
		defaults: {			
			xtype: 'button'
		},
		items: [ '->',
			{ 	bind: { text: '{txtFields.words.save}' },
				handler: 'saveTheme'
			},
			{ 	bind: { text: '{txtFields.words.cancel}' },
				handler: 'resetTheme'
			}, '->'
		]
	}]
	
})