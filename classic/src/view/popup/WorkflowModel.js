Ext.define('ProcessManagement.view.popup.WorkflowModel', {
    extend: 'Ext.window.Window',

    reference: 'workflowModelPopup',

    controller: 'workflowModel',

    width: '75%',
    height: '90%',
	
	title: 'Workflow model',

    resizable: true,
    layout : 'fit',
    modal: true,

    items : [{ 	xtype: 'panel', bodyPadding: 5,
        html: '<div id=\'myDiagram\' style=\'height: ' + (Ext.getBody().getViewSize().height -120) + 'px; background-color: #f6f6f6;\'></div>'
    }],

    listeners: { afterrender: 'initScreen' }
})