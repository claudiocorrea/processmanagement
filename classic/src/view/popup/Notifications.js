Ext.define('ProcessManagement.view.popup.Notifications', {
    extend: 'Ext.window.Window',

    controller: 'notifications',

    reference: 'notificationsPopup',

    width: '75%',
    height: '75%',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    bind: { title: '{txtFields.words.notifications}' },
    modal: true,

    items: [
        {   xtype: 'gridNotifications' }
    ],

    listeners: {
        beforeclose: 'onBeforeclose'
    }
})
