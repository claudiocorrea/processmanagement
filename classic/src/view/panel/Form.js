Ext.define('ProcessManagement.view.panel.Form', {
    extend: 'Ext.Panel',
    xtype: 'formPanel',

	id: 'formPanel',
	reference: 'formPanel',

	controller: 'form',
	viewModel: 'form',

	requires: [
		'Ext.form.field.ComboBox',
		'Ext.form.field.Tag',
		'Ext.form.field.HtmlEditor'
	],

    layout: {
        type: 'fit',
		align: 'stretch'
    },

	baseCls: 'screenBackground',
	padding: '0 10 0 10',	

    items: [
        { 	xtype: 'form',
			reference: "SGPForm",
			frame: false,
			margin: '-3 0 0 0',
			scrollable: 'vertical',
			defaults: {
				anchor: '100%',
				labelWidth: 300,
				labelStyle: 'width: 300px;',
				disabledCls: 'af-item-disabled'
			},
			dockedItems: [{
				xtype: 'toolbar',
				reference: 'botonera',
				dock: 'bottom',
				ui: 'footer',
				baseCls: 'transparentBackground',
				defaults: {
					xtype: 'button',
					userCls: 'circular',
					margin: 5
				},
				items: [ '->',
					{	name: 'grabar',
						reference: 'btnGrabar',
						bind: { tooltip: '{txtFields.words.save}' },
						iconCls: 'x-fa fa-floppy-o',
						handler: 'onGrabar'
					},
					{	reference: 'grabarEnviar',
						bind: { tooltip: '{txtFields.words.save} {txtFields.words.and} {txtFields.words.send}' },
						//iconCls: 'x-fa fa-reply-all',
						iconCls: 'x-fa fa-thumbs-o-up',
						handler: 'onGrabarEnviar'
					},
					{	name: 'rechazar',
						reference: 'btnRechazar',
						bind: { tooltip: '{txtFields.words.reject}' },
						//iconCls: 'x-fa fa-remove',
						iconCls: 'x-fa fa-thumbs-o-down',
						handler: 'rechazarWkf'
					},
					{	name: 'imprimir',
						reference: 'btnImprimir',
						bind: { tooltip: '{txtFields.words.print}' },
						iconCls: 'x-fa fa-print',
						handler: 'printForm',
						hidden: false
					},
					{	name: 'cancelar',
						reference: 'btnCancelar',
						bind: { tooltip: '{txtFields.words.cancel}' },
						iconCls: 'x-fa fa-window-close',
						handler: 'cancelarWkf'
					}, '->'
				]
			},{
				xtype: 'toolbar',
				reference: 'botoneraImpresion',
				dock: 'bottom',
				ui: 'footer',
				baseCls: 'transparentBackground',
				hidden: true,
				defaults: {
					xtype: 'button',
					userCls: 'circular',
					margin: 5
				},
				items: [ '->',
					{	name: 'imprimir',
						reference: 'btnImprimir2',
						bind: { tooltip: '{txtFields.words.print}' },
						iconCls: 'x-fa fa-print',
						handler: 'printForm',
						hidden: false
					}, '->'
				]
			}]
		}
	],
	bbar: {
		xtype: 'toolbar', reference: 'cmpc-botonera',
		dock: 'bottom', ui: 'footer',
		baseCls: 'transparentBackground',
		defaults: {
			xtype: 'button',
			userCls: 'circular',
			margin: 5
		}, hidden: true,
		items: [ '->',
			{	name: 'grabar',
				//reference: 'btnGrabar',
				bind: { tooltip: '{txtFields.words.save}' },
				iconCls: 'x-fa fa-floppy-o',
				handler: ''
			},
			{	//reference: 'grabarEnviar',
				bind: { tooltip: '{txtFields.words.save} {txtFields.words.and} {txtFields.words.send}' },
				//iconCls: 'x-fa fa-reply-all',
				iconCls: 'x-fa fa-thumbs-o-up',
				handler: 'aprobarPasoCMPC'
			},
			{	name: 'rechazar',
				//reference: 'btnRechazar',
				bind: { tooltip: '{txtFields.words.reject}' },
				//iconCls: 'x-fa fa-remove',
				iconCls: 'x-fa fa-thumbs-o-down',
				handler: 'rechazarPasoCMPC'
			},
			{	name: 'imprimir',
				//reference: 'btnImprimir',
				bind: { tooltip: '{txtFields.words.print}' },
				iconCls: 'x-fa fa-print',
				handler: 'imprimirCMPC'
			},
			{	name: 'cancelar',
				//reference: 'btnCancelar',
				bind: { tooltip: '{txtFields.words.cancel}' },
				iconCls: 'x-fa fa-window-close',
				handler: 'cancelarWkf'
			},'->'
		]
	}
})
