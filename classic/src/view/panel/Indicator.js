Ext.define('ProcessManagement.view.panel.Indicator', {
    extend: 'Ext.Panel',
    xtype: 'indicatorPanel',  

	id: 'indicatorPanel',
	layout: 'vbox',

    scrollable: 'vertical',
	
	items: [ 
		{
			xtype: 'fieldcontainer',			
			layout: 'hbox',
			defaults: { flex: 1 },
			items: []
		}
	]
	
})