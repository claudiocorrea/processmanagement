Ext.define('ProcessManagement.view.panel.FileFilter', {
    extend: 'Ext.Panel',
    xtype: 'fileFilterPanel',
	
	requires: [ 
		'Ext.form.DateField'
	],

    reference: 'fileFilterPanel',
	
    layout: 'hbox',

    plugins: 'responsive',

    responsiveConfig: {
        'width >= 1000': {
			height: 120,
            layout: {
                type: 'box',
                align: 'stretch',
                vertical: false
            },  
			hidden: false
        },
        'width < 1000': {
			height: 250,
            layout: {
                type: 'box',
                align: 'stretch',
                vertical: true
            }, 
			hidden: true
        }
    },

    bbar: {
        baseCls: 'screenBackground',
        items: [
            '->', {
                xtype:'button',
                userCls: 'circular',
                iconCls:'x-fa fa-binoculars',
                bind: {
                    tooltip: '{txtFields.words.show}' + ' ' + '{txtFields.words.filter}'.toLocaleLowerCase()
                },
                handler: 'onShowFilters',
                hidden: true
            }, {
                xtype: 'button', margin: 5,
                userCls: 'circular',
                iconCls: 'x-fa fa-trash',
                bind: { tooltip: '{txtFields.words.delete} {txtFields.words.filter}' },
                handler: 'removeFilters'
            }, '->'
        ]
    },

    defaults: {
        xtype: 'fieldcontainer',
        layout: 'hbox',
        labelWidth: 150,
        labelAlign: 'top',
        labelCls:'textDefault',
        anchor: '100%',
        margin: '5 20 -10 20'
    },

    items: [
        {	
            bind: {fieldLabel: '{txtFields.words.type}'},
            items: [
                {
                    xtype: 'segmentedbutton',
                    reference: 'sgmStates',
                    items: [
                        { id: 'state0', bind: {text: '{txtFields.words.current}'}, name: 'Pendiente'  },
                        { id: 'state1', bind: {text: '{txtFields.words.no} {txtFields.words.current}'}, name: 'Aprobado' }
                    ],
                    listeners: {
                        toggle: 'filterStates'
                    }
                }
            ]
        }, {
            bind: {fieldLabel: '{txtFields.words.expiration}'},
            items: [
                {
                    xtype: 'segmentedbutton',
                    reference: 'sgmDates',
                    items: [
                        { id: 'date0', bind: {text: '{txtFields.words.startDate}'}, name: 'startDate'  },
                        { id: 'date1', bind: {text: '{txtFields.words.endDate}'}, name: 'endDate' }
                    ],
                    listeners: {
                        toggle: 'changeDates'
                    }
                }, {
                    xtype: 'datefield', margin: '0 10 0 10',
                    reference: 'dateFieldFrom',
                    bind: {emptyText: '{txtFields.words.from}' + '...'},
                    listeners: {
                        change: 'filterDates'
                    }
                }, {
                    xtype: 'datefield',
                    reference: 'dateFieldTo',
                    bind: {emptyText: '{txtFields.words.to}' + '...'},
                    listeners: {
                        change: 'filterDates'
                    }
                }
            ]
        }, {
            bind: {fieldLabel: '{txtFields.words.personInCharge}' + 's'},
            items: [
                {
                    xtype: 'segmentedbutton',
                    reference: 'sgmResponsible',
                    items: [
                        { id: 'resp0', bind: {text: '{txtFields.words.iParticipateIn}'}, name: 'Mias', pressed: true  },
                        { id: 'resp1', bind: {text: '{txtFields.words.all}'}, name: 'Todas' }
                    ],
                    listeners: {
                        toggle: 'filterResponsible'
                    }
                },
                {   xtype: 'label', cls:'textDefault', bind: {text: ' {txtFields.words.or} '}, margin: '15 0 0 15' },
                {
                    xtype: 'textfield', margin: '0 10 0 10',
                    reference: 'txtResponsible',
                    bind: {emptyText: 'RUT'},
                    listeners: {
                        change: 'filterResponsible'
                    }
                }
            ]
        }
    ]

})