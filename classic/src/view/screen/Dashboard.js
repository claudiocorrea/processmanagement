Ext.define('ProcessManagement.view.screen.Dashboard', {
    extend: 'Ext.Panel',
    xtype: 'dashboardScreen',

    require: [
        'ProcessManagement.view.screen.DashboardController'
    ],

    id: 'dashboardScreen',
	
	controller: 'dashboard',
	
	height: Ext.getBody().getViewSize().height - 70,
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },
	
	tbar: {
        padding: '4 10',
        baseCls: 'screenBackground',
        items: [
            {
                xtype: 'label',
                cls: 'titleSection',
                bind: { text: '{txtFields.words.dashboard}' }
            }, '->', {
                xtype: 'button', margin: 5,
                userCls: 'circular',
                iconCls: 'x-fa fa-plus',
                handler: 'addKPITest',
                hidden: true
            }, {
                xtype: 'button', margin: 5,
                userCls: 'circular',
                iconCls: 'x-fa fa-refresh',
                bind: { tooltip: '{txtFields.words.refresh}' },
                handler: 'refreshScreen'
            }
        ]
    },

    items: [{ xtype: 'indicatorPanel', flex: 1} ],

    listeners: {
        afterrender: 'initScreen',
        resize: 'reorganizer'
    }

})