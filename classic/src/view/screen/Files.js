Ext.define('ProcessManagement.view.screen.Files', {
    extend: 'Ext.Panel',
    xtype: 'filesScreen',    

    requires: [
        'Ext.tree.Panel',
        'ProcessManagement.view.panel.FileFilter',
        'ProcessManagement.view.screen.FilesController'
    ],    

    id: 'filesScreen',

    controller: 'files',
	
	height: Ext.getBody().getViewSize().height - 70,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    tbar: {
		padding: '4 10',
		baseCls: 'screenBackground',
        reference: 'formToolbar',		
		plugins: 'responsive',        
        defaults: { xtype:'button',  margin: 5 },
        items: [
            {   xtype: 'label',
                reference: 'filesTitle',
                cls: 'titleSection',
                bind: { text: '{txtFields.messages.searchForDocument}' }
            }, '->', {
                xtype: 'textfield',
                reference: 'searchField',
                width: '30%',
                margin: '0 0 0 10',
                bind: {
                    //value: '',
                    emptyText: '{txtFields.messages.findDocument}'
                },
                listeners: {
                    change: 'findFiles'
                }
            }, {
                xtype: 'button',
                reference: 'btSearch',
                iconCls: 'x-fa fa-search',
                text: 'Compañia',
                menu: [
                    {   
                        id: 'company', bind: {text: '{txtFields.words.company}'},
                        group: 'filter', checked: true,
                        checkHandler: 'changeFilter'
                    }, {
                        id: 'area', bind: { text: 'Area'},
                        group: 'filter', checked: false,
                        checkHandler: 'changeFilter'
                    }, {
                        id: 'docType', bind: {text: '{txtFields.titles.documentType}' },
                        group: 'filter', checked: false,
                        checkHandler: 'changeFilter'
                    }, {
                        id: 'rut', bind: { text: 'RUT'},
                        group: 'filter', checked: false,
                        checkHandler: 'changeFilter'
                    }, {
                        id: 'name', bind: {text: '{txtFields.words.firstName} {txtFields.words.and} {txtFields.words.lastName}'},
                        group: 'filter', checked: false,
                        checkHandler: 'changeFilter'
                    }
                ]
            }, '->', {
                xtype: 'button', margin: 5,
                userCls: 'circular',
                iconCls: 'x-fa fa-filter',
                enableToggle: true, pressed: false, hidden: true,
                bind: { tooltip: '{txtFields.words.filter}' },
                handler: 'viewFilterPanel'
            }, {
                iconCls: 'x-fa fa-refresh', userCls: 'circular',
                bind: { tooltip: '{txtFields.words.refresh}' },
                handler: 'refreshScreen'
            }
        ]
    },

    items: [
        {   xtype: 'fileFilterPanel' },                
        {   xtype: 'panel', flex: 1,
            layout: 'border',
            defaults: {                 
                collapsible: true,
                split: true, header: false,
                bodyPadding: 10 }, 
            items: [
                {   region:'west', bind: { title: '{txtFields.titles.documentaryStructure}' },
                    width: '33%', bodyPadding: '10 0 0 0', 
                    scrollable: true,
                    items: [
                        {   xtype: 'treepanel', 
                            reference: 'filesTree', flex: 1, minHeight: 490,
                            rootVisible: false, useArrows: true, //reserveScrollbar: true,
                            animate: false, 
                            selModel: {
                                allowDeselect: true,
                                listeners: {
                                    selectionchange: function(selModel, selection) {
                                        console.log('Selected!', this)
                                        Ext.getCmp('filesScreen').controller.rowSelected(selModel, selection[0] ? selection[0].raw : undefined)
                                    }
                                }
                            },
                            listeners: {                        
                                itemexpand: function(selModel, selection) {
                                    //console.log('Expanded!', this)
                                    if(selModel.raw) Ext.getCmp('filesScreen').controller.rowExpanded(this, selModel)
                                },
                            }
                        }
                    ]
                }, {   
                    region:'center', reference: 'centerPanel',
                    bind: { title: '{txtFields.words.form}' },
                    width: '33%', scrollable: true, hidden: true,
                    items: [
                        {   xtype: 'formPanel', reference: 'formPanel', 
			                padding: '0 0 0 10'
			            }
                    ]
                }, {
                    region:'east', bind: { title: '{txtFields.words.file}' },
                    width: '33%', reference: 'imagePanel',
                    html: '', hidden: true
                }
            ]
        }
    ],
	
	listeners: {
        afterrender: 'initScreen'
    }
})
