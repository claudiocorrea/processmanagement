Ext.define('ProcessManagement.view.screen.Search', {
    extend: 'Ext.Panel',
    xtype: 'searchScreen',
	
	requires: [ 
		'Ext.button.Segmented',
		'ProcessManagement.grid.Steps',
		'ProcessManagement.view.panel.Filter',
		'ProcessManagement.view.screen.SearchController' 
	],

    id: 'searchScreen',
	
	height: Ext.getBody().getViewSize().height - 65,

    controller: 'search',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    tbar: {
        padding: '4 10',
        baseCls: 'screenBackground',
        items: [
            {
                xtype: 'label',
                cls: 'titleSection',
                bind: { text: '{txtFields.messages.documentManagement}' }
            }, '->', {
                xtype: 'fieldcontainer',
                items: [{
                    xtype: 'segmentedbutton',
                    items: [
                        { bind: { text: '{txtFields.words.today}' }, name: 'hoy' },
                        { bind: { text: '{txtFields.words.week}' }, name: 'semana' },
                        { bind: { text: '{txtFields.words.month}' }, name: 'mes' },
                        { bind: { text: '{txtFields.words.all}' }, name: 'todas', pressed: true }
                    ],
                    listeners: { toggle: 'filterStartDate' }
                }]
            }, '->', {
                xtype: 'textfield',
                reference: 'searchField',
                width: '30%',
                margin: '0 0 0 10',
                bind: {
                    value: '{StepTxt}',
                    emptyText: '{txtFields.messages.findDocument}'
                },
                listeners: {
                    change: 'findRequest'
                }
            }, {
                xtype: 'button',
                reference: 'btSearch',
                iconCls: 'x-fa fa-search',
                bind: { text: '{txtFields.words.title}' },
                menu: [
                    {   id: 'proceso', bind: {text: '{txtFields.words.code}'},
                        group: 'filter', checked: false,
                        checkHandler: 'changeFilter'
                    }, {
                        id: 'Referencia', bind: { text: '{txtFields.words.title}'},
                        group: 'filter', checked: true,
                        checkHandler: 'changeFilter'
                    }, {
                        id: 'WorkFlow', bind: {text: '{txtFields.words.request}'},
                        group: 'filter', checked: false,
                        checkHandler: 'changeFilter'
                    }, {
                        id: 'StepTxt', bind: {text: '{txtFields.words.task}' },
                        group: 'filter', checked: false,
                        checkHandler: 'changeFilter'
                    }
                ]
            }, '->', {
                xtype: 'button', margin: 5,
                userCls: 'circular',
                iconCls: 'x-fa fa-filter',
                enableToggle: true, pressed: true,
                bind: { tooltip: '{txtFields.words.filter}' },
                handler: 'viewSearchPanel'
            }, {
                xtype: 'button', margin: 5,
                userCls: 'circular',
                iconCls: 'x-fa fa-refresh',
                bind: { tooltip: '{txtFields.words.refresh}' },
                handler: 'refreshScreen'
            }
        ]
    },

    items: [
        {   xtype: 'filterPanel' },
        {   xtype: 'gridSteps', reference: 'gridSteps', flex: 2}
    ],
	
	bbar: {
        items: [ '->', { xtype: 'label', reference: 'lblCountRows', margin: '0 20 0 0' } ]
    },

    listeners: {
        afterrender: 'initScreen'
    }
})