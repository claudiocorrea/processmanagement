Ext.define('ProcessManagement.view.screen.Request', {
    extend: 'Ext.Panel',
    xtype: 'requestScreen',

    id: 'requestScreen',

    requires: [
        'ProcessManagement.view.cmpc.screen.DiseñoProyecto',

        'ProcessManagement.view.screen.RequestController'
    ],    

    controller: 'request',
	
	height: Ext.getBody().getViewSize().height - 60,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    tbar: {
		padding: '4 10',
		baseCls: 'screenBackground',
        reference: 'formToolbar',
		
		plugins: 'responsive',
		responsiveConfig: {
			'width >= 900': { height: 50, padding: 5 },
			'width < 900': { height: 75, padding: 10 }
		},        
        defaults: {
            xtype:'button',
            userCls: 'circular',
            margin: 5
        },
        items: [
            {   reference: 'requestReturn',
                iconCls:'x-fa fa-arrow-left',
                bind: { tooltip: '{txtFields.words.back}' },
                handler: 'returnTab'
            }, {
                xtype: 'label',
                reference: 'requestTitle',
                cls: 'titleSection'
            }, '->', {
                iconCls: 'x-fa fa-bars',
                bind: { tooltip: '{txtFields.words.show}',  },
                enableToggle: true, 
                handler: 'visiblePanels'
            }, {
                iconCls: 'x-fa fa-refresh',
                bind: { tooltip: '{txtFields.words.refresh}' },
                handler: 'refreshScreen'
            }
        ]
    },

    items: [
        { xtype: 'formPanel', flex: 2 },
        {
            xtype: 'panel', reference: 'extraPanels',
            layout: 'border', flex: 1,            
            border: true, hidden: true,
			
            plugins: 'responsive',
            responsiveConfig: { 'width < 900': { hidden: true } },
            defaults: { collapsible: true, split: false },
            items: [
                {   region: 'center',
                    bind: { title: '{txtFields.words.files}' },
                    layout: { type: 'fit', align: 'stretch' },
                    tools: [
                        { reference: 'btnNewFile', iconCls: 'x-fa fa-plus', handler: 'newFile' },
                        { reference: 'btnDownloadFile', iconCls: 'x-fa fa-download', hidden: true, handler: 'downloadFile' },
                        { reference: 'btnDeleteFile', iconCls: 'x-fa fa-trash', hidden: true, handler: 'removeFile'  }
                    ],
                    items: [{ xtype: 'gridFiles' }]
                },
                {   region: 'south', flex: 1,
                    bind: { title: '{txtFields.words.comment}s' },                    
                    layout: { type: 'fit', align: 'stretch' },
                    tools: [
                        { reference: 'btnNewComment', iconCls: 'x-fa fa-plus', handler: 'newComment' },
                        { reference: 'btnDeleteComment', iconCls: 'x-fa fa-trash', hidden: true, handler: 'removeComment' }
                    ],
                    items: [{ xtype: 'gridComments' }]
                }
            ]

        }
    ],
	
	listeners: {
        afterrender: 'initScreen'
    }
})
