/**
 * Created by Antonio on 8/31/2017.
 */
Ext.define('ProcessManagement.view.formIntegrado.Main', {
    extend: 'Ext.container.Container',
    xtype: 'formIntegradoMain',

    id: 'formIntegradoMain',

    requires: [
        'Ext.plugin.Viewport'
    ],

    controller: 'formMainController',
    viewModel: 'formMainModel',

    plugins: 'viewport',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        { xtype: 'mainToolbar', hidden: (appParams["hideHeader"] != null && typeof appParams["hideHeader"] != undefined)?true:false},
        { xtype: 'requestScreen', flex: 1 }
    ]

})