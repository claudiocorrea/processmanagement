Ext.define('ProcessManagement.view.cmpc.screen.DiseñoProyecto', {
    extend: 'Ext.Panel',
    xtype: 'cmpc-diseñoProyectoScreen',    

    requires: [
        'ProcessManagement.view.cmpc.screen.DiseñoProyectoController'
    ],
    
    controller: 'cmpcdiseñoproyecto',

    height: Ext.getBody().getViewSize().height - 60,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    bodyPadding: 10,

    tbar: {
        padding: '4 10',
        baseCls: 'screenBackground',
        items: [
            {   xtype: 'label', cls: 'titleSection', margin: '11 0', 
                html: 'Diseño Proyecto'
            }, '->', {
                xtype: 'button',
                iconCls: 'x-fa fa-save', text: 'Guardar',
                handler: ''
            }
        ], hidden: true
    },

    items: [
        {   xtype: 'form', reference: 'form', padding: '0 25',
            defaults: { width: '100%' },           
            items: [ 
                {   xtpye: 'fieldcontainer', layout: 'hbox', margin: '5 0 10',
                    defaults: { xtype: 'combobox', flex: 1, labelWidth: 150,
                        queryMode: 'local', displayField: 'name', valueField: 'id',
                        editable: false, 
                    },
                    items: [ 
                        {   fieldLabel: 'Tipo de Proyecto', reference: 'cbTipoProyecto', margin: '0 10 0 0' },
                        {   fieldLabel: 'Encargado', reference: 'cbEncargado' } 
                    ]
                },
                {   xtpye: 'fieldcontainer', layout: 'hbox', margin: '5 0 10',
                    defaults: { xtype: 'combobox', flex: 1,
                        queryMode: 'local', displayField: 'name', valueField: 'id',
                        editable: false,
                    },
                    items: [ 
                        {   fieldLabel: 'Comuna', reference: 'cbComuna', margin: 0, labelWidth: 150 },
                        {   fieldLabel: 'Periodo', reference: 'cbPeriodoInit', margin: '0 10' },
                        {   fieldLabel: '', reference: 'cbPeriodoFin' },
                    ]
                }
            ]
        }, {
            xtype: 'panel', border: true, 
            flex: 1, bodyPadding: 0,
            layout: { 
                type: 'accordion',
                titleCollapse: false,
                animate: true, multi: true
            }, defaults: { bodyPadding: 5 },
            items: [{
                title: 'Plantas',
                header: { padding: '5 10',
                    items: [{
                        xtype: 'button', ui: 'default-toolbar',
                        iconCls: 'x-fa fa-edit', text: 'Configurar',
                        handler: ''
                    }]
                }, items: [
                    {   xtype: 'grid', reference: 'gridPlantas',
                        columns: [
                            {   xtype: 'templatecolumn', menuDisabled: true, dataIndex: 'name', 
                                flex: 1, tpl: [ '<p/><div class="tplText">{name}</div>' ]
                            }, {   
                                xtype: 'actioncolumn',
                                items: [
                                    {   tooltip: 'Borrar', iconCls: 'x-fa fa-trash' }
                                ],
                                menuDisabled: true, sortable: false,
                            }
                        ]
                    }
                ]
            },{
                title: 'Establecimientos',
                header: { padding: '5 10',
                    items: [{
                        xtype: 'button', ui: 'default-toolbar',
                        iconCls: 'x-fa fa-edit', text: 'Configurar',
                        handler: ''
                    }]
                }, items: [
                    {   xtype: 'grid',  reference: 'gridEstablecimientos',
                        columns: [
                            {   xtype: 'templatecolumn', menuDisabled: true, dataIndex: 'name', 
                                flex: 1, tpl: [ '<p/><div class="tplText">{name}</div>' ]
                            }, {   xtype: 'actioncolumn',
                                items: [
                                    {   tooltip: 'Borrar', iconCls: 'x-fa fa-trash' }
                                ],
                                menuDisabled: true, sortable: false,
                            }
                        ]
                    }
                ]
            },{
                title: 'Profesionales',
                header: { padding: '5 10',
                    items: [{
                        xtype: 'button', ui: 'default-toolbar',
                        iconCls: 'x-fa fa-edit', text: 'Configurar',
                        handler: ''
                    }]
                }, items: [
                    {   xtype: 'grid', reference: 'gridProfesionales',
                        columns: [
                            {   xtype: 'templatecolumn', menuDisabled: true, dataIndex: 'name', 
                                flex: 1, tpl: [ '<p/><div class="tplText">{name}</div>' ]
                            }, {   xtype: 'actioncolumn',
                                items: [
                                    {   tooltip: 'Borrar', iconCls: 'x-fa fa-trash' }
                                ],
                                menuDisabled: true, sortable: false,
                            }
                        ]
                    }
                ]
            }]

        }
    ],

    listeners: { afterrender: 'initScreen' },

    initComponent: function () { this.callParent(); }

})