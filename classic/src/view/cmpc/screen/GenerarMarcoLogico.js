Ext.define('ProcessManagement.view.cmpc.screen.GenerarMarcoLogico', {
    extend: 'Ext.Panel',
    xtype: 'cmpc-generarMarcoLogicoScreen',

    requires: [
        'Ext.tab.Panel',
        'Ext.layout.container.Accordion',
        'ProcessManagement.store.cmpc.TipoProyecto',
        'ProcessManagement.store.cmpc.ObjetivoGeneral',
        'ProcessManagement.view.cmpc.screen.GenerarMarcoLogicoController'
    ],

    alias: 'widget.cmpc-generarMarcoLogicoScreen',
    controller: 'cmpcgenerarMarcoLogico',

    height: Ext.getBody().getViewSize().height - 65,

    layout: {
        type: 'fit',
        align: 'stretch'
    },

    viewModel: {
        data: {
            loggedUser: 'arojas',
            projectId: null,
            mensaje: null
        }
    },

    bodyPadding: 10,

    tbar: {
        padding: '4 10',
        baseCls: 'screenBackground',
        items: [{
            xtype: 'label', cls: 'titleSection',
            html: 'Generar Marco L&oacute;gico', margin: '11 0'
        }, '->', {
            xtype: 'button',
            reference: 'cmdSaveMarcoLogico',
            iconCls: 'x-fa fa-save',
            text: 'Guardar Marco',
            action: 'saveMarcoLogico'
        }
        ], hidden: true
    },

    items: [
        {
            xtype: 'tabpanel', border: true,
            defaults: {  },
            items: [
                {
                    title: 'Objetivo General', bodyPadding: 10,
                    layout: { type: 'vbox', align: 'stretch' },
                    items: [
                        {
                            xtpye: 'fieldcontainer', layout: 'hbox', margin: '5 0 10',
                            defaults: { xtype: 'combobox', flex: 1, labelWidth: 110 },
                            items: [
                                {
                                    fieldLabel: 'Tipo de proyecto', reference: 'cmbTipoProyecto',
                                    displayField: 'nombrepro', valueField: 'ID', queryMode: 'local',
                                    store: { type: 'tipoProyecto' },
                                    margin: 0, allowBlank: false, editable: false,
                                    listeners: { 'change': 'onChangeTipoProyecto' }
                                }, { 
                                    fieldLabel: 'Objetivo general', reference: 'cmbObjGeneral',
                                    displayField: 'nombre', valueField: 'id', queryMode: 'local',
                                    store: { type: 'objetivoGeneral' },
                                    margin: '0 10', allowBlank: false                                
                                }, { 
                                    xtype: 'textfield', reference: 'txtNombreCorto',
                                    fieldLabel: 'Nombre corto' 
                                },
                            ]
                        }, {
                            xtype: 'panel', border: true,
                            flex: 1, bodyPadding: 0,
                            layout: {
                                type: 'accordion',
                                titleCollapse: false,
                                animate: true, multi: true
                            }, defaults: { bodyPadding: 5 },
                            items: [{
                                title: 'Indicadores',
                                header: {
                                    padding: '5 10',
                                    items: [{
                                        xtype: 'button', ui: 'default-toolbar',
                                        reference: 'cmdAddIndicadoresObjetivoGeneral',
                                        iconCls: 'x-fa fa-plus', text: 'Agregar',
                                        handler: 'onAddIndicadoresObjetivoGeneral'
                                    }]
                                }, items: [
                                    {
                                        xtype: 'grid', reference: 'gridIndicadoresObjGral',
                                        store: Ext.create('Ext.data.Store', {
                                            fields: ['oidIndicador', 'oindicador', 'omvId', 'omv', 'ovalor', 'oidLineaBase', 'olineaBase', 'oano1', 'oano2', 'oset', 'osupuestos'],
                                            proxy: { type: 'memory' }
                                        }),
                                        columns: [
                                            { text: 'Indicador', dataIndex: 'oindicador', flex: 1 },
                                            { text: 'L&iacute;nea Base', dataIndex: 'olineaBase', flex: 1 },
                                            { text: 'Meta A&ntilde;o 1', dataIndex: 'oano1', flex: 0.4 },
                                            { text: 'Meta A&ntilde;o 2', dataIndex: 'oano2', flex: 0.4 },
                                            {
                                                xtype: 'actioncolumn', flex: 0.2, stopSelection: true,
                                                items: [{
                                                    tooltip: 'Borrar',
                                                    iconCls: 'x-fa fa-trash',
                                                    //scope: this,
                                                    handler: 'onRemoveIndicadorObjGral'/*function (grid, rowIndex) {
                                                        grid.up('generarMarcoLogico').fireEvent('removeIndicadorObjGral', grid, rowIndex);
                                                    }*/
                                                }
                                                ],
                                                menuDisabled: true, sortable: false,
                                            }
                                        ],
                                        listeners: {
                                            'select': 'onSelectIndicadorGral',
                                            'itemdblclick': 'onEditIndicadorGral'
                                        }
                                    }
                                ]
                            }, {
                                title: 'Medios de Verificaci&oacute;n', collapsed: true,
                                header: {
                                    padding: '5 10',
                                    items: [{
                                        xtype: 'button', ui: 'default-toolbar',
                                        iconCls: 'x-fa fa-plus', text: 'Agregar',
                                        handler: 'onAddMedioVerifIndicadorOG'
                                    }]
                                }, items: [
                                    {
                                        xtype: 'grid', reference: 'gridMediosVerifIndicadorOG',
                                        store: Ext.create('Ext.data.Store', {
                                            fields: ['omv', 'omvId'],
                                            proxy: { type: 'memory' }
                                        }),
                                        columns: [
                                            { text: 'Medio Verificaci&oacute;n', dataIndex: 'omv', flex: 1 },
                                            {
                                                xtype: 'actioncolumn', flex: 0.2,
                                                items: [{
                                                    tooltip: 'Borrar', scope: this,
                                                    iconCls: 'x-fa fa-trash',
                                                    handler: function (grid, rowIndex) {
                                                        grid.up('generarMarcoLogico').fireEvent('removeMedioVerifIndicadorObjGral', grid, rowIndex);
                                                    }
                                                }
                                                ],
                                                menuDisabled: true, sortable: false
                                            }
                                        ],
                                        listeners: {
                                            'itemdblclick': 'onEditMedioVerifIndicadorOG'
                                        }
                                    }
                                ]
                            }
                            ]
                        }
                    ]
                },
                {
                    title: 'Objetivos Espec&iacute;ficos',
                    layout: { type: 'vbox', align: 'stretch' },
                    scrollable: 'vertical',
                    items: [
                        {   xtype: 'panel', border: true,
                            bodyPadding: 0, height: 250,
                            layout: {
                                type: 'accordion',
                                titleCollapse: false,
                                animate: true, multi: true
                            }, defaults: { bodyPadding: 5 },
                            items: [
                                {
                                    title: 'Objetivos',
                                    header: {
                                        padding: '5 10',
                                        items: [{
                                            xtype: 'button', ui: 'default-toolbar',
                                            iconCls: 'x-fa fa-plus', text: 'Agregar',
                                            handler: 'onAddComponenteObjEspecifico'
                                        }]
                                    }, items: [
                                        {
                                            xtype: 'grid', reference: 'gridObjEspecificos',
                                            store: Ext.create('Ext.data.Store', {
                                                fields: ['idComponente', 'componente', 'idObjetivo', 'nombreObjetivo', 'nombreCorto', 'descripcionObjetivo', 'especificos', 'estrategia', 'rsupuestos', 'isFromDb'],
                                                proxy: { type: 'memory' }
                                            }),
                                            columns: [
                                                { text: 'Componente', dataIndex: 'nombreCorto', flex: 1 },
                                                { text: 'Objetivo Espec&iacute;fico', dataIndex: 'nombreObjetivo', flex: 1 },
                                                {
                                                    xtype: 'actioncolumn', flex: 0.2,
                                                    menuDisabled: true, sortable: false,
                                                    items: [
                                                        {
                                                            tooltip: 'Borrar',
                                                            iconCls: 'x-fa fa-trash',
                                                            handler: 'onRemoveObjetivoEspecifico'
                                                        }
                                                    ]
                                                }
                                            ],
                                            listeners: {
                                                'select': 'onSelectGridObjEspecificos',
                                                'itemdblclick': 'onEditComponenteObjEspecificos'
                                            }
                                        }
                                    ]
                                }, {
                                    title: 'Supuestos', collapsed: true,
                                    header: {
                                        padding: '5 10',
                                        items: [{
                                            xtype: 'button', ui: 'default-toolbar',
                                            iconCls: 'x-fa fa-plus', text: 'Agregar',
                                            handler: 'onAddSupuestoIndicadorObjEspecifico'
                                        }]
                                    }, items: [
                                        {
                                            xtype: 'grid', reference: 'gridSupuestosIndicadoresObjEspecificos',
                                            store: Ext.create('Ext.data.Store', {
                                                fields: ['descripcion'],
                                                proxy: { type: 'memory' }
                                            }),
                                            columns: [
                                                { text: 'Supuesto', dataIndex: 'descripcion', flex: 1 },
                                                {
                                                    xtype: 'actioncolumn', flex: 0.1,
                                                    items: [{
                                                        tooltip: 'Borrar',
                                                        iconCls: 'x-fa fa-trash',
                                                        handler: 'onRemoveSupuestoIndicadorObjetivoEspecifico'
                                                    }
                                                    ],
                                                    menuDisabled: true, sortable: false
                                                }
                                            ],
                                            listeners: {
                                                'itemdblclick': 'onEditSupuestosIndicadoresObjEspecificos'
                                            }
                                        }
                                    ]
                                }
                            ]
                        }, {   
                            xtype: 'tabpanel', border: true,
                            items: [
                                    {   title: 'Indicadores',
                                        layout: { type: 'vbox', align: 'stretch' },
                                        items: [
                                            {   xtype: 'panel', border: true, 
                                                bodyPadding: 0, height: 250,
                                                layout: { 
                                                    type: 'accordion',
                                                    titleCollapse: false,
                                                    animate: true, multi: true
                                                }, defaults: { bodyPadding: 5 },
                                                items: [{
                                                    title: 'Indicadores',
                                                    header: { padding: '5 10',
                                                        items: [{
                                                            xtype: 'button', ui: 'default-toolbar',
                                                            reference: 'cmdAddIndicadoresObjetivoGeneral',
                                                            iconCls: 'x-fa fa-plus', text: 'Agregar',
                                                            handler: 'addIndicadoresObjetivoGeneral'
                                                        }]
                                                    }, items: [
                                                        {   xtype: 'grid', reference: 'gridIndicadoresObjEspecificos',
                                                            store: Ext.create('Ext.data.Store', {
                                                                fields: ['ano1', 'ano2', 'kindicadorId', 'kindicador', 'idLineaBase', 'lineaBase', 'mvId', 'mv', 'rsupuestos', 'valor'],
                                                                proxy: { type: 'memory' }
                                                            }),                                        
                                                            columns: [
                                                                { text: 'Indicador', dataIndex: 'kindicador', flex: 1 },
                                                                { text: 'L&iacute;nea Base', dataIndex: 'lineaBase', flex: 1 },
                                                                { text: 'Meta A&ntilde;o 1', dataIndex: 'ano1', flex: 0.4 },
                                                                { text: 'Meta A&ntilde;o 2', dataIndex: 'ano2', flex: 0.4 },
                                                                { xtype: 'actioncolumn', flex: 0.2, stopSelection: true,
                                                                items: [{
                                                                            tooltip: 'Borrar',
                                                                            iconCls: 'x-fa fa-trash',
                                                                            scope: this,
                                                                            handler: function (grid, rowIndex) {
                                                                                grid.up('generarMarcoLogico').fireEvent('removeIndicadorObjetivoEspecifico', grid, rowIndex);
                                                                            }
                                                                        }
                                                                    ],
                                                                    menuDisabled: true, sortable: false,
                                                                }
                                                            ],
                                                            listeners: {
                                                                'select': 'onSelectGridIndicadoresObjEspecificos',
                                                                'itemdblclick': 'onEditIndicadorGral'
                                                            }
                                                        }
                                                    ]
                                                },{
                                                    title: 'Medios de Verificaci&oacute;n', collapsed: true,
                                                    header: { padding: '5 10',
                                                        items: [{
                                                            xtype: 'button', ui: 'default-toolbar',
                                                            iconCls: 'x-fa fa-plus', text: 'Agregar',
                                                            handler: 'addMedioVerifIndicadorOG'
                                                        }]
                                                    }, items: [
                                                        {   xtype: 'grid',
                                                            reference: 'gridMediosVerifIndicadoresObjEspecificos',
                                                            store: Ext.create('Ext.data.Store', {
                                                                fields: ['mv', 'mvId'],
                                                                proxy: { type: 'memory' }
                                                            }),
                                                            columns: [
                                                                {   text: 'Medio Verificaci&oacute;n', dataIndex: 'mv', flex: 1 },
                                                                {   xtype: 'actioncolumn', flex: 0.2,
                                                                    items: [ {
                                                                            tooltip: 'Borrar', scope: this,
                                                                            iconCls: 'x-fa fa-trash',                                                            
                                                                            handler: function (grid, rowIndex) {
                                                                                grid.up('generarMarcoLogico').fireEvent('removeMedioVerifIndicadorObjetivoEspecifico', grid, rowIndex);
                                                                            }
                                                                        }
                                                                    ],
                                                                    menuDisabled: true, sortable: false
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }, {
                                    title: 'Actividades',
                                    layout: { type: 'vbox', align: 'stretch' },
                                    items: [
                                        {   xtype: 'panel', border: true, 
                                            bodyPadding: 0, height: 400,
                                            layout: { 
                                                type: 'accordion',
                                                titleCollapse: false,
                                                animate: true, multi: true
                                            }, defaults: { bodyPadding: 5 },
                                            items: [
                                                {   title: 'Actividades',
                                                    header: { padding: '5 10',
                                                        items: [{
                                                            xtype: 'button', ui: 'default-toolbar',
                                                            iconCls: 'x-fa fa-plus', text: 'Agregar',
                                                            handler: 'addActivididadObjEspecifico'
                                                        }]
                                                    }, items: [
                                                        {   xtype: 'grid', reference: 'gridActividadesObjEspecificos',
                                                            store: Ext.create('Ext.data.Store', {
                                                                idProperty: 'idArea',
                                                                fields: ['idArea', 'area', 'idCategoria', 'categoria', 'ciclo', 'contenidos', 'idNivel', 'nivel', 'idTipoActividad', 'tipoActividad', 'tipoParticipante', 'canEdit', 'planificada', 'calendarizada'],
                                                                proxy: { type: 'memory' }
                                                            }), columns: [
                                                                { text: 'Tipo actividad', dataIndex: 'tipoActividad', flex: 1 },
                                                                { text: 'Categoria', dataIndex: 'categoria', flex: 1 },
                                                                { text: 'Area', dataIndex: 'area', flex: 1 },
                                                                { text: 'Nivel', dataIndex: 'nivel', flex: 1 },
                                                                { xtype: 'actioncolumn', flex: 0.1, stopSelection: true,
                                                                items: [{
                                                                            tooltip: 'Borrar', scope: this,
                                                                            iconCls: 'x-fa fa-trash',                                                                                
                                                                            handler: function (grid, rowIndex) {
                                                                                grid.up('generarMarcoLogico').fireEvent('removeActividadObjetivoEspecifico', grid, rowIndex);
                                                                            }
                                                                        }, {
                                                                            tooltip: 'Clonar', scope: this,
                                                                            icon: 'resources/icons/mail/clone.png',
                                                                            handler: function (grid, rowIndex) {
                                                                                grid.up('generarMarcoLogico').fireEvent('cloneActividadObjetivoEspecifico', grid, rowIndex);
                                                                            }
                                                                        }
                                                                    ],
                                                                    menuDisabled: true, sortable: false,
                                                                }
                                                            ], listeners: {
                                                                'select': 'onSelectGridActividadesObjEspecificos',
                                                                'itemdblclick': 'onEditActividadObjEspecifico'
                                                            }
                                                        }
                                                    ]
                                                }, {
                                                    title: 'Contenidos', collapsed: true,
                                                    header: { padding: '5 10',
                                                        items: [{
                                                            xtype: 'button', ui: 'default-toolbar',
                                                            iconCls: 'x-fa fa-plus', text: 'Agregar',
                                                            handler: 'addContenidoActividadObjEspecifico'
                                                        }]
                                                    }, items: [
                                                        {   xtype: 'grid', reference: 'gridContenidosActividadesObjEspecifico',
                                                            store: Ext.create('Ext.data.Store', {
                                                                fields: ['descripcion', 'canEdit'],
                                                                proxy: { type: 'memory' }
                                                            }),                                        
                                                            columns: [
                                                                { text: 'Objetivo', dataIndex: 'descripcion', flex: 1 },
                                                                { xtype: 'actioncolumn', flex: 0.1, stopSelection: true,
                                                                items: [{
                                                                            tooltip: 'Borrar',
                                                                            iconCls: 'x-fa fa-trash',
                                                                            scope: this,
                                                                            handler: function (grid, rowIndex) {
                                                                                grid.up('generarMarcoLogico').fireEvent('removeContenidoActividadObjetivoEspecifico', grid, rowIndex);
                                                                            }
                                                                        }
                                                                    ],
                                                                    menuDisabled: true, sortable: false,
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }, {
                                                    title: 'Tipos de Participantes', collapsed: true,                                                   
                                                    header: { padding: '5 10',
                                                        items: [{
                                                            xtype: 'button', ui: 'default-toolbar',
                                                            iconCls: 'x-fa fa-plus', text: 'Agregar',
                                                            handler: 'addTipoParticipanteActividadObjEspecifico'
                                                        }]
                                                    }, items: [
                                                        {   xtype: 'grid', reference: 'gridTipoParticipantesActividadObjEspecifico',
                                                            store: Ext.create('Ext.data.Store', {
                                                                fields: ['id', 'descripcion', 'canEdit'],
                                                                proxy: { type: 'memory' }
                                                            }),                                        
                                                            columns: [
                                                                { text: 'Tipo Participante', dataIndex: 'descripcion', flex: 1 },
                                                                { xtype: 'actioncolumn', flex: 0.1, stopSelection: true,
                                                                items: [{
                                                                            tooltip: 'Borrar',
                                                                            iconCls: 'x-fa fa-trash',
                                                                            scope: this,
                                                                            handler: function (grid, rowIndex) {
                                                                                grid.up('generarMarcoLogico').fireEvent('removeTipoParticipanteActividadObjetivoEspecifico', grid, rowIndex);
                                                                            }
                                                                        }
                                                                    ],
                                                                    menuDisabled: true, sortable: false,
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }, {
                                                    title: 'Ciclos', collapsed: true,                                                   
                                                    header: { padding: '5 10',
                                                        items: [{
                                                            xtype: 'button', ui: 'default-toolbar',
                                                            iconCls: 'x-fa fa-plus', text: 'Agregar',
                                                            handler: 'addCicloActividadObjEspecifico'
                                                        }]
                                                    }, items: [
                                                        {   xtype: 'grid', reference: 'gridCiclosActividadesObjEspecifico',
                                                            store: Ext.create('Ext.data.Store', {
                                                                fields: ['cantidadHoras', 'idperiodo', 'periodo', 'sesiones', 'canEdit'],
                                                                proxy: { type: 'memory' }
                                                            }),                                        
                                                            columns: [
                                                                { text: 'Per&iacute;odo', dataIndex: 'periodo', flex: 1 },
                                                                { text: 'Sesiones', dataIndex: 'sesiones', flex: 0.3 },
                                                                { text: 'Cantidad de Horas', dataIndex: 'cantidadHoras', flex: 0.4 },
                                                                { xtype: 'actioncolumn', flex: 0.1, stopSelection: true,
                                                                items: [{
                                                                            tooltip: 'Borrar',
                                                                            iconCls: 'x-fa fa-trash',
                                                                            scope: this,
                                                                            handler: function (grid, rowIndex) {
                                                                                grid.up('generarMarcoLogico').fireEvent('removeCicloActividadObjetivoEspecifico', grid, rowIndex);
                                                                            }
                                                                        }
                                                                    ],
                                                                    menuDisabled: true, sortable: false,
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        }

                                    ]
                                }
                            ]
                        }
                    ]
                }

            ]
        }
    ]

})
