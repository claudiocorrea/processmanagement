/**
 * Created by Antonio on 1/21/2016.
 */
Ext.define('ProcessManagement.view.cmpc.screen.AltaParticipantesActividadObjEspecifico', {
    extend: 'Ext.window.Window',
    alias: 'widget.altaParticipantesActividadObjEspecifico',

    controller: 'altaParticipantesActividadObjEspecificoController',

    title: 'Guardar Participante',

    modal: true,
    width: 500,

    layout: {
        type: 'fit',
        align: 'stretch'
    },

    viewModel: {},

    items: [
        {
            xtype: 'form',
            reference: 'form',
            layout: {
                type: 'fit',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'combobox',
                    reference: 'cmbTipoParticipante',
                    fieldLabel: 'Tipo Participante',
                    labelWidth: 120,
                    store: { type: 'tiposParticipantes' },
                    displayField: 'nombre',
                    valueField: 'id',
                    editable: false,
                    queryMode: 'local',
                    allowBlank: false
                }
            ]
        }
    ],

    buttons: [
        {
            text: 'Guardar',
            iconCls: 'add',
            handler: 'onSave'
        },
        {
            text: 'Cerrar',
            iconCls: 'cancel',
            handler: 'onClose'
        }
    ]
});