Ext.define('ProcessManagement.view.cmpc.screen.DashboardCMPC', {
    extend: 'Ext.Panel',
    xtype: 'dashboardCMPCScreen',

    requires: [ 
        'Ext.chart.*',
        //'Ext.chart.CartesianChart',        
        'ProcessManagement.grid.cmpc.Sesiones',
        'ProcessManagement.view.cmpc.screen.DashboardCMPCController'
    ],

    id: 'cmpc-dashboardScreen',
	controller: 'cmpcdashboard',
	
	height: Ext.getBody().getViewSize().height - 70,
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },

    bodyPadding: '10 15',
    scrollable: 'vertical',
	
	tbar: {
        padding: '4 10',
        baseCls: 'screenBackground',
        items: [
            {
                xtype: 'label',
                cls: 'titleSection', margin: '11 0', 
                bind: { text: '{txtFields.words.dashboard}' }
            }
        ]
    },

    items: [        
        {   xtype: 'container', layout: 'hbox',            
            items: [{
                xtype: 'cartesian', //border: true,
                width: 400, height: 250, padding: 5,
                title: 'Grado de cumplimiento mensual',
                //captions: { title: 'Grado de cumplimiento mensual', },        
                interactions: {
                    type: 'panzoom',
                    zoomOnPanGesture: true
                },
                animation: { duration: 200 },
                store: {
                    fields: ['month', 'households', 'total'],
                    data: [
                        {   month: 'Enero', point: 0, point1: 0 },
                        {   month: 'Febrero', point: 0, point1: 0 },
                        {   month: 'Marzo', point: 110, point1: 70 },
                        {   month: 'Abril', point: 80, point1: 80 },
                        {   month: 'Mayo', point: 78, point1: 73 },
                        {   month: 'Junio', point: 73, point1: 75 }
                    ]
                },
                innerPadding: { left: 40, right: 40 },
                axes: [{
                    type: 'numeric',
                    position: 'left',
                    grid: true
                }, {
                    type: 'category',
                    position: 'bottom',
                    grid: true
                }],
                series: [{
                    type: 'line',
                    xField: 'month',
                    yField: 'point',
                    style: { lineWidth: 2 },
                    marker: { radius: 4, lineWidth: 2 },
                    //label: { field: 'point', display: 'over' },
                    highlight: {
                        fillStyle: '#000',
                        radius: 5,
                        lineWidth: 2,
                        strokeStyle: '#fff'
                    },
                    tooltip: {
                        trackMouse: true,
                        showDelay: 0,
                        dismissDelay: 0,
                        hideDelay: 0,
                        renderer: function (tooltip, record, item) {
                            tooltip.setHtml(record.get('month') + ': ' + record.get('point'));
                        }
                    }
                }, {
                    type: 'line',
                    xField: 'month',
                    yField: 'point1',
                    style: { lineWidth: 2 },
                    marker: { radius: 4, lineWidth: 2 },
                    //label: { field: 'point', display: 'over' },
                    highlight: {
                        fillStyle: '#000',
                        radius: 5,
                        lineWidth: 2,
                        strokeStyle: '#fff'
                    },
                    tooltip: {
                        trackMouse: true,
                        showDelay: 0,
                        dismissDelay: 0,
                        hideDelay: 0,
                        renderer: function (tooltip, record, item) {
                            tooltip.setHtml(record.get('month') + ': ' + record.get('point1'));
                        }
                    }
                }], 
                setSeriesLineWidth: function (item, lineWidth) {
                    if (item) {
                        item.series.setStyle({ lineWidth: lineWidth });
                    }
                },
                listeners: {
                    itemhighlight: function (chart, newHighlightItem, oldHighlightItem) {
                        chart.setSeriesLineWidth(newHighlightItem, 4);
                        chart.setSeriesLineWidth(oldHighlightItem, 2);
                    }
                }
            }, {
                xtype: 'cartesian', 
                flex: 1, height: 250, padding: 5,
                title: 'Grado de cumplimiento acumulado',     
                interactions: {
                    type: 'panzoom',
                    zoomOnPanGesture: true
                },
                animation: { duration: 200 },
                store: {
                    fields: ['month', 'households', 'total'],
                    data: [
                        {   month: 'Enero', point: 0, point1: 0 },
                        {   month: 'Febrero', point: 0, point1: 0 },
                        {   month: 'Marzo', point: 116.5, point1: 69.5 },
                        {   month: 'Abril', point: 196.5, point1: 149.5 },
                        {   month: 'Mayo', point: 276, point1: 223 },
                        {   month: 'Junio', point: 354, point1: 296.5 }
                    ]
                },
                innerPadding: { top: 10, left: 40, right: 40 },
                axes: [{
                    type: 'numeric',
                    position: 'left',
                    grid: true
                }, {
                    type: 'category',
                    position: 'bottom',
                    grid: true
                }],
                series: [{
                    type: 'line',
                    xField: 'month',
                    yField: 'point',
                    style: { lineWidth: 2 },
                    marker: { radius: 4, lineWidth: 2 },
                    label: { field: 'point', display: 'over' },
                    highlight: {
                        fillStyle: '#000',
                        radius: 5,
                        lineWidth: 2,
                        strokeStyle: '#fff'
                    },
                    tooltip: {
                        trackMouse: true,
                        showDelay: 0,
                        dismissDelay: 0,
                        hideDelay: 0,
                        renderer: function (tooltip, record, item) {
                            tooltip.setHtml(record.get('month') + ': ' + record.get('point'));
                        }
                    }
                }, {
                    type: 'line',
                    xField: 'month',
                    yField: 'point1',
                    style: { lineWidth: 2 },
                    marker: { radius: 4, lineWidth: 2 },
                    label: { field: 'point1', display: 'under' },
                    highlight: {
                        fillStyle: '#000',
                        radius: 5,
                        lineWidth: 2,
                        strokeStyle: '#fff'
                    },
                    tooltip: {
                        trackMouse: true,
                        showDelay: 0,
                        dismissDelay: 0,
                        hideDelay: 0,
                        renderer: function (tooltip, record, item) {
                            tooltip.setHtml(record.get('month') + ': ' + record.get('point1'));
                        }
                    }
                }], 
                setSeriesLineWidth: function (item, lineWidth) {
                    if (item) {
                        item.series.setStyle({ lineWidth: lineWidth });
                    }
                },
                listeners: {
                    itemhighlight: function (chart, newHighlightItem, oldHighlightItem) {
                        chart.setSeriesLineWidth(newHighlightItem, 4);
                        chart.setSeriesLineWidth(oldHighlightItem, 2);
                    }
                }
            }]
        },
        {   xtype: 'gridSesiones', collapsed: true, 
            title: 'Sesiones Cerradas (Todas las sesiones ejecutadas y cerradas)',
            store: {
                type: 'cmpckpis',
                filters: [{ property: 'type', value: 'session' }]
            } 
        },
        {   xtype: 'gridSesiones', header: false,
            store: {
                type: 'cmpckpis',
                filters: [{ property: 'type', value: 'total1' }]
            }, listeners: { afterrender: function(obj) {                
                obj.columns[0].hidden = true
                obj.columns[1].hidden = true
                obj.margin = '5 5 5 405'
            } }  
        },
        {   xtype: 'gridSesiones', collapsed: true,
            title: 'Sesiones calendarizadas (Todas las sesiones calendarizadas)', 
            store: {
                type: 'cmpckpis',
                filters: [{ property: 'type', value: 'calendar' }]
            }
        },
        {   xtype: 'gridSesiones', header: false,
            store: {
                type: 'cmpckpis',
                filters: [{ property: 'type', value: 'total2' }]
            }, listeners: { afterrender: function(obj) {                
                obj.columns[0].hidden = true
                obj.columns[1].hidden = true
                obj.margin = '5 5 5 405'
            } } 
        }
    ],

    listeners: { afterrender: 'initScreen' }

})