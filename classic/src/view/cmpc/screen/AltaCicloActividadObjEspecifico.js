/**
 * Created by Antonio on 1/21/2016.
 */
Ext.define('ProcessManagement.view.cmpc.screen.AltaCicloActividadObjEspecifico', {
    extend: 'Ext.window.Window',
    alias: 'widget.altaCicloActividadObjEspecifico',

    controller: 'altaCicloActividadObjEspecificoController',

    title: 'Guardar Ciclo',

    modal: true,
    width: 500,

    layout: {
        type: 'fit',
        align: 'stretch'
    },

    viewModel: {},

    items: [
        {
            xtype: 'form',
            reference: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'combobox',
                    fieldLabel: 'Per&iacute;odo',
                    reference: 'cmbPeriodo',
                    editable: false,
                    displayField: 'nombre',
                    valueField: 'id',
                    store: { type: 'periodo' },
                    queryMode: 'local',
                    allowBlank: false
                },
                {
                    xtype: 'numberfield',
                    fieldLabel: 'Sesiones',
                    reference: 'txtSesiones',
                    allowBlank: false,
                    maxValue: 99,
                    minValue: 1,
                    decimalPrecision: 0
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Cantidad Horas',
                    reference: 'txtCantHoras',
                    allowBlank: false,
                    maxValue: 99,
                    minValue: 1,
                    decimalPrecision: 0,
                    regex: /^\d+(\,\d{1,2})?$/,
                    regexText: 'Debe ingresar un valor con entero o con 2 decimales'
                }
            ]
        }
    ],

    buttons: [
        {
            text: 'Guardar',
            iconCls: 'add',
            handler: 'onSave'
        },
        {
            text: 'Cerrar',
            iconCls: 'cancel',
            handler: 'onClose'
        }
    ]
});