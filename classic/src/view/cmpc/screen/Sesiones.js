Ext.define('ProcessManagement.view.cmpc.screen.Sesiones', {
    extend: 'Ext.Panel',
    xtype: 'cmpc-sesionesScreen',    

    requires: [
        'Ext.form.FieldSet',
        'ProcessManagement.store.cmpc.Sesiones',
        'ProcessManagement.view.cmpc.screen.SesionesController'
    ],

    controller: 'cmpcSesiones',

    height: Ext.getBody().getViewSize().height - 60,

    layout: {
        type: 'fit',
        align: 'stretch'
    },

    storePonderacion: null,
    storePonderacion2: null,
    storeSiNo: null,

    viewModel: {},

    bodyPadding: 10,

    tbar: {
        padding: '4 10',
        baseCls: 'screenBackground',
        items: [
            {   xtype: 'label', cls: 'titleSection', margin: '11 0', 
                html: 'Fundaci&oacute;n CMPC'
            }, '->', {
                xtype: 'button', margin: 5,
                userCls: 'circular',
                iconCls: 'x-fa fa-refresh',
                bind: { tooltip: '{txtFields.words.refresh}' },
                handler: '', hidden: true
            }
        ], hidden: true
    },

    items: [
        {   xtype: 'panel', flex: 1,
            layout: { type: 'border' },
            deafults: { layout: { type: 'vbox', align: 'stretch' }, flex: 1, }, 
            items: [
                {   region: 'west', width: '50%', 
                    split: true, collapsible: true,                    
                    title: 'Proyecto', 
                    tools: [
                        { iconCls: 'x-fa fa-refresh' },
                        { iconCls: 'x-fa fa-exchange', handler: 'changeTypeScreen' }
                    ], scrollable: 'vertical',
                    items: [{
                            xtype: 'form', reference: 'searchForm',                               
                            title: 'B&uacute;squeda',
                            collapsible: true, hidden: true,
                            layout: { type: 'vbox', align: 'stretch' },
                            defaults: { padding: '0 0 0 15' },
                            items: [ 
                                {   xtype: 'combobox', fieldLabel: 'Tipo de Actividad' },
                                {   xtype: 'combobox', fieldLabel: 'Componente' }
                            ],
                            buttons: [
                                {   text: 'Buscar', iconCls: 'x-fa fa-search', handler: 'buscar' },
                                {   text: 'Limpiar', iconCls: 'x-fa fa-trash', action: 'limpiar' }
                            ]
                        }, {
                            xtype: 'gridpanel', reference: 'gridpanel', flex: 1,                            
                            //store: Ext.create('ProcessManagement.store.cmpc.Sesiones'), 
                            store: { type: 'sesiones' },                                                         
                            /*tbar: [
                                {   xtype: 'combobox', itemId: 'cmbProyectoByComuna', 
                                    queryMode: 'local', fieldLabel: 'Proyecto', flex: 1, 
                                    displayField: 'nombreModelo', valueField: 'idProyecto', 
                                    editable: false
                                }, '->', {
                                    xtype: 'button', margin: 5,
                                    iconCls: 'x-fa fa-refresh', userCls: 'circular',                                    
                                    bind: { tooltip: '{txtFields.words.refresh}' },
                                    handler: 'refresh'
                                }
                            ],*/
                            columns: [
                                {   dataIndex: 'idSESI', hidden: true,
                                }, {
                                    text: 'Tipo de Actividad',
                                    flex: 1, sortable: true,
                                    dataIndex: 'titulo'
                                }, {
                                    text: 'Componente',
                                    flex: 1, sortable: true,
                                    dataIndex: 'componente'
                                }, {
                                    text: 'Fecha Inicio',
                                    flex: 0.50, sortable: true,
                                    xtype: 'datecolumn', format: 'd/m/Y',
                                    dataIndex: 'FEINP'
                                }, {
                                    text: 'Estado',
                                    flex: 0.5, sortable: true,
                                    dataIndex: 'status'
                                }
                            ],
                            listeners: { 'selectionchange': 'onGridSelectionchange' },
                        }
                    ]
                }, {
                    region: 'center', scrollable: 'vertical',
                    items: [
                        {   xtype: 'fieldset', title:'Detalles', 
                            padding: 0, margin: 0,
                            items: [{
                                xtype: 'fieldset',
                                layout: { type: 'vbox', align: 'stretch' }, 
                                style: 'border: none !important;',
                                defaultType: 'textfield',
                                items: [
                                    {   xtype: 'hidden', reference: 'idSESI' },
                                    {   xtype: 'hidden', reference: 'SESI' },
                                    {   xtype: 'hidden', reference: 'urlCalendario' },
                                    {
                                        xtype: 'hiddenfield', flex: 1,
                                        fieldLabel: 'Tipo de Actividad',
                                        labelWidth: 120, readOnly: true,
                                        reference: 'idTIAC'
                                    }, {
                                        fieldLabel: 'Tipo de Actividad',
                                        labelWidth: 120, readOnly: true,
                                        reference: 'titulo', flex: 1
                                    }, {
                                        xtype: 'fieldset',
                                        layout: { type: 'vbox', align: 'stretch' }, border: true,
                                        title: 'Fecha Planificada/Real', margin: 0,
                                        items: [
                                            {   xtype: 'datefield', 
                                                fieldLabel: 'Fecha',
                                                reference: 'fechaRealPlanificada'
                                            }, {
                                                xtype: 'fieldcontainer',
                                                flex: 1,
                                                layout: { type: 'hbox', align: 'stretch' },
                                                defaults: { padding: '0 5 4 0', labelWidth: 100, 
                                                    xtype: 'textfield', 
                                                    regex: /^\s*([01]?\d|2[0-3]):([0-5]\d)\s*$/,
                                                    regexText: 'El formato de la hora puede ser #:## o ##:##' 
                                                }, items: [
                                                    {   fieldLabel: 'Hora Inicio',
                                                        reference: 'horaInicioRealPlanificada'                     
                                                    }, {
                                                        fieldLabel: 'Hora Fin',
                                                        reference: 'horaFinRealPlanificada'
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }]
                        }, {
                            xtype: 'panel',
                            reference: 'panelParticipantesEvaluaciones',
                            layout: { type: 'vbox', align: 'stretch' },
                            items: [
                                {   xtype: 'fieldset',
                                    reference: 'fsCantidadParticipantes',
                                    padding: 0, border: false, hidden: true,
                                    items: [
                                        {
                                            xtype: 'numberfield',
                                            reference: 'cantParticipantes',
                                            fieldLabel: 'Cantidad',
                                            minValue: 0, maxValue: 99,
                                            padding: '3 0 0 0', allowBlank: true
                                        }
                                    ]
                                }, {
                                    xtype: 'fieldset',
                                    reference: 'fsObservacionesGrales',
                                    padding: 0, border: false, hidden: true,
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            reference: 'observacionesGenerales',
                                            fieldLabel: 'Observaciones',
                                            padding: '3 0 0 0',
                                            enforceMaxLength: true,
                                            maxLength: 300, allowBlank: true
                                        }
                                    ]
                                }, {
                                    xtype: 'gridpanel',
                                    reference: 'gridParticipantes',
                                    flex: 1, scrollable: 'vertical', 
                                    collapsible: true, border: true,
                                    title: 'Participantes', 
                                    tools: [
                                        {   reference: 'cmdEditEvaluaciones',
                                            text: 'Evaluaciones', iconCls: 'x-fa fa-edit',                                            
                                            handler: 'editEvaluaciones'
                                        }, {
                                            reference: 'cmdAddParticipante',
                                            text: 'Agregar', iconCls: 'x-fa fa-plus',                                            
                                            handler: 'addParticipante'
                                        }, {
                                            reference: 'cmdUpdate',
                                            text: 'Guardar', iconCls: 'x-fa fa-save',                                            
                                            handler: 'update'
                                        }
                                    ],
                                    store: Ext.create('Ext.data.Store', {
                                        model: Ext.create('ProcessManagement.model.cmpc.ParticipantesSesion')
                                    }),
                                    selType: 'rowmodel',
                                    plugins: [
                                        Ext.create('Ext.grid.plugin.CellEditing', { clicksToEdit: 1 })
                                    ],
                                    columns: [
                                        {   text: 'Nombre', dataIndex: 'fullName', flex: 1 },
                                        {   text: 'Perfil', dataIndex: 'perfilCompleto', flex: 1 },
                                        {   text: 'Establecimiento', dataIndex: 'nombreEstablecimiento', flex: 1},
                                        {   text: 'Asistencia', dataIndex: 'asistencia', flex: 1,
                                            getEditor: function (record, defaultField) {
                                                this.setEditor({
                                                        xtype: 'combobox',
                                                        displayField: 'nombre',
                                                        valueField: 'id',
                                                        editable: false,
                                                        queryMode: 'local'
                                                    });
                                                return Ext.create('Ext.form.field.ComboBox', {
                                                        xtype: 'combobox',
                                                        displayField: 'nombre',
                                                        valueField: 'id',
                                                        editable: false,
                                                        queryMode: 'local'
                                                    });
                                            }, renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                                                    if (record.get('asistencia') == null || record.get('asistencia') == "") {
                                                        return value;
                                                    } else {
                                                        var storeP = this.up('form-grid').storeSiNo;
                                                        if (value != null && value != "") {
                                                            var rec = storeP.getById(value);
                                                            if (rec.get('id') == '1') record.set('observacionAsistencia', null);
                                                            return rec.get('nombre');
                                                        }
                                                    }
                                                    return value;
                                                }
                                        }, {    text: 'Tiempo Asistencia', dataIndex: 'tiempoAsistencia', flex: 1,
                                                    getEditor: function (record, defaultField) {
                                                        this.setEditor({
                                                            xtype: 'numberfield',
                                                            maxValue: 100,
                                                            minValue: 0,
                                                            allowDecimals: true,
                                                            decimalPrecision: 2
                                                        });
                                                        return Ext.create('Ext.form.field.Number', {
                                                            xtype: 'numberfield',
                                                            maxValue: 100,
                                                            minValue: 0,
                                                            allowDecimals: true,
                                                            decimalPrecision: 2
                                                        });
                                                    }
                                        }, {    
                                            text: 'Observaci&oacute;n', dataIndex: 'observacionAsistencia', flex: 1,
                                            getEditor: function (record, defaultField) {
                                                this.setEditor({
                                                    xtype: 'textfield'
                                                });
                                                return Ext.create('Ext.form.field.Text');
                                            }
                                        }, {
                                            text: 'Logro', dataIndex: 'logro', flex: 1,
                                            getEditor: function (record, defaultField) {
                                                this.setEditor({
                                                    xtype: 'numberfield',
                                                    maxValue: 100,
                                                    minValue: 0,
                                                    allowDecimals: true,
                                                    decimalPrecision: 2
                                                });
                                                return Ext.create('Ext.form.field.Number', {
                                                    xtype: 'numberfield',
                                                    maxValue: 100,
                                                    minValue: 0,
                                                    allowDecimals: true,
                                                    decimalPrecision: 2
                                                });
                                            }
                                        }, { text: 'Cursos', dataIndex: 'cursosConcat', flex: 0.5 }, 
                                        {
                                            xtype: 'actioncolumn',
                                            flex: 0.1,
                                            menuDisabled: true,
                                            sortable: false,
                                            align: 'right',
                                            items: [
                                                {
                                                    tooltip: 'Borrar',
                                                    icon: 'resources/icons/delete.png',
                                                    scope: this,
                                                    handler: function (grid, rowIndex) {
                                                        grid.up('form-grid').fireEvent('removeParticipante', grid, rowIndex);
                                                    }
                                                }
                                            ]
                                        }
                                    ]/*, bbar: [
                                        { xtype: 'tbfill' },
                                        {   text: 'Agregar', iconCls: 'x-fa fa-plus',
                                            reference: 'cmdAddParticipante',
                                            action: 'addParticipante', hidden: true
                                        }
                                    ]*/
                                }
                            ]
                               
                        }/*, {
                            xtype: 'toolbar',
                            border: false, hidden: true,
                            items: [
                                {   xtype: 'tbfill' },
                                {
                                    xtype: 'button', reference: 'cmdEditEvaluaciones',
                                    text: 'Evaluaciones', iconCls: 'x-fa fa-edit',                                            
                                    action: 'editEvaluaciones',
                                    scope: this
                                }, {
                                    xtype: 'button', reference: 'cmdUpdate',
                                    text: 'Guardar', iconCls: 'x-fa fa-save',                                            
                                    action: 'update',
                                    scope: this
                                }
                            ]
                        }*/
                    ]
                }
            ]
        }
    ]

})
