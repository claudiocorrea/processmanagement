/**
 * Created by Antonio on 1/21/2016.
 */
Ext.define('ProcessManagement.view.cmpc.screen.AltaComponenteObjEspecificos', {
    extend: 'Ext.window.Window',
    alias: 'widget.altaComponenteObjEspecificos',

    controller: 'altaComponenteObjEspecificosController',

    requires: [
        'ProcessManagement.store.cmpc.ObjetivoEspecifico'
    ],

    modal: true,
    width: 500,

    title: 'Guardar Componente',

    layout: {
        type: 'fit',
        align: 'stretch'
    },

    viewModel: {
        data: {}
    },
    items: [
        {
            xtype: 'form',
            reference: 'form',
            defaults: {
                labelWidth: 120
            },
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                /*{
                    xtype: 'combobox',
                    fieldLabel: 'Componente',
                    itemId: 'cmbComponente',
                    displayField: 'nombre',
                    valueField: 'id',
                    store: 'FCMPC.store.Componente',
                    editable: false,
                    allowBlank: false
                },*/
                {
                    xtype: 'combobox',
                    fieldLabel: 'Objetivo Espec&iacute;fico',
                    reference: 'cmbObjEspecifico',
                    displayField: 'nombre',
                    valueField: 'id',
                    store: {type: 'objetivoEspecifico'}, //'FCMPC.store.ObjetivoEspecifico',
                    editable: false,
                    queryMode: 'local',
                    allowBlank: false
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Guardar',
            iconCls: 'add',
            handler: 'onSave'
        },
        {
            text: 'Cerrar',
            iconCls: 'cancel',
            handler: 'onClose'
        }
    ]
});