/**
 * Created by Antonio on 3/23/2016.
 */
Ext.define('ProcessManagement.view.cmpc.screen.ConfigurarMedioVerificacion', {
    extend: 'Ext.window.Window',
    alias: 'widget.configurarMedioVerificacion',

    controller: 'configurarMedioVerificacion',

    width: 500,
    //height: 500,
    resizable: true,
    modal: true,

    title: 'Medio de Verificaci&oacute;n',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    viewModel: {
        data: {
            idIndicador: null,
            nombreIndicador: null,
            medioVerif: null,
            idMedioVerif: null
        }
    },

    items: [
        {
            xtype: 'form',
            reference: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            flex: 1,
            items: [
                {
                    xtype: 'textfield',
                    readOnly: true,
                    reference: 'dspIndicador',
                    fieldLabel: 'Indicador',
                    displayField: 'nombre',
                    valueField: 'id'
                },
                {
                    xtype: 'combobox',
                    reference: 'itemselector',
                    fieldLabel: 'Medio Verificaci&oacute;n',
                    labelWidth: 120,
                    displayField: 'nombre',
                    valueField: 'id',
                    labelAlign: 'top',
                    fromTitle: 'Disponibles',
                    toTitle: 'Seleccionados',
                    store: {type: 'medioVerificacion'},
                    flex: 1,
                    editable: false,
                    queryMode: 'local',
                    allowBlank: false
                }
            ]
        }
    ],
    buttons: [
        /*{
            text: 'Nuevo',
            action: 'addNewMedioVerif',
            iconCls: 'filenew'
        },*/
        {
            text: 'Guardar',
            handler: 'onSave',
            iconCls: 'save'
        },
        {
            text: 'Cerrar',
            handler: 'onClose',
            iconCls: 'cancel'
        }
    ]
});