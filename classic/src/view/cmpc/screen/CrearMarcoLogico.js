Ext.define('ProcessManagement.view.cmpc.screen.CrearMarcoLogico', {
    extend: 'Ext.Panel',
    xtype: 'cmpc-crearMarcoLogicoScreen',    

    requires: [
        'Ext.tab.Panel',
        
        'ProcessManagement.view.cmpc.screen.CrearMarcoLogicoController'
    ],
    controller: 'cmpcCrearMarcoLogico',

    height: Ext.getBody().getViewSize().height - 60,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    bodyPadding: 10,

    tbar: {
        padding: '4 10',
        baseCls: 'screenBackground',
        items: [
            {   xtype: 'label', cls: 'titleSection', margin: '11 0', 
                html: 'Nuevo Marco Logico'
            }, '->', {
                xtype: 'button',
                iconCls: 'x-fa fa-plus', text: 'Nuevo',
                handler: ''
            }
        ], hidden: true
    },

    items: [
        {   xtype: 'form', reference: 'form', bodyPadding: '0 25',
            tbar: { 
                reference: 'tbarNuevoMarco', hidden: true,
                items: [
                {   xtype: 'textfield', width: '50%',
                    fieldLabel: 'Nombre'
                }, '->', {
                    xtype: 'button',
                    iconCls: 'x-fa fa-save', text: 'Guardar',
                    handler: ''
                }, {
                    xtype: 'button',
                    iconCls: 'x-fa fa-close', text: 'Cancelar',
                    handler: ''
                }
            ]},           
            items: [ 
                {   xtpye: 'fieldcontainer', layout: 'hbox', margin: '5 0 10',
                    defaults: { flex: 1, labelWidth: 150 },
                    items: [ 
                        {   xtype: 'combobox', reference: 'cbProyecto', fieldLabel: 'Proyecto', 
                            queryMode: 'local', displayField: 'name', valueField: 'id',
                            editable: false,margin: '0 20 0 0' },
                        {   xtype: 'textfield', fieldLabel: 'Tipo de proyecto',
                            readOnly: true
                        } 
                    ]
                }
                
            ]
        }, {    xtype: 'tabpanel', flex: 1, border: true,                
                items: [
                    {   title: 'Objetivo General',
                        layout: { type: 'vbox', align: 'stretch' },
                        items: [
                            {   xtype: 'panel', bodyPadding: 0, 
                                flex: 1,                                 
                                layout: { 
                                    type: 'accordion',
                                    titleCollapse: false,
                                    animate: true, multi: true
                                }, defaults: { bodyPadding: 5 },
                                items: [{
                                    title: 'Objetivo General',
                                    header: { padding: '5 10',
                                        items: [{
                                            xtype: 'button', ui: 'default-toolbar',
                                            iconCls: 'x-fa fa-edit', text: 'Configurar',
                                            handler: ''
                                        }]
                                    }, items: [
                                        {   xtype: 'grid', reference: 'gridObjetivoGral',
                                            columns: [
                                                {   xtype: 'templatecolumn', menuDisabled: true, dataIndex: 'name', 
                                                    flex: 1, tpl: [ '<p/><div class="tplText">{name}</div>' ]
                                                }, {   xtype: 'actioncolumn',
                                                    items: [
                                                        {   tooltip: 'Borrar', iconCls: 'x-fa fa-trash' }
                                                    ],
                                                    menuDisabled: true, sortable: false,
                                                }
                                            ]
                                        }
                                    ]
                                },{
                                    title: 'Indicadores',
                                    header: { padding: '5 10',
                                        items: [{
                                            xtype: 'button', ui: 'default-toolbar',
                                            iconCls: 'x-fa fa-edit', text: 'Configurar',
                                            handler: ''
                                        }]
                                    }, items: [
                                        {   xtype: 'grid', reference: 'gridIndicadoresGral',
                                            columns: [
                                                {   xtype: 'templatecolumn', menuDisabled: true, dataIndex: 'name', 
                                                    flex: 1, tpl: [ '<p/><div class="tplText">{name}</div>' ]
                                                }, {   xtype: 'actioncolumn',
                                                    items: [
                                                        {   tooltip: 'Borrar', iconCls: 'x-fa fa-trash' }
                                                    ],
                                                    menuDisabled: true, sortable: false,
                                                }
                                            ]
                                        }
                                    ]
                                },{
                                    title: 'Medio Verificación',
                                    header: { padding: '5 10',
                                        items: [{
                                            xtype: 'button', ui: 'default-toolbar',
                                            iconCls: 'x-fa fa-edit', text: 'Configurar',
                                            handler: ''
                                        }]
                                    }, items: [
                                        {   xtype: 'grid', reference: 'gridMediosGral',
                                            columns: [
                                                {   xtype: 'templatecolumn', menuDisabled: true, dataIndex: 'name', 
                                                    flex: 1, tpl: [ '<p/><div class="tplText">{name}</div>' ]
                                                }, {   xtype: 'actioncolumn',
                                                    items: [
                                                        {   tooltip: 'Borrar', iconCls: 'x-fa fa-trash' }
                                                    ],
                                                    menuDisabled: true, sortable: false,
                                                }
                                            ]
                                        }
                                    ]
                                }]
                            }
                        ]
                    }, {   
                        title: 'Objetivo Específico',
                        layout: { type: 'vbox', align: 'stretch' },
                        items: [
                            {   xtype: 'panel', bodyPadding: 0,
                                flex: 1,                                 
                                layout: { 
                                    type: 'accordion',
                                    titleCollapse: false,
                                    animate: true, multi: true
                                }, defaults: { bodyPadding: 5 },
                                items: [{
                                    title: 'Objetivo Específico',
                                    header: { padding: '5 10',
                                        items: [{
                                            xtype: 'button', ui: 'default-toolbar',
                                            iconCls: 'x-fa fa-edit', text: 'Configurar',
                                            handler: ''
                                        }]
                                    }, items: [
                                        {   xtype: 'grid', reference: 'gridObjetivoEspecifico',
                                            columns: [
                                                {   xtype: 'templatecolumn', menuDisabled: true, dataIndex: 'name', 
                                                    flex: 1, tpl: [ '<p/><div class="tplText">{name}</div>' ]
                                                }, {   
                                                    xtype: 'actioncolumn',
                                                    items: [
                                                        {   tooltip: 'Borrar', iconCls: 'x-fa fa-trash' }
                                                    ],
                                                    menuDisabled: true, sortable: false,
                                                }
                                            ]
                                        }
                                    ]
                                },{
                                    title: 'Indicadores',
                                    header: { padding: '5 10',
                                        items: [{
                                            xtype: 'button', ui: 'default-toolbar',
                                            iconCls: 'x-fa fa-edit', text: 'Configurar',
                                            handler: ''
                                        }]
                                    }, items: [
                                        {   xtype: 'grid', reference: 'gridIndicadoresEspecifico',
                                            columns: [
                                                {   xtype: 'templatecolumn', menuDisabled: true, dataIndex: 'name', 
                                                    flex: 1, tpl: [ '<p/><div class="tplText">{name}</div>' ]
                                                }, {   
                                                    xtype: 'actioncolumn',
                                                    items: [
                                                        {   tooltip: 'Borrar', iconCls: 'x-fa fa-trash' }
                                                    ],
                                                    menuDisabled: true, sortable: false,
                                                }
                                            ]
                                        }
                                    ]
                                },{
                                    title: 'Medio Verificación',
                                    header: { padding: '5 10',
                                        items: [{
                                            xtype: 'button', ui: 'default-toolbar',
                                            iconCls: 'x-fa fa-edit', text: 'Configurar',
                                            handler: ''
                                        }]
                                    }, items: [
                                        {   xtype: 'grid', reference: 'gridMediosEspecifico',
                                            columns: [
                                                {   xtype: 'templatecolumn', menuDisabled: true, dataIndex: 'name', 
                                                    flex: 1, tpl: [ '<p/><div class="tplText">{name}</div>' ]
                                                }, {   xtype: 'actioncolumn',
                                                    items: [
                                                        {   tooltip: 'Borrar', iconCls: 'x-fa fa-trash' }
                                                    ],
                                                    menuDisabled: true, sortable: false,
                                                }
                                            ]
                                        }
                                    ]
                                }]
                            }
                        ]
                    }
                ]
            } 
        
    ],

    listeners: { afterrender: 'initScreen' }

})