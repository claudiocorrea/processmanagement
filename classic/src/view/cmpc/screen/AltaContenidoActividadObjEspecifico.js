/**
 * Created by Antonio on 1/21/2016.
 */
Ext.define('ProcessManagement.view.cmpc.AltaContenidoActividadObjEspecifico', {
    extend: 'Ext.window.Window',
    alias: 'widget.altaContenidoActividadObjEspecifico',

    controller: 'altaContenidoActividadObjEspecificoController',

    modal: true,
    width: 500,

    title: 'Guardar Contenido',

    layout: {
        type: 'fit',
        align: 'stretch'
    },

    viewModel: {},

    items: [
        {
            xtype: 'form',
            reference: 'form',
            flex: 1,
            layout: {
                type: 'fit',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'textarea',
                    flex: 1,
                    reference: 'txtContenido',
                    fieldLabel: 'Objetivo',
                    labelAlign: 'top',
                    allowBlank: false
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Guardar',
            iconCls: 'add',
            handler: 'onSave'
        },
        {
            text: 'Cerrar',
            iconCls: 'cancel',
            handler: 'onClose'
        }
    ]
});