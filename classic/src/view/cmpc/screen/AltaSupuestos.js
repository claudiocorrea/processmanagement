/**
 * Created by Antonio on 1/21/2016.
 */
Ext.define('ProcessManagement.view.cmpc.screen.AltaSupuestos', {
    extend: 'Ext.window.Window',
    alias: 'widget.altaSupuestos',

    controller: 'altaSupuestosController',

    modal: true,
    width: 500,

    title: 'Guardar Supuesto',

    layout: {
        type: 'fit',
        align: 'stretch'
    },

    viewModel: {
        data: {

        }
    },

    items: [
        {
            xtype: 'form',
            reference: 'form',
            flex: 1,
            layout: {
                type: 'fit',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'textarea',
                    flex: 1,
                    reference: 'txtSupuesto',
                    fieldLabel: 'Supuesto',
                    labelAlign: 'top',
                    allowBlank: false
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Guardar',
            handler: 'onSave',
            iconCls: 'add'
        },
        {
            text: 'Cerrar',
            handler: 'onClose',
            iconCls: 'cancel'
        }
    ]
});