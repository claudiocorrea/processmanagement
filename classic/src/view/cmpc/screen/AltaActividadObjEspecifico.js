/**
 * Created by Antonio on 1/21/2016.
 */
Ext.define('ProcessManagement.view.cmpc.screen.AltaActividadObjEspecifico', {
    extend: 'Ext.window.Window',
    alias: 'widget.altaActividadObjEspecifico',

    controller: 'altaActividadObjEspecificoController',

    title: 'Guardar Actividad',

    modal: true,
    width: 500,

    layout: {
        type: 'fit',
        align: 'stretch'
    },

    viewModel: {
    },

    items: [
        {
            xtype: 'form',
            reference: 'form',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'combobox',
                    fieldLabel: 'Tipo Actividad',
                    reference: 'cmbTipoActividad',
                    queryMode: 'local',
                    displayField: 'nombre',
                    valueField: 'id',
                    editable: false,
                    store: { type: 'tipoActividad'},
                    allowBlank: false
                },
                {
                    xtype: 'combobox',
                    fieldLabel: 'Categor&iacute;a',
                    reference: 'cmbCategoria',
                    queryMode: 'local',
                    displayField: 'nombre',
                    valueField: 'id',
                    editable: false,
                    store: { type: 'categoria'},
                    allowBlank: false
                },
                {
                    xtype: 'combobox',
                    fieldLabel: 'Area',
                    reference: 'cmbArea',
                    queryMode: 'local',
                    displayField: 'nombre',
                    valueField: 'id',
                    editable: false,
                    store: { type: 'area' }
                },
                {
                    xtype: 'combobox',
                    fieldLabel: 'Nivel',
                    reference: 'cmbNivel',
                    queryMode: 'local',
                    displayField: 'nombre',
                    valueField: 'id',
                    editable: false,
                    store: { type: 'nivel' }
                }
            ]
        }
    ],

    buttons: [
        {
            text: 'Guardar',
            iconCls: 'add',
            handler: 'onSave'
        },
        {
            text: 'Cerrar',
            iconCls: 'cancel',
            handler: 'onClose'
        }
    ]
});