/**
 * Created by Antonio on 1/20/2016.
 */
Ext.define('ProcessManagement.view.cmpc.screen.AltaIndicadores', {
    extend: 'Ext.window.Window',
    alias: 'widget.altaIndicadores',

    requires: [
        'Ext.form.FieldContainer',
        'ProcessManagement.view.cmpc.screen.AltaIndicadoresController'
    ],

    controller: 'altaIndicadores',

    modal: true,
    width: 600,

    title: 'Guardar Indicador',

    layout: {
        type: 'fit',
        align: 'stretch'
    },

    /*initComponent: function () {
        this.addEvents('refreshComboIndicadores', 'showSelectedIndicador');
        this.callParent(arguments);
    },*/

    viewModel: null,

    items: [
        {
            xtype: 'form',
            frame: false,
            border: false,
            reference: 'form',
            layout: {
                type: 'anchor'
            },
            defaults: {
                labelWidth: 120,
            },
            items: [
                {
                    xtype: 'hiddenfield',
                    fieldLabel: 'Valor',
                    reference: 'valor',
                    anchor: '50%',
                    minValue: 0,
                    maxValue: 100,
                    decimalPrecision: 0
                }/*,
                {
                    xtype: 'fieldset',
                    frame: false,
                    border: false,
                    padding: '0 0 0 0',
                    anchor: '100%',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                    ]
                }*/,
                {
                    xtype: 'combobox',
                    anchor: '100%',
                    fieldLabel: 'Indicador',
                    reference: 'cmbIndicador',
                    flex: 1,
                    displayField: 'nombre',
                    valueField: 'id',
                    queryMode: 'local',
                    editable: false,
                    store: {type: 'indicadorObjGral'},//'FCMPC.store.IndicadorObjGral',
                    allowBlank: false
                },
                {
                    xtype: 'hidden',
                    anchor: '100%',
                    fieldLabel: 'Medio de Verificaci&oacute;n',
                    itemId: 'cmbMedioVerificacion',
                    flex: 1,
                    //padding: '0 0 0 5',
                    queryMode: 'local',
                    editable: false,
                    //store: 'FCMPC.store.MedioVerificacionObjGral',
                    displayField: 'nombre',
                    valueField: 'id',
                    allowBlank: false
                },
                {
                    xtype: 'textfield',
                    reference: 'lineaBase',
                    fieldLabel: 'L&iacute;nea Base',
                    anchor: '100%',
                    decimalPrecision: 2,
                    regex: /^(?:[0-9]+;)*[0-9]+$/,
                    regexText: 'Debe ingresar numeros del 0 al 100 separados por punto y coma (;)'
                },
                {
                    xtype: 'fieldcontainer',
                    frame: false,
                    border: false,
                    anchor: '100%',
                    padding: '0 0 0 0',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    defaults: {
                        labelWidth: 120,
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            reference: 'meta1',
                            fieldLabel: 'Meta A&ntilde;o 1',
                            flex: 1,
                            decimalPrecision: 2,
                            regex: /^(?:[0-9]+;)*[0-9]+$/,
                            regexText: 'Debe ingresar numeros del 0 al 100 separados por punto y coma (;)'
                        },
                        {
                            xtype: 'textfield',
                            reference: 'meta2',
                            fieldLabel: 'Meta A&ntilde;o 2',
                            flex: 1,
                            padding: '0 0 0 5',
                            decimalPrecision: 2,
                            regex: /^(?:[0-9]+;)*[0-9]+$/,
                            regexText: 'Debe ingresar numeros del 0 al 100 separados por punto y coma (;)'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    handler: 'onSave',
                    iconCls: 'save'
                },
                {
                    text: 'Cerrar',
                    action: 'close',
                    iconCls: 'cancel',
                    handler: 'onClose'
                }
            ]
        }
    ]
});