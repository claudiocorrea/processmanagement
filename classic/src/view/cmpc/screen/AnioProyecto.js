Ext.define('ProcessManagement.view.cmpc.screen.AnioProyecto', {
    extend: 'Ext.Panel',
    xtype: 'cmpc-anioProyectoScreen',    

    requires: [
        'Ext.tab.Panel',
        'ProcessManagement.view.cmpc.screen.AnioProyectoController'
    ],

    controller: 'cmpcanioproyecto',

    height: Ext.getBody().getViewSize().height - 60,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    bodyPadding: 10,

    tbar: {
        padding: '4 10',
        baseCls: 'screenBackground',
        items: [
            {   xtype: 'label', cls: 'titleSection', margin: '11 0', 
                html: 'Actualizar metas'
            }, '->', {
                xtype: 'button',
                iconCls: 'x-fa fa-save', text: 'Guardar',
                handler: ''
            }
        ], hidden: true
    },

    items: [
        {   xtype: 'form', reference: 'form', padding: '0 25',           
            items: [ {
                xtype: 'textfield', width: '100%',
                fieldLabel: 'Proyecto',
                readOnly: true
            } ]
        }, {   
            xtype: 'tabpanel', flex: 1,
            defaults: { bodyPadding: 10, }, border: true,
            items: [
                {   title: 'Indicadores Generales', 
                    layout: { type: 'vbox', align: 'stretch' },
                    items: [ {
                        xtype: 'gridpanel', reference: 'gridIndicadoresGrals',
                        flex: 1, border: false,
                        columns: [
                            {   xtype: 'templatecolumn', text: 'Indicador', dataIndex: 'oindicador', 
                                flex: 1, tpl: [ '<div class="tplText">{oindicador}</div>' ]
                            }, {   
                                xtype: 'templatecolumn', text: 'Linea Base', dataIndex: 'obase', 
                                flex: 1, tpl: [ '<div class="tplText">{obase}</div>' ]
                            },{   
                                xtype: 'templatecolumn', text: 'Meta Año 1', dataIndex: 'oanio1', 
                                flex: 1, tpl: [ '<div class="tplText">{oanio1}</div>' ]
                            },{   
                                xtype: 'templatecolumn', text: 'Meta Año 2', dataIndex: 'oanio2', 
                                flex: 1, tpl: [ '<div class="tplText">{oanio2}</div>' ]
                            },{   
                                xtype: 'templatecolumn', text: 'Meta Real Año 1', dataIndex: 'orealanio1', 
                                flex: 1, tpl: [ '<div class="tplText">{orealanio1}</div>' ]
                            },{   
                                xtype: 'templatecolumn', text: 'Meta Real Año 2', dataIndex: 'orealanio2', 
                                flex: 1, tpl: [ '<div class="tplText">{orealanio2}</div>' ]
                            }
                        ]
                    }]
                }, {   
                    title: 'Indicadores Específicos',
                    layout: { type: 'vbox', align: 'stretch' },
                    items: [ {
                        xtype: 'gridpanel', reference: 'gridIndicadoresEspecificos',
                        flex: 1, border: false,
                        columns: [
                            {   xtype: 'templatecolumn', text: 'Indicador', dataIndex: 'oindicador', 
                                flex: 1, tpl: [ '<div class="tplText">{oindicador}</div>' ]
                            }, {   
                                xtype: 'templatecolumn', text: 'Linea Base', dataIndex: 'obase', 
                                flex: 1, tpl: [ '<div class="tplText">{obase}</div>' ]
                            },{   
                                xtype: 'templatecolumn', text: 'Meta Año 1', dataIndex: 'oanio1', 
                                flex: 1, tpl: [ '<div class="tplText">{oanio1}</div>' ]
                            },{   
                                xtype: 'templatecolumn', text: 'Meta Año 2', dataIndex: 'oanio2', 
                                flex: 1, tpl: [ '<div class="tplText">{oanio2}</div>' ]
                            },{   
                                xtype: 'templatecolumn', text: 'Meta Real Año 1', dataIndex: 'orealanio1', 
                                flex: 1, tpl: [ '<div class="tplText">{orealanio1}</div>' ]
                            },{   
                                xtype: 'templatecolumn', text: 'Meta Real Año 2', dataIndex: 'orealanio2', 
                                flex: 1, tpl: [ '<div class="tplText">{orealanio2}</div>' ]
                            }
                        ]
                    } ]
                }
            ]
        }
    ],

    listeners: { afterrender: 'initScreen' }

})