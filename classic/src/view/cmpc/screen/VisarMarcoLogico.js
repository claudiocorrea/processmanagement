Ext.define('ProcessManagement.view.cmpc.screen.VisarMarcoLogico', {
    extend: 'Ext.Panel',
    xtype: 'cmpc-visarMarcoLogicoScreen',    

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items : [{
        xtype : "component",
        height: Ext.getBody().getViewSize().height - 60,
        border: false,
        autoEl : {
            tag : "iframe",
            src : "http://sgp.simbius.com:8888/fcmpc/getWord.aspx?&output=HTML&Tipo=proyecto&codigo=358"
        }
    }]

})