Ext.define('ProcessManagement.view.widget.KPI', {
    extend: 'Ext.Panel',
    xtype: 'kpi',
	
	width: 300,
    height: 150,
	
	header: false,
	frame: true,
	
	margin: '20 0 0 20',
	
	initComponent: function () {
        var me = this
        me.items = [
            {   xtype: 'component', y: 10,
                html: '<div class="circularIcon"><span class="' + me.iconCls + ' fa-3x"></span></div>'
            },
            {
                xtype: 'label',
                html: '<div class="indicator-button">' +
                        '<label class="info-title centerInDiv">' + me.title + '</label>' +
                    '</div>'
            },
            {
                xtype: 'fieldcontainer', y: 85,
                layout: 'hbox',
                cls: 'kpi-container',
                frame: true,
                defaults: {
                    xtype: 'button',
                    cls: 'whiteBackground btnHoverBackground',
                    width: 100, height: 60,
                    border: false, padding: 0
                },
                items: [
                    {   html: '<div class="kpi">' +
                                '<strong>' + me.getViewModel().title1 + '</strong>' +
                                '<span>' + me.getViewModel().value1 + '</span>' +
                            '</div>',

                        handler: function() {
                            Ext.getCmp('main').controller.openModalPopup(me.createUrlKPI(5, me.id))
                        }
                    },
                    {   html: '<div class="kpi">' +
                                '<strong>' + me.getViewModel().title2 + '</strong>' +
                                '<span>' + me.getViewModel().value2 + '</span>' +
                            '</div>',

                        handler: function() {
                            Ext.getCmp('main').controller.openModalPopup(me.createUrlKPI(5, me.id))
                        }
                    },
                    {   html: '<div class="kpi">' +
                                '<strong>' + me.getViewModel().title3 + '</strong>' +
                                '<span>' + me.getViewModel().value3 + '</span>' +
                            '</div>',

                        handler: function() {
                            Ext.getCmp('main').controller.openModalPopup(me.createUrlKPI(1, me.id))
                        }
                    }
                ]
            }
        ]
        this.callParent();
    },

    listeners: {
        afterrender: function (btn) {
            if(btn.getViewModel().titleField) {
                btn.addKPIField(btn)
            }
        }
    },

    addKPIField: function(btn) {
        btn.height = 200
        btn.add({
            xtype: 'button', y: 75,
            width: this.width, height: 40,
            cls: 'whiteBackground btnHoverBackground',
            border: false,
            html: '<div class="kpiField">' +
                    '<label class="info-title">' + btn.getViewModel().titleField.toUpperCase() + '</label>' +
                    '<div class="kpi-container">' +
                        '<div class="kpi">' +
                            '<strong>' + btn.getViewModel().title4 + '</strong>' +
                            '<span>' + btn.getViewModel().value4 + '</span>' +
                        '</div>' +
                        '<div class="kpi">' +
                            '<strong>' + btn.getViewModel().title5 + '</strong>' +
                            '<span>' + btn.getViewModel().value5 + '</span>' +
                        '</div>' +
                    '</div>' +
                '</div>',

            handler: function() {
                Ext.getCmp('main').controller.openModalPopup(btn.createUrlKPI(2, btn.id))
            }
        })
    },

    createUrlKPI: function(version, id) {
        return globalParams.urlKPI + 'kpi_v' + version + '?loggedUserId=' + loginUser.idUser +
            '&token=' + loginUser.token + '&workflowId=' + id.substring(4)
    }
	
})