Ext.define('ProcessManagement.view.screen.Dashboard', {
    extend: 'Ext.Panel',
    xtype: 'dashboardScreen',
	
	id: 'indicatorPanel',

    controller: 'dashboard',

    border: true,

    layout: {
        type: 'vbox',
        pack: 'start'
    },

    items: [
        {   xtype: 'panel',
            id: 'indicatorPanel',
            layout: 'vbox',
            scrollable: 'vertical',
            items: [
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    defaults: {
                        flex: 1
                    },
                    items: []
                }
            ]
        }
    ],

    listeners: {
        initialize: 'initScreen'
    }
	
})