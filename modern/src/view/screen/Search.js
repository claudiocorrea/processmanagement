Ext.define('ProcessManagement.view.screen.Search', {
    extend: 'Ext.Panel',
    xtype: 'searchScreen',

    controller: 'search',

    border: true,

    layout: {
        type: 'hbox',
        pack: 'center'
    },

    tbar: [
        {
            xtype: 'searchfield',
            reference: 'searchField',
            flex: 1,
            margin: '0 10 0 0',
            ui: 'solo',
            shadow: 'true',
            placeholder: 'Search',
            listeners: {
                change: 'findRequest'
            }
        }, {
            xtype: 'button',
            reference: 'btSearch',
            iconCls: 'x-fa fa-search',
            text: '',
            menu: [
                {   id: 'proceso', text: 'Codigo',
                    group: 'filter', checked: false,
                    checkHandler: 'changeFilter'
                }, {
                    id: 'Referencia',  text: 'Titulo',
                    group: 'filter', checked: true,
                    checkHandler: 'changeFilter'
                }, {
                    id: 'WorkFlow', text: 'Solicitud',
                    group: 'filter', checked: false,
                    checkHandler: 'changeFilter'
                }, {
                    id: 'StepTxt', text: 'Tarea' ,
                    group: 'filter', checked: false,
                    checkHandler: 'changeFilter'
                }
            ]
        }, {
            iconCls: 'x-fa fa-refresh',
            ui: 'action',
            margin: '0 0 0 20',
            handler: 'refreshScreen'
        }

    ],

    items: [
        {   xtype: 'gridStepsModern', reference: 'gridSteps', flex: 1}
    ],

    listeners: {
        initialize: 'initScreen'
    }
})