Ext.define('ProcessManagement.view.widget.KPIController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.kpi',
	
	init: function() {
		var me = this
        me.getView().add([
            {
                html: '<div class="indicator-button">' +
						'<div class="circularIcon"><span class="' + me.getView()._iconCls + ' fa-3x"></span></div>' +
                        '<label class="info-title centerInDiv">' + me.getView()._title + '</label>' +
                    '</div>'
            },
            {
                xtype: 'fieldcontainer', 
                layout: 'hbox',
                cls: 'kpi-container',
                frame: true,
                defaults: {
                    xtype: 'button',
                    cls: 'transpBackground',
                    height: 60
                },
                items: [
                    {   html: '<div class="kpi">' +
                                '<strong>' + me.getViewModel().title1 + '</strong>' +
                                '<span>' + me.getViewModel().value1 + '</span>' +
                            '</div>',
						width: 112,
                        handler: function() {
                            window.open(me.createUrlKPI(5, me.id), '_blank')
                        }
                    },
                    {   html: '<div class="kpi">' +
                                '<strong>' + me.getViewModel().title2 + '</strong>' +
                                '<span>' + me.getViewModel().value2 + '</span>' +
                            '</div>',
						width: 117,
                        handler: function() {
                            window.open(me.createUrlKPI(5, me.id), '_blank')
                        }
                    },
                    {   html: '<div class="kpi">' +
                                '<strong>' + me.getViewModel().title3 + '</strong>' +
                                '<span>' + me.getViewModel().value3 + '</span>' +
                            '</div>',
						width: 70,
                        handler: function() {
                            window.open(me.createUrlKPI(1, me.id), '_blank')
                        }
                    }
                ]
            }
		])
		if(me.getViewModel().titleField) {
			me.addKPIField()
		}
	},
	
	addKPIField: function() {
		var me = this
        me.getView().setHeight(200)
        me.getView().add({
            xtype: 'button', 
            width: 294, height: 55,
            cls: 'btnCls',
            border: false,
            html: '<div class="kpiField">' +
                    '<label class="info-title">' + me.getViewModel().titleField.toUpperCase() + '</label>' +
                    '<div class="kpi-container">' +
                        '<div class="kpi">' +
                            '<strong>' + me.getViewModel().title4 + '</strong>' +
                            '<span>' + me.getViewModel().value4 + '</span>' +
                        '</div>' +
                        '<div class="kpi">' +
                            '<strong>' + me.getViewModel().title5 + '</strong>' +
                            '<span>' + me.getViewModel().value5 + '</span>' +
                        '</div>' +
                    '</div>' +
                '</div>',

            handler: function() {
                window.open(me.createUrlKPI(2, me.id), '_blank')
            }
        })
    },

    createUrlKPI: function(version, id) {
        return globalParams.urlKPI + 'kpi_v' + version + '?loggedUserId=' + loginUser.idUser +
            '&token=' + loginUser.token + '&workflowId=' + id.substring(4)
    }
})