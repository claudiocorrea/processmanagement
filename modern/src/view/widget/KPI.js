Ext.define('ProcessManagement.view.widget.KPI', {
    extend: 'Ext.Panel',
    xtype: 'kpi',
	
	controller: 'kpi',
	
	width: 300,
    height: 150,
	
	header: false,
	border: false,
	
	margin: '15 0 0 45',
	
	items: [ ]
})