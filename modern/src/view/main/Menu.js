Ext.define('ProcessManagement.view.main.Menu', {
    extend: 'Ext.tab.Panel',
    xtype: 'mainMenu',

    id: 'navigator',

	requires: [
		'Ext.layout.Fit'
	],	
	
	tabPosition: 'left',
    tabRotation: 0,

	tabBar: {
        border: false,
        height: 60
    },

    defaults: {  
		layout: {
			type: 'fit',
			align: 'stretch'
		},	
        padding: 0
    },

    items: [
		{   id: 'dashboardTab',
            iconCls:'x-fa fa-desktop',			
            items: [{ xtype: 'dashboardScreen' }]
        },
		{   id: 'inboxTab',
            iconCls: 'x-fa fa-files-o',
            items: [{ xtype: 'searchScreen' }]
        }
    ]

})
