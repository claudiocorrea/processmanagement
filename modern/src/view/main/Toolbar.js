Ext.define('ProcessManagement.view.main.Toolbar', {
    extend: 'Ext.Panel',
    xtype: 'mainToolbar',
	
	height: 60,

    tbar: {
        padding: 0,
        border: false,
        items: [
            {
                xtype: 'component',
                cls: 'sgplogo',
                html: '<a href="."><div><img src="resources/images/Logo 60.png"></div></a>'
            }, {
				reference: 'title',
                text: globalParams.appName
            }, {
                iconCls: 'x-fa fa-plus',
                ui: 'action round',
                margin: '0 20 0 0'
            }, '->', {
				xtype: 'image',
				reference: 'imageUser',
				width: 40, height: 40,
				userCls: 'circular', margin: 5,
				src: 'resources/images/users/HSA0.png'
			}, {
				iconCls: 'x-fa fa-cog',
				ui: 'round', arrow: false,
				menu: [
					{ 	bind: { text: '{txtFields.words.lang}' },
						menu: {
							items: [
								{	id: 'es',
									bind: { text: '{txtFields.words.spanish}' },
									checked: true,
									group: 'lang',
									checkHandler: 'changeLanguage'
								}, {
									id: 'en',
									bind: { text: '{txtFields.words.english}' },
									checked: false,
									group: 'lang',
									checkHandler: 'changeLanguage'
								}
							]
						}
					},	
					{ 	text: 'Colores', handler: 'openTheme' }				
				]
			}
        ]
    }

})