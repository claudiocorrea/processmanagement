Ext.define('ProcessManagement.view.main.Main', {
    extend: 'Ext.Container',
    xtype: 'main',

    controller: 'main',
	viewModel: 'main',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        { xtype: 'mainToolbar'},
        { xtype: 'mainMenu', flex: 1 }
    ]

})