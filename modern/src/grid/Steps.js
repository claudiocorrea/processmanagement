Ext.define('ProcessManagement.grid.Steps', {
    extend: 'Ext.grid.Grid',
    xtype: 'gridStepsModern',
	
	requires: [ 
		'Ext.grid.plugin.RowExpander',
		'ProcessManagement.store.Steps'		
	],

    store: { type: 'steps' },

    headerBorders: false,
    border: false,

    scrollable: 'vertical',

    columns: [
        {
            text: 'Solicitud',
            flex: 1,
            tpl: '{StepId} - {WorkFlow} - {StepTxt}'
        }, {
            width: 'auto',
            cell: {
                tools: {
                    search: 'onSearch'
                }
            }
        }
    ],

    plugins: {
        rowexpander: true
    },

    itemConfig: {
        body: {
            tpl: '<div>Titulo: {Referencia}</div>' +
            '<div>Codigo: {proceso}</div>' +
            '<div>Fechas: {fechas}</div>' +
            '<div>Responsable: {responsable}</div>' +
            '<div style="margin-top: 1em;" style="white-space: normal;">{description}</div>'
        }
    },

    listeners: {
        select: 'selectionChange'
    }
});