Ext.define('ProcessManagement.Application', {
    extend: 'Ext.app.Application',

    name: 'ProcessManagement',

    requires: [
		'ProcessManagement.util.Utils',
		'ProcessManagement.util.cmpc.*',
        'ProcessManagement.view.main.Main',
		'ProcessManagement.store.KPIs',
		'ProcessManagement.store.cmpc.*',
		'ProcessManagement.view.popup.*'
    ],

    config: {
      msgBox: null
    },

    quickTips: false,
    platformConfig: {
        desktop: {
            quickTips: true
        }
    },

	launch: function() {
		var lang = ProcessManagement.util.Utils.getQueryVariable('lang'),
			vistaFormIntegrado = appParams["verForm"];
		if (lang) globalParams.lang = lang;

		if (vistaFormIntegrado != null && typeof vistaFormIntegrado != undefined) {
			this.loadFormSite();
		} else {
			this.loadNormalSite();
		}

		this.destroyLoader();		

		//Este fix permite mostrar un RadioGroup como invalido cuando allowBlank=false y no se ha seleccionado ningun item del grupo
		Ext.form.RadioGroup.override({ invalidCls: Ext.baseCSSPrefix + 'form-invalid' });
    },

	loadFormSite: function () {
		var me =  this
		ProcessManagement.util.Utils.loadScript(Ext.String.format('./locale/locale-{0}.js', appParams.lang), function(err, callback){
			if (!err) {
				var urlServiceValidateToken = Ext.String.format('{0}{1}?user={2}&token={3}', globalParams.urlServices, 'queries/users/validateToken', appParams['idUser'], appParams['token']);
				//alert(urlServiceValidateToken);
				Ext.getBody().mask(txtFields.messages.wait + '...');
				Ext.Ajax.request({
					url: urlServiceValidateToken,
					method: 'GET'
				}).then(function (response) {
					Ext.getBody().unmask();
					console.log("Then validateToken: ", response);
					response = Ext.JSON.decode(response.responseText, true);

					if (response == null) {
						Ext.Msg.show({
							title:'Error', msg: txtFields.messages.errors.tokenError,
							buttons: Ext.Msg.YES,
							//fn: function() { window.location.href = 'http://' + window.location.hostname + ':' + window.location.port }
							fn: function() { Ext.create('ProcessManagement.view.screen.Lock') }
						});
					} else {
						if (Array.isArray(response)) {
							response = response[0];
							loginUser = {
							 idUser: response.IdUser,
							 username: response.FirstName,
							 name: response.Login,
							 enterprises: me.getEnterprises(response.Companies),
							 token: appParams['token'],
							 profile: response.IdProfile
							 }
							Ext.create('ProcessManagement.view.formIntegrado.Main');
						} else {
							Ext.Msg.show({
								title:'Error', msg: txtFields.messages.errors.tokenError,
								buttons: Ext.Msg.YES,
								//fn: function() { window.location.href = 'http://' + window.location.hostname + ':' + window.location.port }
								fn: function() { Ext.create('ProcessManagement.view.screen.Lock') }
							});
						}

					}
				}, function (response) {
					Ext.getBody().unmask();
					console.log("Catch validateToken: ", response)
					Ext.Msg.show({
						title:'Error', msg: txtFields.messages.errors.tokenError,
						buttons: Ext.Msg.YES,
						//fn: function() { window.location.href = 'http://' + window.location.hostname + ':' + window.location.port }
						fn: function() { Ext.create('ProcessManagement.view.screen.Lock') }
					});

				});
			}
		});

	},

	loadNormalSite: function () {
		var lang = ProcessManagement.util.Utils.getQueryVariable('lang'),
			test = ProcessManagement.util.Utils.getQueryVariable('test');

		if(test) {
			globalParams.testing = true
			loginUser = {
				idUser: 2507,
				enterprises: [],
				enterpriseSelected: '29',
				name: "Administrador SPM",
				profile: 1,
				token: "5EB5679B-AD53-48D9-93DB-7651A8A13B20",
				username: "admin"
			}
			this.gotoMain();
		} else {
			globalParams.testing = false
			var me = this;
			Ext.Ajax.request({
				url: 'user/userLogin',
				method: 'GET',
				success: function (response) {
					if(response.responseText) {
						response = Ext.decode(response.responseText)[0];
						console.log(response)
						loginUser = {
							idUser: response.LoginResult,
							username: response.UserName,
							name: response.Name,
							enterprises: me.getEnterprises(response.Companies),
							token: response.Token,
							profile: response.IdProfile,
						}
						me.gotoMain();
						me.startPollNotifications();
					} else {
						Ext.create('ProcessManagement.view.screen.Lock')
					}
				},
				failure: function (response) {
					var msg = 'Error al buscar la sesion.'
					if(lang == 'en') msg = 'Error to find session'
					Ext.Msg.show({
						title:'Error', msg: msg,
						buttons: Ext.Msg.YES,
						//fn: function() { window.location.href = 'http://' + window.location.hostname + ':' + window.location.port }
						fn: function() { Ext.create('ProcessManagement.view.screen.Lock') }
					});

				}
			})
		}

	},
	
	gotoMain: function() {
		if(Ext.manifest.profile == 'modern') {
			Ext.Viewport.add({ xtype: 'main' })
		} else {
			if(loginUser.enterprises.length > 1 && !loginUser.enterpriseSelected) {
				this.openEnterprisePopup(loginUser.enterprises)
			} else { 
				if(loginUser.enterprises.length == 1) 
					loginUser.enterpriseSelected = loginUser.enterprises[0].IdEmpresa 
				Ext.create('ProcessManagement.view.main.Main')
			}
		}
	},
	
	destroyLoader: function () {
        var circles = Ext.fly('loadingSplashCircles'),
            bottom = Ext.get('loadingSplashBottom'),
            top = Ext.get('loadingSplashTop'),
            wrapper = Ext.fly('loadingSplash');

        if (circles) {
            circles.destroy();
        }
    },

	startPollNotifications: function () {
		var me = this,
			task = {
				run: function () {
                    me.getQtyNotificationsByUser();
                },
                interval: (300 * 1000) //5 minutos
			};

		Ext.TaskManager.start(task);
	},

    getQtyNotificationsByUser: function () {
    	console.log("Buscando nuevas notificaciones... ", new Date());
		Ext.Ajax.request({
			url: 'notification/qtyByUser',
			method: 'GET',
			params: {
				IdUser: loginUser.idUser
			},
			success: function (response) {
                var buttonNotif = Ext.ComponentQuery.query('button[reference=btnNotifications]')[0];

				response = Ext.JSON.decode(response.responseText, true);

				if (response.success && response.success === true) {
					loginUser.qtyNotifications = response.cantidad;
				} else {
					loginUser.qtyNotifications = 0;
				}

				if (loginUser.qtyNotifications > 0) {

					if (buttonNotif) {
						buttonNotif.setBadgeText(loginUser.qtyNotifications);
						buttonNotif.setVisible(true);
					}
				} else {
                    if (buttonNotif) {
                        buttonNotif.setBadgeText(loginUser.qtyNotifications);
                        buttonNotif.setVisible(false);
                    }
				}
            }
		});
	}, 
	
	getEnterprises: function(enterprise) {
		enterprise = enterprise.split('|')
		var arr = []
		enterprise.forEach(enterprise => {
			enterprise = enterprise.split(',')
			if(enterprise[0] != "") {
				arr.push({ IdEmpresa: enterprise[0], Descripcion: enterprise[1] })
			}
		})
		return arr
	},

	openEnterprisePopup: function(enterprises) {
		var win = new ProcessManagement.view.popup.Enterprise({ data: enterprises });
		win.show()
	}
});
