Ext.define('ProcessManagement.store.KPIs', {
    extend: 'Ext.data.Store',
    alias: 'store.kpis',
	
	requires: [
		'Ext.data.proxy.Rest'
	],

    storeId: 'kpisStore',
    model: 'ProcessManagement.model.KPI',
	
	proxy: {
        type: 'rest',

        api: {
            read: globalParams.urlServices + 'queries/workflow/getKPI'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            callback: function (proxy, response) {
                console.log('CALLBACK')
            },
            exception: function (proxy, response) {
                if (response.status == '0') {
                    Ext.MessageBox.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },

    getKPI: function(user, callback) {
        var store = this
        store.add(new ProcessManagement.model.KPI());
        store.getProxy().setExtraParams({
            idUser: user.idUser,
            token: user.token
        })
        store.load({
            callback: function (o, response) {
                var result = response
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                } else {
                    result = {
                        error: result.error.response.status,
                        message: result.error.response.statusText,
                        response: {}
                    }
                }
                callback(response.exception, result)
            }
        })
    }
	
})