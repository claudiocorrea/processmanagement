Ext.define('ProcessManagement.store.Workflows', {
    extend: 'Ext.data.Store',
    alias: 'store.workflows',

    storeId: 'workflowsStore',
    model: 'ProcessManagement.model.Workflow',

    proxy: {
        type: 'rest',
        api: {
            read: globalParams.urlServices + 'queries/workflow/getAllowed',
            create: globalParams.urlServices + 'queries/workflow/startworkflow'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            callback: function () {
                console.log('CALLBACK')
            },
            exception: function(proxy, response) {
                if (response.status == '0') {
                    Ext.MessageBox.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },

    getWorkflows: function(user, callback) {
        var store = this
        store.load({
            scope: this,
            params: {
                idUser: loginUser.idUser,
                token: loginUser.token
            },
            callback: function (o, response) {
                var result
                if(!response.success) {
                    result = {
                        error: response.error.response.status,
                        message: response.error.response.statusText,
                        response: {}
                    }
                }
                callback(response.exception, result)
            }
        })
    },

    startWorkflow: function(user, data, callback) {
        var store = this
        store.add(new ProcessManagement.model.Workflow())
        store.save({
            scope: this,
            params: {
                idUser: user.idUser,
                token: user.token,
                username: user.username,
                workflowId: data.workflowId
            },
            callback: function (response) {				
                var result = response.operations[0]
                if(result.success) {
                    result = result._resultSet.records[0]
                    if(result.Resultado == 'OK') {
                        result = {
                            error: -1,
                            message: 'OK',
                            response: result
                        }
                    } else {
                        response.exception = true
                        result = {
                            error: 100,
                            message: result.Resultado,
                            response: result
                        }
                    }
                } else {
                    result = {
                        error: result.error.response.status,
                        message: result.error.response.statusText,
                        response: {}
                    }
                }
                callback(response.exception, result)
            }
        })
    }

})