Ext.define('ProcessManagement.store.Steps', {
    extend: 'Ext.data.Store',
    alias: 'store.steps',
	
	requires: [
		'Ext.data.proxy.Rest'
	],

    storeId: 'stepsStore',
    model: 'ProcessManagement.model.Step',

    proxy: {
        type: 'rest',

        api: {
            read: globalParams.urlServices + 'queries/workflow/steps/filterByUsers'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
           exception: function (proxy, response) {
                if (response.status == '0') {
                    Ext.MessageBox.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },

    getSteps: function(user, callback) {
        var store = this
        store.add(new ProcessManagement.model.Step());
        store.getProxy().setExtraParams({
            IdUser: user.idUser,
            Token: user.token,
            profileId: user.profile
        })
        store.load({
            callback: function (o, response) {
                var result = response
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records[0]
                    }
                } else {
                    result = {
                        error: result.error.response.status,
                        message: result.error.response.statusText,
                        response: {}
                    }
                }
                callback(response.exception, result)
            }
        })
    }

})