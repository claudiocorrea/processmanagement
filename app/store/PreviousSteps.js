Ext.define('ProcessManagement.store.PreviousSteps', {
    extend: 'Ext.data.Store',
    alias: 'store.previousSteps',
	
	requires: [
		'Ext.data.proxy.Rest'
	],

    storeId: 'previousStepsStore',
    model: 'ProcessManagement.model.Key',
	
    proxy: {
        type: 'ajax',
        url: 'pasosWorkflow/pasosAnteriores',
        reader: {
            type: 'json',
            rootProperty: 'listaPasosAnteriores'
        },
        timeout: 500000
    }
})