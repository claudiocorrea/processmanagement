/**
 * Created by Antonio on 2/5/2016.
 */
Ext.define('ProcessManagement.store.cmpc.TipoActividad', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.tipoActividad',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.TipoActividad',

    autoLoad: true,

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getTipoActividad(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});