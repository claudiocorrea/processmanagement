/**
 * Created by Antonio on 2/1/2016.
 */
Ext.define('ProcessManagement.store.cmpc.TipoProyecto', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',
    alias: 'store.tipoProyecto',

    requires: [
        'Ext.data.proxy.Rest',
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.TipoProyecto',

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getTipoProyecto(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});