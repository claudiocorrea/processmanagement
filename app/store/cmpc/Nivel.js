/**
 * Created by Antonio on 2/5/2016.
 */
Ext.define('ProcessManagement.store.cmpc.Nivel', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.nivel',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.Nivel',

    autoLoad: true,

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getNivel(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});