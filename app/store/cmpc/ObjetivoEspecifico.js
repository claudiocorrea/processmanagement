/**
 * Created by Antonio on 2/3/2016.
 */
Ext.define('ProcessManagement.store.cmpc.ObjetivoEspecifico', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.objetivoEspecifico',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.ObjetivoEspecifico',

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getObjetivoEspecifico(),
        reader: {
            type: 'json',
            root: 'data.results'
        },
        extraParams: {
            callback: 'callback'
        }
    }
});