Ext.define('ProcessManagement.store.cmpc.KPIs', {
    extend: 'Ext.data.Store',
    alias: 'store.cmpckpis',
	
	storeId: 'cmpcKpisStore',
    fields:[ 'proyect', 'actividad', 'ano', 
        'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre',
        'total'
    ],
    data: [
        {   'type': 'session', 'proyect': 'CAD00354 - Talagante', 'actividad': 'Asesoría', 'ano': '2', 
            'enero': 0, 'febrero': 0, 'marzo': '8,00', 'abril': '63,50', 'mayo': '63,00', 'junio': '70,00', 
            'julio': 0, 'agosto': 0, 'septiembre': 0, 'octubre': 0, 'noviembre': 0, 'diciembre': 0,
            'total': '204,50' 
        },{   
            'type': 'session', 'proyect': 'CAD00354 - Talagante', 'actividad': 'Asesoría de análisis de resultado', 'ano': '2', 
            'enero': 0, 'febrero': 0, 'marzo': '57,00', 'abril': 0, 'mayo': '2,00', 'junio': 0, 
            'julio': 0, 'agosto': 0, 'septiembre': 0, 'octubre': 0, 'noviembre': 0, 'diciembre': 0,
            'total': '59,00' 
        },{   
            'type': 'session', 'proyect': 'CAD00354 - Talagante', 'actividad': 'Perfeccionamiento', 'ano': '2', 
            'enero': 0, 'febrero': 0, 'marzo': '56,00', 'abril': 0, 'mayo': 0, 'junio': 0, 
            'julio': 0, 'agosto': 0, 'septiembre': 0, 'octubre': 0, 'noviembre': 0, 'diciembre': 0,
            'total': '56,00' 
        },{   
            'type': 'session', 'proyect': 'CAD00354 - Talagante', 'actividad': 'Taller', 'ano': '2', 
            'enero': 0, 'febrero': 0, 'marzo': '5,00', 'abril': '15,00', 'mayo': '12,00', 'junio': '8,00', 
            'julio': 0, 'agosto': 0, 'septiembre': 0, 'octubre': 0, 'noviembre': 0, 'diciembre': 0,
            'total': '40,00' 
        },{   
            'type': 'session', 'proyect': 'CAD00354 - Talagante', 'actividad': 'Asesoría flexible', 'ano': '2', 
            'type': 'session', 'enero': 0, 'febrero': 0, 'marzo': '4,50', 'abril': '4,50', 'mayo': '6,00', 'junio': '4,50', 
            'julio': 0, 'agosto': 0, 'septiembre': 0, 'octubre': 0, 'noviembre': 0, 'diciembre': 0,
            'total': '19,50' 
        },{   
            'type': 'session', 'proyect': 'CAD00354 - Talagante', 'actividad': 'Charla', 'ano': '2', 
            'enero': 0, 'febrero': 0, 'marzo': 0, 'abril': '1,50', 'mayo': 0, 'junio': '4,00', 
            'julio': 0, 'agosto': 0, 'septiembre': 0, 'octubre': 0, 'noviembre': 0, 'diciembre': 0,
            'total': '5,50' 
        },{   
            'type': 'total1', 'proyect': '', 'actividad': '', 'ano': '2', 
            'enero': 0, 'febrero': 0, 'marzo': '130,50', 'abril': '84,50', 'mayo': '8,00', 'junio': '86,50', 
            'julio': 0, 'agosto': 0, 'septiembre': 0, 'octubre': 0, 'noviembre': 0, 'diciembre': 0,
            'total': '384,50' 
        },

        {   'type': 'calendar', 'proyect': 'CAD00354 - Talagante', 'actividad': 'Asesoría', 'ano': '2', 
            'enero': 0, 'febrero': 0, 'marzo': '8,00', 'abril': '65,00', 'mayo': '63,00', 'junio': '65,00', 
            'julio': '6,00', 'agosto': '65,00', 'septiembre': 0, 'octubre': '65,00', 'noviembre': '6,00', 'diciembre': 0,
            'total': '406,00' 
        },{   
            'type': 'calendar', 'proyect': 'CAD00354 - Talagante', 'actividad': 'Asesoría de análisis de resultado', 'ano': '2', 
            'enero': 0, 'febrero': 0, 'marzo': '57,00', 'abril': 0, 'mayo': '2,00', 'junio': 0, 
            'julio': '59,00', 'agosto': 0, 'septiembre': '2,00', 'octubre': 0, 'noviembre': '2,00', 'diciembre': '2,00',
            'total': '124,00' 
        },{   
            'type': 'calendar', 'proyect': 'CAD00354 - Talagante', 'actividad': 'Perfeccionamiento', 'ano': '2', 
            'enero': 0, 'febrero': 0, 'marzo': '5,00', 'abril': '15,00', 'mayo': '12,00', 'junio': '10,00', 
            'julio': '5,00', 'agosto': '17,00', 'septiembre': '10,00', 'octubre': '12,00', 'noviembre': 0, 'diciembre': 0,
            'total': '86,00' 
        },{   
            'type': 'calendar', 'proyect': 'CAD00354 - Talagante', 'actividad': 'Taller', 'ano': '2', 
            'enero': 0, 'febrero': 0, 'marzo': '56,00', 'abril': 0, 'mayo': 0, 'junio': 0, 
            'julio': 0, 'agosto': 0, 'septiembre': 0, 'octubre': 0, 'noviembre': 0, 'diciembre': 0,
            'total': '56,00' 
        },{   
            'type': 'calendar', 'proyect': 'CAD00354 - Talagante', 'actividad': 'Asesoría flexible', 'ano': '2', 
            'enero': 0, 'febrero': 0, 'marzo': '4,50', 'abril': '4,50', 'mayo': '4,50', 'junio': '4,50', 
            'julio': '4,50', 'agosto': '4,50', 'septiembre': '4,50', 'octubre': '4,50', 'noviembre': '4,50', 'diciembre': 0,
            'total': '40,50' 
        },{   
            'type': 'calendar', 'proyect': 'CAD00354 - Talagante', 'actividad': 'Charla', 'ano': '2', 
            'enero': 0, 'febrero': 0, 'marzo': 0, 'abril': '1,50', 'mayo': 0, 'junio': '1,50', 
            'julio': 0, 'agosto': '1,50', 'septiembre': 0, 'octubre': '1,50', 'noviembre': 0, 'diciembre': 0,
            'total': '6,00' 
        },{   
            'type': 'total2', 'proyect': '', 'actividad': '', 'ano': '2', 
            'enero': 0, 'febrero': 0, 'marzo': '130,50', 'abril': '86,00', 'mayo': '81,50', 'junio': '81,00', 
            'julio': '74,50', 'agosto': '88,00', 'septiembre': '79,50', 'octubre': '83,50', 'noviembre': '12,50', 'diciembre': '2,00',
            'total': '718,50' 
        }
    ]
	
})