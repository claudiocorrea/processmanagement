/**
 * Created by Antonio on 1/19/2016.
 */
Ext.define('ProcessManagement.store.cmpc.Sesiones', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',
    alias: 'store.sesiones',

    requires: [ ],
    model: 'ProcessManagement.model.cmpc.Sesiones',
    autoSync: true,
    /*proxy: {
        type: 'jsonp',
        url: 'http://sgp.simbius.com:5555/rest/CMPCGoogleAPI/fcmpc/proyecto/Capacitación%20Docente-Talca-2015-2017',
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data'
        },
        callbackKey: 'callback',
        timeout: 5000000
    } */
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data.sesiones'
        },
        callbackKey: 'callback',
        timeout: 5000000
    }
});