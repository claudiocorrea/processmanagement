/**
 * Created by Antonio on 2/2/2016.
 */
Ext.define('ProcessManagement.store.cmpc.IndicadorObjGral', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.indicadorObjGral',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.IndicadorObjGral',

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getIndicadorObjGral(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});