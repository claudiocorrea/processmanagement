/**
 * Created by Antonio on 2/2/2016.
 */
Ext.define('ProcessManagement.store.cmpc.ObjetivoGeneral', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',
    alias: 'store.objetivoGeneral',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.ObjetivoGeneral',

    autoLoad: true,

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getObjetivoGeneral(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});