/**
 * Created by Antonio on 2/7/2016.
 */
Ext.define('ProcessManagement.store.cmpc.Periodo', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.periodo',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    autoLoad: true,

    model: 'ProcessManagement.model.cmpc.Periodo',

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getPeriodo(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});