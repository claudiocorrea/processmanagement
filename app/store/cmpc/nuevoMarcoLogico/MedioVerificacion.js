/**
 * Created by Antonio on 2/18/2016.
 */
Ext.define('ProcessManagement.store.cmpc.nuevoMarcoLogico.MedioVerificacion', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.medioVerificacion',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.IdNombre',

    proxy: {
        type: 'rest',
        filterParam: undefined,
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getMediosVerificacion(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});