/**
 * Created by Antonio on 2/19/2016.
 */
Ext.define('ProcessManagement.store.cmpc.nuevoMarcoLogico.ObjetivoEspecifico', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.objetivoEspecifico',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.ObjetivoGeneral',

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getObjetivoEspecifico(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});