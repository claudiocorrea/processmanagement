/**
 * Created by Antonio on 2/17/2016.
 */
Ext.define('ProcessManagement.store.cmpc.nuevoMarcoLogico.IndicadoresProyecto', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.indicadoresProyecto',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.IdNombre',

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getIndicadorObjGral(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});