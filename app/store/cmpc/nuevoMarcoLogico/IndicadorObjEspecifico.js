/**
 * Created by Antonio on 2/19/2016.
 */
Ext.define('ProcessManagement.store.cmpc.nuevoMarcoLogico.IndicadorObjEspecifico', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.indicadorObjEspecifico',
    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.IndicadorObjGral',

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getIndicadorObjGral(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});