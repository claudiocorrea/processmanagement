/**
 * Created by antonio on 16/02/16.
 */
Ext.define('ProcessManagement.store.cmpc.nuevoMarcoLogico.Indicador', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.indicador',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.IndicadorObjGral',

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getIndicadorObjGral(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});