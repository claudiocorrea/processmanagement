/**
 * Created by Antonio on 2/19/2016.
 */
Ext.define('ProcessManagement.store.cmpc.nuevoMarcoLogico.IndicadoresObjEspecificoProyecto', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.indicadoresObjEspecificoProyecto',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.IdNombre',

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getIndicadorObjGral(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});