/**
 * Created by Antonio on 2/16/2016.
 */
Ext.define('ProcessManagement.store.cmpc.nuevoMarcoLogico.Indicadores', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.indicadores',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.IndicadorObjGral',

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getIndicadorObjGral(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});