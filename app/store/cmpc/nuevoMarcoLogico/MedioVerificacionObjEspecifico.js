/**
 * Created by Antonio on 2/21/2016.
 */
Ext.define('ProcessManagement.store.cmpc.nuevoMarcoLogico.MedioVerificacionObjEspecifico', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.medioVerificacionObjEspecifico',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.IdNombre',

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getMediosVerificacion(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});