/**
 * Created by Antonio on 2/5/2016.
 */
Ext.define('ProcessManagement.store.cmpc.Area', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.area',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.Area',

    autoLoad: true,

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getArea(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});