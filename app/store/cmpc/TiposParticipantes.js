/**
 * Created by Antonio on 2/7/2016.
 */
Ext.define('ProcessManagement.store.cmpc.TiposParticipantes', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.tiposParticipantes',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    autoLoad: true,

    model: 'ProcessManagement.model.cmpc.TiposParticipantes',

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getTiposParticipantes(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});