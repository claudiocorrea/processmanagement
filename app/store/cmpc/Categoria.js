/**
 * Created by Antonio on 2/5/2016.
 */
Ext.define('ProcessManagement.store.cmpc.Categoria', {
    extend: 'ProcessManagement.store.cmpc.staticData.Abstract',

    alias: 'store.categoria',

    requires: [
        'ProcessManagement.util.cmpc.ServiceEndPoint'
    ],

    model: 'ProcessManagement.model.cmpc.Categoria',

    autoLoad: true,

    proxy: {
        type: 'rest',
        url: ProcessManagement.util.cmpc.ServiceEndPoint.getCategoria(),
        reader: {
            type: 'json',
            root: 'data.results'
        }
    }
});