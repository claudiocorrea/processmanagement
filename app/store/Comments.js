Ext.define('ProcessManagement.store.Comments', {
    extend: 'Ext.data.Store',
    alias: 'store.comments',

    storeId: 'commentsStore',
    model: 'ProcessManagement.model.Comment',

    proxy: {
        type: 'rest',

        api: {
            read: globalParams.urlServices + 'queries/workflow/getCommentsByWorkflowId',
            create: globalParams.urlServices + 'queries/workflow/addComments',
            destroy: globalParams.urlServices + 'queries/workflow/removeComments'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            exception: function (proxy, response) {
                if (response.status == '0') {
                    Ext.MessageBox.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },

    getComments: function(user, id, callback) {
        var store = this
        store.load({
            scope: this,
            params: {
                idUser: user.idUser,
                token: user.token,
                workflowId: id
            },
            callback: function (o, response) {
                var result = response
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records[0]
                    }
                } else {
                    result = {
                        error: result.error.response.status,
                        message: result.error.response.statusText,
                        response: {}
                    }
                }
                callback(response.exception, result)
            }
        })
    },

    addComment: function(user, id, stepId, msg, grid, callback) {
        var store = this
        store.add(new ProcessManagement.model.Comment())
        grid.mask('Espere...');
        store.save({
            scope: this,
            params: {
                idUser: user.idUser,
                token: user.token,
                workflowId: id,
                stepId: stepId,
                message: msg
            },
            callback: function (response) {
                grid.unmask();
                var result = response.operations[0]
                if(result.success) {
                    store.getComments(user, id, callback)
                }
            }
        })
    },

    removeComment: function(user, id, rowIndex, grid, callback) {
        var store = this;
        var records = store.getAt(rowIndex).data;
        store.remove(records)
        grid.mask('Espere...');
        store.erase({
            scope: this,
            id: records.IdComment,
            params: {
                idUser: user.idUser,
                token: user.token,
                workflowId: id,
                idComment: records.IdComment
            },
            callback: function (o, response) {
                grid.unmask();
                console.log(response);
                store.getComments(user, id, callback);
            }
        })
    }

})