Ext.define('ProcessManagement.store.Notifications', {
    extend: 'Ext.data.Store',
    alias: 'store.notifications',

    storeId: 'notificationsStore',
    model: 'ProcessManagement.model.Notifications',

    proxy: {
        type: 'rest',
        api: {
            read: 'notification/getPendingsByUser'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            callback: function () {
                console.log('CALLBACK');
            },
            exception: function(proxy, response) {
                if (response.status == '0') {
                    Ext.MessageBox.alert('Error', 'El servidor no responde.');
                }
            }
        }
    },

    getPendingsByUser: function(idUser, callback) {
        var store = this;
        store.load({
            scope: this,
            params: {
                idUser: idUser,
                token: loginUser.token
            },
            callback: function (o, response) {
                var result
                if(!response.success) {
                    result = {
                        error: response.error.response.status,
                        message: response.error.response.statusText,
                        response: {}
                    };
                }
                callback(response.exception, response);
            }
        });
    }

});