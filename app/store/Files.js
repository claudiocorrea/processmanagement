Ext.define('ProcessManagement.store.Files', {
    extend: 'Ext.data.Store',
    alias: 'store.files',

    storeId: 'filesStore',
    model: 'ProcessManagement.model.File',

    proxy: {
        type: 'rest',

        api: {
            read: globalParams.urlServices + 'queries/workflow/getFilesByWorkflowId',
            create: globalParams.urlServices + 'queries/workflow/addFile',
            destroy: globalParams.urlServices + 'queries/workflow/removeFile'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            callback: function (proxy, response) {
                console.log('CALLBACK')
            },
            exception: function (proxy, response) {
                if (response.status == '0') {
                    Ext.MessageBox.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },

    getFiles: function(user, workflowId, callback) {
        var store = this
        store.load({
            scope: this,
            params: {
                idUser: user.idUser,
                token: user.token,
                workflowId: workflowId
            },
            callback: function (o, response) {
                var result = response
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records[0]
                    }
                } else {
                    result = {
                        error: result.error.response.status,
                        message: result.error.response.statusText,
                        response: {}
                    }
                }
                callback(response.exception, result)
            }
        })
    },
    uploadedFile: function(user, workflowId, stepId, filename, grid, callback) {
        var store = this;

        if (grid) {
            grid.mask('Espere...');
        } else {
            grid = Ext.ComponentQuery.query('gridFiles')[0];
        }

        store.add(new ProcessManagement.model.File());
        store.save({
            scope: this,
            params: {
                idUser: user.idUser,
                token: user.token,
                workflowId: workflowId,
                stepId: stepId,
                file: filename.replace(/C:\\fakepath\\/g, "")
            },
            callback: function (response) {
                grid.unmask();
				console.log(response)
                var result = response.operations[0]
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records[0]
                    }
                } else {
                    result = {
                        error: result.error.response.status,
                        message: JSON.parse(result.error.response.responseText).errorMsg,
                        response: {}
                    }
                }
                callback(response.exception, result)
            }
        })
    },

    removeFile: function (user, workflowId, rowIndex, grid, callback) {
        var store = this,
            records = store.getAt(rowIndex).data;

        store.remove(records);
        grid.mask('Espere...');
        Ext.Ajax.request({
            url: globalParams.urlServices + 'queries/workflow/removeFile' + '/' + records.IdArchivo + '/' + records.Archivo.replace(/C:\\fakepath\\/g, ""),
            method: 'DELETE'
        }).then(function (response, opts) {
            grid.unmask();
            console.log('Remove File!!')
            store.getFiles(user, workflowId, callback);
        }, function (response, opts) {
            grid.unmask();
            Ext.MessageBox.alert('Error', 'El servidor no responde.');
            console.log(response);
        });
    }

})