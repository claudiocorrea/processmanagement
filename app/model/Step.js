Ext.define('ProcessManagement.model.Step', {
    extend: 'ProcessManagement.model.Base',

    fields: [
        {  name: 'WorkflowId', type: 'string' },
        {  name: 'StepId', type: 'string' },
        {  name: 'proceso', type: 'string' },
        {  name: 'Referencia', type: 'string' },
        {  name: 'StepTxt', type: 'string',
            convert: function(value, record) {
                if(value) return value.substring(0,1).toUpperCase() + value.substring(1).toLowerCase()
                else ''
            } },
        {  name: 'IdState', type: 'string' },
        {  name: 'description', type: 'string' },
        {  name: 'startDate', type: 'date', format: 'd/m/Y', convert: function (value, record) {
            var retVal = Ext.Date.parse(value, "d/m/Y");
            return retVal;
        }
        },
        {  name: 'endDate', type: 'date', format: 'd/m/Y', convert: function (value, record) {
            var retVal = Ext.Date.parse(value, "d/m/Y");
            return retVal;
        }
        },
        {  name: 'AllowDocs', type: 'boolean', convert: function (value, record) {
            var v = false;
            if (value != null && (value == "Si" || value == "si")) v = true;
            return v;
        }
        },
        {  name: 'responsable', type: 'string' },
        {  name: 'WorkFlow', type: 'string',
            convert: function(value, record) {
                if(value) return value.substring(0,1).toUpperCase() + value.substring(1).toLowerCase()
                else ''
            }
        },
        {  name: 'IdEmpresa', type: 'string' },
        {  name: 'fechas', type: 'string' },
        {  name: 'Estado', type: 'string' },
        {  name: 'WorkflowIDoriginal', type: 'string' },
        {  name: 'IdForm', type: 'string' },
        {  name: 'Form',  
            convert: function(value, record) {
                var ext = record.get('Referencia').split(' | '),
                    result = {}
                if(ext && ext.length > 0) {
                    result = {
                        company: ext[0],
                        area: ext[1],
                        name: ext[2]
                    }
                    ext = record.get('proceso').split('|')
                    if(ext && ext.length > 0) {
                        result.dateCreated = ext[0]
                        result.rut = ext[1]
                        result.docType = ext[2]
                    }
                }
                return result
            }
        }
    ]
})