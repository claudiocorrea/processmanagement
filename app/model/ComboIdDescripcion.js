Ext.define('ProcessManagement.model.ComboIdDescripcion', {
    extend: 'Ext.data.Model',

    fields: [
        {  name: 'id', type: 'string' },
        {  name: 'descripcion', type: 'string', convert: function (val, row) {
                if (val) {
                    return Ext.util.Format.htmlDecode(val);
                } else {
                    return val;
                }
            }
        },
        { name: 'idDescripcion', type: 'string', convert: function (val, row) {
            var tval = (row.get('id'))?row.get('id'):"", tdesc = (row.get('descripcion'))?row.get('descripcion'):"";

            return Ext.String.format('{0}-{1}', tval, tdesc);
        }}
    ]
})