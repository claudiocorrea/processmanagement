Ext.define('ProcessManagement.model.Notifications', {
    extend: 'ProcessManagement.model.Base',

    fields: [
        {   name: 'IdMensaje', type: 'int' },
        {   name: 'WorkFlowID', type: 'int' },
        {   name: 'Mensaje', type: 'string'},
        {   name: 'Leido', type: 'string'},
        {   name: 'FechaLeido', type: 'string'},
        {   name: 'HoraLeido', type: 'string'},
        {   name: 'IdUser', type: 'int'},
        {   name: 'Fecha', type: 'string'},
        {   name: 'Hora', type: 'string'},
        {   name: 'Referencia', type: 'string'},
        {   name: 'proceso', type: 'string'},
        {   name: 'workflow', type: 'string'},
        {   name: 'WorkflowProceso', type: 'string'},
        {   name: 'UserName', type: 'string'},
        {   name: 'FechaHora', type: 'date', convert: function (value, record) {
                if (value !== null && value !== undefined) {
                    var fecha = record.get('Fecha'),
                        hora = record.get('Hora'),
                        arrHora = (hora !== null && hora !== undefined && hora !== "")?hora.trim().split(':'):null;

                    value = Ext.Date.parse(fecha, "d/m/Y");

                    if (value) {
                        if (arrHora !== null && arrHora.length >= 3) {
                            value.setHours(arrHora[0]);
                            value.setMinutes(arrHora[1]);
                            value.setSeconds(arrHora[2]);
                        }
                    }
                }

                return value;
        }}
    ]

});