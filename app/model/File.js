Ext.define('ProcessManagement.model.File', {
    extend: 'ProcessManagement.model.Base',

    fields: [
        {  name: 'IdArchivo', type: 'string' },
        {  name: 'IdImage', type: 'string',
            convert: function(value, record) {
                var ext = record.get('Archivo')
                if(ext && ext.indexOf('|') != -1) {
                    ext = ext.substring(0, ext.indexOf('|'))
                }
                return ext
            } 
        },
        {  name: 'Archivo', type: 'string',
            convert: function(value, record) {
                var ext = record.get('Archivo')
                if(ext && ext.indexOf('|') != -1) {
                    ext = ext.substring(ext.indexOf('|') + 1)
                }
                return ext
            } 
        },        
        {  name: 'WorkflowID', type: 'string' },
        {  name: 'Date', type: 'string' },
        {  name: 'Time', type: 'string' },
        {  name: 'IdUser', type: 'string' },
        {  name: 'FullName', type: 'string' },
        {  name: 'icon', type: 'string',
            convert: function(value, record) {
                var ext = record.get('Archivo')
                if(ext) {
                    ext = ext.substring(ext.indexOf('.') + 1)
                    if (ext == 'pdf')
                        return 'x-fa fa-file-pdf-o'
                    else if (ext == 'doc' || ext == 'docx')
                        return 'x-fa fa-file-word-o'
                    else if (ext == 'xls' || ext == 'xlsx')
                        return 'x-fa fa-file-excel-o'
                }
                return 'x-fa fa-file-text'
            }
        },
        {  name: 'StepId', type: 'string' },
        {  name: 'Url', type: 'string',
            convert: function(value, record) {
                return globalParams.urlFileStore + record.get('IdImage')
            } 
        }
    ]

})