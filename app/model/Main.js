Ext.define('ProcessManagement.model.Main', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.main',

    data: {
        currentView: null,
        username: '',
        pass: '',
        testing: false,
        txtFields: {}
    },

    formulas: {
        labels: function (key) {
            var me = this;
            //console.log("key: ", key);
            //console.log("This VM: ", this);
            console.log("Cargando el locale: " + Ext.String.format('./locale/locale-{0}.js', appParams.lang));
            ProcessManagement.util.Utils.loadScript(Ext.String.format('./locale/locale-{0}.js', appParams.lang), function(err, callback){
                if (!err) { me.set({ txtFields: txtFields } ); }
            });
        },

        testing: function () {
            this.set({ testing : globalParams })
        }
    }
})