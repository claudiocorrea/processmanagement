Ext.define('ProcessManagement.model.Comment', {
    extend: 'ProcessManagement.model.Base',

    fields: [
        {  name: 'IdComment', type: 'string' },
        {  name: 'Description', type: 'string' },
        {  name: 'WorkFlowId', type: 'string' },
        {  name: 'Cdate', type: 'string' },
        {  name: 'Ctime', type: 'string' },
        {  name: 'IdUser', type: 'string' },
        {  name: 'UserName', type: 'string' },
        {  name: 'StepId', type: 'string' }
    ]

})