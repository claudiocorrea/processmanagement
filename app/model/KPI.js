Ext.define('ProcessManagement.model.KPI', {
    extend: 'ProcessManagement.model.Base',

	fields: [
        {  name: 'WorkflowID', type: 'string' },
        {  name: 'NombreWorkflow', type: 'string' },
        {  name: 'Icon', type: 'string' },
        {  name: 'KPI_CANT_WK', type: 'string' },
        {  name: 'KPI_PENDIENTE', type: 'int' },
        {  name: 'KPI_TERMINADO', type: 'string' },
        {  name: 'KPI_AVG_DIAS', type: 'string' },
        {  name: 'KPI_CAMPO', type: 'string' },
        {  name: 'KPI_SUM', type: 'string' },
        {  name: 'KPI_AVG', type: 'string' }
    ]
})