Ext.define('ProcessManagement.model.Menu', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.menu', 
	
	stores: {
        navItems: {
            type: 'tree',
            root: {
                expanded: false,
                children: [
					{	text: 'Tablero',
						iconCls: 'x-fa fa-desktop',
						routeId: 'dashboard',
						leaf: true
					}, {
						text: 'Gestión',
						iconCls: 'x-fa fa-files-o',
						routeId: 'search',
						leaf: true
					}, {
                		text: 'Búsqueda',
						iconCls: 'x-fa fa-search',
                        routeId: 'files',
                        leaf: true
					}
				]
			}
		},
		navCmpc: {
            type: 'tree',
            root: {
                expanded: false,
                children: [
					{	text: 'Tablero',
						iconCls: 'x-fa fa-desktop',
						routeId: 'dashboardCMPC',
						leaf: true
					}, {
						text: 'Solicitudes',
						iconCls: 'x-fa fa-files-o',
						routeId: 'search',
						leaf: true
					}/*, {
                		text: 'Sesiones',
						iconCls: 'x-fa fa-files-o',
                        routeId: 'cmpc-sesiones',
                        leaf: true
					}, {
                		text: 'Generar Marco L&oacute;gico',
						iconCls: 'x-fa fa-files-o',
                        routeId: 'cmpc-generarMarcoLogico',
                        leaf: true
					}, {
                		text: 'Actualizar Metas',
						iconCls: 'x-fa fa-files-o',
                        routeId: 'cmpc-anioProyecto',
                        leaf: true
					}, {
                		text: 'Diseño Proyecto',
						iconCls: 'x-fa fa-files-o',
                        routeId: 'cmpc-diseñoProyecto',
                        leaf: true
					}, {
                		text: 'Nuevo Marco Logico',
						iconCls: 'x-fa fa-files-o',
                        routeId: 'cmpc-crearMarcoLogico',
                        leaf: true
					}, {
                		text: 'Visar Marco Logico',
						iconCls: 'x-fa fa-files-o',
                        routeId: 'cmpc-visarMarcoLogico',
                        leaf: true
					}*/
				]
			}
		}
	}
})