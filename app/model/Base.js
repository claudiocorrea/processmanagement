Ext.define('ProcessManagement.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'ProcessManagement.model'
    }
});