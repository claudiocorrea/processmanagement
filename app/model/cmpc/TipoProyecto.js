/**
 * Created by Antonio on 2/1/2016.
 */
Ext.define('ProcessManagement.model.cmpc.TipoProyecto', {
    extend: 'Ext.data.Model',

    idProperty: 'ID',

    fields: [
        {name: 'ID'},
        {name: 'nombrepro'},
        {name: 'nombreModelo'}
    ]
})