/**
 * Created by Antonio on 1/19/2016.
 */
Ext.define('ProcessManagement.model.cmpc.Sesiones', {
    extend: 'Ext.data.Model',
    idProperty: 'idSESI',
    fields: [
        {
            name: 'idSESI', mapping: 'idSESI'
        },
        {
            name: 'idTIPR', mapping: 'idTIPR'
        },
        {
            name: 'idTIAC', mapping: 'idTIAC'
        },
        {
            name: 'SESI', mapping: 'SESI'
        },
        {
            name: 'idGoogle', mapping: 'idGoogle'
        },
        {
            name: 'urlCalendario', mapping: 'urlCalendario'
        },
        {
            name: 'idContenido', mapping: 'idContenido'
        },
        {
            name: 'urlContenido', mapping: 'urlContenido'
        },
        {
            name: 'startDate', mapping: 'startDate'
        },
        {
            name: 'endDate', mapping: 'endDate'
        },
        {
            name: 'FEINP', mapping: 'FEINP', type: 'date', convert: function (value, row) {
                    if (value == null || value == "") return value;
                    var d = Ext.Date.parse(value, "d/m/Y");

                    return d;
            }//, format: 'd/m/Y'
        },
        {
            name: 'FEFIP', mapping: 'FEFIP', type: 'date', format: 'd/m/Y'
        },
        {
            name: 'HOINP', mapping: 'HOINP'
        },
        {
            name: 'HOFIP', mapping: 'HOFIP'
        },
        {
            name: 'FEINE', mapping: 'FEINE', type: 'date', format: 'd/m/Y'
        },
        {
            name: 'FEFIE', mapping: 'FEFIE', type: 'date', format: 'd/m/Y'
        },
        {
            name: 'HOINE', mapping: 'HOINE'
        },
        {
            name: 'HOIFE', mapping: 'HOIFE'
        },
        {
            name: 'encargado', mapping: 'encargado'
        },
        {
            name: 'email', mapping: 'email'
        },
        {
            name: 'username', mapping: 'username'
        },
        {
            name: 'titulo', mapping: 'titulo'
        },
        {
            name: 'status', mapping: 'estado'
        },
        {
            name: 'idComponente', mapping: 'idComponente'
        },
        {
            name: 'componente', mapping: 'componente'
        },
        {
            name: 'participantes', mapping: 'participantes'
        },
        {
            name: 'idTipoActividad', type: 'string', convert: function (value, row) {
                var idTIAC = row.get('idTIAC'),
                    id = null;
                if (idTIAC != null && idTIAC != "") {
                    id = idTIAC.substring(0, idTIAC.indexOf("-"))
                }
                return id;
            }
        },
        {
            name: 'idArea', type: 'string', convert: function (value, row) {
                var idTIAC = row.get('idTIAC'),
                    id = null;
                if (idTIAC != null && idTIAC != "") {
                    var arr = idTIAC.split("-");
                    id = arr[2]
                }
                return id;
            }
        },
        {
            name: 'idCategoria', type: 'string', convert: function (value, row) {
                var idTIAC = row.get('idTIAC'),
                    id = null;
                if (idTIAC != null && idTIAC != "") {
                    var arr = idTIAC.split("-");
                    id = arr[1]
                }
                return id;
            }
        },
        {
            name: 'isAsesoriaIndividual', type: 'boolean', convert: function (value, row) {
                var idTIAC = row.get('idTipoActividad'),
                    idCate = row.get('idCategoria');

                if ((idTIAC == '7' || idTIAC == '25' || idTIAC == '32') && idCate == '5') {
                    return true;
                } else {
                    return false;
                }
            }
        },
        {
            name: 'participanteAsesoriaIndividual', convert: function (value, row) {
                var arr = row.get('participantes'),
                    esAsesoriaIndividual = row.get('isAsesoriaIndividual'),
                    fullName = null;

                if (esAsesoriaIndividual && (Ext.isArray(arr) && arr.length > 0)) {
                    fullName = arr[0].fullName;
                }

                return fullName;
            }
        },
        {
            name: 'perfilParticipanteAsesoriaIndividual', type: 'string', convert: function (value, row) {
                var arr = row.get('participantes'),
                    esAsesoriaIndividual = row.get('isAsesoriaIndividual');

                if (esAsesoriaIndividual && (Ext.isArray(arr) && arr.length > 0)) {
                    return arr[0].perfil;
                }

                return null;
            }
        },
        {
            name: 'establecimientoParticipanteAsesoriaIndividual', type: 'string', convert: function (value, row) {
                var arr = row.get('participantes'),
                    esAsesoriaIndividual = row.get('isAsesoriaIndividual');

                if (esAsesoriaIndividual && (Ext.isArray(arr) && arr.length > 0)) {
                    return arr[0].nombreEstablecimiento;
                }

                return null;
            }
        }
    ]
});