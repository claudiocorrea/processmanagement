/**
 * Created by Antonio on 2/3/2016.
 */
Ext.define('ProcessManagement.model.cmpc.ObjetivoEspecifico', {
    extend: 'Ext.data.Model',

    idProperty: 'id',

    fields: [
        {name: 'id'},
        {name: 'nombre'},
        {name: 'nombreCorto'},
        {name: 'seleccionado'}
    ]
});