/**
 * Created by Antonio on 2/2/2016.
 */
Ext.define('ProcessManagement.model.cmpc.ObjetivoGeneral', {
    extend: 'Ext.data.Model',

    idProperty: 'id',

    fields: [
        {name: 'id'},
        {name: 'nombre'},
        {name: 'seleccionado'}
    ]
});