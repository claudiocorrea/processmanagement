/**
 * Created by Antonio on 2/17/2016.
 */
Ext.define('ProcessManagement.model.cmpc.IdNombre', {
    extend: 'Ext.data.Model',

    idProperty: 'id',

    fields: [
        {name: 'id'},
        {name: 'nombre'},
        {name: 'seleccionado'}
    ]
});