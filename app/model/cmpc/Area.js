/**
 * Created by Antonio on 2/5/2016.
 */
Ext.define('ProcessManagement.model.cmpc.Area', {
    extend: 'Ext.data.Model',

    idProperty: 'id',

    fields: [
        {name: 'id'},
        {name: 'nombre'}
    ]
});