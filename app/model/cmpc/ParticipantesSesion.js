/**
 * Created by Antonio on 1/25/2016.
 */
Ext.define('ProcessManagement.model.cmpc.ParticipantesSesion', {
    extend: 'ProcessManagement.model.Base',
    
    idProperty: 'idParticipante',
    fields: [
        {
            name: 'idParticipante', mapping: 'idParticipante'
        },
        {
            name: 'email', mapping: 'email'
        },
        {
            name: 'responseStatus', mapping: 'responseStatus'
        },
        {
            name: 'comment', mapping: 'comment'
        },
        {
            name: 'asistencia', type: 'string', convert: function (val, row) { //mapping: 'asistencia'
                try {
                    //var retVal = eval(val);
                    if (val == null || val == undefined) return null;
                    if (val == "true" || eval(val) == true) return "1";
                    else return "0";

                    //return (retVal == true)?true:false;
                }catch (e) {
                    return val;
                }
            }
        },
        {
            name: 'fullName', mapping: 'fullName'
        },
        {
            name: 'perfil', mapping: 'perfil'
        },
        {
            name: 'observacionAsistencia', mapping: 'observacionAsistencia'
        },
        {
            name: 'logro', mapping: 'logro'
        },
        {
            name: 'evaluaciones', mapping: 'evaluaciones'
        },
        {
            name: 'idSubtipoPerfil', mapping: 'idSubtipoPerfil'
        },
        {
            name: 'subtipoPerfil', mapping: 'subtipoPerfil'
        },
        {
            name: 'perfilCompleto', type: 'string', convert: function (val, row) {
                    var retVal = "";

                    if (row.get('subtipoPerfil') != null && row.get('subtipoPerfil') != "") {
                        retVal = Ext.String.format("{0} - {1}", row.get('perfil'), row.get('subtipoPerfil'));
                    } else {
                        retVal = row.get('perfil');
                    }

                    return retVal;
            }
        },
        {
            name: 'cursos', mapping: 'cursos'
        },
        {
            name: 'idNivel', mapping: 'idNivel'
        },
        {
            name: 'idEstablecimientoComuna', mapping: 'idEstablecimientoComuna'
        },
        {
            name: 'nombreEstablecimiento', mapping: 'nombreEstablecimiento'
        },
        {
            name: 'cursosConcat', type: 'string', convert: function (val, row) {
                var retVal = "";

                if (row.get('cursos') != null && row.get('cursos') != "") {
                    var json = row.get('cursos');

                    if (json != null && json.cursos != null && json.cursos != "") {
                        Ext.each(json.cursos, function (c1) {
                            retVal += c1.nombre + " | ";
                        });
                        if (retVal.length > 0) {
                            retVal = retVal.substring(0, retVal.length - 3)
                        }
                    }
                }
                return retVal;
            }
        },
        {
            name: 'tiempoAsistencia', mapping: 'tiempoAsistencia'
        },
        {
            name: 'idPerfil', mapping: 'idPerfil'
        }
    ]
});