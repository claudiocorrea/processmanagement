Ext.define('ProcessManagement.model.Key', {
    extend: 'ProcessManagement.model.Base',

    rootProperty: 'clave',
    fields: [
        { name: 'clave' },
        { name: 'valor'},
        { name: 'valornumerico'}
    ]
})