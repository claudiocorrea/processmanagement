Ext.define('ProcessManagement.model.Workflow', {
    extend: 'ProcessManagement.model.Base',

    fields: [
        {   name: 'WorkflowID', type: 'string' },
        {   name: 'icon', type: 'string',
            convert: function(value, record) {
                var desc = record.get('description');
                if(desc) {
                    var pos = desc.indexOf(']')
                    if (pos != -1)
                        return desc.substring(1, pos)
                }
                return globalParams.iconDefault
            }
        },
        {   name: 'Workflow', type: 'string' },
        {   name: 'description', type: 'string',
            convert: function(value, record) {
                var desc = record.get('description');
                if(desc) return desc.substring(desc.indexOf(']') +1)
                else return ''
            }
        }
    ]

})