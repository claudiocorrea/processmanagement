Ext.define('ProcessManagement.view.panel.FormModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.form',

    data: {
        idForm: null,
		WorkflowId: null,
		StepId: null,
		StepTxt: null,
        formFields: null
    },

    parent: 'main'
})