Ext.define('ProcessManagement.view.panel.FormController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.form',

    requires: [
        'ProcessManagement.model.ComboIdDescripcion'
    ],

	config: {
        data: null,
        storesToLoad: [],
        idFieldsToLoad: [],
        formsHijo: []
    },
    savedForm: false,

	init: function() {
        this.data = { };
        console.log(this)
    },

    isWorkflowTerminated: function () {
        var flag = false,
            finishedStatesIds = ['3', '4', '8'];

        if (finishedStatesIds.includes(this.data.IdState) || this.data.IdState == null || this.data.IdState == undefined) {
            flag = true;
        }

        return flag;
    },

    initScreen: function (data) {

        var me = this,
                finishedStatesIds = ['3', '4', '8'],
            screen = Ext.ComponentQuery.query('requestScreen')[0];

        this.savedForm = false;

		this.data = data;
		this.getView().getViewModel().set('idForm', data.IdForm);
		this.getView().getViewModel().set('WorkflowId', data.WorkflowId);
		this.getView().getViewModel().set('StepId', data.StepId);
		this.getView().getViewModel().set('StepTxt', data.StepTxt);

		this.getView().getViewModel().set('WorkFlow', data.WorkFlow);
		this.getView().getViewModel().set('proceso', data.proceso);

		this.getView().getViewModel().set('childForms', []);
		this.getView().getViewModel().set('formFields', null);
        if (appParams["verForm"]) {
            me.loadFormFields();
        } else {
        this.getFormIdFromWorkflow()
            .then(function () {
                console.log('Cargando campos...')
                me.loadFormFields();
            },
            function () {
                Ext.Msg.alert('ERROR', txtFields.messages.errors.gettingFormId);
            })
        }

        if (finishedStatesIds.includes(this.data.IdState) || this.data.IdState == null || this.data.IdState == undefined) {

            if(screen) {
                screen.getController().lookupReference('btnNewFile').setHidden(true);
                screen.getController().lookupReference('btnNewComment').setHidden(true);
            }
            this.lookup('botonera').setVisible(false);
            this.lookup('botoneraImpresion').setVisible(true);

        } else {
            if(screen) screen.getController().lookupReference('btnNewComment').setHidden(false);
            this.lookup('botonera').setVisible(true);
            this.lookup('botoneraImpresion').setVisible(false);
        }

        if (this.getView().getViewModel().get('WorkflowId') == null || this.getView().getViewModel().get('WorkflowId') == undefined) {
            this.lookup('botonera').setVisible(false);
            this.lookup('botoneraImpresion').setVisible(false);
        }
    },

    disableFormsFields: function () {
        var form = this.lookupReference('SGPForm'),
            formFields = form.query('.field'),
            formEditores = form.query('.htmleditor');

        Ext.each(formFields, function(field) {
            field.setReadOnly(true);
        });

        Ext.each(formEditores, function(field) {
            field.setReadOnly(true);
        });
    },

    getFormIdFromWorkflow: function () {
        var me = this,
            view = null;

        if (appParams["verForm"]) {
            view = Ext.getBody();
        } else {
            view = me.getView();
        }
        view.mask(txtFields.messages.wait + '...');

        return new Ext.Promise(function (resolve, reject) {
            Ext.Ajax.request({
                url: 'workflowRules/workflowForms',
                method: 'GET',
                params: {
                    workflowId: me.getView().getViewModel().get('WorkflowId')
                },
                success: function (response) {
                    view.unmask();
                    response = Ext.JSON.decode(response.responseText, true);

                    if (response != null) {
                        try {
                        if (response.result.IdForm != null && response.result.IdForm != undefined) {
                            me.getView().getViewModel().set('idForm', response.result.IdForm);
                            me.data.IdForm = response.result.IdForm;
                        }
                        } catch(e) {}
                    }
                    resolve(true);
                },
                failure: function (response) {
                    view.unmask();
                    reject(false);
                }
            });
        });
    },

	refreshScreen: function() {
		this.loadFormFields();

        if (appParams["verForm"] == null || typeof appParams["verForm"] == undefined) {
            this.refreshWorkflowsList();
        }
	},

	getFormFieldsFromService: function (idForm, ignoraCampoHabilitados) {
		var me = this,
            finishedStatesIds = ['3', '4', '8'],
            params = {
                idForm: idForm,
                workflowId: me.data.WorkflowId,
                userId: loginUser.idUser
            };

        if (ignoraCampoHabilitados) {
            params.ignoraCampoHabilitados = true;
        }

        if (finishedStatesIds.includes(this.data.IdState) || this.data.IdState == null || this.data.IdState == undefined) {
            params.ignoraCampoHabilitados = true;
        }
		return new Ext.Promise(function(resolve, reject) {
			Ext.Ajax.request({
		            url: 'form',
					method: 'GET',
		            params: params
		        }).then (function(response, opts) {
		        	var resp = {success: true, result: Ext.JSON.decode(response.responseText)};
		        	resolve(resp);
		        },
		        function(response, opts) {
		        	reject({success: false});
		        });
		});
	},

    updateViewModel: function (jsonFields) {
        var me = this,
            formFieldsModel = {};

        Ext.each(jsonFields.secciones, function (seccion) {
            if (seccion.items) {
                var arrItems = seccion.items.sort(me.customSort);

                Ext.each(arrItems, function (item) {
                    //Guarda los campos del form en el objeto que ira al ViewModel
                    formFieldsModel[item.Id] = item.ValorActual;
                })
            }
        });

        //Actualiza el viewModel
        me.getView().getViewModel().set('formFields', formFieldsModel);

    },

	loadFormFields: function() {
		var me = this,
            view = null;

        if (appParams["verForm"]) {
            view = Ext.getBody();
        } else {
            view = me.getView();
        }
        console.log('Mask 1')
		view.mask(txtFields.messages.wait + '...');

		me.getFormFieldsFromService(me.data.IdForm).then(function(response, opts) {
			view.unmask();
			try {
				var form = me.lookupReference('SGPForm'),
                fieldsArray = [],
				formReadOnly = false,
				hayCampoCheckbox = false, botonRechazar = me.lookupReference('btnRechazar'),
				botonGrabarEnviar= me.lookupReference('grabarEnviar');

				form.removeAll(true);
				jsonFields = response.result;
				formReadOnly = jsonFields.readOnly;

                me.updateViewModel(jsonFields);

                console.log("Secciones: ", jsonFields.secciones);

                Ext.each(jsonFields.secciones, function(seccion) {
                    var items = [],
                        posicionesSeccion = me.getPosicionesEnSeccion(seccion);
                    if (seccion.items) {
                        try {
                            var arrItems = seccion.items.sort(me.customSort);
                        } catch (e) {
                        }
                        try {
                            //Recorre los elementos de esta seccion
                            Ext.each(arrItems, function(item) {
                                var tempItem = me.getExtJSItem(item, form),
                                    itemPos = eval(item.Posicion),
                                    nextItemPos = itemPos + 1,
                                    prevItemPos = itemPos - 1;
                                tempItem.padding = '0 0 0 0';

                                if (tempItem.xtype == "checkbox") {
                                    //console.log("Valor item.OcultaAprobarSiEsFalso:", item.OcultaAprobarSiEsFalso);
                                    //console.log("Valor evaluacion item.OcultaAprobarSiEsFalso:", (eval(item.OcultaAprobarSiEsFalso) == 0 || eval(item.OcultaAprobarSiEsFalso) == true)?false:true);

                                    hayCampoCheckbox = true;
                                    //botonGrabarEnviar.setVisible((eval(item.OcultaAprobarSiEsFalso) == 0 || eval(item.OcultaAprobarSiEsFalso) == true)?false:true);
                                }
                                items.push(tempItem);
                            });
                        } catch (e) {
                            alert(e);
                        }
                    }
					if(me.isSectionVisible(items)) {
						//console.log('Add Section: ' + seccion.texto, items)
						form.add({
							xtype: 'panel',
							collapsible: true,
							padding: '3 0 3 0',
							bodyPadding: '3 15 0 15',
							title: seccion.texto,
							padding: 10,
							defaults: { width: '100%' },

							items: items
						});
					}
                });


                if (!hayCampoCheckbox) {
                    botonGrabarEnviar.setVisible(true);
                } else {
                    //Se usa el valor de jsonFields.ocultaBotonGrabarEnviar para mostrar/ocultar el boton de grabar
                    botonGrabarEnviar.setVisible(jsonFields.ocultaBotonGrabarEnviar)
                }
                me.afterRenderCamposForm(form);

                me.fixfieldContainersChilds(form);

                if (formReadOnly) {
                    form.query('field').forEach(function(field) {
                        field.setDisabled(true);
                    });

                    form.query('htmleditor').forEach(function(field) {
                        field.setDisabled(true);
                    });

                    form.query('button').forEach(function(button) {
                        button.setDisabled(true);
                    });
                }

                botonRechazar.setVisible(jsonFields.rechazoHabilitado);

                Ext.each(me.getConfig('idFieldsToLoad'), function (item) {
                    var query = Ext.String.format('{0}[name={1}]', item.xtype, item.id),
                        component = Ext.ComponentQuery.query(query)[0];

                    console.log('Mask 2')
                    if (component) component.mask(txtFields.messages.wait);
                });

                if (me.isWorkflowTerminated()) {
                    me.disableFormsFields();
                }
			} catch(exp) {alert('Error: ' + exp); console.log(exp); }

		}, function(response, opts) {
                view.unmask();
        });
	},

    afterRender: function (component) {
        var me = this,
            finishedStatesIds = ['3', '4', '8'];

        if (appParams['verForm'] != null && typeof appParams['verForm'] != undefined) {
            try {
                var me = this,
                    screen = Ext.ComponentQuery.query('requestScreen')[0],
                    buttonBack = screen.getController().lookup('requestReturn');

                buttonBack.setVisible(false);

                if (appParams["verForm"]) {
                    var btnNewRequest = Ext.ComponentQuery.query('button[reference=btnNewRequest]')[0],
                        btnLogout = Ext.ComponentQuery.query('button[reference=btnLogout]')[0];

                    btnNewRequest.setVisible(false);
                    btnLogout.setVisible(false);

                }

                if (finishedStatesIds.includes(me.data.IdState) || me.data.IdState == null || me.data.IdState == undefined) {
                    me.lookup('botonera').setVisible(false);
                    me.lookup('botoneraImpresion').setVisible(true);
                } else {
                    me.lookup('botonera').setVisible(true);
                    me.lookup('botoneraImpresion').setVisible(false);
                }

                if ((appParams["stepId"] != null && typeof appParams["stepId"] != undefined) &&
                    (appParams["workflowId"] != null && typeof appParams["workflowId"] != undefined)) {
                    Ext.Ajax.request({
                        url: 'tareasPersonales/V2',
                        method: 'GET',
                        params: {
                            userId: appParams["idUser"],
                            stepId: appParams["stepId"]
                        }
                    }).then(function (result) {
                        result = Ext.JSON.decode(result.responseText, true);
                        console.log("Result tareasPersonalesV2: ", result);

                        if (result.result == null) {
                            result.result = {
                                    StepTxt: '',
                                    IdState: '3',
                                    proceso: '',
                                    AllowDocs: 'no',
                                    WorkFlow: ''
                            }
                        }

                        screen.getController().data = result.result;

                        console.log("result: ", result)
                        me.initScreen({
                            IdForm: appParams['idForm'],
                            WorkflowId: appParams['workflowId'],
                            StepId: appParams['stepId'],
                            StepTxt: result.result.StepTxt,
                            IdState: result.result.IdState + '',
                            WorkFlow: result.result.WorkFlow,
                            proceso: result.result.proceso
                        });

                        screen.getController().getFiles(function () {
                            screen.getController().getComments();
                        });

                        screen.getController().lookupReference('requestTitle').setText(result.result.WorkFlow +
                            ((result.result.proceso) ? ' - ' + result.result.proceso + ' - ' + result.result.StepTxt : ''));

                        var allowDocs = result.result.AllowDocs;

                        allowDocs = (allowDocs != null && (allowDocs == "Si" || allowDocs == "si")) ? true : false;

                        screen.getController().lookupReference('btnNewFile').setHidden(!allowDocs);

                    }, function (result) {

                    });
                } else { //NO vino el parametro stepId
                    me.initScreen({
                        IdForm: appParams['idForm'],
                        WorkflowId: (appParams['workflowId'] != null && typeof appParams['workflowId'] != undefined)?appParams['workflowId']:null,
                        StepId: (appParams['stepId'] != null && typeof appParams['stepId'] != undefined)?appParams['stepId']:-1,
                        StepTxt: null
                    });
                }

                if (appParams["readOnly"]) {
                    me.lookup('botonera').setVisible(false);
                    me.lookup('botoneraImpresion').setVisible(false);
                }

            } catch(e) {
                console.log(e);
            }

        }
    },

    fixfieldContainersChilds: function (elForm) {
        var me = this,
            fcChilds = elForm.query('.field[parentFieldContainer!=undefined]');

        Ext.each(fcChilds, function (f) {
            var parent = elForm.query('panel[name=' + f.parentFieldContainer + ']')[0];

            if (parent) {
                parent.add(f);
            }
        })
    },

	afterRenderCamposForm: function(elForm) {
        var me = this,
            combosDep = elForm.query('combobox[dependsOn!=""]');
            //combos = elForm.query('combobox');
        if (combosDep && combosDep.length > 0) {
            Ext.each(combosDep, function(combo) {
                console.log("Combo a procesar:", combo);
                var padre = elForm.down('#' + combo.dependsOn),
                    value = combo.value;

                console.log("padre: ", combo.dependsOn, padre);

                me.__refreshComboHIjo(elForm, padre, padre.value, combo, function(valH) {
                        console.log("combo:", combo)
                    combo.setValue(valH);
                    //Crea el listener para el combo padre

                        console.log("padre:", padre)
                    padre.on('change', function(thisEl, newValue, oldValue) {
                        me.__refreshComboHIjo(elForm, thisEl, newValue, combo);
                    });
                }, value);

            });
        }
    },
    __refreshComboHIjo: function(elForm, comboP, newValueComboP, comboH, p_callback, origValueComboH) {
        var me = this,
            idComboP = comboP.itemId,
            idComboH = comboH.itemId,
            valueParam = comboP.value,
            idForm = me.getView().getViewModel().get('idForm'),
            idStep = me.getView().getViewModel().get('StepId'),
            workflowId = me.getView().getViewModel().get('WorkflowId'),
            valueSelected = comboH.value,
            storeDep = Ext.create('Ext.data.Store', {
                fields: [
                    'valor',
                    'clave'
                ]
            }),
            url = 'comboAnidado',//'soap/valoresComboHijo',
            params = {
                comboPadre: idComboP,
                comboHijo: idComboH,
                parameter: Ext.util.Format.htmlDecode(newValueComboP),
                //valueParam,
                idForm: idForm,
                idStep: idStep,
                workFlowId: workflowId,
                idUser: loginUser.idUser,
                token: loginUser.token
            };

        comboH.setValue(null);
        try {
            if (!Ext.isIE || Ext.ieVersion > 9)  {
                comboH.mask(txtFields.messages.wait + '...');
            }

        } //Esto genera errores en versiones viejas de IE
        catch (e) {}
        Ext.Ajax.request({
            //url: 'soap/valoresComboHijo',
            url: 'comboAnidado',
            params: params,
            method: 'GET',
            async: false,
            timeout: 500000,
            success: function (response, opts) {
                try {
                    if (!Ext.isIE || Ext.ieVersion > 9)  {
                        comboH.unmask();
                    }

                } //Esto genera errores en versiones viejas de IE
                catch (e) {}
                response = Ext.JSON.decode(response.responseText, true);
                if (response == null) {
                    response = {
                        success: false,
                        errorMsg: txtFields.messages.errorGettingData
                    };
                }
                if (response.success) {
                    var results = [],
                        defaultValue = null,
                        flag = false;

                    storeDep.setProxy({
                        type: 'memory',
                        data: response.values
                    });
                    storeDep.load({
                        callback: function() {
                            //comboH.setValue(valueSelected);
                            comboH.bindStore(storeDep);
                            if (p_callback)  {
                                p_callback(origValueComboH);
                            }

                        }
                    });
                } else {
                    if (!Ext.isIE || Ext.ieVersion > 9)  {
                        comboH.unmask();
                    }

                }

            },
            failure: function (response, opts) {
                if (!Ext.isIE || Ext.ieVersion > 9)  {
                    comboH.unmask();
                }

                if (p_callback)  {
                    p_callback(origValueComboH);
                }
            }
        });
    },

    customSort: function(a, b) {
        var valA = eval(a.Posicion),
            valB = eval(b.Posicion);
        return valA - valB;
    },
    getPosicionesEnSeccion: function(seccion) {
        var pos = [];
        if (seccion.items) {
            Ext.each(seccion.items, function(item) {
                pos.push(eval(item.Posicion));
            });
        }
        return pos;
    },
    getExtJSItem: function(item, form) {
        var me = this,
            i = (item.Tipo == "Radio Button")? {}: {
                bind: {value: '{formFields.' + item.Id + '}'},
                value: item.ValorActual,
                flex: 1,
                itemId: item.Id,
                name: item.Id,
                readOnly: eval(item.SoloLectura),
                allowBlank: !eval(item.Obligatorio),
                fieldLabel: item.TextoFrontEnd,
                posicion: item.Posicion,
                afterSubTpl: item.CampoHtml,
                idField: item.IdField,
                campoValueCombo: item.CampoValueCombo,
                campoTextCombo: item.CampoTextCombo,
                //labelWidth: (item.TextoFrontEnd.length > 15)?150:100
                labelAlign: (item.TextoFrontEnd.length > 17)?'top':'left',
                labelWidth: 125,
                hidden: (item.Visible == 1)?false:true,
                labelClsExtra: 'labelField',
                seccion: item.Seccion,
                parentFieldContainer: item.fieldContainerPadre
            };

        if (item.Style != "") {
            i.fieldStyle = item.Style;
        }

        if(item.Id == 'cpbakpi' && item.TextoFrontEnd == 'KPI') {
            i.hidden = true
        }

        try {
            switch (item.Tipo) {
                case "FieldContainer":
                    i = {
                        xtype: 'panel',
                        idField: item.Id,
                        name: item.Id,
                        posicion: item.Posicion,
                        layout: 'hbox',
                        bodyPadding: 0, border: false, margin: '5 0 5 0',
                        defaults: { flex: 1, margin: '0 5 5 0' },
                        items: [],
                        isUsedAsFieldContainer: true
                    };
                    break;
                case "TextBox":
                    i.xtype = 'textfield';
                    if (i.itemId.startsWith('IP')) {
                        i.vtype = 'IPAddress';
                    };
                    break;
                case "DateField":
                    var mayorHoy = eval(item.FechaMayorOIgualaHoy),
                        valor = Ext.Date.parse(item.ValorActual, "d/m/Y");
                    i.xtype = 'datefield';
                    i.value = valor;
                    i.campoMenorQue = item.MenorQueCampo;

                    if (mayorHoy) {
                        i.minValue = new Date();
                    }
                    if (form) {
                        if (item.MenorQueCampo && item.MenorQueCampo != "") { // Si es un form, el chequeo se hace de otra manera
                            i.listeners = {
                                change: function (field) {
                                    var parentObj = form.down('#' + item.MenorQueCampo);

                                    console.log("item.MenorQueCampo: ", parentObj);
                                    if (parentObj) {
                                        me.__onCheckValorMenorQue(field, parentObj);
                                    }
                                }
                            }
                        }

                    } else { //No es un form, sino una grilla

                    }
                    break;
                case "Combo":
                    i.xtype = 'combobox';
                    i.editable = false;
                    i.typeAhead = false;
                    i.dependsOn = (item.ComboPadre == null || item.ComboPadre == undefined)?"":item.ComboPadre;

                    if (i.dependsOn == "" || i.dependsOn == null || i.dependsOn == undefined) {
                        var store = Ext.create('Ext.data.Store', {
                            model: 'ProcessManagement.model.ComboIdDescripcion',
                            proxy: {
                                type: 'ajax',
                                //url: 'soap/valoresPosibles', //SOLO util si se usan webservices de .NET
                                url: 'form/getFieldValues',
                                //url: 'form/getFieldValues',
                                reader: {
                                    type: 'json',
                                    rootProperty: 'result'
                                },
                                timeout: 500000
                            }
                        });
                        i.store = store;

                        if (!form) me.getConfig('storesToLoad').push(store);

                        me.getConfig('idFieldsToLoad').push({id: item.Id, xtype: i.xtype});

                        store.load({
                             params: {
                                 userId: loginUser.idUser,
                                 stepId: me.getView().getViewModel().get('StepId'),
                                 token: loginUser.token,
                                 fieldId: item.IdField,
                                 fieldValue: item.CampoValueCombo,
                                 fieldDescription: item.CampoTextCombo
                             },
                            callback: function () {
                                var query = Ext.String.format('{0}[name={1}]', i.xtype, item.Id),
                                    component = Ext.ComponentQuery.query(query)[0];

                                if (component) component.unmask();

                                if (!form) {
                                    me.getConfig('storesToLoad').pop(store);

                                    if (me.getConfig('storesToLoad').length <= 0) {
                                        me.abrirFormHijo(eval(item.SoloLectura));
                                    }
                                }
                            }
                        });
                    };
                    i.valueField = 'id';
                    i.displayField = 'descripcion';
                    //i.queryMode = 'local';
                    break;
                case "NumberField":
                    i.xtype = 'numberfield';
                    i.campoMenorQue = item.MenorQueCampo;

                    if (form) {
                        if (item.MenorQueCampo && item.MenorQueCampo != "") {
                            i.listeners = {
                                change: function (field) {
                                    var parentObj = form.down('#' + item.MenorQueCampo);

                                    console.log("item.MenorQueCampo: ", parentObj);
                                    if (parentObj) {
                                        me.__onCheckValorMenorQue(field, parentObj);
                                    }
                                }
                            }
                        }
                    }

                    break;
                case "Lista a Buscar Multiple":
                    var store = Ext.create('Ext.data.Store', {
                        model: 'ProcessManagement.model.ComboIdDescripcion',
                        proxy: {
                            type: 'ajax',
                            //url: 'soap/valoresPosibles',
                            url: 'form/getFieldValues',
                            //url: 'form/getFieldValues',
                            reader: {
                                type: 'json',
                                rootProperty: 'result'
                            },
                            timeout: 500000
                        }
                    });
                    i.queryMode = 'local';
                    i.xtype = 'tagfield';
                    i.valueField = 'id';
                    i.displayField = 'idDescripcion';
                    i.store = store;

                    if (!form) me.getConfig('storesToLoad').push(store);

                    me.getConfig('idFieldsToLoad').push({id: item.Id, xtype: i.xtype});

                    store.load({
                         params: {
                             userId: loginUser.idUser,
                             stepId: me.getView().getViewModel().get('StepId'),
                             token: loginUser.token,
                             fieldId: item.IdField,
                             fieldValue: item.CampoValueCombo,
                             fieldDescription: item.CampoTextCombo
                         },
                        callback: function () {
                            var query = Ext.String.format('{0}[name={1}]', i.xtype, item.Id),
                                component = Ext.ComponentQuery.query(query)[0];

                            if (component) component.unmask();

                            if (!form) {
                                me.getConfig('storesToLoad').pop(store);

                                if (me.getConfig('storesToLoad').length <= 0) {
                                    me.abrirFormHijo(eval(item.SoloLectura));
                                }
                            }
                        }
                     });

                    break;
                case "TextArea":
                    i.xtype = 'htmleditor';
                    break;
                case "CheckBox":
                    i.xtype = 'checkbox';
                    i.fieldLabel = null;
                    i.boxLabel = item.TextoFrontEnd;
                    break;
                case "Form":
                    var formHijo = {};

                    i = {
                        xtype: 'fieldcontainer',
                        fieldLabel: (item.TextoFrontEnd != null && item.TextoFrontEnd != "")?item.TextoFrontEnd:null,
                        labelClsExtra: 'labelField',
                        items: [{
                                xtype: 'button',
                                iconCls: 'x-fa fa-file-text-o',
                                userCls: 'circular',
                                xtype: 'button',
                                tooltip: item.TextoFrontEnd,
                                handler: function () {
                                    me.cargarDatosFormHijo(item.IdFormularioHijo, item.IdField, eval(item.SoloLectura));
                                }
                            },
                            {
                                xtype: 'hiddenfield',
                                itemId: item.Id,
                                name: item.Id,
                                value: item.IdFormularioHijo,
                                isFormField: true
                            }
                        ],
                        seccion: item.Seccion
                    }

                    formHijo = {
                        id: item.IdFormularioHijo,
                        readOnly: eval(item.SoloLectura)
                    }
                    me.getConfig('formsHijo').push(formHijo);
                    break;
                case "Radio Button":
                    var value = {};
                    value[item.Id] = item.ValorActual;

                    i = {
                        xtype: 'radiogroup',
                        allowBlank: false, //!eval(item.Obligatorio),
                        fieldLabel: item.TextoFrontEnd,
                        posicion: item.Posicion,
                        columns: 2,
                        layout: {
                            type: 'column'
                        },
                        columnWidth: 1,
                        idField: item.Id,
                        items: [
                            {
                                bind: {boxLabel: '{txtFields.words.yes}'},
                                inputValue: 'si',
                                name: item.Id,
                                labelClsExtra: 'labelField'
                            },
                            {
                                boxLabel: 'No',
                                inputValue: 'no',
                                name: item.Id,
                                padding: '0 0 0 15',
                                labelClsExtra: 'labelField'
                            }
                        ],
                        value: value,
                        labelClsExtra: 'labelField',
                        seccion: item.Seccion
                    }
                    break;
                default:
                    i.xtype = 'textfield';
            }
            if (i.xtype == "multiselect" && i.data == null) {
                i.height = 100;
            }

        } catch (e) {
            alert(e);
            console.log("Item que da error: ", item);
            console.log(e);
        }
        return i;
    },

    /**
     * Comprueba que un campo del formulario cumpla con la regla de tener un valor Menor Que otro campo
     * Esta regla viene en el servicio que se ejecuta para obtener un formulario
     *
     * @param childObj  Campo que debe cumplir la regla
     * @param parentObj Campo que se usa como base para comparar los valores
     * @private
     */
    __onCheckValorMenorQue: function (childObj, parentObj) {
        if (parentObj.getValue() == null || parentObj.getValue() == "") return;

        if (childObj.getValue() > parentObj.getValue()) {
            var str = Ext.String.format(txtFields.messages.fieldXLessThanY, childObj.fieldLabel, parentObj.fieldLabel);
            Ext.Msg.show({
                title: 'Error',
                msg: str,
                //icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK,
                fn: function () {
                    childObj.setValue(null);
                }
            });
        }
    },

    getPosiblesValores: function(listaPV) {
        var retList = [],
            arr = listaPV.replace(/\|\|/g, "\\").split("|");
        Ext.each(arr, function(str) {
            var a = str.split("\\");
            if (Ext.isArray(a)) {
                retList.push({
                    id: a[1],
                    desc: a[0]
                });
            }
        });
        return retList;
    },

	refreshWorkflowsList: function () {
        var sscr = Ext.ComponentQuery.query('searchScreen')[0],
            store = sscr.lookupReference('gridSteps').getStore();
        store.getFilters().removeAll();
        store.getSteps(loginUser, function(error, result) {
            if (error) {
                if (result.error == 401) {
                    Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError);
                } else if (result.error != 0) {
                    Ext.Msg.alert('ERROR', result.message);
                }
            }
        });
        store.getSorters().add({
            property: 'StepId',
            direction: 'DESC'
        });
        store.getFilters().add({
            id: 'fltResponsible',
            property: 'responsable',
            value: loginUser.name
        });
        store.getFilters().add({
            id: 'fltIdStatePendiente',
            property: 'IdState',
            value: '2'
        });
    },

	replaceHtmlEntityCode: function(str) {
        return '&#' + str.charCodeAt(0) + ';';
    },

    getFormFieldValues: function(elForm, aprueba) {
        var me = this;
        var url = "";
        var formItems = elForm.query('field'),
            jsonData = {campo: []};

        Ext.each(elForm.query('htmleditor'), function(fi) {
            formItems.push(fi);
        });

        for (var i in formItems) {
            if (formItems[i].xtype != 'panel' && formItems[i].xtype != "multiplelistbox" && formItems[i].xtype != "fieldset" && formItems[i].xtype != "radio") {
                var campito = {};
                switch (formItems[i].xtype) {
                    case 'datefield':
                        url += formItems[i].name + '=' + formItems[i].getSubmitValue() + '|';
                        campito = {
                            nombre: formItems[i].name,
                            valor: formItems[i].getSubmitValue()
                        }
                        break;
                    case 'htmleditor':
                        var val = formItems[i].getValue();

                        url += formItems[i].name + '=' + val + '|';

                        campito = {
                            nombre: formItems[i].name,
                            valor: val
                        }

                        break;
                    case 'textfield':
                        var val = formItems[i].getValue();

                        url += formItems[i].name + '=' + val + '|';

                        campito = {
                            nombre: formItems[i].name,
                            valor: val
                        }

                        break;
                    case 'combobox':
                        var val = formItems[i].getValue();

                        if (val != null) {
                            if (!Array.isArray(val)) {
                                var tempArr = [];

                                tempArr.push(val.replace(/\u007C/g, me.replaceHtmlEntityCode));
                                val = tempArr; //Lo transformo a array
                            }
                        } else {
                            val = [];
                        }
                        url += formItems[i].name + '=' + val + '|';

                        //console.log("Campo " + formItems[i].name );
                        //console.log(val);
                        campito = {
                            nombre: formItems[i].name,
                            valor: JSON.stringify(val)
                        }

                        break;
                    case 'tagfield':
                        var values = me.getFullSelectedValuesFromTagField(formItems[i]), str = "";

                        Ext.each(values, function(value) {
                            str += Ext.String.format('{0}\\\\{1}\\', value.descripcion.replace(/[\u00A0-\u00FF]/g, me.replaceHtmlEntityCode).replace(/\u007C/g, me.replaceHtmlEntityCode), value.id.replace(/[\u00A0-\u00FF]/g, me.replaceHtmlEntityCode).replace(/\u007C/g, me.replaceHtmlEntityCode));
                        });
                        url += Ext.String.format('H{0}={1}|', formItems[i].name, str);
                        url += Ext.String.format('{0}={1}|', formItems[i].name, str);

                        campito = {
                            nombre: formItems[i].name,
                            valor: JSON.stringify(formItems[i].getValue())
                        }
                        break;
                    case "radiofield":
                        break;
                    default:
                        url += formItems[i].name + '=' + formItems[i].getValue() + '|';

                        campito = {
                            nombre: formItems[i].name,
                            valor: formItems[i].getValue()
                        }

                }
                jsonData.campo.push(campito);
            }
        }

        formItems = elForm.query('radiogroup');
        for (var i in formItems) {
            var item = formItems[i],
                val = "";

            if (item.getValue()[item.idField] != null && item.getValue()[item.idField] != undefined) {
                val = item.getValue()[item.idField];
            }
            url += formItems[i].name + '=' + val + '|';

            campito = {
                nombre: formItems[i].idField,
                valor: val
            }

            jsonData.campo.push(campito);
        }
        url = Ext.String.format('SystemForm={0}|SystemIdStep={1}|SystemIdUser={2}|Apruebo={3}|SoloGrabo={4}|Diseno2={5}|PasosAnterioresSeleccionado={6}|SubmitUrl={7}|idStep={8}|idUser={9}|',
                me.getView().getViewModel().get('idForm'),
                me.getView().getViewModel().get('StepId'),
                loginUser.idUser,
                0,
                1,
                0,
                0,
                null,
                me.getView().getViewModel().get('StepId'),
                loginUser.idUser) + url;

        //return url;
        jsonData.campo.push({nombre: 'SystemForm', valor: me.getView().getViewModel().get('idForm')});
        jsonData.campo.push({nombre: 'SystemIdStep', valor: me.getView().getViewModel().get('StepId')});
        jsonData.campo.push({nombre: 'SystemIdUser', valor: loginUser.idUser});
        jsonData.campo.push({nombre: 'Apruebo', valor: aprueba});
        jsonData.campo.push({nombre: 'SoloGrabo', valor: (aprueba == 0)?1:0});
        jsonData.campo.push({nombre: 'Diseno2', valor: 0});
        jsonData.campo.push({nombre: 'PasosAnterioresSeleccionado', valor: 0});
        jsonData.campo.push({nombre: 'SubmitUrl', valor: null});
        jsonData.campo.push({nombre: 'idStep', valor: me.getView().getViewModel().get('StepId')});
        jsonData.campo.push({nombre: 'idUser', valor: loginUser.idUser});

        jsonData.SystemForm = me.getView().getViewModel().get('idForm');
        jsonData.SystemIdStep = me.getView().getViewModel().get('StepId');
        jsonData.SystemIdUser = loginUser.idUser;
        jsonData.Apruebo = aprueba;
        jsonData.SoloGrabo = (aprueba == 0)?1:0;
        jsonData.Diseno2 = 0;
        jsonData.PasosAnterioresSeleccionado = 0;
        jsonData.SubmitUrl = null;
        jsonData.idStep = me.getView().getViewModel().get('StepId');
        jsonData.idUser = loginUser.idUser;
        jsonData.languageId = 1;
        jsonData.enabledMessages = 1;
        jsonData.enabledEmails = 1;

        jsonData.childForms = me.getView().getViewModel().get('childForms');
        return jsonData;
        //this.__captureForm(url, "grabar");
    },

    __validarCampoMenorQue: function (elForm) {
        var fieldsArray = elForm.query('datefield, numberfield'),
            valid = true;

        if (fieldsArray && fieldsArray.length > 0) {
            for (var i in fieldsArray) {
                if (fieldsArray[i].campoMenorQue && fieldsArray[i].campoMenorQue != "") {
                    var mayor = elForm.down('#' + fieldsArray[i].campoMenorQue);
                    if (mayor) {
                        if (fieldsArray[i].getValue() > mayor.getValue()) {
                            var str = Ext.String.format(txtFields.messages.fieldXLessThanY, fieldsArray[i].fieldLabel, mayor.fieldLabel);
                            Ext.Msg.show({
                                title: 'Error',
                                msg: str,
                                //icon: Ext.Msg.ERROR,
                                buttons: Ext.Msg.OK,
                                fn: function () {
                                    //childObj.setValue(null);
                                }
                            });
                            valid = false;
                            break;
                        }
                    }
                }
            }
        }

        return valid;
    },

    onGrabar: function(button, e, options) {
        var me = this,
            elForm = button.up('form'),
            url = "";
        url = me.getFormFieldValues(button.up('form'), 0);
        console.log('OnGrabar: ', url)
        this.__captureForm(url, "grabar");
    },
    onGrabarEnviar: function(button, e, options) {
        var me = this,
            url = {},
            elForm = button.up('form'),
            menorQueValidado = this.__validarCampoMenorQue(elForm);

        if (!menorQueValidado)  {
            return;
        }

        if (!elForm.isValid()) {
            Ext.Msg.show({
                title: 'Error',
                msg: txtFields.messages.fillRequiredFields,
                //icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
            return;
        }

        url = me.getFormFieldValues(elForm, 1);
        this.__captureForm(url, "grabarEnviar");

    },
    getFullSelectedValuesFromTagField: function(tagField) {
        var result = [],
            store = tagField.getStore(),
            valuesArray = tagField.getValue();
        Ext.each(valuesArray, function(value) {
            var record = store.findRecord('id', value),
                t = (record) ? {
                    id: record.get('id'),
                    descripcion: record.get('descripcion')
                } : null;
            if (t) {
                result.push(t);
            }
        });
        return result;
    },
    __captureForm: function(v_fieldsStr, button) {

        if (appParams["verForm"]) return;

        var me = this,
            url = 'form',
            timeout = 500000,
            params = {
                userId: loginUser.idUser,
                fields: v_fieldsStr,
                token: loginUser.token,
                languageId: 1,
                enabledMessages: 1,
                enabledEmails: 1
            };
        console.log('__captureForm: ', v_fieldsStr);
        //return;
        Ext.getBody().mask(txtFields.messages.savingData + '....');
        Ext.Ajax.request({
            url: url,
            timeout: timeout,
            //jsonData: params, //Esto es necesario si los campos se envian como un string separado por un |
            jsonData: v_fieldsStr, //Esto es necesario si los campos se envian como un json
            method: 'POST',
            success: function(conn, r, opts) {
                me.savedForm = true;
                Ext.getBody().unmask();
                console.log("Respuesta CaptureForms: ", conn, r);
                r = Ext.JSON.decode(conn.responseText);
                if (r.success) {
                    Ext.Msg.alert(txtFields.words.confirm, 
                        txtFields.messages.saveSuccessfull + '. ' + r.comment, function() {
                        if (button == "grabarEnviar") {
                            //me.returnTab();
                            var screen = Ext.ComponentQuery.query('requestScreen')[0];
                            screen.getController().returnTab();
                            me.mostrarGraficoWkf();
                        }
                    });
                } else {                    
                    Ext.Msg.alert('Error', txtFields.messages.saveUnsuccessful);
                }
            },
            failure: function(conn, r, opts) {
                Ext.getBody().unmask();
                Ext.Msg.alert('Error', txtFields.messages.saveUnsuccessful);
            }
        });
    },

    mostrarGraficoWkf: function () {
        if (appParams["verForm"]) {
            var me = this, url = globalParams.urlForms + '?p_loggedUserId=' + loginUser.idUser + '&p_token=' + loginUser.token +
                '&p_stepId=' + me.getView().getViewModel().get('StepId') + '&p_verificarPermisoUsuario=1&p_mostrarHeader=false&p_soloGraficoWkf=true';
            Ext.getCmp('main').controller.openModalPopup(url);
        }
    },

    rechazarWkf: function(button, e, options) {

        if (appParams["verForm"]) return;

		var me = this
		/*Ext.getCmp('main').controller*/this.openRejectForm({
			action: 'rechazo',
			title: Ext.String.format('{0} {1}?', txtFields.titles.rejectStep, me.getView().getViewModel().get('StepTxt')),
			workflowId: me.getView().getViewModel().get('WorkflowId'),
			stepId: me.getView().getViewModel().get('StepId')
		})
    },
    cancelarWkf: function(button, e, options) {

        if (appParams["verForm"]) return;

        var me = this
		/*Ext.getCmp('main').controller*/this.openRejectForm({
			action: 'cancelar',
			title: Ext.String.format('{0} {1} ?', txtFields.titles.rejectStep, me.getView().getViewModel().get('StepTxt')),
			workflowId: me.getView().getViewModel().get('WorkflowId'),
			stepId: me.getView().getViewModel().get('StepId')
		})
    },

    cargarDatosFormHijo: function (idFormHijo, idFieldInParentForm, campoEsReadOnly) {
	    var me = this,
            formEsReadOnly = me.isWorkflowTerminated() || campoEsReadOnly;

        me.getView().mask(txtFields.messages.wait + '...');

        me.getFormFieldsFromService(idFormHijo, true)
            .then(
                function (response) {
                    var formFields = [],
                        storeFields = [],
                        storeRecords = [],
                        columns = [],
                        store = null,
                        obj = {};//Esto es temporal, hasta arreglar el sp que devuelve campos de un form hijo

                    me.getView().unmask();

                    Ext.each(response.result.secciones, function (seccion) {
                        Ext.each(seccion.items, function (item) {
                            formFields.push(item);
                        })
                    });


                    Ext.each(formFields, function (formField) {
                        var editor = me.getExtJSItem(formField, null),
                            column = {
                                header: formField.TextoFrontEnd,
                                dataIndex: formField.Id,
                                flex: 1
                            };

                        editor.fieldLabel = null;
                        editor.readOnly = formEsReadOnly;
                        console.log("Editor.value: ", editor.value);
                        column.editor = editor;

                        switch (editor.xtype) {
                            case "datefield":
                                //column.type = 'date';
                                column.renderer = function (value, metadata, record) {
                                    var formatedDate = null;//

                                    if (value != null && value != "") {
                                        if (!Ext.isDate(value)) {
                                            //formatedDate = Ext.Date.format(Ext.Date.parse(value, "c"), "d/m/Y"); //Ext.util.Format.date(field, 'd/m/Y');
                                            formatedDate = Ext.Date.parse(value, "c");
                                            formatedDate = Ext.Date.format(formatedDate, "d/m/Y");
                                        } else {
                                            formatedDate = Ext.Date.format(value, "d/m/Y"); //Ext.util.Format.date(field, 'd/m/Y');
                                        }
                                    }

                                    console.log("value - formatedDate, record: ", value, formatedDate, record);
                                    return formatedDate;
                                }
                                break;
                            case "combobox":
                                column.renderer = function (field, p2, p3) {
                                    try {
                                        var idx = editor.store.find('id', field);

                                        if (idx != -1) {
                                            return editor.store.getAt(idx).get('descripcion');
                                        } else {
                                            return "";
                                        }
                                    } catch (e) {
                                        return "";
                                    }
                                }
                                break;
                            case "tagfield":
                                column.renderer = function (valuesArray) {
                                    var store = editor.store;

                                    if (valuesArray != null) {
                                        var str = "";
                                        Ext.each(valuesArray, function (value) {
                                            var idx = store.find('id', value);

                                            if (idx != -1) {
                                                str += Ext.String.format("{0}<br>", store.getAt(idx).get('descripcion'));
                                            } else {
                                                str += "<br>";
                                            }
                                        });

                                        return str;
                                    }
                                    return "";
                                }
                                break;
                        }

                        columns.push(column);
                        if (editor.xtype == "datefield") {
                            storeFields.push({name: formField.Id, type: 'date'})
                        } else
                        {
                        storeFields.push(formField.Id);
                        }
                        obj[formField.Id] = formField.ValorActual;

                    });

                    storeFields.push('sequence'); //Este campo se agrega a mano y representa el nro de cada linea del detalle
                    storeFields.push('DELETEFIELD'); //Este campo se agrega a mano y define si se debe eliminar o no una linea de detalle

                    //Agrega la columna con el boton Eliminar
                    columns.push({
                        xtype: 'actioncolumn',
                        menuDisabled: true,
                        flex: 0.4,
                        align: 'center',
                        items: [
                            {
                                iconCls: 'x-fa fa-trash',
                                handler: 'onRemoveDetailRow',
                                disabled: formEsReadOnly
                            }
                        ]
                    });

                    var formHijoConfig = {
                        title: response.result.id,
                        idFormHijo: idFormHijo,
                        idFieldInParentForm: idFieldInParentForm,
                        columns: columns,
                        storeFields: storeFields
                    }

                    me.getView().getViewModel().set('formHijoConfig', formHijoConfig);

                    //Si no hay stored en la pantalla, se carga directamente
                    if (me.getConfig('storesToLoad').length <= 0) {
                        console.log("2:", formEsReadOnly)
                        me.abrirFormHijo(formEsReadOnly);
                    }
                },
                function (response) {
                    me.getView().unmask();

                }
            );
    },

    abrirFormHijo: function (formEsReadOnly) {
        var me = this,
            w = Ext.create('ProcessManagement.view.popup.ChildForm'),
            grid = w.lookup('gridCamposFormHijo'),
            store = null,
            formHijoConfig = me.getView().getViewModel().get('formHijoConfig');

        formEsReadOnly = false;

        store = Ext.create('Ext.data.Store', {
            fields: formHijoConfig.storeFields,
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootPropert: 'sequence'
                }
            }
        });
        w.getViewModel().set('idForm', formHijoConfig.idFormHijo);
        w.getViewModel().set('idField', formHijoConfig.idFieldInParentForm);
        w.getViewModel().set('WorkflowId', me.getView().getViewModel().get('WorkflowId'));
        w.getViewModel().set('IdState', me.data.IdState);

        Ext.each(me.getConfig('formsHijo'), function (obj) {
            if (obj.id == formHijoConfig.idFormHijo) {
                formEsReadOnly = obj.readOnly;
            }
        })
        console.log("readOnly: ", formEsReadOnly);

        w.getViewModel().set('formEsReadOnly', formEsReadOnly);

        grid.reconfigure(store, formHijoConfig.columns);
        w.setTitle(formHijoConfig.title); //Titulo del form que viene en la respuesta del servicio
        //grid.setTitle(formHijoConfig.title); //Titulo del form que viene en la respuesta del servicio
        w.show();

        //Invoca al servicio que obtiene los valores
        w.getController().loadChildFormValues();

    },

    onAcceptChildForm: function (childData) {
        var seq = 1,
            childFormsData = [];

        if (childData !== null && typeof childData !== undefined) {
            var cd = {
                    sequence: seq,
                    campos: []
                };

            Ext.each(childData.rows, function (row) {
                var c = {};
                Ext.each(Object.keys(row), function (key) {

                    c[key] = row[key];

                });

                c.System_Sequence = c.sequence;//seq;
                cd.campos.push(c);

                seq += 1;
            });
            childFormsData.push(cd);
        }

        this.getView().getViewModel().set('childForms', childFormsData);
        if (!this.isWorkflowTerminated()) this.onGrabar(this.lookup('btnGrabar'));
        //console.log("Asi queda el childForm: ", this.getView().getViewModel().get('childForms'));
        this.getView().getViewModel().set('childForms', []);
    },

    openRejectForm: function(data) {

        if (appParams["verForm"]) return;

        var win = this.lookupReference('rejectPopup');

        if (!win) {
            win = new ProcessManagement.view.popup.RejectForm({
                fileData: data
            });
            this.getView().add(win);
        }
        win.show()
    },

    printForm: function () {
        var me = this,
            form = this.lookupReference('SGPForm'),
            secciones = form.query('panel'),
            jsonForm = {
                secciones: []
            };


        console.log("En printForm, data: ", me.data);
        console.log("En printForm, viewModel: ", me.getView().getViewModel());

        Ext.getBody().mask(txtFields.messages.savingData + '....');
        Ext.Ajax.request({
            url: 'form/generatePDF',
            timeout: 50000,
            params: {
                idForm: me.getView().getViewModel().get('idForm'),
                workflowId: me.getView().getViewModel().get('WorkflowId'),
                userId: loginUser.idUser,
                workflow: me.data.WorkFlow,
                proceso: me.data.proceso,
                stepTxt: me.data.StepTxt
            },
            method: 'GET',
            success: function (response) {
                Ext.getBody().unmask();
                response = Ext.JSON.decode(response.responseText, true);
                if (response == null) {
                    response = {
                        success: false,
                        message: txtFields.messages.errors.gettingDataError
                    }
                }

                if (!response.success) {
                    Ext.Msg.alert('ERROR', txtFields.messages.errors.gettingFormId);
                } else {
                    window.open('form/PDF?fileId=' + response.fileId)
                }

            }, failure: function () {
                Ext.Msg.alert('ERROR', txtFields.messages.errors.gettingFormId);
                Ext.getBody().unmask();
            }
        });
    },
	
	isSectionVisible: function(items) {
		var visible = false
		for(var i=0; i < items.length;) {
			if(items[i].xtype != 'panel' && !items[i].hidden) {
				visible = true; i = items.length;
			} else i++
		}
		return visible
    },
    
    //METODOS PARA LA DEMO DE CMPC

    aprobarPasoCMPC: function() {
        var data = this.getView().items.items[0].dataScreen
        console.log(data)
        Ext.getBody().mask(txtFields.messages.savingData + '....');
        Ext.Ajax.request({
            url: 'pasosWorkflow/aprobarPaso',
            timeout: 50000,
            params: {
                userId: loginUser.idUser,
                stepId: data.WorkflowData.StepId,
                approveOrReject: 1,
                tipoRechazo: 0,
                pasoAnterior: '',
                motivoRechazo: ''
            },
            method: 'GET',
            success: function (response) {
                console.log(response)
                response = JSON.parse(response.responseText)
                if(response.success) {
                    Ext.getBody().unmask();
                    var screen = Ext.ComponentQuery.query('requestScreen')[0];
                    screen.getController().returnTab();
                }
            },
            failure: function () {
                Ext.Msg.alert('ERROR', txtFields.messages.errors.gettingFormId);
                Ext.getBody().unmask();
            }
        })
    },

    rechazarPasoCMPC: function() {
        var me = this, data = this.getView().items.items[0].dataScreen
        console.log(data)
		this.openRejectForm({
			action: 'rechazo',
			title: Ext.String.format('{0} {1}?', txtFields.titles.rejectStep, me.getView().getViewModel().get('StepTxt')),
			workflowId: data.WorkflowData.WorkflowId,
			stepId: data.WorkflowData.StepId
		})
    },

    imprimirCMPC: function() {
        var url = 'http://sgp.simbius.com:8888/fcmpc/getWord.aspx?&output=word&Tipo=proyecto&codigo=358'
        window.open(url);
    }
})
