Ext.define('ProcessManagement.view.screen.DashboardController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.dashboard',
	
	initScreen: function() {
        console.log('Iniciando dashboard...')
        Ext.getCmp('main').controller.setTheme('dashboard')
		this.getKPIs()
	},
	
	refreshScreen: function() {
		var panel = Ext.getCmp('indicatorPanel')
		panel.removeAll()
        panel.add(Ext.create({
			xtype: 'fieldcontainer',
			layout: 'hbox',
			defaults: {flex: 1},
			items: []
		}))
		this.initScreen()
	},
	
	getKPIs: function() {
        var controller = this
		var store = new ProcessManagement.store.KPIs()
        console.log(loginUser)
        //Ext.getCmp('main').controller.getView().getEl().mask('Armando KPIs...');
        store.getKPI(loginUser, function(error, result) {
            if(!error) {
                console.log(result)
                var records = result.response				
                for(var i = 0; i < records.length; i++) {
                    records[i].data.Icon = records[i].data.Icon == '' ? 'x-fa fa-tag' : records[i].data.Icon
                    records[i].data.KPI_CAMPO = undefined
                    controller.addKPI(controller.createKPI(records[i].data))
                }
                //Ext.getCmp('main').controller.getView().getEl().unmask();
				//if(records.length == 0)
					//Ext.window.Toast({ html: globalParams.lang == 'es' ? 'No tiene indicadores' : 'No bookmarks...', align: 't', slideInDuration: 200 });
            } else {
                if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
                //Ext.getCmp('main').controller.getView().getEl().unmask();
            }
        })
    },

    reorganizer: function(obj, width, height, oldWidth, oldHeight) {
		console.log('reorganizer... width: ' + width + ', height: ' + height + ', oldWidth: ' + oldWidth + ', oldHeight: ' + oldHeight)
        var kpis = Ext.ComponentQuery.query('component[xtype="kpi"]')
        if(kpis.length > 0) {
            var panel = Ext.getCmp('indicatorPanel')
            var container = Ext.create({
                xtype: 'fieldcontainer',
                layout: 'hbox',
                defaults: {flex: 1},
                items: []
            })
            var items = [container]
            var space = Ext.getBody().getViewSize().width
            var widthKPI = kpis[0].width
            kpis.forEach(function (kpi) {
                if (space >= widthKPI) {
                    container.add(kpi)
                    space -= widthKPI
                } else {
                    space = Ext.getBody().getViewSize().width - widthKPI
                    container = Ext.create({
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        defaults: {flex: 1},
                        items: [kpi]
                    })
                    items.push(container)
                }
            })
            panel.removeAll()
            items.forEach(function(c) { panel.add(c) })
        }
    },

    addKPI: function(kpi) {		
        var panel = Ext.getCmp('indicatorPanel')
        if(panel) {
            var lastContainer = (panel.items) ? panel.items.items[panel.items.items.length -1] : null
            var items = lastContainer ? lastContainer.items.items : null
            var space = 0
            var widthKPI = 0
            items.forEach(function(kpi) {
                widthKPI = kpi.width + 10
                space += widthKPI
            })
            space = Ext.getBody().getViewSize().width - space
            if(space >= widthKPI) lastContainer.add(kpi)
            else {
                var container = Ext.create({
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    defaults: { flex: 1 },
                    items: [ kpi ]
                })
                panel.add(container)
            }
        }
    },
    
    createKPI: function(data) {
        //console.log('CREATE KPI: ', data)
        return {
            xtype: 'kpi',
            //id: 'wgt_' + data.WorkflowID,
            title: data.NombreWorkflow,
            iconCls: data.Icon,
            viewModel: {
                title1: txtFields.words.pendings, value1: data.KPI_PENDIENTE,                
                title2: txtFields.words.expirated, value2: data.KPI_AVG_DIAS,
                title3: txtFields.words.finished, value3: data.KPI_TERMINADO,
                titleField: data.KPI_CAMPO,
                title4: txtFields.words.sum, value4: data.KPI_SUM,
                title5: txtFields.words.average, value5: data.KPI_AVG
            }
        }
    },

    addKPITest: function() {
        this.addKPI(this.createKPI({
            NombreWorkflow: 'KPI Test',
            Icon: 'x-fa fa-tag',
            KPI_PENDIENTE: 0,
            KPI_TERMINADO: 0,
            KPI_AVG_DIAS: 0,
            KPI_CAMPO: 'Field/Campo',
            KPI_SUM: 0,
            KPI_AVG: 0
        }))
    }
	
})