Ext.define('ProcessManagement.view.screen.SearchController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.search',

    config: {
        filter: null,
        dates: null
    },

    init: function () {
        this.dates = ProcessManagement.util.Date;
        this.filter = { field: 'Referencia' };
    },

    initScreen: function() {
        this.getSteps(function(error, store, callback) { if(!error){ if (callback) { callback(); } } })
    },

    refreshScreen: function() {
        //this.removeFilters();
        this.initScreen();
    },

    viewSearchPanel: function() {
        var group = this.lookupReference('filterPanel');
        group.setVisible(!group.isVisible());
    },

    removeFilters: function() {
        this.setterFields()
        var store = this.lookupReference('gridSteps').getStore()
		store.getSorters().removeAll()
        store.getFilters().removeAll()
        store.getSorters().add({ property: 'StepId', direction: 'DESC' })
        store.getFilters().add({id: 'fltResponsible', property: 'responsable', value: loginUser.name});
		//this.lookupReference('btSearch').setText('')
		this.lookupReference('searchField').setValue('');
    },

    setterFields: function() {
        var btn = Ext.getCmp('state' + this.lookupReference('sgmStates').getValue())
        if(btn) btn.setPressed(false)

        var btn = Ext.getCmp('date' + this.lookupReference('sgmDates').getValue())
        if(btn) btn.setPressed(false)
        this.lookupReference('dateFieldFrom').setValue('')
        this.lookupReference('dateFieldTo').setValue('')

        Ext.getCmp('resp0').setPressed(true)
        this.lookupReference('txtResponsible').setValue('')
    },

    openRequest: function(grid, rowIndex) {
		var data = grid.getStore().getAt(rowIndex).data
		data.back = 'search'
        Ext.getCmp('main').controller.setCurrentView('request', data)
    },

    openModel: function(grid, rowIndex) {
		console.log('Abriendo modelo...')
		Ext.getCmp('main').controller.openPopup('workflowModel', grid.getStore().getAt(rowIndex).data)
    },

// GETTERS ----

    getSteps: function(callback) {
        var store = this.lookupReference('gridSteps').getStore()
        store.getSteps(loginUser, function(error, result) {
            if(error) {
                if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
                callback(true, null)
            } else callback(false, result.response)
        })
        store.getSorters().add({ property: 'StepId', direction: 'DESC' })
        store.getFilters().add({id: 'fltResponsible', property: 'responsable', value: loginUser.name});
    },

// FILTERS ----

    onShowFilters: function () {
        var data = [];
        this.lookupReference('gridSteps').getStore().getFilters().each(function (filter) {
            data.push(filter.serialize());
        });
        data = Ext.JSON.encodeValue(data, '\n').replace(/^[ ]+/gm, function (s) {
            for (var r = '', i = s.length; i--; ) { r += '&#160;'; }
            return r;
        });
        data = data.replace(/\n/g, '<br>');
        Ext.Msg.alert('Filter Data', data);
    },

    changeFilter: function(item, checked) {
        this.filter.field = item.id;
        this.lookupReference('btSearch').setText(item.text)
        this.lookupReference('searchField').setValue('');
    },

    findRequest: function(o, record) {
        var store = this.lookupReference('gridSteps').getStore()
        store.getFilters().add({id: 'fltDefault', property: this.filter.field, 
            operator: '/=', value: record})
    },

    filterStates: function(o, btn){
        var store = this.lookupReference('gridSteps').getStore()
        store.getFilters().add({id: 'fltState', property: 'Estado', value: btn.name})
    },

    changeDates: function(o, btn) {
        this.filter.field = btn.name
        var str = 'Inicio'
        if(btn.name == 'endDate')
            str = 'Fin'
        var field = this.lookupReference('dateFieldFrom')
        field.setValue('')
        field.setEmptyText(str + ' desde...')
        field = this.lookupReference('dateFieldTo')
        field.setValue('')
        field.setEmptyText(str + ' hasta...')
        var store = this.lookupReference('gridSteps').getStore()
        var fltr = store.getFilters().get('fltDateInit')
        if(fltr) store.getFilters().remove(fltr)
        fltr = store.getFilters().get('fltDateEnd')
        if(fltr) store.getFilters().remove(fltr)
    },

    filterDates: function(o, p) {
        var controller = this
        if(this.filter.field != 'startDate' && this.filter.field != 'endDate') this.filter.field = 'startDate'
        var store = this.lookupReference('gridSteps').getStore()
        if(o.reference == 'dateFieldFrom') {
            store.getFilters().add(new Ext.util.Filter({
                id: 'fltDateInit',
                filterFn: function(item) {
                    var field = item.data.startDate
                    if(controller.filter.field == 'endDate')
                        field = item.data.endDate
                    //return controller.dates.stringToDate(field) > p;
                    return field >= p;
                }
            }))
        } else {
            store.getFilters().add(new Ext.util.Filter({
                id: 'fltDateEnd',
                filterFn: function(item) {
                    var field = item.data.startDate
                    if(controller.filter.field == 'endDate')
                        field = item.data.endDate
                    //return controller.dates.stringToDate(field) < p;
                    return field <= p;
                }
            }))
        }
    },

    filterStartDate: function(o, btn) {
        var store = this.lookupReference('gridSteps').getStore();
        this.filter.field = 'startDate';
        var controller = this;
        var fltr = store.getFilters().get('fltDate');
        if(fltr) store.getFilters().remove(fltr);
        if(btn.name == 'hoy') {
			console.log('Filtro hoy: ', Ext.Date.parse(controller.dates.getToday(), "d/m/Y"))
            store.getFilters().add({id: 'fltDate', property: 'startDate', value: Ext.Date.parse(controller.dates.getToday(), "d/m/Y")});
        } else if(btn.name == 'semana') {
            var fechas = controller.dates.getWeek();
			console.log('Filtro semana: ', fechas[0], fechas[1])
            store.getFilters().add(new Ext.util.Filter({
                id: 'fltDate',
                filterFn: function(item) {
                    return Ext.Date.between(item.data.startDate, fechas[0], fechas[1]);
                }
            }))			
        } else if(btn.name == 'mes') {
			var fechas = this.dates.getMonth();
			console.log('Filtro mes: ', fechas[0], fechas[1])
            store.getFilters().add(new Ext.util.Filter({
                id: 'fltDate',
                filterFn: function(item) {
                    return Ext.Date.between(item.data.startDate, fechas[0], fechas[1]);
                }
            }))
        } else {
			store.getFilters().remove('fltDate')
		}
    },

    filterResponsible: function(o, btn) {
        var store = this.lookupReference('gridSteps').getStore()
        if(btn instanceof Object) {
            this.lookupReference('txtResponsible').setValue('')
            if(btn.name == 'Todas') {
                var fltr = store.getFilters().get('fltResponsible')
                if(fltr) store.getFilters().remove(fltr)
            } else {
                store.getFilters().add({id: 'fltResponsible', property: 'responsable', value: loginUser.name})
            }
        } else {
            if(btn == '') store.getFilters().add({id: 'fltResponsible', property: 'responsable', value: loginUser.name})
            else {
                var resp = Ext.getCmp('resp' + this.lookupReference('sgmResponsible').getValue())
                if(resp) resp.setPressed(false)
                store.getFilters().add({id: 'fltResponsible', property: 'responsable', value: btn})
            }
        }
    },

    testModern: function() {
        console.log(loginUser)
    }




});