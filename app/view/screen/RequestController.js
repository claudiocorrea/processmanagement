Ext.define('ProcessManagement.view.screen.RequestController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.request',

    config: {
        data: null
    },

    finishedStatesIds: ['3', '4', '8'], //Estados que representan un wkf terminado

    init: function () {
        console.log('Iniciando request screen...')
        Ext.getCmp('main').controller.setTheme('request')
        this.data = this.getView().config.data
        if (appParams['hideToolbar']) {
            this.lookup('formToolbar').setVisible(false);
        }
		console.log(this.data)
    },

    initScreen: function() {
        console.log('Init screen ', this.data)
        if(this.data.back == 'dashboard' || this.data.back == 'dashboardCMPC') {
            this.data.WorkflowId = this.data.WorkflowID
            delete this.data.WorkflowID
            this.data.WorkFlow = this.data.Workflow
            delete this.data.Workflow
        }        
		
		this.lookupReference('btnNewFile').setHidden(!this.data.AllowDocs)

        var controller = this
        if(this.data) {            
            if(this.data.WorkflowIDoriginal == "7235") {
                this.setPantallasCMPC()
            } else { 
                this.lookupReference('requestTitle').setText(this.data.WorkFlow +
                    ((this.data.proceso) ? ' - ' + this.data.proceso + ' - ' + this.data.StepTxt : ''))
                Ext.getCmp('formPanel').controller.initScreen(this.data) 
            }
            
            this.getFiles(function () { controller.getComments() })
        }
    },

    refreshScreen: function() {
        var controller = this
        this.getFiles(function () { controller.getComments() })
		Ext.getCmp('formPanel').controller.refreshScreen()
    },

    returnTab: function() {
        if (appParams["verForm"] == null || typeof appParams["verForm"] == undefined) {
            Ext.getCmp('main').controller.setCurrentView(this.data.back)
        }
    },

    visiblePanels: function() {
        var hidden = this.lookupReference('extraPanels').hidden
        this.lookupReference('extraPanels').setHidden(!hidden)
    },

// FILES ----

    selectionFile: function(grid, record) {
        console.log("selectionFile: ", this.data);
        this.lookupReference('btnDownloadFile').setHidden(true);
        this.lookupReference('btnDeleteFile').setHidden(true);
        if(record[0] != undefined) {
            record = record[0].data;
            console.log("File data: ", record);
            this.lookupReference('btnDownloadFile').setHidden(false);
            //if (this.data.StepId == record.StepId && record.IdUser == loginUser.idUser)
                //this.lookupReference('btnDeleteFile').setHidden(false);
            //else 
            if (record.IdUser == loginUser.idUser || loginUser.profile == 1)
                this.lookupReference('btnDeleteFile').setHidden(false);
        }

        if (this.finishedStatesIds.includes(this.data.IdState)) {
            this.lookupReference('btnDeleteFile').setHidden(true);
        }
    },

    newFile: function() {
        var wkfId = null,
            stepId = null;

        if  (appParams["verForm"]) {
            wkfId = appParams["workflowId"];
            stepId = appParams["stepId"];
        } else {
            wkfId = this.data.WorkflowId;
            stepId = this.data.StepId;
        }

        var controller = this
        new Ext.Promise(function(resolve, reject) {
			Ext.Ajax.request({
		            url: globalParams.urlServices + 'queries/workflow/getNewFileName',
					method: 'GET',
		            params: {
                        idWorkflow: wkfId
                    }
		        }).then (function(response, opts) {
                    console.log('SUCCESS: ', response)
                    /*Ext.getCmp('main').controller*/
                    controller.openUploadFile({ id: wkfId, stepId: stepId, fileName: JSON.parse(response.responseText)[0].NOMBRE_ARCHIVO })
		        },
		        function(response, opts) {
		        	console.log('FAIL: ', response)
		        });
		});
        
    },

    openUploadFile: function(data) {
        var win = this.lookupReference('uploadPopup');
        if (!win) {
            win = new ProcessManagement.view.popup.Upload({
                fileData: data
            });
            this.getView().add(win);
        }
        win.show()
    },

    getFiles: function(callback, workflowId) {
        var store = this.lookupReference('gridFiles').getStore();
        workflowId = (workflowId) ? workflowId : this.data.WorkflowId;

        if (appParams["verForm"]) {
            workflowId = appParams["workflowId"];
        }

        store.getFiles(loginUser, workflowId, function(error, result) {
            if(error) {
                if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
            }
            if(callback)
                callback()
        })
    },

    downloadFile: function(btn) {
        var grid = this.lookupReference('gridFiles')
        var rowIndex = grid.selModel.selectionStartIdx
        var store = grid.getStore()
        new Ext.Promise(function(resolve, reject) {
            Ext.Ajax.request({
                    url: 'tareasPersonales/downloadFileFromURL',
                    method: 'GET',
                    params: {
                        fileUrl: store.getAt(rowIndex).data.Url,
                        fileName: store.getAt(rowIndex).data.Archivo
                    }
                }).then (function(response, opts) {
                    console.log('SUCCESS: ', response)
                    window.open(JSON.parse(response.responseText).path)
                },
                function(response, opts) {
                    response = JSON.parse(response.responseText)
                    console.log('FAIL: ', response)
                    if(response.error.includes(ENOENT)) Ext.Msg.alert('ERROR', 'Archivo dañado!')
                    else Ext.Msg.alert('ERROR', response.error)
                });
        });
        
    },

    removeFile: function (btn, event) {
        var controller = this;
        Ext.Msg.confirm(txtFields.words.confirm, txtFields.messages.confirmRemoveFile, function(btn) {
            if (btn == 'yes') {
                var grid = controller.lookupReference('gridFiles'),
                    rowIndex = grid.selModel.selectionStartIdx,
                    store = grid.getStore(),
                    wkfId = null;

                if (appParams["verForm"]) {
                    wkfId = appParams["workflowId"];
                } else {
                    wkfId = controller.data.WorkflowId;
                }

                store.removeFile(loginUser, wkfId, rowIndex, grid, function(error, result) {
                    if(error) {
                        if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                        else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
                    }
                });
            }
        });
    },

// COMMENTS ----

    selectionComment: function(grid, record) {
        this.lookupReference('btnDeleteComment').setHidden(true)
        if(record[0] != undefined) {
            record = record[0].data
            if (record.Description.substring(0, record.UserName.length) != record.UserName) { //Mensaje de Sistema
                if (record.StepId)
                    if (this.data.StepId == record.StepId && record.IdUser == loginUser.idUser)
                        this.lookupReference('btnDeleteComment').setHidden(false)

                if (this.lookupReference('btnDeleteComment').hidden == true)
                    if (loginUser.profile == 1)
                        this.lookupReference('btnDeleteComment').setHidden(false)
            }
        }

        if (this.finishedStatesIds.includes(this.data.IdState)) {
            this.lookupReference('btnDeleteComment').setHidden(true);
        }
    },

    newComment: function() {
        var controller = this,
            wkfId = null,
            stepId = null;

        if  (appParams["verForm"]) {
            wkfId = appParams["workflowId"];
            stepId = appParams["stepId"];
        } else {
            wkfId = controller.data.WorkflowId;
            stepId = controller.data.StepId;
        }
        Ext.Msg.show({
            width:300, title: txtFields.words.comment,
            msg: txtFields.messages.writeComment,
            multiline: true,buttons: Ext.Msg.OKCANCEL,
            fn: function(btn, text) {
                if (btn == 'ok') {
                    var grid = controller.lookupReference('gridComments'),
                        store = grid.getStore()
                    store.addComment(loginUser, wkfId, stepId, text, grid, function (error, result) {
                        if (error) {
                            if (result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                            else if (result.error != 0) Ext.Msg.alert('ERROR', result.message)
                        }
                    })
                }
            }
        })
    },

    getComments: function(workflowId, callback) {
        var store = this.lookupReference('gridComments').getStore();
        workflowId = (workflowId)? workflowId : this.data.WorkflowId;

        if (appParams["verForm"]) {
            workflowId = appParams["workflowId"];
        }

        store.getComments(loginUser, workflowId, function(error, result) {
            if(error) {
                if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
            }

            if (callback) callback();
        })
    },

    removeComment: function(btn, event) {
        var controller = this
        Ext.Msg.confirm(txtFields.words.confirm, txtFields.messages.confirmRemoveComment, function(btn) {
            if (btn == 'yes') {
                var grid = controller.lookupReference('gridComments'),
                    rowIndex = grid.selModel.selectionStartIdx,
                    store = grid.getStore(),
                    wkfId = null;

                if (appParams['verForm']) {
                    wkfId = appParams["workflowId"];
                } else {
                    wkfId = controller.data.WorkflowId;
                }

                store.removeComment(loginUser, wkfId, rowIndex, grid, function(error, result) {
                    if(error) {
                        if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                        else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
                    }
                })
            }
        })
    },

    //METODOS PARA LA DEMO DE CMPC

    getData: function(callback) {
        console.log('Cargando datos...')
        Ext.Ajax.request({
            url: './DataFCMPC.json',
        }).then(function (o) {
            //console.log('them... ', o)
            callback(JSON.parse(o.responseText))
        }, function (o) {
            //console.log('error... ', o)
            callback(null)
        });
    },

    setPantallasCMPC: function() {
        var data = null, me = this        
        this.lookupReference('requestTitle').setText(this.data.WorkFlow +
            ((this.data.proceso) ? ' - ' + this.data.StepTxt : ''))
        
        this.getData(function(result) { 
            data = result; 
            data.pantallas.forEach(pantalla => {
                if(pantalla.name == me.data.StepTxt) data = pantalla
            })
            //console.log(data) 
            if(!data.name) alert('No se encontraron los datos de la pantalla')
            data.WorkflowData = me.data
            Ext.getCmp('formPanel').removeAll()
            switch(data.name) {
                case 'Proyecto creado': 
                    Ext.getCmp('formPanel').add(Ext.create({ xtype: 'cmpc-diseñoProyectoScreen', dataScreen: data }))
                    break;
                case 'Adaptar marco logico y actividades': 
                    Ext.getCmp('formPanel').add(Ext.create({ xtype: 'cmpc-generarMarcoLogicoScreen', dataScreen: data }))
                    break;
                case 'Visar marco logico y actividades': 
                    Ext.getCmp('formPanel').add(Ext.create({ xtype: 'cmpc-visarMarcoLogicoScreen', dataScreen: data }))
                    break;
                case 'Actualizar calendario y dotacion': 
                    Ext.getCmp('formPanel').add(Ext.create({ xtype: 'cmpc-sesionesScreen', dataScreen: data }))
                    break;
                case 'Calendarizar proyecto': 
                    Ext.getCmp('formPanel').add(Ext.create({ xtype: 'cmpc-sesionesScreen', dataScreen: data }))
                    break;
                case 'Completar metas anuales': 
                    Ext.getCmp('formPanel').add(Ext.create({ xtype: 'cmpc-anioProyectoScreen', dataScreen: data }))
                    break;
            }
            console.log(Ext.getCmp('formPanel'))
            
            Ext.getCmp('formPanel').dockedItems.items[0].setHidden(false)
        } )
    }
	

})