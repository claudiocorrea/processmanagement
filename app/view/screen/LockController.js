Ext.define('ProcessManagement.view.screen.LockController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.lock',
	
	config: {
		user: null
	},

	init: function () {
		if(Ext.getCmp(globalParams.lang)) Ext.getCmp(globalParams.lang).checked = true
	},

	initScreen: function() {
		var enterprise = ProcessManagement.util.Utils.getQueryVariable('empresa')

		var color = '#1E90FF', favicon = 'favicon', image = 'wallpaper', bigLogo = 'BigLogo';
        switch(enterprise) {
			case '25': 
				color = '#19481A'; bigLogo = ''; break;
			case '49': 
				color = '#3A9AA9'; image = 'wallpaperAMSA'; favicon = 'favicon AMSA', bigLogo = ''; break; 
			case '48': 
				color = '#3AAA35'; image = 'wallpaperFCMPC'; favicon = 'favicon CMPC'; bigLogo = ''; break;            
		}

		document.getElementById('favicon').setAttribute('href', 'resources/images/favicon/' + favicon + '.png')
		if(this.lookupReference('logoLock')) this.lookupReference('logoLock').setVisible(false)
		if(enterprise && bigLogo != '') {
			this.lookupReference('logoLock').setVisible(true)	
			this.lookupReference('logoLock').setHtml('<div><img src="resources/images/logo/' + bigLogo + '.png"></div>')
		}
		if(this.lookupReference('infoPanel'))
			this.lookupReference('infoPanel').setStyle('background-image', 'url(../resources/images/wallpaper/' + image + '.jpg)')

		Ext.util.CSS.updateRule('.x-window-default', 'border-color', color)
        Ext.util.CSS.updateRule('.x-window-header-default', 'border-color', color)
        Ext.util.CSS.updateRule('.x-window-header-default-top', 'background-color', color)

		Ext.util.CSS.updateRule('.x-btn-login-small', 'background-color', color)
		Ext.util.CSS.updateRule('.x-btn-login-small', 'border', 'none')
		Ext.util.CSS.updateRule('.x-btn-default-medium', 'background-color', color)	
		Ext.util.CSS.updateRule('.x-btn-default-medium', 'border', 'none')
		
	},
	
	login: function() {
		Ext.Msg.show({ msg: txtFields.messages.alerts.initing, progressText: '', wait: { interval: 200 } })
		this.user = this.lookupReference('loginForm').getValues()
		//console.log('Login user...', this.user)
		var controller = this
		Ext.Ajax.request({
            url: 'user/login?username=' + this.user.username + '&password=' + this.user.password,
            method: 'POST',
            success: function (response) {
				console.log('SUCCESS...', response)
                response = Ext.decode(response.responseText)[0];
				if(response == undefined) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
				if(response.LoginResult != -1) {
					loginUser = {
						idUser: response.LoginResult,
						username: controller.user.username,
						name: response.Name,
						profile: response.IdProfile,
						token: response.Token,
						enterprises: Ext.getApplication().getEnterprises(response.Companies),
					}
					console.log(loginUser)
					if(loginUser.idProfile != 2) {
						controller.getView().destroy();
						Ext.Msg.hide()
						Ext.getApplication().gotoMain()
						//Ext.create('ProcessManagement.view.main.Main' )
						Ext.getApplication().startPollNotifications()
					} else Ext.Msg.alert('ERROR', txtFields.messages.errors.profileError)
				} else Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
            },
            failure: function (error) {
				console.log('FAIL...', error)
				if(error.status == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
				else Ext.Msg.alert('ERROR', JSON.parse(error.responseText).message)
					
            }
		})
    },
	
	changeLanguage: function(bt) {
		globalParams.lang = bt.id
        window.location.href = window.location.pathname + '?lang=' + bt.id
    },
	
	onSpecialKey: function (field, e, options) {
        if (e.getKey() == e.ENTER) {
            var submitBtn = this.lookupReference('btnLogin');
            if (!submitBtn.disabled) {
                this.login();
            }
        }
	},
	
	initEnterprisePopup: function() {
		console.log('Iniciando Popup...')
		this.initScreen()
		var panel = this.lookupReference('radioEnterprise'),
			radio = {
				xtype: 'segmentedbutton', vertical: true,				
				items: [], defaults: { scale: 'medium', handler: 'goMain' }
			}
		loginUser.enterprises.forEach(enterprise => {
			radio.items.push({ text: enterprise.Descripcion, id: enterprise.IdEmpresa})
		});
		panel.add(radio)
	},

	goMain: function(btn) {
		//console.log('goMain: ', btn)
		loginUser.enterpriseSelected = btn.id
		Ext.getApplication().gotoMain()
		btn.ownerCt.ownerCt.ownerCt.close()
	}

});
