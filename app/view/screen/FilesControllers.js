Ext.define('ProcessManagement.view.screen.FilesController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.files',

    config: {
        filter: null,
        dates: null
    },

    init: function () {
        console.log('Iniciando files screen...')
        this.dates = ProcessManagement.util.Date;
        this.filter = { field: 'company' };
    },

    initScreen: function() {
        console.log('Init screen ', this.data)        
        this.reorganizerFiles()
        this.lookupReference('fileFilterPanel').setVisible(false)        
    },

    refreshScreen: function() {
        this.lookupReference('imagePanel').setVisible(false)
        this.lookupReference('centerPanel').setVisible(false)
        this.removeFilters();
        this.initScreen();
    },

    viewFilterPanel: function() {
        var filterPanel = this.lookupReference('fileFilterPanel');
        filterPanel.setVisible(!filterPanel.isVisible());
        var tree = this.lookupReference('filesTree');
        tree.setHeight(tree.getHeight() + (!filterPanel.isVisible() ?  + 120 : - 120))
        var panel = this.lookupReference('imagePanel');
        panel.setHeight(panel.getHeight() + (!filterPanel.isVisible() ?  + 120 : - 120))
    },

    createFolder: function(data) {
        return { 
            text: data.Form.company,  
            data: data, level: 0, 
            root: true,
            children: [{ 
                text: data.Form.area,
                data: data, level: 1, 
                root: true,
                children: [{ 
                    text: data.WorkFlow,
                    data: data, level: 2, 
                    root: true,
                    children: [{ 
                        text: data.Form.docType, 
                        data: data, level: 3, 
                        root: true, 
                        children: [{ 
                            text: data.Form.rut  + " - " + data.Form.name,
                            data: data, level: 4, 
                            children: []
                        }]
                    }]
                }]
            }]
        }
    },

    reorganizerFiles: function(callback) {
        var tree = this.lookupReference('filesTree'),
            controller = this;
        this.getView().getEl().mask('Buscando documentos...');
        this.getSteps(function(error, response) { 
            if(!error) { if (response) { 
                console.log(response); 
                var items = response.store.data.items,
                    folders = [];

                items.forEach(function(it) {
                    //if(it.data.AllowDocs) {
                        //Ordenamiento de folders
                        var subfolder = null
                        folders.forEach(function(fld) {
                            var band = true
                            if(fld.text == it.data.Form.company) {
                                subfolder = fld
                                var addFolder = fld, 
                                    child = controller.createFolder(it.data).children[0]

                                //NIVEL 1
                                subfolder.children.forEach(function(f1) {                                    
                                    if(f1.text == it.data.Form.area) {
                                        child = child.children[0]
                                        addFolder = f1
                                        //NIVEL 2
                                        f1.children.forEach(function(f2) {                                    
                                            if(f2.text == it.data.WorkFlow) {
                                                child = child.children[0]
                                                addFolder = f2
                                                
                                                //NIVEL 3
                                                f2.children.forEach(function(f3) {                                    
                                                    if(f3.text == it.data.Form.docType) {
                                                        child = child.children[0]
                                                        addFolder = f3
                                                        
                                                        //NIVEL 4
                                                        f3.children.forEach(function(f4) {                                    
                                                            if(f4.text == it.data.Form.rut  + " - " + it.data.Form.name) {
                                                                child = child.children[0]
                                                                addFolder = f4
                                                                band = false
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })


                                if(band) addFolder.children.push(child) 
                            }
                             
                        })
                        if(subfolder == null) 
                            folders.push(controller.createFolder(it.data)  )                        
                    //}
                })
                console.log('Asignando store...');
                
                tree.setStore(Ext.create('Ext.data.TreeStore', {
                    fields: [
                        {name: 'text', type: 'string'},
                        {name: 'children' },
                        {name: 'data'}
                    ],
                    root: { expanded: true, children: folders },
                    filterer: 'bottomup'
                }));
                tree.getStore().getSorters().add({ property: 'text', direction: 'ASC' })
                controller.getView().getEl().unmask();
            }}
        })
    },

    getSteps: function(callback) {
        var store = new ProcessManagement.store.Steps();
        store.getSteps(loginUser, function(error, result) {
            if(error) {
                if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
                callback(true, null)
            } else callback(false, result.response)
        })
    },

    getFiles: function(workflowId, callback) {
        var store = new ProcessManagement.store.Files();
        store.getFiles(loginUser, workflowId, function(error, result) {
            if(error) {
                if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
                callback(true, null)
            } else callback(false, result.response)
        })
    },

    rowExpanded: function(panel, row) {
        console.log('Row Expanded!', row)        
        if(row.childNodes.length == 0) {
            panel.getView().getEl().mask('Expanding tree...');
            this.getFiles(row.data.data.WorkflowId, function(error, response) {  
                if(!error) { if (response) { 
                    console.log(response); 
                    var items = response.store.data.items;
                    items.forEach(function(it) {
                        row.appendChild({ text: it.data.Archivo, leaf: true, 
                            data: it.data, workflow: row.data.data })
                    })
                }}
                panel.getView().getEl().unmask();
            })
        }        
    },

    rowSelected: function(treePanel, row) {
        console.log('Row Selected!', treePanel, row)
        var imagePanel = this.lookupReference('imagePanel'),
            formPanel = this.lookupReference('centerPanel');
        if(!row) {
            formPanel.setHidden(false)
            imagePanel.setHidden(false)

            var file = treePanel.lastSelected ? treePanel.lastSelected.data : undefined;
            console.log(file)
            if(file) {
                file.workflow.IdState = null
                Ext.getCmp('formPanel').controller.initScreen(file.workflow)

                new Ext.Promise(function(resolve, reject) {
                    Ext.Ajax.request({
                            url: 'tareasPersonales/downloadFileFromURL',
                            method: 'GET',
                            params: {
                                fileUrl: file.data.Url,
                                fileName: file.data.Archivo
                            }
                        }).then (function(response, opts) {
                            console.log('SUCCESS: ', response)
                            imagePanel.setHtml('<iframe src="' + JSON.parse(response.responseText).path + '" width="100%" height="100%" ></iframe>')
                        },
                        function(response, opts) {
                            response = JSON.parse(response.responseText)
                            console.log('FAIL: ', response)
                            if(response.error.includes(ENOENT)) Ext.Msg.alert('ERROR', 'Archivo dañado!')
                            else Ext.Msg.alert('ERROR', response.error)
                        });
                });
            } else {
                formPanel.setHidden(true)
                imagePanel.setHidden(true)
            }
        } else {
            formPanel.setHidden(true)
            imagePanel.setHidden(true)
        }
    },

    setterFields: function() {
        var btn = Ext.getCmp('state' + this.lookupReference('sgmStates').getValue())
        if(btn) btn.setPressed(false)

        var btn = Ext.getCmp('date' + this.lookupReference('sgmDates').getValue())
        if(btn) btn.setPressed(false)
        this.lookupReference('dateFieldFrom').setValue('')
        this.lookupReference('dateFieldTo').setValue('')

        Ext.getCmp('resp0').setPressed(true)
        this.lookupReference('txtResponsible').setValue('')
    },

    //FILTERS
    removeFilters: function() {
        this.setterFields()
        var store = this.lookupReference('filesTree').getStore()
		store.getSorters().removeAll()
        store.getFilters().removeAll()
        //this.lookupReference('btSearch').setText('')
		this.lookupReference('searchField').setValue('');
    },

    changeFilter: function(item, checked) {
        this.filter.field = item.id;
        this.lookupReference('btSearch').setText(item.text)
        this.lookupReference('searchField').setValue('');
    },
    
    findFiles: function(o, record) {
        console.log('Filtering...')
        var treePanel = this.lookupReference('filesTree'),
            store = treePanel.getStore(),
            controller = this;
        var fltr = store.getFilters().get('fltFiles');
        if(fltr) store.removeFilter(fltr, false);
        if(record == '') {            
            treePanel.collapseAll()
        } else {
            var filter = new Ext.util.Filter({
                id: 'fltFiles',
                filterFn: function(it) {                
                    item = it.data
                    console.log(item)
                    if(item.data && !item.data.Archivo) {
                        switch (controller.filter.field) {
                            case 'company':
                                if(item.data.Form.company) return item.data.Form.company.toLowerCase().includes(record.toLowerCase())
                                break;
                            case 'area':
                                if(item.data.Form.area) return item.data.Form.area.toLowerCase().includes(record.toLowerCase())
                                break;
                            case 'docType':
                                if(item.data.Form.docType) return item.data.Form.docType.toLowerCase().includes(record.toLowerCase())
                                break;
                            case 'rut':
                            case 'name':
                                if(item.data.Form.rut && item.data.Form.name) {
                                    if(controller.filter.field == 'rut') { 
                                        return item.data.Form.rut.toLowerCase().includes(record.toLowerCase())
                                    } else return item.data.Form.name.toLowerCase().includes(record.toLowerCase())
                                }
                        }
                    } else if(item.data && item.data.Archivo) return true  

                    return false
                }
            })
            store.getFilters().add(filter)
        }
    }
    
})
