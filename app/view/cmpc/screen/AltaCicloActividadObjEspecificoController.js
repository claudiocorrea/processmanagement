/**
 * Created by Antonio on 2/7/2016.
 */
Ext.define('FCMPC.controller.AltaCicloActividadObjEspecificoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.altaCicloActividadObjEspecificoController',

    requires: [
        'ProcessManagement.util.cmpc.Messages'
    ],

    closeCallback: null,
    
    onSetCloseCallback: function (cbk) {
        this.closeCallback = cbk;
    },

    onShowSelected: function (record) {
        this.lookupReference('cmbPeriodo').setValue(record.get('idperiodo'))
        this.lookupReference('txtSesiones').setValue(record.get('sesiones'))
        this.lookupReference('txtCantHoras').setValue(record.get('cantidadHoras'))
    },

    onSave: function () {
        var me = this;
        if (!this.lookupReference('form').isValid()) {
            ProcessManagement.util.cmpc.Messages.error('Por favor complete los campos obligatorios', 'Error', this);
            return;
        }

        if (!this.__validateCantidadHoras()) {
            ProcessManagement.util.cmpc.Messages.error("Los valores decimales posibles para el campo Cantidad Horas<br>son 0, 25, 50, 75", "Error", this, function () {
                me.lookupReference('txtCantHoras').focus(true, 100);
                me.lookupReference('txtCantHoras').markInvalid('Debe ingresar un valor con entero o con 2 decimales');
            });
            return;
        }
        var datos = {
            idperiodo: this.lookupReference('cmbPeriodo').getValue(),
            periodo: this.lookupReference('cmbPeriodo').getRawValue(),
            sesiones: this.lookupReference('txtSesiones').getValue(),
            cantidadHoras: this.lookupReference('txtCantHoras').getValue()
        };
        this.getView().getViewModel().set('datos', datos);

        this.onClose();
    },

    onClose: function () {
        if (this.closeCallback) {
            this.closeCallback(this.getView().getViewModel().get('datos'));
        }

        this.getView().close();
    },

    __validateCantidadHoras: function () {
        var value = this.lookupReference('txtCantHoras').getValue(),
            flag = true;

        if (value != null) {
            var idx = value.indexOf(",");

            if (idx >= 0) {
                var dec = /*parseInt(*/value.substring(idx + 1)/*)*/,
                    posibles = ["0", "00", "5", "25", "50", "75"];

                flag = posibles.includes(dec);
            }
        }

        return flag
    }
});