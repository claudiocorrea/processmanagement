Ext.define('ProcessManagement.view.cmpc.screen.SesionesController', {

    extend: 'Ext.app.ViewController',
    alias: 'controller.cmpcSesiones',

    config: {
        data: {
            params: {
                loggedUser: 'AROJAS',
                projectId: 358,
                mensaje: null,
                operacion: 'calendarizar',
                anio: 2,
                fechaDesde: 2017,
                fechaHasta: 2018
            }
        }
    },

    OPERACION_CALENDARIZAR: null,
    OPERACION_CERRAR: null,
    TIPO_ACTIVIDAD_ASESORIA: null,
    TIPO_ACTIVIDAD_ASESORIA_FLEXIBLE: null,
    TIPO_ACTIVIDAD_CHARLA: null,
    TIPO_ACTIVIDAD_TALLER_PADRES: null,
    TIPO_ACTIVIDAD_TALLER_INTERACTIVO: null,
    TIPO_ACTIVIDAD_TALLER_APLICACION: null,
    TIPO_ACTIVIDAD_PASANTIA: null,
    TIPO_ACTIVIDAD_PERFECCIONAMIENTOS_DE_NIVELACION: null,
    TIPO_ACTIVIDAD_ASESORIA_DE_ANALISIS_DE_RESULTADOS: null,
    TIPO_ACTIVIDAD_PERFECCIONAMIENTO: null,
    TIPO_ACTIVIDAD_TALLER: null,

    init: function(application) {
        this.OPERACION_CALENDARIZAR = 'calendarizar';
        this.OPERACION_CERRAR = 'cerrar';
        this.TIPO_ACTIVIDAD_ASESORIA = 7;
        this.TIPO_ACTIVIDAD_CHARLA = 24;
        this.TIPO_ACTIVIDAD_ASESORIA_FLEXIBLE = 25;
        this.TIPO_ACTIVIDAD_TALLER_PADRES = 26;
        this.TIPO_ACTIVIDAD_TALLER_INTERACTIVO = 28;
        this.TIPO_ACTIVIDAD_TALLER_APLICACION = 29;
        this.TIPO_ACTIVIDAD_PASANTIA = 30;
        this.TIPO_ACTIVIDAD_ASESORIA_DE_ANALISIS_DE_RESULTADOS = 32;
        this.TIPO_ACTIVIDAD_PERFECCIONAMIENTOS_DE_NIVELACION = 31;
        this.TIPO_ACTIVIDAD_PERFECCIONAMIENTO = 11;
        this.TIPO_ACTIVIDAD_TALLER = 22;
    },

    onBeforeEditAsistencia: function (editor, editEvent, eOpts) {
        var isObservaciones = editEvent.field == "observacionAsistencia",
            isLogro = editEvent.field == "logro",
            isTiempoAsistencia  = editEvent.field == "tiempoAsistencia";

        if (isObservaciones) { //Se quiere editar el campo Observaciones...
            var asistencia = editEvent.record.get('asistencia');

            if (asistencia == "0") {
                return true;
            }
            else {
                editEvent.record.set('observacionAsistencia', null);
                return false;
            }
        }

        if (isLogro) { //Se quiere editar el campo Logro
            if (operacion != this.OPERACION_CERRAR) return false;

            var asistencia = editEvent.record.get('asistencia');

            if (asistencia == "1") {
                return true;
            }
            else {
                editEvent.record.set('logro', null);
                return false;
            }
        }

        if (isTiempoAsistencia) { //Se quiere editar el campo Tiempo Asistencia
            if (operacion != this.OPERACION_CERRAR) return false;

            var asistencia = editEvent.record.get('asistencia');

            if (asistencia == "1") {
                return true;
            } else {
                editEvent.record.set('tiempoAsistencia', null);
                return false;
            }
        }
        return true;
    },

    afterRender: function () {

        if (!Ext.isEmpty(this.config.data.params.projectId)) {
            if(this.lookupReference('cmbProyecto'))
                this.lookupReference('cmbProyecto').setVisible(false);
        }

        if (this.config.data.params.mensaje != null && this.config.data.params.mensaje.trim() != "") {
            FCMPC.utils.Messages.info(mensaje, 'Aviso', this);
        }

        if (this.config.data.params.projectId === null || this.config.data.params.projectId === undefined || this.config.data.params.projectId == "") {
            Ext.getBody().mask('Espere...');
            this.lookupReference('cmbProyecto').getStore().load({
                params: {
                    loggedUser: this.config.data.params.loggedUser
                },
                callback: function () {
                    Ext.getBody().unmask();
                }
            });
        } else {
            this.onRefreshData();
        }

        if (this.config.data.params.operacion == 'calendarizar') {
            this.lookupReference('cmdAddParticipante').setVisible(false);
            this.lookupReference('cmdEditEvaluaciones').setVisible(false);

            Ext.each(this.lookupReference('gridParticipantes').columns, function (column) {
                if (column.dataIndex == "asistencia" || column.dataIndex == "observacionAsistencia" || column.dataIndex == "logro" || column.dataIndex == "tiempoAsistencia") {
                    column.setVisible(false);
                }
            });
        } else {
            this.lookupReference('cmdAddParticipante').setVisible(true);
            this.lookupReference('cmdEditEvaluaciones').setVisible(true);
            Ext.each(this.lookupReference('gridParticipantes').columns, function (column) {
                if (column.xtype == "actioncolumn") {
                    column.setVisible(false);
                }
            });

        }

        this.lookupReference('cmdAddParticipante').setVisible(this.config.data.params.operacion == 'calendarizar');
    },

    onRefreshData: function (button, e, options) {
        var url = ProcessManagement.util.cmpc.ServiceEndPoint.getProyecto() + '/' + this.config.data.params.projectId + '?loggedUser=' + this.config.data.params.loggedUser + '&operacion=' + this.config.data.params.operacion + '&anio=' + this.config.data.params.anio + '&fechaDesde=' + this.config.data.params.fechaDesde + '&fechaHasta=' + this.config.data.params.fechaHasta + '&diasPosteriores=' + 45;//'http://sgp.simbius.com:5555/rest/CMPCGoogleAPI/fcmpc/proyecto/' + projectId + '?loggedUser=' + loggedUser + '';
        var me = this,
            jsonData = {
                operacion: this.config.data.params.operacion,
                anio: this.config.data.params.anio,
                fechaDesde: this.config.data.params.fechaDesde,
                fechaHasta: this.config.data.params.fechaHasta,
                diasPosteriores: 45
            };
        Ext.getBody().mask('Espere...');
        ProcessManagement.util.cmpc.AjaxRequest.request(url, ProcessManagement.util.cmpc.HttpMethods.get, null, this, function (response) {
            Ext.getBody().unmask();
            var r = Ext.JSON.decode(response.responseText, true);

            me.__cargarDatosProyecto(me.config.data.params.projectId);

            if (r == null) {
                r = {
                    data: {
                        sesiones: []
                    }
                }
            }

            if (r.data.sesiones == null || r.data.sesiones == undefined) {
                Ext.getBody().unmask();
                Ext.Msg.show({
                    title: 'Error',
                    msg: 'Error al parsear la respuesta',
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK,
                    scope: this
                });
                return;
            }


            try {
                var grid = me.lookupReference('gridpanel');
                if (grid) {
                    var store = grid.getStore();
                    store.setProxy({type: 'memory', data: r.data.sesiones});
                    store.load({
                        callback: function () {
                            Ext.getBody().unmask();
                            console.log("Cantidad de registros: " + store.getTotalCount());
                            console.log("Cantidad de asesorias: " + me.__getCountAsesorias(store));
                        }
                    });
                }
            } catch (e) {
            }

        }, function (p1, p2, p3) {
            Ext.getBody().unmask();
            Ext.Msg.show({
                title: 'Error',
                msg: 'Error al consultar los datos',
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK,
                scope: this
            });
        },null, 3000000);
    },

    onChangeProyecto: function (combo, newValue, oldValue) {
        projectId = newValue;

        this.onRefreshData()
    },

    __getCountAsesorias: function (store) {
        var count = 0;
        store.each(function (record) {
            var tipo = record.get('idTipoActividad');
            if (tipo == "7" || tipo == "25" || tipo == "32") count += 1;
        });

        return count;
    },

    __cargarDatosProyecto: function (id) {
        var me = this,
            url = ProcessManagement.util.cmpc.ServiceEndPoint.getMarcoLogico() + '/' + id;
        ProcessManagement.util.cmpc.AjaxRequest.request(url, ProcessManagement.util.cmpc.HttpMethods.get, null, this, function (response) {
            response = Ext.JSON.decode(response.responseText, true);

            if (response == null) {
                response = {
                    success: false,
                    data: "Error al obtener el proyecto"
                }
            }

            if (response.success) {
                me.getViewModel().projectData = response;
            } else {
                ProcessManagement.util.cmpc.Messages.error(response.data, 'Error', this);
            }
        })
    },

    __getSelectedRecordInGrid: function (grid) {
        var data = null;
        if (grid.getSelectionModel() && grid.getSelectionModel().getSelection() && grid.getSelectionModel().getSelection().length > 0) {
            data = grid.getSelectionModel().getSelection()[0].data;
        }

        return data;
    },

    __getTipoActividadSesion: function (strTiAc) {
        var tiAc = null;

        if (strTiAc != null) {
            tiAc = strTiAc.substring(0, strTiAc.indexOf("-"));
        }

        return tiAc;
    },

    __getDatosSesionFromTitulo: function (titulo) {
        var arr = (titulo != null && titulo != "")?titulo.replace(" - ", "-").split("-"):null,
            datos = null;

        if (arr != null && arr.length > 0) {
            datos = {
                nombre: (arr[0] != null && arr[0] != undefined)?arr[0].trim():"",
                comuna: (arr[1] != null && arr[1] != undefined)?arr[1].trim():"",
                tipoActividad: (arr[2] != null && arr[2] != undefined)?arr[2].trim():"",
                categoria: (arr[3] != null && arr[3] != undefined)?arr[3].trim():"",
                area: (arr[4] != null && arr[4] != undefined)?arr[4].trim():"",
                nivel: (arr[5] != null && arr[5] != undefined)?arr[5].trim():"",
                nroSesion: (arr[6] != null && arr[6] != undefined)?arr[6].trim():""
            }
        }

        console.log("__getDatosSesionFromTitulo:", titulo, datos)

        return datos;
    },

    __getIdsEstablecimientosFromProyecto: function () {
        var project = this.getForm().viewModel.projectData.data,
            array = [];

        Ext.each(project.establecimientos, function (establecimiento) {
            array.push(establecimiento.id);
        });

        return array;
    },

    __getTiposParticipantesPosibles: function () {
        var project = this.getForm().viewModel.projectData.data,
            sesion = this.__getSelectedRecordInGrid(this.getGrid()),
            idTiac = this.__getTipoActividadSesion(sesion.idTIAC),
            tiposParticipantes = new Ext.util.HashMap(),
            array = [],
            datosSesion = this.__getDatosSesionFromTitulo(sesion.titulo);

        datosSesion.nivel = datosSesion.nivel.replace(', ', ',');

        console.log("datos prelims: ", idTiac, datosSesion);

        Ext.each(project.objetivosEspecificos, function (oe) {
            if (oe.estrategia) {
                Ext.each(oe.estrategia, function (estrategia) {
                    var estrategianivel = "",
                        estrategiaNivelSplitted, nivelesSesionSplitted;

                    console.log("estrategia a evaluar:", estrategia);
                    if (estrategia.nivel != null && estrategia.nivel != undefined) {
                        estrategianivel = estrategia.nivel.replace(', ', ',');
                    } else {
                        estrategianivel = "";
                    }

                    if (datosSesion.nivel != null && datosSesion.nivel !== undefined) {
                        nivelesSesionSplitted = datosSesion.nivel.replace(", ", ',').split(',');
                    } else {
                        nivelesSesionSplitted = "".split(',');
                    }

                    estrategiaNivelSplitted = estrategianivel.split(',');

                    //console.log("datos: ", idTiac, datosSesion.categoria, datosSesion.area, datosSesion.nivel);
                    if (estrategia.idTipoActividad == idTiac && estrategia.categoria == datosSesion.categoria && estrategia.area == datosSesion.area) {
                        var flag = false;

                        estrategiaNivelSplitted.forEach(function(en) {
                            if (nivelesSesionSplitted.includes(en)) {
                                flag = true;
                            }
                        });
                        //console.log("estrategia encontrada", estrategia);
                        if (flag && estrategia.tipoParticipante) {
                            Ext.each(estrategia.tipoParticipante, function (tipoParticipante) {
                                tiposParticipantes.add(tipoParticipante.id, tipoParticipante.descripcion);
                            })
                        }
                    }
                })
            }
        });

        tiposParticipantes.each(function (key, value, length) {
            array.push(key);
        })

        return array;
    },

    __huboAsistenciaParticipantes: function () {
        var store = this.lookupReference('gridParticipantes').getStore(),
            flag = false;

        store.each(function (participante) {
            if (participante.get('asistencia') != null && participante.get('asistencia') != "" && participante.get('asistencia') == "1" ) {
                flag = true;
                return;
            }
        });

        return flag;
    },

    __isValidData: function () {
        var me = this,
            valid = true,
            esAsesoria = (eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_ASESORIA || eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_ASESORIA_FLEXIBLE || eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_ASESORIA_DE_ANALISIS_DE_RESULTADOS)?true:false,
            esAsesoriaFlexible = (eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_ASESORIA_FLEXIBLE)?true:false,
            esAsesoriaDeAnalisisDeResultados = (eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_ASESORIA_DE_ANALISIS_DE_RESULTADOS)?true:false,
            esCharla = (eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_CHARLA || eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_PASANTIA)?true:false,
            esSoloCharla = (eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_CHARLA)?true:false,
            esTallerPadres = (eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_TALLER_PADRES || eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_TALLER_INTERACTIVO)?true:false,
            esTaller = (eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_TALLER_PADRES || eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_TALLER_INTERACTIVO || eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_TALLER_APLICACION || eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_TALLER)?true:false,
            esPerfeccionamiento = (eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_PERFECCIONAMIENTO || eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_PERFECCIONAMIENTOS_DE_NIVELACION)?true:false,
            huboAsistentes = this.__huboAsistenciaParticipantes();

        if (!this.lookupReference('horaInicioRealPlanificada').isValid()) {
            FCMPC.utils.Messages.error('La hora de inicio no es v&aacute;lida', 'Error', this);
            return;
        }

        if (!this.lookupReference('horaFinRealPlanificada').isValid()) {
            FCMPC.utils.Messages.error('La hora de fin no es v&aacute;lida', 'Error', this);
            return;
        }

        if (operacion == this.OPERACION_CALENDARIZAR) {
            var store = this.lookupReference('gridParticipantes').getStore();
            if (store.getCount() <= 0) {
                valid = false;
                FCMPC.utils.Messages.error('Debe asignar participantes para continuar', 'Error de validaci&oacute;n', this);
                return valid;
            }
            console.log("Validdata: ", this.getForm().viewModel.tipoActividad, esCharla)
            if (esCharla) {
                var cantProfTerreno = 0,
                    cantOtros = 0,
                    cantApoderados = 0;
                store.each(function (participante) {
                    if (participante.get('perfil') == FCMPC.Constants.PERFIL_APODERADO_STR) cantApoderados += 1;
                    if (participante.get('perfil') == FCMPC.Constants.PERFIL_PROFESIONAL_DE_TERRENO_STR ||participante.get('perfil') == FCMPC.Constants.PERFIL_RELATOR_PROFESIONAL_DE_TERRENO_STR) cantProfTerreno += 1;
                    if (participante.get('perfil') == FCMPC.Constants.PERFIL_DOCENTE_STR || participante.get('perfil') == FCMPC.Constants.PERFIL_DIRECTOR_STR || participante.get('perfil') == FCMPC.Constants.PERFIL_JEFE_UTP_STR) cantOtros += 1;
                });

                if (cantApoderados <= 0 && esSoloCharla) {
                    FCMPC.utils.Messages.error('Debe agregar al menos 1 Apoderado', 'Error', this);
                    valid = false;
                    return valid;
                }
                /*if (cantProfTerreno <= 0) {
                    FCMPC.utils.Messages.error('Debe agregar al menos 1 Profesional de Terreno', 'Error', this);
                    valid = false;
                    return valid;
                }

                if (cantOtros <= 0) {
                    FCMPC.utils.Messages.error('Debe agregar al menos 1 Docente, Director o Jefe UTP', 'Error', this);
                    valid = false;
                    return valid;
                }*/
            }

            if (esTallerPadres) {
                if (this.lookupReference('cantParticipantes').getValue() === null || this.lookupReference('cantParticipantes').getValue() === "") {
                    FCMPC.utils.Messages.error('Por favor complete la cantidad de participantes', 'Error', this);
                    this.lookupReference('cantParticipantes').markInvalid(FCMPC.GeneralConstants.REQUIRED_FIELD_TEXT);
                    valid = false;
                    return valid;
                }
            }
        }

        if (operacion == this.OPERACION_CERRAR) {
            if (eval(this.getForm().viewModel.tipoActividad) == me.TIPO_ACTIVIDAD_CHARLA) {
                if (this.lookupReference('cantParticipantes').getValue() === null || this.lookupReference('cantParticipantes').getValue() === "") {
                    FCMPC.utils.Messages.error('Por favor complete la cantidad de participantes', 'Error', this);
                    this.lookupReference('cantParticipantes').markInvalid(FCMPC.GeneralConstants.REQUIRED_FIELD_TEXT);
                    valid = false;
                    return false;
                }
            }

            var store = this.lookupReference('gridParticipantes').getStore();
            if (store.getCount() <= 0) {
                valid = false;
                FCMPC.utils.Messages.error('Debe asignar participantes para continuar', 'Error de validaci&oacute;n', this);
                return valid;
            }

            if (esTallerPadres) {
                if (this.lookupReference('cantParticipantes').getValue() != this.__obtenerCantParticipantesPorPerfil('Padre')) {
                    FCMPC.utils.Messages.error('La cantidad de participantes no coincide con la cantidad de participantes asignados', 'Error de validaci&oacute;n', this);
                    valid = false;
                    return valid;
                }
            }

            if (!valid) return valid;

            store.each(function (participante) {
                //console.log(participante);
                if (participante.get('perfil') != FCMPC.Constants.PERFIL_PROFESIONAL_DE_TERRENO_STR && participante.get('perfil') != FCMPC.Constants.PERFIL_RELATOR_PROFESIONAL_DE_TERRENO_STR && participante.get('perfil') != FCMPC.Constants.PERFIL_RELATOR_STR && participante.get('perfil') != FCMPC.Constants.PERFIL_ENCARGADO_DE_PROYECTO_STR && participante.get('perfil') != FCMPC.Constants.PERFIL_RELATOR_EXTERNO_STR) {


                    //if (participante.get('perfil') != FCMPC.Constants.PERFIL_PROFESIONAL_DE_TERRENO_STR && participante.get('perfil') != FCMPC.Constants.PERFIL_RELATOR_PROFESIONAL_DE_TERRENO_STR && participante.get('perfil') != FCMPC.Constants.PERFIL_RELATOR_STR && participante.get('perfil') != FCMPC.Constants.PERFIL_ENCARGADO_DE_PROYECTO_STR) {
                    if (participante.get('asistencia') == null || participante.get('asistencia') == "") {
                        FCMPC.utils.Messages.error('Por favor complete la Asistencia de cada Participante', 'Error', this);
                        valid = false;
                        return valid;
                    }
                    if (!valid) return valid;
                    //}

                    if (eval(participante.get('asistencia')) == 1) { //Si asiste, se evalua el Logro
                        //console.log("esAsesoria?: " + esAsesoria)
                        //if (participante.get('tiempoAsistencia') == null || participante.get('tiempoAsistencia') == "") {
                        if (Ext.isEmpty(participante.get('tiempoAsistencia'))) {
                            FCMPC.utils.Messages.error('Por favor complete el campo Tiempo de Asistencia para todos los participantes que asisten', 'Error', this);
                            valid = false;
                            return valid;
                        } else {
                            valid = me.__validarTiempoAsistencia(participante.get('tiempoAsistencia').toString());
                            if (!valid) return valid;
                        }

                        if (!esAsesoria && !esCharla && !esAsesoriaFlexible && !esTallerPadres && !esTaller && !esPerfeccionamiento) {
                            if (participante.get('logro') === null || participante.get('logro') === "") {
                                FCMPC.utils.Messages.error('Por favor complete el campo Logro para todos los participantes que asisten', 'Error', this);
                                valid = false;
                                return valid;
                            }
                        }
                        //Verificar Evaluaciones
                        if (huboAsistentes) { //Solo se valida evaluaciones si hubo algun asistente
                            if (esAsesoria && !esAsesoriaFlexible && !esAsesoriaDeAnalisisDeResultados) {
                                if (participante.get('perfil') == FCMPC.Constants.PERFIL_DOCENTE_STR) {
                                    if (participante.get('evaluaciones') == null || participante.get('evaluaciones') == undefined || participante.get('evaluaciones').toString().trim() == "" ) {
                                        FCMPC.utils.Messages.error('Por favor complete las Evaluaciones de los Docentes que asisten', 'Error', this);
                                        console.log("1 evaluacion: ", participante.get('evaluaciones'));
                                        valid = false;
                                        return valid;
                                    }
                                }
                            }
                            /*if (esCharla && (participante.get('perfil') == FCMPC.Constants.PERFIL_APODERADO_STR)) {
                                if (participante.get('evaluaciones') == null || participante.get('evaluaciones') == undefined || participante.get('evaluaciones').toString().trim() == "") {
                                    FCMPC.utils.Messages.error('Por favor complete las Evaluaciones de los Apoderados que asisten', 'Error', this);
                                    console.log("2 evaluacion: ", participante.get('evaluaciones'));
                                    valid = false;
                                    return valid;
                                }
                            }*/
                        }
                    }
                    if (!valid) return valid;

                    if (eval(participante.get('asistencia')) == 0) { //Si no asiste, se evalua la Observacion
                        if (participante.get('observacionAsistencia') === null || participante.get('observacionAsistencia') === "") {
                            FCMPC.utils.Messages.error('Por favor complete la Observaci&oacute;n para aquellos participantes que no asisten', 'Error', this);
                            valid = false;
                            return valid;
                        }
                    }
                    if (!valid) return valid;

                }

            });

            if (huboAsistentes) { //Solo se valida evaluaciones si hubo algun asistente
                if (!esAsesoria && !esCharla && !esAsesoriaFlexible) {
                    if (this.getForm().viewModel.evaluaciones == "" || this.getForm().viewModel.evaluaciones == null || this.getForm().viewModel.evaluaciones == undefined) {
                        FCMPC.utils.Messages.error('Por favor complete las Evaluaciones', 'Error', this);
                        valid = false;
                    }
                }
            }
        }
        return valid;
    },

    __obtenerCantParticipantesPorPerfil: function (perfil) {
        var cant = 0;
        this.lookupReference('gridParticipantes').getStore().each(function (record) {
            if (record.get('perfil') == perfil) cant++;
        });

        return cant;
    },

    __validarTiempoAsistencia: function (p_tiempo) {
        var fecha = this.lookupReference('fechaRealPlanificada').getValue();
        horaI = this.lookupReference('horaInicioRealPlanificada').getValue(),
            horaF = this.lookupReference('horaFinRealPlanificada').getValue(),
            flag = true,
            posibles = ["0", "00", "5", "25", "50", "75"],
            tiempo = Ext.String.format("{0}", p_tiempo);

        tiempo = tiempo.replace(",", ".").split(".");

        if (tiempo.length > 1) { //Tiene decimales
            var decimales = tiempo[1];
            flag = posibles.includes(decimales);
            if (!flag) {
                FCMPC.utils.Messages.error("Los valores decimales posibles para el campo Tiempo Asistencia<br>son 0, 25, 50, 75", "Error", this)
            }
        }

        return flag;
    },

    __obtenerHoraIngresada: function (objeto) {
        if (objeto.getValue() == null || objeto.getValue().trim() == "") return "00:00";

        var split = objeto.getValue().split(":");

        if (split[0].length < 2) split[0] = '0' + split[0];

        var hora = Ext.String.format("{0}:{1}", split[0], split[1]);

        return hora;
    },

    __clearFields: function () {
        this.lookupReference('idTIAC').setValue(null);
        this.lookupReference('titulo').setValue(null);
        this.lookupReference('fechaRealPlanificada').setValue(null);
        this.lookupReference('horaInicioRealPlanificada').setValue(null);
        this.lookupReference('horaFinRealPlanificada').setValue(null);
        this.lookupReference('gridParticipantes').getStore().removeAll();
        this.lookupReference('gridParticipantes').getSelectionModel().clearSelections();
        this.lookupReference('cantParticipantes').setValue('');
        this.lookupReference('cantParticipantes').clearInvalid();
    },

    onUpdateData: function (button, e, options) {
        var me = this;
        var componente = Ext.ComponentQuery.query('form-grid')[0];
        var record = componente.getForm().getRecord();
        var grid = componente.down('gridpanel');

        //console.log(grid.getSelectionModel().getSelection()[0]);

        if (grid) {
            if (grid.getSelectionModel() && grid.getSelectionModel().getSelection() && grid.getSelectionModel().getSelection().length > 0) {
                if (!this.__isValidData()) return;
                Ext.Msg.confirm('Confirmaci&oacute;n', 'Seguro desea guardar los datos?', function (btnId) {
                    if (btnId == "yes") {
                        var gridStore = grid.getStore(),
                            registro = grid.getSelectionModel().getSelection()[0];

                        var jsonData = {
                            loggedUser: loggedUser,
                            idSESI: me.getIdSESI().getValue(),
                            idTIAC: me.lookupReference('idTIAC').getValue(),
                            idTIPR: registro.get('idTIPR'),
                            encargado: registro.get('encargado'),
                            titulo: registro.get('titulo'),
                            SESI: me.getSESI().getValue(),
                            urlCalendario: me.getUrlCalendario().getValue(),
                            FEINP: null,
                            HOINP: null,
                            HOFIP: null,
                            FEINE: null,
                            FEFIE: null,
                            HOINE: null,
                            HOFIE: null,
                            startDate: null,
                            endDate: null,
                            participantes: [],
                            evaluaciones: [],
                            cantidadParticipantes: null
                        };


                        if (registro.get('status') == 'Ejecutada') {
                            jsonData.FEINE = Ext.Date.format(me.lookupReference('fechaRealPlanificada').getValue(), "d/m/Y");
                            jsonData.HOINE = me.lookupReference('horaInicioRealPlanificada').getValue();
                            jsonData.HOFIE = me.lookupReference('horaFinRealPlanificada').getValue();
                        } else {
                            jsonData.FEINP = Ext.Date.format(me.lookupReference('fechaRealPlanificada').getValue(), "d/m/Y");
                            jsonData.HOINP = me.lookupReference('horaInicioRealPlanificada').getValue();
                            jsonData.HOFIP = me.lookupReference('horaFinRealPlanificada').getValue();
                            var startDate = Ext.String.format("{0}T{1}:00Z", Ext.Date.format(me.lookupReference('fechaRealPlanificada').getValue(), "Y-m-d"), me.__obtenerHoraIngresada(me.lookupReference('horaInicioRealPlanificada')));
                            var endDate = Ext.String.format("{0}T{1}:00Z", Ext.Date.format(me.lookupReference('fechaRealPlanificada').getValue(), "Y-m-d"), me.__obtenerHoraIngresada(me.lookupReference('horaFinRealPlanificada')));
                            jsonData.startDate = startDate;
                            jsonData.endDate = endDate;
                        }

                        var participantes = [];
                        var storeParticipantes = me.lookupReference('gridParticipantes').getStore();
                        if (storeParticipantes.data) {
                            storeParticipantes.data.each(function (rec) {
                                //participantes.push(rec.data);
                                rec.data.asistencia = Ext.String.format("{0}", rec.data.asistencia);
                                if (rec.data.tiempoAsistencia != null) rec.data.tiempoAsistencia = Ext.String.format("{0}", rec.data.tiempoAsistencia);
                                jsonData.participantes.push(rec.data);
                            });
                        }

                        if (operacion == me.OPERACION_CERRAR) {

                            var idTipoActividad = this.getForm().viewModel.tipoActividad

                            if (eval(idTipoActividad) != FCMPC.GeneralConstants.TIPO_ACTIVIDAD_ASESORIA && eval(idTipoActividad) != FCMPC.GeneralConstants.TIPO_ACTIVIDAD_ASESORIA_FLEXIBLE && eval(idTipoActividad) != FCMPC.GeneralConstants.TIPO_ACTIVIDAD_ASESORIA_DE_ANALISIS_DE_RESULTADOS) {
                                jsonData.evaluaciones = this.getForm().viewModel.evaluaciones;
                                Ext.each(jsonData.participantes, function (p) {
                                    p.evaluaciones = null;
                                })
                            } else {
                                Ext.each(jsonData.participantes, function (p) {
                                    if (p.evaluaciones == "") {
                                        p.evaluaciones = null;
                                    }
                                    if (p.cursos == "") {
                                        p.cursos = null;
                                    }
                                })
                            }

                            jsonData.cantidadParticipantes = '' + this.lookupReference('cantParticipantes').getValue();

                            jsonData.observacionesGrales = this.lookupReference('observacionesGenerales').getValue();
                        }

                        if (operacion == me.OPERACION_CALENDARIZAR) {
                            jsonData.cantidadParticipantes = '' + this.lookupReference('cantParticipantes').getValue();
                        }

                        Ext.each(jsonData.participantes, function (p) {
                            if (p.cursos == "") {
                                p.cursos = null;
                            }
                        })

                        console.log(jsonData);

                        //return;

                        Ext.Ajax.useDefaultXhrHeader = false;
                        Ext.Ajax.cors = true;

                        var url = FCMPC.utils.ServiceEndPoint.getSesion() + '/' + this.getIdSESI().getValue() + '?operacion=' + operacion;
                        Ext.getBody().mask('Espere...');
                        Ext.Ajax.request({
                            url: url,
                            disableCaching:true,
                            jsonData: jsonData,
                            method: FCMPC.utils.HttpMethods.post,
                            success: function (response, options) {
                                Ext.getBody().unmask();
                                var resp = Ext.JSON.decode(response.responseText, true);

                                if (resp == null) {
                                    resp = {success: false, errorMsg: 'Error al parsear la respuesta'};
                                }

                                if (resp.success == true) {
                                    Ext.Msg.show({
                                        msg: 'Datos grabados con &eacute;xito',
                                        title: 'Aviso',
                                        icon: Ext.Msg.INFO,
                                        buttons: Ext.Msg.OK,
                                        scope: this,
                                        fn: function () {
                                            me.onRefreshData();
                                            me.__clearFields();
                                        }
                                    });
                                    me.lookupReference('cmdUpdate').setDisabled(true);
                                    me.lookupReference('cmdUpdate').setVisible(false);
                                } else {
                                    Ext.Msg.show({
                                        msg: 'Se produjo un error al grabar los datos',
                                        title: 'Error',
                                        icon: Ext.Msg.ERROR,
                                        buttons: Ext.Msg.OK,
                                        scope: this
                                    });
                                }
                            },
                            failure: function () {
                                Ext.getBody().unmask();
                                Ext.Msg.show({
                                    msg: 'Error al grabar los datos',
                                    title: 'Error',
                                    icon: Ext.Msg.ERROR,
                                    buttons: Ext.Msg.OK
                                });
                            }
                        });
                    }
                }, this);
            } else {
                Ext.Msg.alert("Error", "Debe seleccionar un registro para editar", null, this);
            }
        }
    },

    __obtenerNivelFromTituloSesion: function (titulo) {
        if (titulo == null || titulo.trim() == "") return null;

        var tsplit = titulo.replace(" - ", "-").split("-"),
            nivel = tsplit[tsplit.length - 2],
            nivelSplitted = nivel.trim().replace(', ', ',').split(','),
            index = "",
            url = FCMPC.utils.ServiceEndPoint.getNivel();

        FCMPC.utils.AjaxRequest.request(url, FCMPC.utils.HttpMethods.get, null, this, function (response) {
            response = Ext.JSON.decode(response.responseText, true);

            if (response != null) {
                Ext.each(response.data.results, function (r) {
                    if (nivelSplitted.includes(r.nombre)) {
                        index += (r.id + ",");
                    }
                })
            }
        }, function (response) {

        }, true);

        if (index.endsWith(',')) {
            index = index.substring(0, index.length - 1);
        }

        if (index == "") {
            index = "-1";
        }
        return index;



    },

    __obtenerFechaDesdeFechaBD: function (fecha) {
        if (fecha == null || fecha.trim() == "") return null;

        var f = Ext.Date.format(Ext.Date.parse(fecha.substr(0, fecha.indexOf("T")), "Y-m-d"), "d/m/Y");

        return f;
    },

    __obtenerHoraDesdeFechaBD: function (fecha) {
        if (fecha == null || fecha.trim() == "") return null;

        fecha = fecha.trim();

        var index = fecha.indexOf("T"),
            hora = null;

        hora = fecha.substr(fecha.indexOf("T") + 1, 5);

        return hora;
    },

    onGridSelectionchange: function (model, records, o) {
        var me = this,
            esAsesoriaTallerOPerfeccionamiento = false,
            actividad = null,
            rec = records[0];
        if (rec) {

            this.__clearFields();
            this.lookupReference('titulo').setValue(rec.get('titulo'));
            this.lookupReference('idTIAC').setValue(rec.get('idTIAC'));

            var idSesion = rec.get('idSESI');
            var url = ProcessManagement.util.cmpc.ServiceEndPoint.getSesion() + '/' + idSesion + "?loggedUser/" + me.config.data.params.loggedUser;

            Ext.getBody().mask('Espere...');

            this.lookupReference('cantParticipantes').setFieldLabel('Cantidad');

            ProcessManagement.util.cmpc.AjaxRequest.request(url, ProcessManagement.util.cmpc.HttpMethods.get, null, this, function (response) {
                Ext.getBody().unmask();

                var r = Ext.JSON.decode(response.responseText, true);

                if (r == null) {
                    Ext.Msg.show({
                        msg: 'Error al obtener los datos',
                        title: 'Error',
                        icon: Ext.Msg.ERROR,
                        buttons: Ext.Msg.OK,
                        scope: this
                    });

                    return;
                }
                if (r) {
                    //Guardo los datos de la sesion en un objeto en memoria
                    console.log("datos sesion: ", r.data);
                    me.getViewModel().datosSesion = r.data;
                    me.getViewModel().evaluacionesCargadas = [];
                    me.getViewModel().evaluaciones = [];
                    me.getViewModel().tipoActividad = me.__getTipoActividadSesion(r.data.idTIAC);

                    actividad = Math.abs(me.getViewModel().tipoActividad);

                    esAsesoriaTallerOPerfeccionamiento = (actividad == me.TIPO_ACTIVIDAD_ASESORIA || actividad == me.TIPO_ACTIVIDAD_ASESORIA_FLEXIBLE || actividad == me.TIPO_ACTIVIDAD_ASESORIA_DE_ANALISIS_DE_RESULTADOS
                        || actividad == me.TIPO_ACTIVIDAD_PERFECCIONAMIENTO || actividad == me.TIPO_ACTIVIDAD_PERFECCIONAMIENTOS_DE_NIVELACION
                        || actividad == me.TIPO_ACTIVIDAD_TALLER || actividad == me.TIPO_ACTIVIDAD_TALLER_PADRES || actividad == me.TIPO_ACTIVIDAD_TALLER_APLICACION || actividad == me.TIPO_ACTIVIDAD_TALLER_INTERACTIVO);

                    switch (actividad) {
                        case Math.abs(me.TIPO_ACTIVIDAD_ASESORIA):
                            me.lookupReference('fsCantidadParticipantes').setVisible(false);
                            me.lookupReference('cantParticipantes').allowBlank = true;
                            Ext.each(me.lookupReference('gridParticipantes').columns, function (column) {
                                if (column.dataIndex == "logro") {
                                    column.setVisible(false);
                                }
                            });
                            break;
                        case Math.abs(me.TIPO_ACTIVIDAD_ASESORIA_FLEXIBLE):
                            me.lookupReference('fsCantidadParticipantes').setVisible(false);
                            me.lookupReference('cantParticipantes').allowBlank = true;
                            Ext.each(me.lookupReference('gridParticipantes').columns, function (column) {
                                if (column.dataIndex == "logro") {
                                    column.setVisible(false);
                                }
                            });
                            break;
                        case Math.abs(me.TIPO_ACTIVIDAD_ASESORIA_DE_ANALISIS_DE_RESULTADOS):
                            me.lookupReference('fsCantidadParticipantes').setVisible(false);
                            me.lookupReference('cantParticipantes').allowBlank = true;
                            Ext.each(me.lookupReference('gridParticipantes').columns, function (column) {
                                if (column.dataIndex == "logro") {
                                    column.setVisible(false);
                                }
                            });
                            break;
                        case Math.abs(me.TIPO_ACTIVIDAD_CHARLA):
                            if (me.config.data.params.operacion == me.OPERACION_CERRAR) {
                                me.lookupReference('fsCantidadParticipantes').setVisible(true);
                                me.lookupReference('cantParticipantes').allowBlank = false;
                            } else {
                                me.lookupReference('fsCantidadParticipantes').setVisible(false);
                                me.lookupReference('cantParticipantes').allowBlank = true;
                            }
                            Ext.each(me.lookupReference('gridParticipantes').columns, function (column) {
                                if (column.dataIndex == "logro") {
                                    column.setVisible(false);
                                }
                            });
                            break;
                        case Math.abs(me.TIPO_ACTIVIDAD_TALLER_PADRES):
                            me.lookupReference('fsCantidadParticipantes').setVisible(true);
                            me.lookupReference('cantParticipantes').allowBlank = false;
                            me.lookupReference('cantParticipantes').setFieldLabel('Cant. Padres');
                            if (me.config.data.params.operacion == me.OPERACION_CALENDARIZAR) {
                                Ext.each(me.lookupReference('gridParticipantes').columns, function (column) {
                                    if (column.dataIndex == "logro" || column.dataIndex == "asistencia" || column.dataIndex == "tiempoAsistencia" || column.dataIndex == "observacionAsistencia" || column.dataIndex == "cursosConcat") {
                                        column.setVisible(false);
                                    }
                                });
                            } else {
                                Ext.each(me.lookupReference('gridParticipantes').columns, function (column) {
                                    if (column.dataIndex == "asistencia" || column.dataIndex == "observacionAsistencia" || column.dataIndex == "tiempoAsistencia" || column.xtype == "actioncolumn") {
                                        column.setVisible(true);
                                    }
                                    if (column.dataIndex == "logro" || column.dataIndex == "cursosConcat") {
                                        column.setVisible(false);
                                    }
                                });
                                me.lookupReference('cmdAddParticipante').setVisible(true);
                            }
                            break;
                        case Math.abs(me.TIPO_ACTIVIDAD_TALLER_INTERACTIVO):
                            me.lookupReference('fsCantidadParticipantes').setVisible(true);
                            me.lookupReference('cantParticipantes').allowBlank = false;
                            me.lookupReference('cantParticipantes').setFieldLabel('Cant. Padres');
                            if (me.config.data.params.operacion == me.OPERACION_CALENDARIZAR) {
                                Ext.each(me.lookupReference('gridParticipantes').columns, function (column) {
                                    if (column.dataIndex == "logro" || column.dataIndex == "asistencia" || column.dataIndex == "tiempoAsistencia" || column.dataIndex == "observacionAsistencia" || column.dataIndex == "cursosConcat") {
                                        column.setVisible(false);
                                    }
                                });
                            } else {
                                Ext.each(me.lookupReference('gridParticipantes').columns, function (column) {
                                    if (column.dataIndex == "asistencia" || column.dataIndex == "observacionAsistencia" || column.dataIndex == "tiempoAsistencia" || column.xtype == "actioncolumn") {
                                        column.setVisible(true);
                                    }
                                    if (column.dataIndex == "logro" || column.dataIndex == "cursosConcat") {
                                        column.setVisible(false);
                                    }
                                });
                                me.lookupReference('cmdAddParticipante').setVisible(true);
                            }
                            break;
                        case Math.abs(me.TIPO_ACTIVIDAD_PASANTIA):
                            if (me.config.data.params.operacion == me.OPERACION_CERRAR) {
                                me.lookupReference('fsCantidadParticipantes').setVisible(false);
                                me.lookupReference('cantParticipantes').allowBlank = true;
                            } else {
                                me.lookupReference('fsCantidadParticipantes').setVisible(false);
                                me.lookupReference('cantParticipantes').allowBlank = true;
                            }
                            Ext.each(me.lookupReference('gridParticipantes').columns, function (column) {
                                if (column.dataIndex == "logro") {
                                    column.setVisible(false);
                                }
                            });
                            break;
                        default:
                            me.lookupReference('fsCantidadParticipantes').setVisible(false);
                            me.lookupReference('cantParticipantes').allowBlank = true;
                            Ext.each(me.lookupReference('gridParticipantes').columns, function (column) {
                                if (column.dataIndex == "logro") {
                                    column.setVisible(true);
                                }
                            });
                    }

                    me.lookupReference('observacionesGenerales').setValue('');

                    if (me.config.data.params.operacion == me.OPERACION_CERRAR) {
                        console.log("esAsesoriaTallerOPerfeccionamiento:", esAsesoriaTallerOPerfeccionamiento);
                        me.lookupReference('fsObservacionesGrales').setVisible(esAsesoriaTallerOPerfeccionamiento)
                    } else {
                        me.lookupReference('fsObservacionesGrales').setVisible(false);
                    }
                    /*
                                        if (me.getForm().viewModel.tipoActividad == me.TIPO_ACTIVIDAD_ASESORIA || me.getForm().viewModel.tipoActividad == me.TIPO_ACTIVIDAD_ASESORIA_FLEXIBLE) {
                                        } else {
                                            if (me.getForm().viewModel.tipoActividad == me.TIPO_ACTIVIDAD_CHARLA) {
                                            } else {
                                                if (me.getForm().viewModel.tipoActividad == me.TIPO_ACTIVIDAD_TALLER_PADRES) {
                                                } else {
                                                }
                                            }
                                        }
                    */


                    var feine = r.data.FEINE;
                    var hoine = r.data.HOINE;
                    var fefie = r.data.FEFIE;
                    var hofie = r.data.HOFIE;

                    var feinp = r.data.FEINP;
                    var hoinp = r.data.HOINP;
                    var fefip = r.data.FEFIP;
                    var hofip = r.data.HOFIP;

                    var idSESI = r.data.idSESI;
                    var SESI = r.data.SESI;

                    if (feine != null && feine != "") {//La sesion esta cerrada, NO se puede editar
                        //console.log('sesion cerrada: ' + feine)
                        me.lookupReference('cmdUpdate').setDisabled(true);
                        me.lookupReference('cmdUpdate').setVisible(false);
                    } else {
                        me.lookupReference('cmdUpdate').setDisabled(false);
                        me.lookupReference('cmdUpdate').setVisible(true);
                    }


                    var rec = [];
                    rec['idSESI'] = idSESI;
                    rec['SESI'] = SESI;

                    rec['FEINP'] = feinp;
                    rec['HOINP'] = (hoinp)?hoinp.substring(0, 5):hoinp;
                    rec['FEFIP'] = fefip;
                    rec['HOFIP'] = (hofip)?hofip.substring(0, 5):hofip;

                    rec['FEINE'] = feine;
                    rec['HOINE'] = (hoine)?hoine.substring(0, 5):hoine;
                    rec['FEFIE'] = fefie;
                    rec['HOFIE'] = (hofie)?hofie.substring(0, 5):hofie;

                    rec['urlCalendario'] = r.data.urlCalendario

                    //Seteo a mano los campos del form
                    //Ext.ComponentQuery.query('form-grid')[0].getForm().getFields().each(function (field) {
                    //  me.lookupReference('formDatosSesion').getForm().getFields().each(function (field) {
                    //      if (!(field.itemId == "idTIPR" || field.itemId == "idTIAC" || field.itemId == "encargado" || field.itemId == "titulo" || field.itemId == "cmbProyectoByComuna")) field.setValue(rec[field.itemId]);
                    //  });


                    if (records[0].get('status') == 'Activa' || records[0].get('status') == 'Calendarizada') {
                        me.lookupReference('fechaRealPlanificada').setValue(rec['FEINP']);
                        me.lookupReference('horaInicioRealPlanificada').setValue(rec['HOINP']);
                        me.lookupReference('horaFinRealPlanificada').setValue(rec['HOFIP']);
                    } else {
                        var fecha = records[0].get('startDate'),
                            fechah = records[0].get('endDate');
                        me.lookupReference('fechaRealPlanificada').setValue(me.__obtenerFechaDesdeFechaBD(fecha));
                        me.lookupReference('horaInicioRealPlanificada').setValue(me.__obtenerHoraDesdeFechaBD(fecha));
                        me.lookupReference('horaFinRealPlanificada').setValue(me.__obtenerHoraDesdeFechaBD(fechah));
                    }

                    //Si tiene participantes
                    if (r.data.participantes) {
                        var participantes = r.data.participantes;
                        var recs = [];
                        Ext.each(participantes, function (participante) {

                            recs.push(participante);
                        })
                        var store = me.lookupReference('gridParticipantes').getStore();
                        store.setProxy({type: 'memory', data: recs});
                        store.load();
                    } else {
                        var store = me.lookupReference('gridParticipantes').getStore();
                        store.removeAll();
                    }

                    if (r.data.cantidadParticipantes) {
                        me.lookupReference('cantParticipantes').setValue(r.data.cantidadParticipantes);
                    }

                }

            }, function () {
                Ext.getBody().unmask();
                Ext.Msg.show({
                    msg: 'Error al obtener los datos',
                    title: 'Error',
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK,
                    scope: this
                });

            });
        }
    },

    onAddParticipante: function () {
        var comuna = null;

        if (this.getGrid().getSelectionModel() && this.getGrid().getSelectionModel().getSelection() && this.getGrid().getSelectionModel().getSelection().length > 0) {
            var proyecto = this.getForm().viewModel.projectData,
                idTiAc = this.getForm().viewModel.tipoActividad;

            /*if (operacion == this.OPERACION_CERRAR) {
                var

                switch (Math.abs(idTiAc)) {
                    case this.TIPO_ACTIVIDAD_TALLER_PADRES, this.TIPO_ACTIVIDAD_TALLER_INTERACTIVO:
                        this.onAddNewPerson();
                        break;
                    default:
                }
            } else {*/
            if (proyecto && proyecto.data && proyecto.data.comuna) {
                if (proyecto.data.comuna.id) {
                    comuna = proyecto.data.comuna;
                } else {
                    FCMPC.utils.Messages.error('No se ha definido una comuna para el proyecto seleccionado', 'Error', this);
                    return;
                }
            } else {
                FCMPC.utils.Messages.error('No se ha definido una comuna para el proyecto seleccionado', 'Error', this);
                return;
            }

            var me = this,
                w = Ext.create('FCMPC.view.AgregarDocentesParticipantesSesion'),
                queryParams = {
                    perfiles: this.__getTiposParticipantesPosibles().toString(),
                    comuna: comuna,
                    nivel: this.__obtenerNivelFromTituloSesion(this.getForm().viewModel.datosSesion.titulo),
                    establecimientos: this.__getIdsEstablecimientosFromProyecto().toString(),
                    showNewPersonButton: false
                };

            if ((operacion == this.OPERACION_CERRAR) &&
                ((Math.abs(idTiAc) == this.TIPO_ACTIVIDAD_TALLER_PADRES) ||
                    (Math.abs(idTiAc) == this.TIPO_ACTIVIDAD_TALLER_INTERACTIVO))) {
                queryParams.defaultTipoPersonaToAdd = FCMPC.Constants.PERFIL_PADRE;
                queryParams.showNewPersonButton = true;
            }
            w.viewModel = {
                queryParams: queryParams
            }

            console.log("Datos para participantes:", w.viewModel);

            w.fireEvent('setComuna', comuna);
            w.fireEvent('setSelectedRecords', this.__getSelectedParticipantes());
            w.show();
            w.fireEvent('setCloseCallback', function (records) {
                if (!records || records.length <= 0) return;

                var selected = [];
                Ext.Array.each(records, function (r) {
                    //console.log(r);
                    var cursos = null;
                    if (r.cursos != null && r.cursos != "") cursos = r.cursos;
                    var sel = {
                        idParticipante: '' + r.idContacto,
                        fullName: r.fullName,
                        perfil: r.perfil,
                        email: r.email,
                        cursos: {
                            cursos: r.cursos
                        },
                        idNivel: r.idNivel,
                        idEstablecimientoComuna: r.idEstablecimientoComuna,
                        idSubtipoPerfil: r.idSubtipoPerfil,
                        subtipoPerfil: r.subtipoPerfil,
                        nombreEstablecimiento: r.nombreEstablecimiento
                    };

                    selected.push(sel);
                });

                me.lookupReference('gridParticipantes').getStore().setProxy({type: 'memory', data: selected});
                me.lookupReference('gridParticipantes').getStore().load();
            });

            //////////////////}

        } else FCMPC.utils.Messages.error('Por favor seleccione una sesi&oacute;n', 'Error', this);
    },

    onAddNewPerson: function () {
    },

    onRemoveParticipante: function (grid, rowIndex) {
        var me = this;

        FCMPC.utils.Messages.question('Seguro desea borrar este participante?', 'Confirmaci&oacute;n', this, function (btnId) {
            if (btnId == "yes") {
                me.lookupReference('gridParticipantes').getStore().removeAt(rowIndex);
            }
        })
    },

    onEditEvaluaciones: function () {
        if (!this.__getSelectedRecordInGrid(this.getGrid())) {
            FCMPC.utils.Messages.error('Debe seleccionar una sesi&oacute;n', 'Error', this);
            return;
        }

        var me = this,
            idTiAc = this.getForm().viewModel.tipoActividad,
            w = Ext.create('FCMPC.view.EdicionEvaluacionesSesion');

        //console.log(this.getForm().viewModel);
        //console.log("idTiAc " + idTiAc);
        w.viewModel = this.getForm().viewModel;

        if (eval(idTiAc) == FCMPC.GeneralConstants.TIPO_ACTIVIDAD_ASESORIA || eval(idTiAc) == FCMPC.GeneralConstants.TIPO_ACTIVIDAD_ASESORIA_FLEXIBLE || eval(idTiAc) == FCMPC.GeneralConstants.TIPO_ACTIVIDAD_ASESORIA_DE_ANALISIS_DE_RESULTADOS) {
            if (!this.__getSelectedRecordInGrid(this.lookupReference('gridParticipantes'))) {
                FCMPC.utils.Messages.error('Por favor seleccione un participante', 'Error', this);
                return;
            }

            var store = this.lookupReference('gridParticipantes').getStore();

            w.viewModel.evaluacionesCargadas = store.getAt(store.indexOf(this.lookupReference('gridParticipantes').getSelectionModel().getSelection()[0])).get('evaluaciones');

        } else {
            w.viewModel.evaluacionesCargadas = this.getForm().viewModel.evaluaciones;
        }

        w.show();
        w.fireEvent('showEvaluaciones');
        w.fireEvent('setCloseCallback', function (evaluaciones) {
            if (!evaluaciones) return;

            if (eval(idTiAc) == FCMPC.GeneralConstants.TIPO_ACTIVIDAD_ASESORIA || eval(idTiAc) == FCMPC.GeneralConstants.TIPO_ACTIVIDAD_ASESORIA_FLEXIBLE || eval(idTiAc) == FCMPC.GeneralConstants.TIPO_ACTIVIDAD_ASESORIA_DE_ANALISIS_DE_RESULTADOS) { //La evaluacion es por cada participante
                var store = me.lookupReference('gridParticipantes').getStore(),
                    evals = {
                        evaluacion: evaluaciones
                    };

                store.getAt(store.indexOf(me.lookupReference('gridParticipantes').getSelectionModel().getSelection()[0])).set('evaluaciones', evaluaciones);

            } else {
                if (eval(idTiAc) == FCMPC.GeneralConstants.TIPO_ACTIVIDAD_CHARLA) {
                    var store = me.lookupReference('gridParticipantes').getStore();
                    store.each(function (record) {
                        if (record.get('perfil') == FCMPC.Constants.PERFIL_APODERADO_STR) {
                            record.set('evaluaciones', evaluaciones);
                        }
                    })
                } else {
                    //La evaluacion es por sesion
                    me.getForm().viewModel.evaluaciones = evaluaciones;
                }
            }
        })
    },

    __getSelectedParticipantes: function () {
        var selected = [];
        this.lookupReference('gridParticipantes').getStore().each(function (record) {
            selected.push(record.data);
        });

        return selected;
    },

    changeTypeScreen: function() {
        if(this.config.data.params.operacion == 'calendarizar')
            this.config.data.params.operacion = 'cerrar'
        else this.config.data.params.operacion = 'calendarizar'
        this.afterRender()
    }

});