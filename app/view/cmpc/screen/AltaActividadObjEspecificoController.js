/**
 * Created by Antonio on 2/5/2016.
 */
Ext.define('FCMPC.controller.AltaActividadObjEspecificoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.altaActividadObjEspecificoController',

    requires: [
        'ProcessManagement.util.cmpc.Messages'
    ],


    closeCallback: null,
    shouldClearValueComboNivel: false,
    
    onSave: function () {
        var vm = this.getView().getViewModel(),
            idNiveles = this.lookupReference('cmbNivel').getValue(),
            datos = {};

        if (!this.lookupReference('form').isValid()) {
            ProcessManagement.util.cmpc.Messages.error('Por favor complete los campos obligatorios', 'Error', this);
            return;
        }

        if (!this.lookupReference('cmbNivel').multiSelect && this.lookupReference('cmbNivel').getValue() != null && (typeof(this.lookupReference('cmbNivel').getValue()) == "object" && this.lookupReference('cmbNivel').getValue().length > 1)) {
            ProcessManagement.util.cmpc.Messages.error('Solo se permite 1 Nivel para la actividad seleccionada', 'Error', this);
            return;
        }

        if (idNiveles != null && Array.isArray(idNiveles)) {
            var str = "";
            idNiveles.forEach(function (nivel) {
                str += nivel;
                str += ",";
            });

            if (str.endsWith(",")) {
                str = str.substring(0, str.length - 1);
            }

            idNiveles = str;
        }

        datos = {
            idTipoActividad: this.lookupReference('cmbTipoActividad').getValue(),
            tipoActividad: this.lookupReference('cmbTipoActividad').getRawValue(),
            idCategoria: this.lookupReference('cmbCategoria').getValue(),
            categoria: this.lookupReference('cmbCategoria').getRawValue(),
            idArea: this.lookupReference('cmbArea').getValue(),
            area: this.lookupReference('cmbArea').getRawValue(),
            idNivel: idNiveles, //(this.lookupReference('cmbNivel').multiSelect == true)?this.lookupReference('cmbNivel').getValue().toString():this.lookupReference('cmbNivel').getValue(), //Revisar aca porque si viene null el value, se rompe el toString
            nivel: this.lookupReference('cmbNivel').getRawValue()
        };

        this.getView().getViewModel().set('datos', datos);

        //console.log("Datos guardados: ", this.getWin().viewModel);

        if (vm && vm != null) {
            datos.calendarizada = vm.calendarizada;
            datos.planificada = vm.planificada;

            this.getView().getViewModel().set('datos', datos);
        }

        this.onClose();
    },

    onClose: function () {
        if (this.closeCallback) {
            this.closeCallback(this.getView().getViewModel().get('datos'));
        }

        this.getView().close();
    },

    onShowSelected: function (record) {
        if (record.idNivel != null && !Array.isArray(record.idNivel)) {
            var s = "[" + record.idNivel + "]";
            record.idNivel = JSON.parse(s).map(String);
        }

        this.shouldClearValueComboNivel = false;

        this.lookupReference('cmbTipoActividad').setValue(record.idTipoActividad);
        this.lookupReference('cmbCategoria').setValue(record.idCategoria);
        this.lookupReference('cmbArea').setValue(record.idArea);
        this.lookupReference('cmbNivel').setValue(record.idNivel);

        if (!this.__shouldMultiSelectNivel(this.lookupReference('cmbTipoActividad').getValue())) {
            if (Array.isArray(record.idNivel) && record.idNivel.length > 1) {
                //FCMPC.utils.Messages.error('Solo se permite 1 Nivel para la actividad seleccionada', 'Error', this, function () {
                    record.idNivel = record.idNivel[0];
                    this.shouldClearValueComboNivel = true;
                //});
            }
        }

        this.lookupReference('cmbNivel').setValue(record.idNivel);
        this.getView().getViewModel().set('datos', record);

        //Fuerza el disparo del evento
        this.lookupReference('cmbTipoActividad').fireEvent('select', this.lookupReference('cmbTipoActividad'), null, null, this.shouldClearValueComboNivel);

    },

    onSetCloseCallback: function (cbk) {
        this.closeCallback = cbk;
    },

    onSelectActividad: function (combo, records, eOpts, shouldClearValue) {
        this.__enableMultiSelectNivel(combo.value, shouldClearValue);
        if (!this.lookupReference('cmbNivel').multiSelect) {
            if (Array.isArray(this.lookupReference('cmbNivel').getValue()) && this.lookupReference('cmbNivel').getValue().length > 1) {
                this.lookupReference('cmbNivel').clearValue();
            }
        }
    },

    __shouldMultiSelectNivel: function (value) {
        var allowedActivities = ["11", "22", "26", "28", "29", "31"],
            flag = false;


        allowedActivities.forEach(function (a) {
            if (value == a) flag = true;
        });

        return flag;
    },

    __enableMultiSelectNivel: function (value, shouldClearValue) {
        var flag = this.__shouldMultiSelectNivel(value);

        //console.log("Value: ", value, typeof(value), flag);

        //this.lookupReference('cmbNivel').multiSelect = flag;

        //Si se debe desactivar el multiSelect y el valor del combo es un array, se limpia el combo
        if (!flag /*&& typeof (this.lookupReference('cmbNivel').getValue()) == "object"*/) {
            //console.log("Se limpia el combo")
            if (shouldClearValue != null && shouldClearValue != undefined && shouldClearValue == true) this.lookupReference('cmbNivel').clearValue();
        }

        Ext.apply(this.lookupReference('cmbNivel'), {multiSelect: flag});
        delete this.lookupReference('cmbNivel').picker;

    }
});