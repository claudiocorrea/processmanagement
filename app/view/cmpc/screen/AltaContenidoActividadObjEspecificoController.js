/**
 * Created by Antonio on 2/5/2016.
 */
Ext.define('ProcessManagement.view.cmpc.screen.AltaContenidoActividadObjEspecificoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.altaContenidoActividadObjEspecificoController',

    closeCallback: null,

    onSave: function () {
        if (!this.lookupReference('form').isValid()) {
            Ext.Msg.show({
                msg: 'Por favor complete los campos obligatorios',
                title: 'Error',
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
            return;
        }

        this.getView().getViewModel().set('datos', {
            descripcion: this.lookupReference('txtContenido').getValue()
        });

        this.onClose();
    },

    onClose: function () {
        if (this.closeCallback) {
            this.closeCallback(this.getView().getViewModel().get('datos'));
        };

        this.getView().close();
    },

    onSetCloseCallback: function (cbk) {
        this.closeCallback = cbk;
    },

    onShowSelected: function (record) {
        this.lookupReference('txtContenido').setValue(record.get('descripcion'));
    }
});