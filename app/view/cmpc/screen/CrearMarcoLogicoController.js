Ext.define('ProcessManagement.view.cmpc.screen.CrearMarcoLogicoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.cmpcCrearMarcoLogico',

    config: {
        data: null
    },

    init: function() {
        this.data = this.getView().dataScreen
        console.log(this.data)
    },

    initScreen: function() {        
        if(this.data == undefined || this.data == {}) { 
            var controller = this
            this.getData(function(data) {
                controller.data = data.pantallas[1]
                controller.viewData()
            })   
        } else this.viewData()    
    },

    getData: function(callback) {
        console.log('Cargando datos...')
        Ext.Ajax.request({
            url: './DataFCMPC.json',
        }).then(function (o) {
            //console.log('them... ', o)
            callback(JSON.parse(o.responseText))
        }, function (o) {
            //console.log('error... ', o)
            callback(null)
        });
    },

    viewData: function() {
        console.log(this.data)
        this.lookupReference('cbProyecto').setStore({ data: this.data.proyecto})

        this.lookupReference('gridObjetivoGral').setStore(
            Ext.create('Ext.data.Store', { data: this.data.objetivoGral })
        )
        this.lookupReference('gridIndicadoresGral').setStore(
            Ext.create('Ext.data.Store', { data: this.data.indicadoresGral })
        )
        this.lookupReference('gridMediosGral').setStore(
            Ext.create('Ext.data.Store', { data: this.data.mediosGral })
        )
        this.lookupReference('gridObjetivoEspecifico').setStore(
            Ext.create('Ext.data.Store', { data: this.data.objetivoEspecifico })
        )
        this.lookupReference('gridIndicadoresEspecifico').setStore(
            Ext.create('Ext.data.Store', { data: this.data.indicadoresEspecifico })
        )
        this.lookupReference('gridMediosEspecifico').setStore(
            Ext.create('Ext.data.Store', { data: this.data.mediosEspecifico })
        )
    }

})