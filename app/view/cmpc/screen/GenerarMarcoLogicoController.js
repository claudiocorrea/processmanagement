Ext.define('ProcessManagement.view.cmpc.screen.GenerarMarcoLogicoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.cmpcgenerarMarcoLogico',

    requires: [
        'ProcessManagement.util.cmpc.JsonpRequest'
    ],

    config: {
        data: {
            params: {
                loggedUser: 'arojas',
                projectId: 358,
                edicion: 'L',
                mensaje: null
            }
        }
    },

    getSelectedRecordInGrid: function (grid) {
    if (!this.isRecordSelectedInGrid(grid)) return null;

        return grid.getSelectionModel().getSelection()[0];
    },

    isRecordSelectedInGrid: function (grid) {
        if (!grid) return false;
        return (grid.getSelectionModel() && grid.getSelectionModel().getSelection() && grid.getSelectionModel().getSelection().length > 0);
    },

    afterRender: function () {
        var me = this;

        console.log("vm: ", this.getView(), this.getViewModel(), this.config, this.getView().config);
         if (this.config.data.params.loggedUser !== null && this.config.data.params.loggedUser !== undefined && this.config.data.params.loggedUser != "") {
             this.lookupReference('cmbTipoProyecto').getStore().load({
                 scope: this,
                 params: {
                     loggedUser: this.config.data.params.loggedUser
                 },
                 callback: function (records, operation, success) {
                     if (this.config.data.params.projectId !== null && this.config.data.params.projectId != "") me.lookupReference('cmbTipoProyecto').setValue(this.config.data.params.projectId);
                 }
             });
         } else {
             this.lookupReference('cmbTipoProyecto').getStore().load({
                 scope: this,
                 /*params: {
                  tipo: 'marcoLogico'
                  },*/
                 callback: function (records, operation, success) {
                     if (this.config.data.params.projectId !== null && this.config.data.params.projectId != "") me.lookupReference('cmbTipoProyecto').setValue(this.config.data.params.projectId);
                 }
             });
         }
        //
         if (this.config.data.params.projectId !== null && this.config.data.params.projectId != "") {
             this.lookupReference('cmbTipoProyecto').setReadOnly(true);
         }

         if (this.config.data.params.mensaje != null && this.config.data.params.mensaje.trim() != "") this.__mostrarMensajeAlUsuario();

         if (this.config.data.params.edicion) {
             this.__setupParametroEdicion();
         }
    },

    /**
     * Setea el estado de la pantalla en base al parametro Edicion que viene en la URI
     *
     * @private
     */
    __setupParametroEdicion: function () {
        if (this.config.data.params.edicion == "L") {
            this.lookupReference('cmbObjGeneral').setReadOnly(true);
            this.lookupReference('cmdSaveMarcoLogico').setVisible(false);
            this.getViewModel().canEdit = false;
            return;
        }
        if (this.config.data.params.edicion == "E") {
            this.getViewModel().canEdit = true;
            return;
        }
        if (this.config.data.params.edicion == "EP") {
            this.lookupReference('cmbObjGeneral').setReadOnly(true)
            return;
        }
        if (this.config.data.params.edicion == "EPA") {
            this.lookupReference('cmbObjGeneral').setReadOnly(true)
            return;
        }

    },

    __mostrarMensajeAlUsuario: function () {

        FCMPC.utils.Messages.info(mensaje, 'Aviso', this);

    },

    /**
     * Elimina todas las "filas" en blanco que se generan en las grillas. Como tiene data especifica de esta pantalla, no se implementa en una clase helper
     *
     * @param jsonData  JSON que alimenta a todas las grillas
     * @private
     */
    __cleanEmptyObjectsFromJson: function (jsonData) {
        var objGrales = [], objEsp = [];

        //Comienzo limpiando Objetivos generales y sus Medios de Verificacion
        Ext.each(jsonData.objetivo, function (objetivo) {
            if (Object.keys(objetivo).length > 0) { //Si no es un objeto vac�o
                var mediosVerif = [];
                Ext.each(objetivo.omv, function (medioVerif) {
                    if (Object.keys(medioVerif).length > 0) {
                        mediosVerif.push(medioVerif);
                    }
                });
                objetivo.omv = mediosVerif;
                objGrales.push(objetivo);
            }
        });
        jsonData.objetivo = objGrales;

        //Limpia los Objetivos Especificos y sus hijos
        Ext.each(jsonData.objetivosEspecificos, function (objetivoE) {
            if (Object.keys(objetivoE).length > 0) {
                var indicadores = [], supuestos = [], actividades = [];

                //Limpia indicadores
                Ext.each(objetivoE.especificos, function (indicador) {
                    if (Object.keys(indicador).length > 0 && indicador.kindicadorId != null && indicador.kindicadorId != "") {
                        //console.log("indicador a agregar: ", indicador)
                        var mediosVerif = [];

                        //Limipia Medios de Verificacion
                        Ext.each(indicador.mv, function (medioVerif) {
                            if (Object.keys(medioVerif).length > 0) {
                                mediosVerif.push(medioVerif);
                            }
                        });
                        indicador.mv = mediosVerif;
                        indicadores.push(indicador);
                    }
                });

                objetivoE.especificos = indicadores;

                //Limpia Supuestos
                Ext.each(objetivoE.rsupuestos, function (supuesto) {
                    if (typeof(supuesto) == "string") {
                        supuestos.push(supuesto);
                    } else if (Object.keys(supuesto).length > 0) {
                        supuestos.push(supuesto);
                    }
                });
                objetivoE.rsupuestos = supuestos;

                //Limpia Actividades
                Ext.each(objetivoE.estrategia, function (actividad) {
                    if (Object.keys(actividad).length > 0) {
                        //console.log("actividad: ", actividad)
                        var ciclos = [], contenidos = [], tiposParticipantes = [];

                        //Limpia Ciclos
                        Ext.each(actividad.ciclo, function (ciclo) {
                            if (Object.keys(ciclo).length > 0) {
                                //console.log("ciclo a agregar: ", ciclo);
                                ciclos.push(ciclo);
                            }
                        });
                        actividad.ciclo = ciclos;

                        //Limpia Contenidos
                        Ext.each(actividad.contenidos, function (contenido) {
                            if (typeof(contenido) == "string") {
                                contenidos.push(contenido);
                            } else if (Object.keys(contenido).length > 0) {
                                contenidos.push(contenido);
                            }
                        });
                        actividad.contenidos = contenidos;

                        //Limpia Tipos Participantes
                        Ext.each(actividad.tipoParticipante, function (tipoParticipante) {
                            if (Object.keys(tipoParticipante).length > 0) {
                                tiposParticipantes.push(tipoParticipante);
                            }
                        });
                        actividad.tipoParticipante = tiposParticipantes;

                        actividades.push(actividad)
                    }
                });
                objetivoE.estrategia = actividades;

                objEsp.push(objetivoE);
            }
        });
        jsonData.objetivosEspecificos = objEsp;

        //console.log("JsonData limpio: ", jsonData)
        return jsonData;

    },

    /**
     * Obtiene los datos de un proyecto a partir del nombre del mismo
     * @param projectName
     * @private
     */
    __loadProjectData: function (projectName) {
        var me = this,
            url = ProcessManagement.util.cmpc.ServiceEndPoint.getMarcoLogico() + '/' + projectName + '/callback/Ext.data.JsonP.self.prototype.callbackObtenerMarcoLogico';

        Ext.getBody().mask('Cargando datos<br>Espere...');

        if (!Ext.data.JsonP.self.prototype.callbackObtenerMarcoLogico) {
            var me = this;
            Ext.data.JsonP.self.prototype.callbackObtenerMarcoLogico = function (jsonData) {
                Ext.getBody().unmask();

                var storeProyecto = me.lookupReference('cmbTipoProyecto').getStore(),
                    storeObjGeneral = me.lookupReference('cmbObjGeneral').getStore(),
                    storeIndicadoresObjGral = me.lookupReference('gridIndicadoresObjGral').getStore(),
                    storeObjEspecificos = me.lookupReference('gridObjEspecificos').getStore();

                var data = me.__cleanEmptyObjectsFromJson(jsonData.data); //jsonData.data;
                me.getViewModel().data = data;
                projectId = data.idProyecto;

                if (projectId == null || projectId == undefined) projectId = me.lookupReference('cmbTipoProyecto').getValue();

                if (data) {

                    me.lookupReference('txtNombreCorto').setValue(data.nombreProCorto);
                    me.lookupReference('cmbObjGeneral').clearValue();
                    me.lookupReference('cmbObjGeneral').getStore().load({
                        scope: me,
                        params: {
                            idTipoProyecto: projectId //this.lookupReference('cmbTipoProyecto').getValue()
                        },
                        callback: function () {
                            if (data.idObjetivoGeneral) {
                                me.lookupReference('cmbObjGeneral').setValue(data.idObjetivoGeneral);
                            }
                        }
                    });

                    if (data.objetivo) {
                        storeIndicadoresObjGral.setProxy({type: 'memory', data: data.objetivo});
                        storeIndicadoresObjGral.load();
                    }

                    if (data.objetivosEspecificos) {
                        Ext.each(data.objetivosEspecificos, function (oe) {
                            oe.isFromDb = true; //Indica que este OE viene almacenado en el json
                            if (oe.estrategia) {
                                Ext.each(oe.estrategia, function (actividad) {
                                    actividad.canEdit = (me.config.data.params.edicion == "EPA")?false:true;
                                })
                            }
                        })
                        storeObjEspecificos.setProxy({type: 'memory', data: data.objetivosEspecificos});
                        storeObjEspecificos.load();
                    } else {

                    }
                    me.getViewModel().plantas = (data.plantas)?data.plantas:[];
                    me.getViewModel().comuna = (data.comuna)?data.comuna:{};
                    me.getViewModel().periodo = data.periodo;
                    me.getViewModel().encargado = (data.encargado)?data.encargado:{};

                }
            }
        }
        ProcessManagement.util.cmpc.JsonpRequest.request(url, 'Ext.data.JsonP.self.prototype.callbackObtenerMarcoLogico', 'Ext.data.JsonP.self.prototype.callbackObtenerMarcoLogico', null, this, function () {

        },true);

    },

    onChangeTipoProyecto: function () {

        this.__clearAllLists();

        this.__loadProjectData(this.lookupReference('cmbTipoProyecto').getValue())
    },

    __clearAllLists: function () {
        this.lookupReference('gridIndicadoresObjGral').getStore().removeAll();

        this.lookupReference('gridObjEspecificos').getStore().removeAll();
        this.lookupReference('gridIndicadoresObjEspecificos').getStore().removeAll();
        this.lookupReference('gridSupuestosIndicadoresObjEspecificos').getStore().removeAll();
        this.lookupReference('gridActividadesObjEspecificos').getStore().removeAll();
        this.lookupReference('gridContenidosActividadesObjEspecifico').getStore().removeAll();
        this.lookupReference('gridTipoParticipantesActividadObjEspecifico').getStore().removeAll();
        this.lookupReference('gridCiclosActividadesObjEspecifico').getStore().removeAll();
    },

    onAddTipoProyecto: function (button, e, options) {
        var me = this;
        Ext.Msg.prompt('Nuevo Tipo de Proyecto', '', function (buttonId, text) {
            if (buttonId != "ok") return;

            if (text == null || text.trim() == "") {
                Ext.Msg.show({
                    title: 'Error',
                    msg: 'Debe ingresar una descripci&oacute;n para el nuevo Tipo de Proyecto',
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                })
            } else {
                var rec = {descripcion: text, id: Math.floor((Math.random() * 100) + 1)};
                me.lookupReference('cmbTipoProyecto').getStore().add(rec);
            }
        }, this);
    },

    onAddIndicadoresObjetivoGeneral: function (button, e, options) {
        //Descomentar estas 2 lineas en prod
        //if (!this.getViewModel().viewModel.canEdit) return;
        //if (!this.getViewModel().viewModel.canSave) return;

        if (this.lookupReference('cmbTipoProyecto').getValue() == null || this.lookupReference('cmbTipoProyecto').getValue() == "") return;

        var me = this;
        var w = Ext.create('ProcessManagement.view.cmpc.screen.AltaIndicadores');

        /*this.getView().add(w);

        w.show();

        if (this.lookupReference('altaIndicadores')) {
            console.log("wind:", this.lookupReference('altaIndicadores'));
        }

        return;*/
        w.show(null, function () {
            w.getController().onRefreshComboIndicadores('objGral', projectId);
            w.getController().onSetCloseCallback(function (record) {
                if (!record) return;
                var store = me.lookupReference('gridIndicadoresObjGral').getStore();
                store.add({
                    oano1: record.meta1,
                    oano2: record.meta2,
                    oindicador: record.indicadorStr,
                    oidIndicador: record.indicador,
                    oidLineaBase: record.idLineaBase,
                    olineaBase: record.lineaBase,
                    omv: record.medioVerifStr,
                    omvId: record.medioVerif,
                    oset: '',
                    osupuestos: null,
                    ovalor: record.valor
                });
            })
        }, this);
    },

    onSelectIndicadorGral: function (view, record) {
        this.__showMediosVerifIndicadorObjetivoGral(record);
        //this.__showSupuestosIndicadorObjetivoGral(record);
    },

    __showSupuestosIndicadorObjetivoGral: function (record) {
        if (record.data.osupuestos) {
            var recsupuestos = [];

            Ext.each(record.data.osupuestos, function (s) {
                var r = {descripcion: s};
                recsupuestos.push(r);
            });

            this.getGridSupuestosObjGral().getStore().setProxy({type: 'memory', data: recsupuestos});
            this.getGridSupuestosObjGral().getStore().load();
        } else {
            this.getGridSupuestosObjGral().getStore().removeAll();
        }
    },

    onEditIndicadorGral: function (view, record) {
        //Descomentar estas 2 lineas antes de enviar a prod
        //if (!this.getWin().viewModel.canEdit) return;
        //if (!this.getWin().viewModel.canSave) return;

        var me = this;
        var w = Ext.create('ProcessManagement.view.cmpc.screen.AltaIndicadores');
        w.show(null, function () {
            var rec = {
                valor: record.get('ovalor'),
                meta1: record.get('oano1'),
                meta2: record.get('oano2'),
                indicador: record.get('oindicador'),
                idIndicador: record.get('oidIndicador'),
                lineaBase: record.get('olineaBase'),
                medioVerif: record.get('omv'),
                idMedioVerif: record.get('omvId'),
                objetivoGral: me.lookupReference('cmbObjGeneral').getValue(),
                supuestos: record.get('osupuestos'),
                tipoIndicador: 'general',
                idTipoProyecto: projectId //me.lookupReference('cmbTipoProyecto').getValue()
            }
            console.log("rec a editar:", rec);
            w.getController().onShowSelectedIndicador(rec);
            w.getController().onSetCloseCallback(function (record) {
                if (!record) return;
                var store = me.lookupReference('gridIndicadoresObjGral').getStore();
                var editedRec = {
                    oano1: record.meta1,
                    oano2: record.meta2,
                    oindicador: record.indicadorStr,
                    oindicadorId: record.indicador,
                    olineaBase: record.lineaBase,
                    omv: record.medioVerifStr,
                    omvId: record.medioVerif,
                    oset: '',
                    //osupuestos: null,
                    ovalor: record.valor
                };
                console.log("record:", editedRec);
                console.log("editedRec:", editedRec);
                store.getAt(store.indexOf(me.lookupReference('gridIndicadoresObjGral').getSelectionModel().getSelection()[0])).set(editedRec);
            })
        }, this);
    },

    onSelectGridObjEspecificos: function (view, record) {
        //Limpia todas las grillas dependientes de los objetivos especificos
        this.lookupReference('gridIndicadoresObjEspecificos').getStore().removeAll();
        this.lookupReference('gridSupuestosIndicadoresObjEspecificos').getStore().removeAll();
        this.lookupReference('gridActividadesObjEspecificos').getStore().removeAll();
        this.lookupReference('gridContenidosActividadesObjEspecifico').getStore().removeAll();
        this.lookupReference('gridTipoParticipantesActividadObjEspecifico').getStore().removeAll();
        this.lookupReference('gridCiclosActividadesObjEspecifico').getStore().removeAll();
        this.lookupReference('gridMediosVerifIndicadoresObjEspecificos').getStore().removeAll();


        if (record.data.especificos) {
            var recs = [];
            console.log("Indicadores: ", record.data.especificos, record.data);
            this.lookupReference('gridIndicadoresObjEspecificos').getStore().setProxy({type: 'memory', data: record.data.especificos});
            this.lookupReference('gridIndicadoresObjEspecificos').getStore().load();
        } else {
            this.lookupReference('gridIndicadoresObjEspecificos').getStore().removeAll();
        }

        if (record.data.estrategia) {
            this.lookupReference('gridActividadesObjEspecificos').getStore().setProxy({type: 'memory', data: record.data.estrategia});
            this.lookupReference('gridActividadesObjEspecificos').getStore().load();
        } else {
            this.lookupReference('gridActividadesObjEspecificos').getStore().removeAll();
        }

        this.__showSupuestosObjEspecifico(view, record);
    },

    __showSupuestosObjEspecifico: function (view, record) {
        if (record.data.rsupuestos) {
            var recs = [];
            Ext.each(record.data.rsupuestos, function (s) {
                var r = {descripcion: s}
                recs.push(r);
            });
            this.lookupReference('gridSupuestosIndicadoresObjEspecificos').getStore().setProxy({type: 'memory', data: recs});
            this.lookupReference('gridSupuestosIndicadoresObjEspecificos').getStore().load();
        } else {
            this.lookupReference('gridSupuestosIndicadoresObjEspecificos').getStore().removeAll();
        }
    },

    onSelectGridIndicadoresObjEspecificos: function (view, record) {

        this.__showMediosVerifIndicadorObjetivoEspec(record);
    },

    onSelectGridActividadesObjEspecificos: function (view, record) {
        if (record.data.contenidos) {
            var recs = [];
            Ext.each(record.data.contenidos, function (c) {
                var r = {descripcion: c};
                recs.push(r);
            })
            this.lookupReference('gridContenidosActividadesObjEspecifico').getStore().setProxy({type: 'memory', data: recs});
            this.lookupReference('gridContenidosActividadesObjEspecifico').getStore().load();
        } else {
            this.lookupReference('gridContenidosActividadesObjEspecifico').getStore().removeAll();
        }

        if (record.data.tipoParticipante) {
            var recs = [];

            if (record.data.tipoParticipante instanceof Array) {
                Ext.each(record.data.tipoParticipante, function (r) {
                    if (r instanceof Object) {
                        //console.log(r);
                        recs.push(r);
                    }
                    else {
                        //console.log(r);
                        var t = {id: null, descripcion: r};
                        recs.push(t);
                    }
                })
            } else {
                Ext.each(record.data.tipoParticipante, function (t) {
                    //var r = {id: null, descripcion: t};
                    //recs.push(r);
                    recs.push(t);
                });
            }
            this.lookupReference('gridTipoParticipantesActividadObjEspecifico').getStore().setProxy({type: 'memory', data: recs});
            this.lookupReference('gridTipoParticipantesActividadObjEspecifico').getStore().load();
        } else {
            this.lookupReference('gridTipoParticipantesActividadObjEspecifico').getStore().removeAll();
        }

        if (record.data.ciclo) {
            this.lookupReference('gridCiclosActividadesObjEspecifico').getStore().setProxy({type: 'memory', data: record.data.ciclo});
            this.lookupReference('gridCiclosActividadesObjEspecifico').getStore().load();
        } else {
            this.lookupReference('gridCiclosActividadesObjEspecifico').getStore().removeAll();
        }
    },

    /*********************************************
     Bloque de manejo de Medios de Verif Indicador Gral
     */

    __showMediosVerifIndicadorObjetivoGral: function (record) {
        var mvRecs;

        if ((typeof  record.data.omv) == "string") {
            mvRecs = [{omvId: record.data.omvId, omv: record.data.omv}];
        } else {
            mvRecs = record.data.omv;
        }

        this.lookupReference('gridMediosVerifIndicadorOG').getStore().setProxy({type: 'memory', data: mvRecs});
        this.lookupReference('gridMediosVerifIndicadorOG').getStore().load();
    },

    onAddMedioVerifIndicadorOG: function () {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        //if (!this.getWin().viewModel.canSave) return;
        //if (!this.getWin().viewModel.canEdit) return;
        var me = this;

        if (this.isRecordSelectedInGrid(this.lookupReference('gridIndicadoresObjGral'))) {
            var store = this.lookupReference('gridIndicadoresObjGral').getStore();
            var recordSel = store.getAt(store.indexOf(this.lookupReference('gridIndicadoresObjGral').getSelectionModel().getSelection()[0]));

            var w = Ext.create('ProcessManagement.view.cmpc.screen.ConfigurarMedioVerificacion');
            w.getViewModel().set('idIndicador', recordSel.get('oidIndicador'));
            w.getViewModel().set('nombreIndicador', recordSel.get('oindicador'));
            w.show();
            w.getController().onSetCloseCallback(function (record) {
                if (!record) return;

                var store = me.lookupReference('gridIndicadoresObjGral').getStore();
                var recordSel = store.getAt(store.indexOf(me.lookupReference('gridIndicadoresObjGral').getSelectionModel().getSelection()[0]));

                var medios = [];
                Ext.each(recordSel.data.omv, function (s) {
                    if ((typeof  s) == "string") {
                        var r = {omvId: recordSel.data.omvId, omv: s};
                        medios.push(r);
                    } else medios.push(s);
                    //if (s != null && s != "") medios.push(s);
                });

                var r = {omvId: record.idMedioVerif, omv: record.medioVerif}
                medios.push(r);

                store.getAt(store.indexOf(me.lookupReference('gridIndicadoresObjGral').getSelectionModel().getSelection()[0])).set('omv', medios);
                me.__showMediosVerifIndicadorObjetivoGral(store.getAt(store.indexOf(me.lookupReference('gridIndicadoresObjGral').getSelectionModel().getSelection()[0])));
            });

        } else {
            ProcessManagement.util.cmpc.Messages.error('Por favor seleccione un Indicador', 'Error', this);
        }
    },

    onEditMedioVerifIndicadorOG: function (view, record) {
        //Descomentar estas 2 lineas antes de pasar a prod
        //if (!this.getWin().viewModel.canEdit) return;
        //if (!this.getWin().viewModel.canSave) return;

        var me = this;
        var w = Ext.create('ProcessManagement.view.cmpc.screen.ConfigurarMedioVerificacion');

        var recordInd = this.lookupReference('gridIndicadoresObjGral').getSelectionModel().getSelection()[0];

        w.getViewModel().set('idIndicador', recordInd.get('oidIndicador'));
        w.getViewModel().set('nombreIndicador', recordInd.get('oindicador'));

        w.show();
        w.getController().onSetCloseCallback(function (record) {
            if (!record) return;

            var store = me.lookupReference('gridMediosVerifIndicadorOG').getStore();
            var editedRec = {
                omv: record.medioVerif,
                omvId: record.idMedioVerif
            }

            store.getAt(store.indexOf(me.lookupReference('gridMediosVerifIndicadorOG').getSelectionModel().getSelection()[0])).set(editedRec);
            me.__actualizarListaMediosVerifIndicadorGral();

            store = me.lookupReference('gridIndicadoresObjGral').getStore();
            me.__showMediosVerifIndicadorObjetivoGral(store.getAt(store.indexOf(me.lookupReference('gridIndicadoresObjGral').getSelectionModel().getSelection()[0])));
        });
    },

    onRemoveMedioVerifIndicadorObjGral: function (grid, rowIndex) {
        if (!this.getWin().viewModel.canEdit) return;
        if (!this.getWin().viewModel.canSave) return;

        Ext.Msg.confirm('Confirmaci&oacute;n', 'Seguro desea borrar el Medio de Verificaci&oacute;n seleccionado?', function (btnId) {
            if (btnId == "yes") {
                grid.getStore().removeAt(rowIndex);
                this.__actualizarListaMediosVerifIndicadorGral();
            }
        }, this);
    },

    __actualizarListaMediosVerifIndicadorGral: function () {
        var recs = [];

        var store = this.lookupReference('gridMediosVerifIndicadorOG').getStore();
        store.data.each(function (r) {
            recs.push(r.data);
        })
        store = this.lookupReference('gridIndicadoresObjGral').getStore();
        store.getAt(store.indexOf(this.lookupReference('gridIndicadoresObjGral').getSelectionModel().getSelection()[0])).set('omv', recs);

    },

    /*********************************************
     Fin Bloque de manejo de Medios de Verif Indicador Gral
     */

    onAddSupuestoObjGral: function () {
        if (!this.getWin().viewModel.canEdit) return;
        if (!this.getWin().viewModel.canSave) return;


        if (this.isRecordSelectedInGrid(this.lookupReference('gridIndicadoresObjGral'))) {//.getSelectionModel() && this.lookupReference('gridIndicadoresObjGral').getSelectionModel().getSelection() && this.lookupReference('gridIndicadoresObjGral').getSelectionModel().getSelection().length > 0) {
            var me = this;
            var w = Ext.create('FCMPC.view.AltaSupuestos');
            w.show();
            w.fireEvent('setCloseCallback', function (record) {
                if (!record) return;
                var store = me.lookupReference('gridIndicadoresObjGral').getStore();
                var recordSel = store.getAt(store.indexOf(me.lookupReference('gridIndicadoresObjGral').getSelectionModel().getSelection()[0]));

                var supuestos = [];
                Ext.each(recordSel.data.osupuestos, function (s) {
                    if (s != null && s != "") supuestos.push(s);
                });
                supuestos.push(record.supuesto);
                store.getAt(store.indexOf(me.lookupReference('gridIndicadoresObjGral').getSelectionModel().getSelection()[0])).set('osupuestos', supuestos);

                //me.__showSupuestosIndicadorObjetivoGral(store.getAt(store.indexOf(me.lookupReference('gridIndicadoresObjGral').getSelectionModel().getSelection()[0])));
            }, this);
        } else {
            Ext.Msg.show({
                msg: 'Por favor seleccione un Indicador',
                title: 'Aviso',
                icon: Ext.Msg.WARNING,
                buttons: Ext.Msg.OK
            });
        }
    },

    onRemoveIndicadorObjGral: function (grid, rowIndex) {
        //Descomentar estas 2 lineas antes de pasar a prod
        //if (!this.getWin().viewModel.canEdit) return;
        //if (!this.getWin().viewModel.canSave) return;

        console.log('parametros:', grid, rowIndex);
        Ext.Msg.confirm('Confirmaci&oacute;n', 'Seguro desea borrar el Indicador seleccionado?', function (btnId) {
            if (btnId == "yes") {
                grid.getSelectionModel().deselectAll();
                //this.getGridSupuestosObjGral().getStore().removeAll();
                grid.getStore().removeAt(rowIndex);
            }
        }, this);
    },

    onRemoveSupuestoIndicadorObjGral: function (grid, rowIndex) {
        if (!this.getWin().viewModel.canEdit) return;
        if (!this.getWin().viewModel.canSave) return;

        Ext.Msg.confirm('Confirmaci&oacute;n', 'Seguro desea borrar el Supuesto seleccionado?', function (btnId) {
            if (btnId == "yes") {
                grid.getStore().removeAt(rowIndex);
                this.__actualizarListaSupuestosIndicadorGral();
            }
        }, this);
    },

    onEditSupuestoIndicadorGral: function (view, record) {
        if (!this.getWin().viewModel.canSave) return;

        var me = this;
        var w = Ext.create('FCMPC.view.AltaSupuestos');
        w.show(null, function () {
            var rec = {
                supuesto: record.data.descripcion
            };

            w.fireEvent('showSelectedSupuesto', rec);

            w.fireEvent('setCloseCallback', function (record) {
                if (!record) return;
                var store = me.getGridSupuestosObjGral().getStore();
                var editedRec = {
                    descripcion: record.supuesto
                };
                store.getAt(store.indexOf(me.getGridSupuestosObjGral().getSelectionModel().getSelection()[0])).set(editedRec);

                //Actualiza los datos del Indicador
                me.__actualizarListaSupuestosIndicadorGral();
            });
        }, this);
    },

    __actualizarListaSupuestosIndicadorGral: function () {
        var recs = [];

        var store = this.getGridSupuestosObjGral().getStore();
        store.data.each(function (r) {
            recs.push(r.get('descripcion'));
        })
        store = this.lookupReference('gridIndicadoresObjGral').getStore();
        store.getAt(store.indexOf(this.lookupReference('gridIndicadoresObjGral').getSelectionModel().getSelection()[0])).set('osupuestos', recs);

    },

    onAddComponenteObjEspecifico: function () {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        //if (!this.getWin().viewModel.canEdit) return;
        //if (!this.getWin().viewModel.canSave) return;

        if (this.lookupReference('cmbTipoProyecto').getValue() == null || this.lookupReference('cmbTipoProyecto').getValue() == "") return;

        var me = this;

        var w = Ext.create('ProcessManagement.view.cmpc.screen.AltaComponenteObjEspecificos');

        w.show();

        w.getController().onRefreshCombos(projectId);
        w.getController().onSetCloseCallback(function (record) {
            if (!record) return;
            var store = me.lookupReference('gridObjEspecificos').getStore();
            store.add({
                //idComponente: record.idComponente,
                //componente: record.componente,
                nombreObjetivo: record.nombreObjetivo,
                nombreCorto: record.nombreCorto,
                idObjetivo: record.idObjetivo,
                isFromDb: false
            });
        });
    },

    onEditComponenteObjEspecificos: function (view, record) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        //if (!this.getWin().viewModel.canEdit) return;
        //if (!this.getWin().viewModel.canSave) return;

        var me = this;
        var w = Ext.create('ProcessManagement.view.cmpc.screen.AltaComponenteObjEspecificos');
        w.show(null, function () {
            w.getController().onRefreshCombos(projectId);
            w.getController().onShowSelected(record);

            w.getController().onSetCloseCallback(function (record) {
                if (!record) return;
                //console.log(record)
                var store = me.lookupReference('gridObjEspecificos').getStore();

                store.getAt(store.indexOf(me.lookupReference('gridObjEspecificos').getSelectionModel().getSelection()[0])).set(record);

                //Actualiza los datos del Indicador
                //me.__actualizarListaSupuestosIndicadorGral();
            });
        });
    },

    onRemoveObjetivoEspecifico: function (grid, rowIndex) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        //if (!this.getWin().viewModel.canSave) return;

        var me = this;
        if (this.config.data.params.edicion == "EPA") {
            var obj = this.lookupReference('gridObjEspecificos').getStore().getAt(rowIndex)/*,
                actividades = obj.get('estrategia')*/;

            if (obj.data.isFromDb) {
                var actividades = [];
                Ext.each(obj.get('estrategia'), function (act) {
                    actividades.push(act);
                });

                if (actividades && actividades.length > 0) return; //Si tiene actividades, no se puede borrar
            }
            //return;
        }
        //TODO Descomentar esta linea antes de pasar a prod
        //else if (!this.getView().getViewModel().get('canEdit')) return;


        Ext.Msg.confirm('Confirmaci&oacute;n', 'Seguro desea borrar el Objetivo seleccionado?', function (btnId) {
            if (btnId == "yes") {
                grid.getSelectionModel().deselectAll();
                me.lookupReference('gridIndicadoresObjEspecificos').getStore().removeAll();
                me.lookupReference('gridSupuestosIndicadoresObjEspecificos').getStore().removeAll();
                me.lookupReference('gridMediosVerifIndicadoresObjEspecificos').getStore().removeAll();
                grid.getStore().removeAt(rowIndex);
            }
        }, this);
    },

    onAddIndicadorObjEspecifico: function () {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        if (this.isRecordSelectedInGrid(this.lookupReference('gridObjEspecificos'))) {

            var me = this;
            var w = Ext.create('ProcessManagement.view.cmpc.screen.AltaIndicadores');
            w.show(null, function () {
                var store = me.lookupReference('gridObjEspecificos').getStore();
                var id = projectId;
                w.getController().onRefreshComboIndicadores('objEspecifico', id);
                w.getController().onSetCloseCallback(function (record) {
                    if (!record) return;
                    var store = me.lookupReference('gridIndicadoresObjEspecificos').getStore();
                    store.add({
                        ano1: record.meta1,
                        ano2: record.meta2,
                        kindicador: record.indicadorStr,
                        kindicadorId: record.indicador,
                        lineaBase: record.lineaBase,
                        idLineaBase: record.idLineaBase,
                        mv: record.medioVerifStr,
                        mvId: record.medioVerif,
                        rsupuestos: null,
                        valor: record.valor
                    });
                    var recs = [];
                    store.data.each(function(r){
                        recs.push(r.data);
                    });
                    store = me.lookupReference('gridObjEspecificos').getStore();
                    store.getAt(store.indexOf(me.lookupReference('gridObjEspecificos').getSelectionModel().getSelection()[0])).set('especificos', recs);
                })
            }, this);
        } else {
            Ext.Msg.show({
                msg: 'Por favor seleccione un Objetivo Espec&iacute;fico',
                title: 'Aviso',
                icon: Ext.Msg.WARNING,
                buttons: Ext.Msg.OK
            });
        }
    },

    onEditIndicadorObjEspecifico: function (view, record) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        var me = this;
        var w = Ext.create('ProcessManagement.view.cmpc.screen.AltaIndicadores');
        w.show(null, function () {
            var store = me.lookupReference('gridObjEspecificos').getStore();
            var id = store.getAt(store.indexOf(me.lookupReference('gridObjEspecificos').getSelectionModel().getSelection()[0])).get('idObjetivo');

            var rec = {
                valor: record.get('valor'),
                meta1: record.get('ano1'),
                meta2: record.get('ano2'),
                indicador: record.get('kindicador'),
                idIndicador: record.get('kindicadorId'),
                lineaBase: record.get('idLineaBase'),
                medioVerif: record.get('mvId'),
                idMedioVerif: record.get('mvId'),
                objetivoGral: id,
                supuestos: record.get('rsupuestos'),
                tipoObjetivo: 'objEspecifico',
                idTipoProyecto: projectId//me.lookupReference('cmbTipoProyecto').getValue()
            }

            w.getController().onShowSelectedIndicador(rec);
            w.getController().onSetCloseCallback(function (record) {
                if (!record) return;
                var store = me.lookupReference('gridIndicadoresObjEspecificos').getStore();
                var editedRec = {
                    ano1: record.meta1,
                    ano2: record.meta2,
                    kindicador: record.indicadorStr,
                    kindicadorId: record.indicador,
                    lineaBase: record.lineaBase,
                    idLineaBase: record.idLineaBase,
                    mv: record.medioVerifStr,
                    mvId: record.medioVerif,
                    valor: record.valor
                };
                store.getAt(store.indexOf(me.lookupReference('gridIndicadoresObjEspecificos').getSelectionModel().getSelection()[0])).set(editedRec);

                //Actualiza la grilla de Objetivos Especificos
                var recs = [];
                store.data.each(function(r){
                    recs.push(r.data);
                });
                store = me.lookupReference('gridObjEspecificos').getStore();
                store.getAt(store.indexOf(me.lookupReference('gridObjEspecificos').getSelectionModel().getSelection()[0])).set('especificos', recs);

            })
        });
    },

    onRemoveIndicadorObjetivoEspecifico: function (grid, rowIndex) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        Ext.Msg.confirm('Confirmaci&oacute;n', 'Seguro desea borrar el Indicador seleccionado?', function (btnId) {
            if (btnId == "yes") {
                grid.getSelectionModel().deselectAll();
                this.lookupReference('gridSupuestosIndicadoresObjEspecificos').getStore().removeAll();
                grid.getStore().removeAt(rowIndex);
                this.__updateListaIndicadoresObjEspecifico();
            }
        }, this);

    },

    __updateListaIndicadoresObjEspecifico: function () {
        var store = this.lookupReference('gridIndicadoresObjEspecificos').getStore();
        var ind = [];
        store.data.each(function (record) {
            ind.push(record.data);
        });
        store = this.lookupReference('gridObjEspecificos').getStore();
        store.getAt(store.indexOf(this.lookupReference('gridObjEspecificos').getSelectionModel().getSelection()[0])).set('especificos', ind);
    },

    /********************************************
     * Comienzo bloque manejo de Medios Verif de OE
     */
    __showMediosVerifIndicadorObjetivoEspec: function (record) {
        var mvRecs;

        if ((typeof  record.data.mv) == "string") {
            mvRecs = [{mvId: record.data.mvId, mv: record.data.mv}];
        } else {
            mvRecs = record.data.mv;
        }

        this.lookupReference('gridMediosVerifIndicadoresObjEspecificos').getStore().setProxy({type: 'memory', data: mvRecs});
        this.lookupReference('gridMediosVerifIndicadoresObjEspecificos').getStore().load();
    },

    onAddMedioVerifIndicadorOE: function () {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        var me = this;

        if (this.isRecordSelectedInGrid(this.lookupReference('gridIndicadoresObjEspecificos'))) {
            var store = this.lookupReference('gridIndicadoresObjEspecificos').getStore();
            var recordSel = store.getAt(store.indexOf(this.lookupReference('gridIndicadoresObjEspecificos').getSelectionModel().getSelection()[0]));

            var w = Ext.create('ProcessManagement.view.cmpc.screen.ConfigurarMedioVerificacion');


            w.getViewModel().set('idIndicador', recordSel.get('kindicadorId'));
            w.getViewModel().set('nombreIndicador', recordSel.get('kindicador'));

            w.show();

            w.getController().onSetCloseCallback(function (record) {
                if (!record) return;

                var store = me.lookupReference('gridIndicadoresObjEspecificos').getStore();
                var recordSel = store.getAt(store.indexOf(me.lookupReference('gridIndicadoresObjEspecificos').getSelectionModel().getSelection()[0]));

                var medios = [];
                Ext.each(recordSel.data.mv, function (s) {
                    if ((typeof  s) == "string") {
                        var r = {mvId: recordSel.data.mvId, mv: s};
                        medios.push(r);
                    } else medios.push(s);
                    //if (s != null && s != "") medios.push(s);
                });

                var r = {mvId: record.idMedioVerif, mv: record.medioVerif}
                medios.push(r);

                store.getAt(store.indexOf(me.lookupReference('gridIndicadoresObjEspecificos').getSelectionModel().getSelection()[0])).set('mv', medios);
                me.__updateListaIndicadoresObjEspecifico();
                me.__showMediosVerifIndicadorObjetivoEspec(store.getAt(store.indexOf(me.lookupReference('gridIndicadoresObjEspecificos').getSelectionModel().getSelection()[0])));
            });

        } else {
            ProcessManagement.util.cmpc.Messages.error('Por favor seleccione un Indicador', 'Error', this);
        }
    },

    onEditMedioVerifObjEspecifico: function (view, record) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        var me = this;
        var w = Ext.create('ProcessManagement.view.cmpc.screen.ConfigurarMedioVerificacion');

        var recordInd = this.lookupReference('gridIndicadoresObjEspecificos').getSelectionModel().getSelection()[0];

        w.getViewModel().set('idIndicador', recordInd.get('kindicadorId'));
        w.getViewModel().set('nombreIndicador', recordInd.get('kindicador'));

        w.show();
        w.getController().onSetCloseCallback(function (record) {
            if (!record) return;

            var store = me.lookupReference('gridMediosVerifIndicadoresObjEspecificos').getStore();
            var editedRec = {
                mv: record.medioVerif,
                mvId: record.idMedioVerif
            }

            store.getAt(store.indexOf(me.lookupReference('gridMediosVerifIndicadoresObjEspecificos').getSelectionModel().getSelection()[0])).set(editedRec);
            me.__actualizarListaMediosVerifIndicadorObjEspec();

            me.__updateListaIndicadoresObjEspecifico();
        });
    },

    onRemoveMedioVerifIndicadorObjetivoEspecifico: function (grid, rowIndex) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        Ext.Msg.confirm('Confirmaci&oacute;n', 'Seguro desea borrar el Medio de Verificaci&oacute;n seleccionado?', function (btnId) {
            if (btnId == "yes") {
                grid.getStore().removeAt(rowIndex);
                this.__actualizarListaMediosVerifIndicadorObjEspec();
                this.__updateListaIndicadoresObjEspecifico();
            }
        }, this);
    },

    __actualizarListaMediosVerifIndicadorObjEspec: function () {
        var recs = [];

        var store = this.lookupReference('gridMediosVerifIndicadoresObjEspecificos').getStore();
        store.data.each(function (r) {
            recs.push(r.data);
        })
        store = this.lookupReference('gridIndicadoresObjEspecificos').getStore();
        store.getAt(store.indexOf(this.lookupReference('gridIndicadoresObjEspecificos').getSelectionModel().getSelection()[0])).set('mv', recs);

    },

    /********************************************
     * Fin bloque manejo de Medios Verif de OE
     */

    onAddSupuestoIndicadorObjEspecifico: function () {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        //if (!this.getWin().viewModel.canEdit) return;
        //if (!this.getWin().viewModel.canSave) return;

        if (this.isRecordSelectedInGrid(this.lookupReference('gridObjEspecificos'))) {
            var me = this;
            var w = Ext.create('ProcessManagement.view.cmpc.screen.AltaSupuestos');
            w.show();
            w.getController().onSetCloseCallback(function (record) {
                if (!record) return;
                var store = me.lookupReference('gridObjEspecificos').getStore();
                var recordSel = store.getAt(store.indexOf(me.lookupReference('gridObjEspecificos').getSelectionModel().getSelection()[0]));

                var supuestos = [];
                Ext.each(recordSel.data.rsupuestos, function (s) {
                    if (s != null && s != "") supuestos.push(s);
                });
                supuestos.push(record.supuesto);
                store.getAt(store.indexOf(me.lookupReference('gridObjEspecificos').getSelectionModel().getSelection()[0])).set('rsupuestos', supuestos);

                me.__showSupuestosIndicadorObjetivoEspecifico(store.getAt(store.indexOf(me.lookupReference('gridObjEspecificos').getSelectionModel().getSelection()[0])));

                me.__actualizarListaSupuestosObjEspecifico();
                //Actualiza la lista de Indicadores del Objetivo Especifico
                //me.__updateListaIndicadoresObjEspecifico();
            }, this);
        } else {
            Ext.Msg.show({
                msg: 'Por favor seleccione un Objetivo Espec&iacute;fico',
                title: 'Aviso',
                icon: Ext.Msg.WARNING,
                buttons: Ext.Msg.OK
            });
        }
    },

    onEditSupuestosIndicadoresObjEspecificos: function (view, record) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        var me = this;
        var w = Ext.create('ProcessManagement.view.cmpc.screen.AltaSupuestos');
        w.show(null, function () {
            var rec = {
                supuesto: record.data.descripcion
            };

            w.getController().onShowSelectedSupuesto(rec);

            w.getController().onSetCloseCallback(function (record) {
                if (!record) return;
                var store = me.lookupReference('gridSupuestosIndicadoresObjEspecificos').getStore();
                var editedRec = {
                    descripcion: record.supuesto
                };
                store.getAt(store.indexOf(me.lookupReference('gridSupuestosIndicadoresObjEspecificos').getSelectionModel().getSelection()[0])).set(editedRec);

                //Actualiza los datos del Indicador
                me.__actualizarListaSupuestosObjEspecifico();
                //me.__updateListaIndicadoresObjEspecifico();
            });
        }, this);

    },

    onRemoveSupuestoIndicadorObjetivoEspecifico: function (grid, rowIndex) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        Ext.Msg.confirm('Confirmaci&oacute;n', 'Seguro desea borrar el Supuesto seleccionado?', function (btnId) {
            if (btnId == "yes") {
                grid.getStore().removeAt(rowIndex);
                this.__actualizarListaSupuestosObjEspecifico();
                //this.__updateListaIndicadoresObjEspecifico();
            }
        }, this);
    },

    __actualizarListaSupuestosObjEspecifico: function () {
        var recs = [];

        var store = this.lookupReference('gridSupuestosIndicadoresObjEspecificos').getStore();
        store.data.each(function (r) {
            recs.push(r.get('descripcion'));
        })
        store = this.lookupReference('gridObjEspecificos').getStore();
        store.getAt(store.indexOf(this.lookupReference('gridObjEspecificos').getSelectionModel().getSelection()[0])).set('rsupuestos', recs);

    },

    __showSupuestosIndicadorObjetivoEspecifico: function (record) {
        if (record.data.rsupuestos) {
            var recsupuestos = [];

            Ext.each(record.data.rsupuestos, function (s) {
                var r = {descripcion: s};
                recsupuestos.push(r);
            });

            this.lookupReference('gridSupuestosIndicadoresObjEspecificos').getStore().setProxy({type: 'memory', data: recsupuestos});
            this.lookupReference('gridSupuestosIndicadoresObjEspecificos').getStore().load();
        } else {
            this.lookupReference('gridSupuestosIndicadoresObjEspecificos').getStore().removeAll();
        }
    },

    __addRecordsToStore: function (store, records) {
        if (store) {
            store.removeAll();
            store.add(records);
        }
    },

    __updateActividadesObjEspecifico: function () {
        //var me = this;
        var store = this.lookupReference('gridActividadesObjEspecificos').getStore();
        var records = [];
        store.each(function (record) {
            records.push(record.data);
        });

        store = this.lookupReference('gridObjEspecificos').getStore();
        store.getAt(store.indexOf(this.lookupReference('gridObjEspecificos').getSelectionModel().getSelection()[0])).set('estrategia', records);
    },

    onAddActivididadObjEspecifico: function () {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        if (this.isRecordSelectedInGrid(this.lookupReference('gridObjEspecificos'))) {
            var me = this;
            var w = Ext.create('ProcessManagement.view.cmpc.screen.AltaActividadObjEspecifico');
            w.show();
            w.getController().onSetCloseCallback(function (record) {
                //gridActividadesObjEspecificos
                if (!record) return;

                record.canEdit = true;
                record.calendarizada = "";
                record.planificada = (me.config.data.params.edicion && me.config.data.params.edicion != null && me.config.data.params.edicion == "EPA")?"no":"si";
                //console.log(record)
                var store = me.lookupReference('gridActividadesObjEspecificos').getStore();
                store.add(record);

                me.__updateActividadesObjEspecifico();
            });
        } else {
            Ext.Msg.show({
                msg: 'Por favor seleccione un Objetivo Espec&iacute;fico',
                title: 'Aviso',
                icon: Ext.Msg.WARNING,
                buttons: Ext.Msg.OK
            });
        }
    },

    onEditActividadObjEspecifico: function (view, record) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        if (!record.get('canEdit')) return;

        var me = this;
        var w = Ext.create('ProcessManagement.view.cmpc.screen.AltaActividadObjEspecifico');
        w.show();
        w.getController().onShowSelected(record.data);
        w.getController().onSetCloseCallback(function (record) {
            //gridActividadesObjEspecificos
            if (!record) return;

            //console.log("Record editado");
            //console.log(record);
            record.planificada = (me.config.data.params.edicion && me.config.data.params.edicion != null && me.config.data.params.edicion == "EPA")?"no":"si";

            var store = me.lookupReference('gridActividadesObjEspecificos').getStore();
            store.getAt(store.indexOf(me.lookupReference('gridActividadesObjEspecificos').getSelectionModel().getSelection()[0])).set(record);

            me.__updateActividadesObjEspecifico();
        });
    },

    onRemoveActividadObjetivoEspecifico: function (grid, rowIndex) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        var rec = grid.getStore().getAt(rowIndex);
        if (!rec.get('canEdit')) return;

        Ext.Msg.confirm('Confirmaci&oacute;n', 'Seguro desea borrar la Actividad seleccionada?', function (btnId) {
            if (btnId == "yes") {
                grid.getStore().removeAt(rowIndex);
                this.__updateActividadesObjEspecifico();
                this.lookupReference('gridContenidosActividadesObjEspecifico').getStore().removeAll();
                this.lookupReference('gridTipoParticipantesActividadObjEspecifico').getStore().removeAll();
                this.lookupReference('gridCiclosActividadesObjEspecifico').getStore().removeAll();
            }
        }, this);

    },

    __updateContenidosActividadObjEspecifico: function () {
        var store = this.lookupReference('gridContenidosActividadesObjEspecifico').getStore();
        var records = [];

        store.each(function (record) {
            records.push(record.data.descripcion);
        });

        store = this.lookupReference('gridActividadesObjEspecificos').getStore();
        store.getAt(store.indexOf(this.lookupReference('gridActividadesObjEspecificos').getSelectionModel().getSelection()[0])).set('contenidos', records);
        this.__updateActividadesObjEspecifico();
    },

    onAddContenidoActividadObjEspecifico: function () {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        if (this.isRecordSelectedInGrid(this.lookupReference('gridActividadesObjEspecificos'))) {
            if (this.lookupReference('gridContenidosActividadesObjEspecifico').getStore().getCount() >0) {return;}

            if (this.config.data.params.edicion == "EPA") {
                var actividad = this.getSelectedRecordInGrid(this.lookupReference('gridActividadesObjEspecificos'));
                if (!actividad.get('canEdit')) return;
            }

            var me = this;
            var w = Ext.create('ProcessManagement.view.cmpc.AltaContenidoActividadObjEspecifico');
            w.show();
            w.getController().onSetCloseCallback(function (record) {
                if (!record)return;
                var store = me.lookupReference('gridContenidosActividadesObjEspecifico').getStore();
                store.add(record);
                me.__updateContenidosActividadObjEspecifico();
            })
        } else {
            Ext.Msg.show({
                msg: 'Por favor seleccione una Actividad',
                title: 'Aviso',
                icon: Ext.Msg.WARNING,
                buttons: Ext.Msg.OK
            });
        }
    },

    onEditContenidoActividadObjEspecifico: function (view, record) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        if (this.config.data.params.edicion == "EPA") {
            var actividad = this.getSelectedRecordInGrid(this.lookupReference('gridActividadesObjEspecificos'));
            if (!actividad.get('canEdit')) return;
        }

        var me = this;
        var w = Ext.create('ProcessManagement.view.cmpc.AltaContenidoActividadObjEspecifico');
        w.show();

        w.getController().onShowSelected(record);
        w.getController().onSetCloseCallback(function (record) {
            //gridActividadesObjEspecificos
            if (!record) return;
            var store = me.lookupReference('gridContenidosActividadesObjEspecifico').getStore();
            store.getAt(store.indexOf(me.lookupReference('gridContenidosActividadesObjEspecifico').getSelectionModel().getSelection()[0])).set('descripcion', record.descripcion);

            me.__updateContenidosActividadObjEspecifico();
        });
    },

    onRemoveContenidoActividadObjetivoEspecifico: function (grid, rowIndex) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        if (this.config.data.params.edicion == "EPA") {
            var actividad = this.getSelectedRecordInGrid(this.lookupReference('gridActividadesObjEspecificos'));
            if (!actividad.get('canEdit')) return;
        }

        Ext.Msg.confirm('Confirmaci&oacute;n', 'Seguro desea borrar el Contenido seleccionado?', function (btnId) {
            if (btnId == "yes") {
                grid.getStore().removeAt(rowIndex);
                this.__updateContenidosActividadObjEspecifico();
            }
        }, this);
    },


    __updateTipoParticipantesActividadObjEspecifico: function () {
        var store = this.lookupReference('gridTipoParticipantesActividadObjEspecifico').getStore();
        var records = [];

        store.each(function (record) {
            records.push(record.data);
        });

        store = this.lookupReference('gridActividadesObjEspecificos').getStore();
        store.getAt(store.indexOf(this.lookupReference('gridActividadesObjEspecificos').getSelectionModel().getSelection()[0])).set('tipoParticipante', records);
        this.__updateActividadesObjEspecifico();
    },

    onAddTipoParticipanteActividadObjEspecifico: function () {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        if (this.isRecordSelectedInGrid(this.lookupReference('gridActividadesObjEspecificos'))) {
            if (this.config.data.params.edicion == "EPA") {
                var actividad = this.getSelectedRecordInGrid(this.lookupReference('gridActividadesObjEspecificos'));
                if (!actividad.get('canEdit')) return;
            }

            var me = this;
            var w = Ext.create('ProcessManagement.view.cmpc.screen.AltaParticipantesActividadObjEspecifico');
            w.show();
            w.getController().onSetCloseCallback(function (record) {
                if (!record)return;
                var store = me.lookupReference('gridTipoParticipantesActividadObjEspecifico').getStore();
                store.add(record);
                me.__updateTipoParticipantesActividadObjEspecifico();
            });
        } else {
            Ext.Msg.show({
                msg: 'Por favor seleccione una Actividad',
                title: 'Aviso',
                icon: Ext.Msg.WARNING,
                buttons: Ext.Msg.OK
            });
        }
    },

    onEditTipoParticipanteActividadObjEspecifico: function (view, record) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        if (this.config.data.params.edicion == "EPA") {
            var actividad = this.getSelectedRecordInGrid(this.lookupReference('gridActividadesObjEspecificos'));
            if (!actividad.get('canEdit')) return;
        }

        var me = this;
        var w = Ext.create('ProcessManagement.view.cmpc.screen.AltaParticipantesActividadObjEspecifico');
        w.show();

        w.getController().onShowSelected(record);
        w.getController().onSetCloseCallback(function (record) {
            if (!record) return;
            var store = me.lookupReference('gridTipoParticipantesActividadObjEspecifico').getStore();
            store.getAt(store.indexOf(me.lookupReference('gridTipoParticipantesActividadObjEspecifico').getSelectionModel().getSelection()[0])).set(record);

            me.__updateTipoParticipantesActividadObjEspecifico();
        });
    },

    onRemoveTipoParticipanteActividadObjetivoEspecifico: function (grid, rowIndex) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        if (this.config.data.params.edicion == "EPA") {
            var actividad = this.getSelectedRecordInGrid(this.lookupReference('gridActividadesObjEspecificos'));
            if (!actividad.get('canEdit')) return;
        }

        Ext.Msg.confirm('Confirmaci&oacute;n', 'Seguro desea borrar el Tipo de Participante seleccionado?', function (btnId) {
            if (btnId == "yes") {
                grid.getStore().removeAt(rowIndex);
                this.__updateTipoParticipantesActividadObjEspecifico();
            }
        }, this);
    },

    __updateCiclosActividadObjEspecifico: function () {
        var store = this.lookupReference('gridCiclosActividadesObjEspecifico').getStore();
        var records = [];

        store.each(function (record) {
            records.push(record.data);
        });

        store = this.lookupReference('gridActividadesObjEspecificos').getStore();
        store.getAt(store.indexOf(this.lookupReference('gridActividadesObjEspecificos').getSelectionModel().getSelection()[0])).set('ciclo', records);
        this.__updateActividadesObjEspecifico();
    },

    onAddCicloActividadObjEspecifico: function () {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        if (this.isRecordSelectedInGrid(this.lookupReference('gridActividadesObjEspecificos'))) {
            if (this.config.data.params.edicion == "EPA") {
                var actividad = this.getSelectedRecordInGrid(this.lookupReference('gridActividadesObjEspecificos'));
                if (!actividad.get('canEdit')) return;
            }

            var me = this;
            var w = Ext.create('ProcessManagement.view.cmpc.screen.AltaCicloActividadObjEspecifico');
            w.show();
            w.getController().onSetCloseCallback(function (record) {
                if (!record) return;
                var store = me.lookupReference('gridCiclosActividadesObjEspecifico').getStore();
                //console.log(store);
                store.add(record);
                //console.log(store);
                me.__updateCiclosActividadObjEspecifico();
            });
        } else {
            Ext.Msg.show({
                msg: 'Por favor seleccione una Actividad',
                title: 'Aviso',
                icon: Ext.Msg.WARNING,
                buttons: Ext.Msg.OK
            });
        }

    },

    onEditCicloActividadObjEspecifico: function (view, record) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        if (this.config.data.params.edicion == "EPA") {
            var actividad = this.getSelectedRecordInGrid(this.lookupReference('gridActividadesObjEspecificos'));
            if (!actividad.get('canEdit')) return;
        }

        var me = this;
        var w = Ext.create('ProcessManagement.view.cmpc.screen.AltaCicloActividadObjEspecifico');
        w.show();

        w.getController().onShowSelected(record);
        w.getController().onSetCloseCallback(function (record) {
            if (!record) return;
            var store = me.lookupReference('gridCiclosActividadesObjEspecifico').getStore();
            store.getAt(store.indexOf(me.lookupReference('gridCiclosActividadesObjEspecifico').getSelectionModel().getSelection()[0])).set(record);

            me.__updateCiclosActividadObjEspecifico();
        });
    },

    onRemoveCicloActividadObjetivoEspecifico: function (grid, rowIndex) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        if (this.config.data.params.edicion == "EPA") {
            var actividad = this.getSelectedRecordInGrid(this.lookupReference('gridActividadesObjEspecificos'));
            if (!actividad.get('canEdit')) return;
        }

        Ext.Msg.confirm('Confirmaci&oacute;n', 'Seguro desea borrar el Ciclo seleccionado?', function (btnId) {
            if (btnId == "yes") {
                grid.getStore().removeAt(rowIndex);
                this.__updateCiclosActividadObjEspecifico();
            }
        }, this);
    },

    onCloneActividadObjetivoEspecifico: function (grid, rowIndex) {
        //TODO Descomentar estas 2 lineas antes de pasar a prod
        // if (!this.getWin().viewModel.canEdit) return;
        // if (!this.getWin().viewModel.canSave) return;

        var rec = grid.getStore().getAt(rowIndex).data;
        var store = this.lookupReference('gridActividadesObjEspecificos').getStore();

        var tempRec = {
            idArea: rec.idArea,
            area: rec.area,
            idCategoria: rec.idCategoria,
            categoria: rec.categoria,
            ciclo: rec.ciclo,
            contenidos: rec.contenidos,
            idNivel: rec.idNivel,
            nivel: rec.nivel,
            idTipoActividad: rec.idTipoActividad,
            tipoActividad: rec.tipoActividad,
            tipoParticipante: rec.tipoParticipante,
            canEdit: true,
            planificada: (this.config.data.params.edicion && this.config.data.params.edicion != null && this.config.data.params.edicion == "EPA")?"no":"si",
            calendarizada: ""
        };

        //console.log(tempRec)
        store.add(tempRec);
        this.__updateActividadesObjEspecifico();
    },

    /**
     * Obtiene los datos cargados para todos los Indicadores Generales
     * @returns {Array}
     * @private
     */
    __getIndicadoresObjGral: function () {
        var store = this.lookupReference('gridIndicadoresObjGral').getStore();
        var recs = [];

        store.each(function(r){
            if (r.data != "" && Ext.JSON.encode(r.data) != "{}") {
                recs.push(r.data);
            }
        });

        return recs;
    },

    /**
     * Obtiene los datos cargados para todos los Objetivos Especificos
     * @returns {Array}
     * @private
     */
    __getObjetivosEspecificos: function () {
        var store = this.lookupReference('gridObjEspecificos').getStore();
        var recs = [];
        store.each(function (r) {
            if (r.data != "" && Ext.JSON.encode(r.data) != "{}") {
                recs.push(r.data);
            }
        });
        return recs;
    },

    __getEstablecimientos: function () {

    },

    /**
     * Elimina objetos vacios {} que vienen en el json
     * @param jsonData
     * @returns {*}
     * @private
     */
    __removeEmptyObjectsFromJSON: function (jsonData) {
        var arr = [];

        //console.log("----------------------- jsonData recibido: ");
        //console.log(jsonData);
        //Verifica OG
        Ext.each(jsonData.marcoLogicoV2.objetivo, function (og) {
            if (Object.keys(og).length > 0) {
                var ogt = og,
                    arrMv = [];

                //Verifica todos los Medios de Verificacion
                Ext.each(og.omv, function (mv) {
                    if (Object.keys(mv).length > 0) {
                        arrMv.push(mv);
                    }
                });

                ogt.omv = arrMv;

                arr.push(ogt);
            }
        });

        jsonData.marcoLogicoV2.objetivo = arr;

        //Verifica OE
        arr = [];
        Ext.each(jsonData.marcoLogicoV2.objetivosEspecificos, function (oe) {
            if (Object.keys(oe).length > 0) {
                var oet = oe;

                //Verifica OE
                var arrE = [];
                Ext.each(oe.especificos, function (e) {
                    if (Object.keys(e).length > 0) {
                        var et = e,
                            arrMv = [];

                        //Verifica Medios de Verificacion
                        Ext.each(e.mv, function (mv) {
                            if (Object.keys(mv).length > 0) {
                                arrMv.push(mv);
                            }
                        });

                        et.mv = arrMv;
                        arrE.push(et);
                    }
                });
                oet.especificos = arrE;

                //Verifica estrategia
                var arrEt = [];
                Ext.each(oe.estrategia, function (estrategia) {
                    if (Object.keys(estrategia).length > 0) {
                        var estrategiaT = estrategia,
                            arrCiclo = [],
                            arrContenidos = [],
                            arrTipoParticipante = [];

                        //Verifica ciclos
                        Ext.each(estrategia.ciclo, function (ciclo) {
                            if (Object.keys(ciclo).length > 0) {
                                arrCiclo.push(ciclo);
                            }
                        });

                        //Verifica contenidos
                        Ext.each(estrategia.contenidos, function (contenido) {
                            if (contenido != null && contenido.trim() != "") {
                                arrContenidos.push(contenido);
                            }
                        });

                        //Verifica tipoParticipante
                        Ext.each(estrategia.tipoParticipante, function (tipoParticipante) {
                            if (Object.keys(tipoParticipante).length > 0) {
                                arrTipoParticipante.push(tipoParticipante);
                            }
                        });

                        estrategiaT.ciclo = arrCiclo;
                        estrategiaT.contenidos = arrContenidos;
                        estrategiaT.tipoParticipante = arrTipoParticipante;

                        arrEt.push(estrategiaT);
                    }
                });
                oet.estrategia = arrEt;

                arr.push(oet);
            }
        });
        jsonData.marcoLogicoV2.objetivosEspecificos = arr;

        return jsonData;
    },

    /**
     * Guarda el Marco Logico
     */
    __generarDatos: function () {
        //console.log(this.getWin().viewModel);
        var jsonData = {
            marcoLogicoV2: {
                "@version": '1.0',
                nombrePro: /*(projectName != null && projectName != "")?projectName:*/this.lookupReference('cmbTipoProyecto').getRawValue(),
                nombreProCorto: this.getWin().viewModel.data.nombreProCorto,
                idProyecto: this.lookupReference('cmbTipoProyecto').getValue(),
                periodo: this.getWin().viewModel.periodo,
                fechaDesde: '',
                fechaHasta: '',
                encargado: this.getWin().viewModel.encargado,
                comuna: this.getWin().viewModel.comuna,
                plantas: this.getWin().viewModel.plantas,
                ObjetivoGeneral: this.getCmbObjGeneral().getRawValue(),
                idObjetivoGeneral: this.getCmbObjGeneral().getValue(),
                objetivo: this.__getIndicadoresObjGral(),
                objetivosEspecificos: this.__getObjetivosEspecificos(),
                idMarcoLogico: (this.getWin().viewModel.data.idMarcoLogico)?this.getWin().viewModel.data.idMarcoLogico:null,
                establecimientos: this.getWin().viewModel.data.establecimientos,
                profesionales: this.getWin().viewModel.data.profesionales
            }
        }

        return this.__removeEmptyObjectsFromJSON(jsonData);
    },

    onSaveMarcoLogico: function () {
        if (!this.__isDatosValidados()) return;
        if (this.lookupReference('cmbTipoProyecto').getValue() == null || this.lookupReference('cmbTipoProyecto').getValue() == "") return;

        Ext.Msg.confirm('Confirmaci&oacute;n', 'Seguro desea guardar los datos?', function (btnId) {
            if (btnId == "yes") {

                //console.log("---------------------------------- Antes del remove")
                var jsonData = this.__generarDatos();
                //console.log(jsonData);
                /*var jsonData = FCMPC.Helpers.removeEmptyObjectsFromJson(this.__generarDatos());
                //console.log("---------------------------------- Despues del remove")
                //console.log(Ext.JSON.encode(jsonData));
                console.log(jsonData);*/

                //return;

                Ext.Ajax.useDefaultXhrHeader = false;
                Ext.Ajax.cors = true;

                var url = FCMPC.utils.ServiceEndPoint.getMarcoLogico() + '/' + this.lookupReference('cmbTipoProyecto').getValue();
                Ext.getBody().mask('Espere...');
                Ext.Ajax.request({
                    url: url,
                    disableCaching:true,
                    jsonData: jsonData,
                    method: 'PUT',
                    scope: this,
                    success: function (response, options) {
                        Ext.getBody().unmask();
                        var resp = Ext.JSON.decode(response.responseText, true);

                        if (resp == null) {
                            resp = {success: false, errorMsg: 'Error al parsear la respuesta'};
                        }

                        if (resp.success == true) {
                            Ext.Msg.show({
                                msg: (resp.data)?resp.data:'Datos grabados con &eacute;xito',
                                title: 'Aviso',
                                icon: Ext.Msg.INFO,
                                buttons: Ext.Msg.OK,
                                scope: this
                            });

                            //Desactiva el modo de grabacion
                            this.getWin().viewModel.canSave = false;
                            this.getCmdSaveMarcoLogico().setVisible(false);
                            //Desactiva el combo para que no se pueda seleccionar otro proyecto
                            this.lookupReference('cmbTipoProyecto').setReadOnly(true);
                            this.getCmbObjGeneral().setReadOnly(true);

                        } else {
                            Ext.Msg.show({
                                msg: 'Se produjo un error al grabar los datos',
                                title: 'Error',
                                icon: Ext.Msg.ERROR,
                                buttons: Ext.Msg.OK,
                                scope: this
                            });
                        }
                    },
                    failure: function () {
                        Ext.getBody().unmask();
                        Ext.Msg.show({
                            msg: 'Error al grabar los datos',
                            title: 'Error',
                            icon: Ext.Msg.ERROR,
                            buttons: Ext.Msg.OK,
                            scope: this
                        });
                    }
                });
            }
        }, this);


    },

    __isDatosValidados: function () {
        var flag = true;
        if (!this.getForm().isValid()) {
            Ext.Msg.show({
                msg: 'Por favor complete los campos obligatorios',
                title: 'Error',
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK,
                scope: this
            });
            return false;
        }

        var datos = this.__generarDatos();

        //Verifica si hay Indicadores en OG
        if (datos.marcoLogicoV2.objetivo && datos.marcoLogicoV2.objetivo.length <= 0) {
            FCMPC.utils.Messages.error('Debe cargar al menos 1(un) Indicador en Objetivo General', 'Error', this);
            return false;
        } /*else {
            Ext.each(datos.marcoLogicoV2.objetivo, function (r) {
                if (r.osupuestos) {
                    if (r.osupuestos.length <= 0) {
                        FCMPC.utils.Messages.error('Debe cargar al menos 1(un) Supuesto para cada Indicador en Objetivo General', 'Error', this);
                        flag = false;
                        return flag;
                    }
                } else {
                    FCMPC.utils.Messages.error('Debe cargar al menos 1(un) Supuesto para cada Indicador en Objetivo General', 'Error', this);
                    flag = false;
                    return flag;
                }
            });
            if (!flag) return false;
        }*/

        //Verifica si tiene Objetivos Especificos
        if (datos.marcoLogicoV2.objetivosEspecificos && datos.marcoLogicoV2.objetivosEspecificos.length <= 0) {
            FCMPC.utils.Messages.error('Debe cargar al menos 1(un) Objetivo Espec&iacute;fico', 'Error', this);
            return false;
        }

        //Verifica si hay al menos 1 Indicador en cada OE
        flag = true;
        Ext.each(datos.marcoLogicoV2.objetivosEspecificos, function (record) {
            if (record.especificos) {
                if (record.especificos.length <= 0) {
                    FCMPC.utils.Messages.error('Debe cargar al menos 1(un) Indicador en cada Objetivo Espec&iacute;fico', 'Error', this);
                    flag = false;
                    return flag;
                }
            } else {
                FCMPC.utils.Messages.error('Debe cargar al menos 1(un) Indicador en cada Objetivo Espec&iacute;fico', 'Error', this);
                flag = false;
                return flag;
            }
        });
        if (!flag) return false;

        //Verifica si hay al menos 1 Supuesto por cada Indicador en cada OE
        flag = true;
        Ext.each(datos.marcoLogicoV2.objetivosEspecificos, function (record) {
            //Ext.each(record.especificos, function (rec) {
            //    console.log(rec);
            if (record.rsupuestos) {
                if (record.rsupuestos.length <= 0) {
                    FCMPC.utils.Messages.error('Debe cargar al menos 1(un) Supuesto para cada Objetivo Espec&iacute;fico', 'Error', this);
                    flag = false;
                    return flag;
                }
            } else {
                FCMPC.utils.Messages.error('Debe cargar al menos 1(un) Supuesto para cada Objetivo Espec&iacute;fico', 'Error', this);
                flag = false;
                return flag;
            }
            //});
            //if (!flag) return false;
        });
        if (!flag) return false;

        //Verifica si hay al menos 1 actividad en cada OE
        flag = true;
        Ext.each(datos.marcoLogicoV2.objetivosEspecificos, function (record) {
            if (record.estrategia) {
                if (record.estrategia.length <= 0) {
                    FCMPC.utils.Messages.error('Debe cargar al menos 1(una) Actividad en cada Objetivo Espec&iacute;fico', 'Error', this);
                    flag = false;
                    return flag;
                }
            } else {
                FCMPC.utils.Messages.error('Debe cargar al menos 1(una) Actividad en cada Objetivo Espec&iacute;fico', 'Error', this);
                flag = false;
                return flag;
            }
        });
        if (!flag) return false;

        //Verifica que por cada actividad haya al menos 1 Contenido, 1 Tipo de Participante y 1 Ciclo
        flag = true;
        Ext.each(datos.marcoLogicoV2.objetivosEspecificos, function (record) {
            //Verifica si hay contenidos en esta actividad
            Ext.each(record.estrategia, function (rec) {
                if (rec.contenidos) {
                    if (rec.contenidos.length <= 0) {
                        FCMPC.utils.Messages.error('Debe cargar al menos 1(un) Contenido por Actividad en cada Objetivo Espec&iacute;fico', 'Error', this);
                        flag = false;
                        return false;
                    }
                } else {
                    FCMPC.utils.Messages.error('Debe cargar al menos 1(un) Contenido por Actividad en cada Objetivo Espec&iacute;fico', 'Error', this);
                    flag = false;
                    return false;
                }

                //Verifica si hay Tipo de Participantes en esta actividad
                if (rec.tipoParticipante) {
                    if (rec.tipoParticipante.length <= 0) {
                        FCMPC.utils.Messages.error('Debe cargar al menos 1(un) Tipo de Participante por Actividad en cada Objetivo Espec&iacute;fico', 'Error', this);
                        flag = false;
                        return false;
                    }
                } else {
                    FCMPC.utils.Messages.error('Debe cargar al menos 1(un) Tipo de Participante por Actividad en cada Objetivo Espec&iacute;fico', 'Error', this);
                    flag = false;
                    return false;
                }

                //Verifica si hay Ciclos en esta actividad
                if (rec.ciclo) {
                    if (rec.ciclo.length <= 0) {
                        FCMPC.utils.Messages.error('Debe cargar al menos 1(un) Ciclo por Actividad en cada Objetivo Espec&iacute;fico', 'Error', this);
                        flag = false;
                        return false;
                    }
                } else {
                    FCMPC.utils.Messages.error('Debe cargar al menos 1(un) Ciclo por Actividad en cada Objetivo Espec&iacute;fico', 'Error', this);
                    flag = false;
                    return false;
                }

            });
            if (!flag) return false;
        });
        if (!flag) return false;

        return true;
    }

})