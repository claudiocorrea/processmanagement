/**
 * Created by Antonio on 3/23/2016.
 */
Ext.define('ProcessManagement.view.cmpc.screen.ConfigurarMedioVerificacionController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.configurarMedioVerificacion',


    closeCallback: null,

    onSetCloseCallback: function (cbk) {
        this.closeCallback = cbk;
    },

    afterRender: function () {
        this.lookupReference('itemselector').getStore().load({
            params: {
                idIndicador: this.getView().getViewModel().get('idIndicador')
            }
        });
        this.lookupReference('dspIndicador').setValue(this.getView().getViewModel().get('nombreIndicador'));
    },

    onSave: function () {
        if (!this.lookupReference('form').isValid()) {
            ProcessManagement.util.cmpc.Messages.error('Por favor complete los campos obligatorios', 'Error', this);
            return;
        }
        var str = this.lookupReference('itemselector').getRawValue(),
            val = this.lookupReference('itemselector').getValue(),
            retObj = {};

        this.getView().getViewModel().set('idMedioVerif', this.lookupReference('itemselector').getValue());
        this.getView().getViewModel().set('medioVerif', this.lookupReference('itemselector').getRawValue());

        retObj = {
            idMedioVerif: this.lookupReference('itemselector').getValue(),
            medioVerif: this.lookupReference('itemselector').getRawValue()
        }

        if (this.closeCallback) {
            this.closeCallback(retObj);
        }

        this.onClose();
    },

    onClose: function () {
        this.getView().close();
    }
});