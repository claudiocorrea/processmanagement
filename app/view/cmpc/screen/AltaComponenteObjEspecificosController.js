/**
 * Created by Antonio on 2/3/2016.
 */
Ext.define('ProcessManagement.view.cmpc.screen.AltaComponenteObjEspecificosController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.altaComponenteObjEspecificosController',

    closeCallback: null,

    onSave: function () {
        if (!this.lookupReference('form').isValid()) {
            ProcessManagement.util.cmpc.Messages.error('Por favor complete los campos obligatorios', 'Error', this);
            return;
        }

        var store = this.lookupReference('cmbObjEspecifico').getStore();
        var rec = store.getAt(store.find('id', this.lookupReference('cmbObjEspecifico').getValue()));
        this.getView().getViewModel().set('datos', {
            //componente: this.getCmbComponente().getRawValue(),
            //idComponente: this.getCmbComponente().getValue(),
            nombreObjetivo: this.lookupReference('cmbObjEspecifico').getRawValue(),
            nombreCorto: rec.get('nombreCorto'),
            idObjetivo: this.lookupReference('cmbObjEspecifico').getValue()
        })
        this.onClose();
    },

    onClose: function () {
        if (this.closeCallback) {
            this.closeCallback(this.getView().getViewModel().get('datos'));
        }
        this.getView().close();
    },

    onSetCloseCallback: function (cbk) {
        this.closeCallback = cbk;
    },

    onShowSelected: function (record) {
        //this.getCmbComponente().setValue(record.get('idComponente'));
        this.lookupReference('cmbObjEspecifico').setValue(record.get('idObjetivo'));
    },

    onRefreshCombos: function (idTipoProyecto) {
        this.lookupReference('cmbObjEspecifico').getStore().load({
            params: {
                idTipoProyecto: idTipoProyecto
            }
        });
    }
});