Ext.define('ProcessManagement.view.cmpc.screen.AnioProyectoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.cmpcanioproyecto',

    config: {
        data: null
    },

    init: function() {
        this.data = this.getView().dataScreen
        console.log(this.data)
    },

    initScreen: function() {        
        if(this.data == undefined || this.data == {}) { 
            var controller = this
            this.getData(function(data) {
                controller.data = data.pantallas[5]
                controller.viewData()
            })   
        } else this.viewData()    
    },

    getData: function(callback) {
        console.log('Cargando datos...')
        Ext.Ajax.request({
            url: './DataFCMPC.json',
        }).then(function (o) {
            //console.log('them... ', o)
            callback(JSON.parse(o.responseText))
        }, function (o) {
            //console.log('error... ', o)
            callback(null)
        });
    },

    viewData: function() {
        console.log(this.data)
        this.lookupReference('gridIndicadoresGrals').setStore(
            Ext.create('Ext.data.Store', { 
                fields:[ 'oindicador', 'obase', 'oanio1', 'oanio2', 'orealanio1', 'orealanio2'],
                data: this.data.indicadoresGral })
        )
        this.lookupReference('gridIndicadoresEspecificos').setStore(
            Ext.create('Ext.data.Store', { 
                fields:[ 'oindicador', 'obase', 'oanio1', 'oanio2', 'orealanio1', 'orealanio2'],
                data: this.data.indicadoresEspecifico })
        )
    }

})