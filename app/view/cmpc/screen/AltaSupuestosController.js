/**
 * Created by Antonio on 2/2/2016.
 */
Ext.define('ProcessManagement.view.cmpc.screen.AltaSupuestosController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.altaSupuestosController',

    closeCallback: null,

    onSave: function () {
        if (!this.lookupReference('form').isValid()) {
            Ext.Msg.show({
                msg: 'Por favor complete los campos obligatorios',
                title: 'Error',
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
            return;
        }
        this.getView().getViewModel().set('datos', {
            supuesto: this.lookupReference('txtSupuesto').getValue()
        });

        this.onClose();
    },

    onClose: function () {
        if (this.closeCallback) {
            this.closeCallback(this.getView().getViewModel().get('datos'));
        }
        this.getView().close();
    },

    onSetCloseCallback: function (cbk) {
        this.closeCallback = cbk;
    },

    onShowSelectedSupuesto: function (record) {
        if (record) {
            this.lookupReference('txtSupuesto').setValue(record.supuesto);
        }
    }
});