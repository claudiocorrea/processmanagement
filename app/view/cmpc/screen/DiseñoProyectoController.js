Ext.define('ProcessManagement.view.cmpc.screen.DiseñoProyectoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.cmpcdiseñoproyecto',

    config: {
        data: null
    },

    init: function() {
        this.data = this.getView().dataScreen
        console.log(this.data)
    },

    initScreen: function() {        
        if(this.data == undefined || this.data == {}) { 
            var controller = this
            this.getData(function(data) {
                controller.data = data.pantallas[0]
                controller.viewData()
            })   
        } else this.viewData()    
    },

    getData: function(callback) {
        console.log('Cargando datos...')
        Ext.Ajax.request({
            url: './DataFCMPC.json',
        }).then(function (o) {
            //console.log('them... ', o)
            callback(JSON.parse(o.responseText))
        }, function (o) {
            //console.log('error... ', o)
            callback(null)
        });
    },

    viewData: function() {
        console.log(this.data)
        this.lookupReference('cbTipoProyecto').setStore({ data: this.data.tipoProyecto})
        this.lookupReference('cbEncargado').setStore({ data: this.data.encargado})
        this.lookupReference('cbComuna').setStore({ data: this.data.comuna})
        this.lookupReference('cbPeriodoInit').setStore({ data: this.data.periodo})
        this.lookupReference('cbPeriodoFin').setStore({ data: this.data.periodo})

        this.lookupReference('gridPlantas').setStore(
            Ext.create('Ext.data.Store', { data: this.data.plantas })
        )
        this.lookupReference('gridEstablecimientos').setStore(
            Ext.create('Ext.data.Store', { data: this.data.establecimientos })
        )
        this.lookupReference('gridProfesionales').setStore(
            Ext.create('Ext.data.Store', { data: this.data.profesionales })
        )
    }

})