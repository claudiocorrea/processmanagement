/**
 * Created by Antonio on 2/7/2016.
 */
Ext.define('FCMPC.controller.AltaParticipantesActividadObjEspecificoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.altaParticipantesActividadObjEspecificoController',

    requires: [
        'ProcessManagement.util.cmpc.Messages'
    ],
    
    closeCallback: null,

    onSetCloseCallback: function (cbk) {
        this.closeCallback = cbk;
    },

    onShowSelected: function (record) {
        this.lookupReference('cmbTipoParticipante').setValue(eval(record.get('id')));
    },

    onSave: function () {
        if (!this.lookupReference('form').isValid()) {
            ProcessManagement.util.cmpc.Messages.error('Por favor complete los campos obligatorios', 'Error', this);
            return;
        }
        var datos = {};

        datos = {
            descripcion: this.lookupReference('cmbTipoParticipante').getRawValue(),
            id: this.lookupReference('cmbTipoParticipante').getValue()
        };

        this.getView().getViewModel().set('datos', datos);

        this.onClose();
    },

    onClose: function () {
        if (this.closeCallback) {
            this.closeCallback(this.getView().getViewModel().get('datos'));
        };

        this.getView().close();
    }
});