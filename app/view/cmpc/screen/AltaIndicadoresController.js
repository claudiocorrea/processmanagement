/**
 * Created by Antonio on 2/2/2016.
 */
Ext.define('ProcessManagement.view.cmpc.screen.AltaIndicadoresController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.altaIndicadores',

    closeCallback: null,

    config: null,

    onAfterRender: function () {
        if (anio) {
            if (anio == 1) {
                this.lookupReference('lineaBase').setReadOnly(false);
                this.lookupReference('meta1')().setReadOnly(false);
                this.lookupReference('meta2').setReadOnly(false);
            } else {
                if (anio == 2) {
                    this.lookupReference('lineaBase').setReadOnly(true);
                    this.lookupReference('meta1')().setReadOnly(true);
                    this.lookupReference('meta2').setReadOnly(true);
                } else {
                    if (anio == 0) {
                        this.lookupReference('lineaBase').setReadOnly(true);
                        this.lookupReference('meta1')().setReadOnly(true);
                        this.lookupReference('meta2').setReadOnly(true);
                    }
                }
            }
        }
    },

    onRefreshComboIndicadores: function (idTipoObj, idObj) {
        var me = this;
        if (idTipoObj == "objGral") {
            this.getView().mask('Espere...');
            this.lookupReference('cmbIndicador').getStore().load({
                params: {
                    //idObjetivoGeneral: idObj
                    tipo: 'general',
                    idTipoProyecto: idObj
                },
                callback: function () {
                    me.lookupReference('cmbIndicador').getStore().filter("seleccionado", "true");
                    me.getView().unmask();
                }
            });
        } else {
            this.getView().mask('Espere...');
            this.lookupReference('cmbIndicador').getStore().load({
                params: {
                    //idObjetivoEspecifico: idObj
                    tipo: 'especifico',
                    idTipoProyecto: idObj
                },
                callback: function () {
                    me.getView().unmask();
                }
            });
        }
    },

    onSelectIndicador: function () {
        //var me = this;
        //this.getWin().mask('Espere...');
        //this.getCmbMedioVerificacion().clearValue();
        /*this.getCmbMedioVerificacion().getStore().load({
            scope: this,
            params: {
                idIndicador: this.lookupReference('cmbIndicador').getValue()
            },
            callback: function () {
                me.getWin().unmask();
            }
        });*/
    },

    onSetCloseCallback: function (cbk) {
        this.closeCallback = cbk;
    },

    onClose: function () {
        if (this.closeCallback) {
            this.closeCallback(this.config);
        };
        this.getView().close();
    },

    onSave: function () {
        if (!this.lookupReference('form').isValid()) {
            Ext.Msg.show({
                msg: 'Por favor complete los campos obligatorios',
                title: 'Error',
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
            return;
        }

        if (!this.__validateValueRegex(this.lookupReference('lineaBase'))) return;
        if (!this.__validateValueRegex(this.lookupReference('meta1'))) return;
        if (!this.__validateValueRegex(this.lookupReference('meta2'))) return;

        this.config = {
            valor: this.lookupReference('valor').getValue(),
            indicador: this.lookupReference('cmbIndicador').getValue(),
            indicadorStr: this.lookupReference('cmbIndicador').getRawValue(),
            medioVerif: null, //this.getCmbMedioVerificacion().getValue(),
            medioVerifStr: null, //this.getCmbMedioVerificacion().getRawValue(),
            lineaBase: this.lookupReference('lineaBase').getRawValue(),
            idLineaBase: this.lookupReference('lineaBase').getValue(),
            meta1: this.lookupReference('meta1').getValue(),
            meta2: this.lookupReference('meta2').getValue()
        };

        this.onClose();
    },

    onShowSelectedIndicador: function (record) {
        var me = this;

        if (record) {
            this.config = {
                valor: record.valor,
                indicador: record.indicador,
                indicadorStr: '',
                medioVerif: record.medioVerif,
                medioVerifStr: null,
                lineaBase: record.lineaBase,
                idLineaBase: record.lineaBase,
                meta1: record.meta1,
                meta2: record.meta2
            }
            this.lookupReference('valor').setValue(record.valor);
            this.lookupReference('lineaBase').setValue(record.lineaBase);
            this.lookupReference('meta1').setValue(record.meta1);
            this.lookupReference('meta2').setValue(record.meta2);

            if (record.tipoIndicador && record.tipoIndicador == 'general') {
                this.getView().mask('Espere...');
                this.lookupReference('cmbIndicador').getStore().load({
                    scope: this,
                    params: {
                        //idObjetivoEspecifico: record.objetivoGral,
                        tipo: 'general',
                        idTipoProyecto: record.idTipoProyecto
                    },
                    callback: function (p1, p2) {
                        me.getView().unmask();
                        this.lookupReference('cmbIndicador').setValue(record.idIndicador);
                        //this.getCmbMedioVerificacion().setValue(record.idMedioVerif);
                        me.config.indicadorStr = this.lookupReference('cmbIndicador').getRawValue();
                    }
                });
            } else {
                this.getView().mask('Espere...');
                this.lookupReference('cmbIndicador').getStore().load({
                    scope: this,
                    params: {
                        //idObjetivoGeneral: record.objetivoGral
                        tipo: 'especifico',
                        idTipoProyecto: record.idTipoProyecto
                    },
                    callback: function (p1, p2) {
                        me.getView().unmask();
                        this.lookupReference('cmbIndicador').setValue(record.idIndicador);
                        //this.getCmbMedioVerificacion().setValue(record.idMedioVerif);
                    }
                });
            }
        }
    },

    onChangeLineaBase: function (field, newValue, oldValue) {
        console.log("en onChangeLineaBase");
        //Zreturn true;
    },

    __validateValueRegex: function (field) {
        var flag = true;

        if (field.getValue() != null && field.getValue().trim() != "") {
            var array = field.getValue().split(";");

            Ext.each(array, function (v) {
                if (eval(v) < 0 || eval(v) > 100) {
                    ProcessManagement.util.cmpc.Messages.error('Los valores num&eacute;ricos para este campo deben estar entre 0 y 100', 'Error', this, function () {
                        field.focus(true, 100);
                        field.markInvalid('Debe ingresar numeros del 0 al 100 separados por punto y coma (;)');
                    });
                    flag = false;
                    return flag;
                }
            })
        }

        return flag;
    }
});