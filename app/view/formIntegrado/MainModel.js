/**
 * Created by Antonio on 8/31/2017.
 */
Ext.define('ProcessManagement.view.formIntegrado.MainModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.formMainModel',

    data: {
        currentView: null,
        username: '',
        pass: '',
        txtFields: {}
    },

    formulas: {
        labels: function (key) {
            var me = this;
            //console.log("key: ", key);
            //console.log("This VM: ", this);
            if (txtFields == null || typeof txtFields == undefined) {
                console.log("Cargando el locale del Form: " + Ext.String.format('./locale/locale-{0}.js', appParams.lang));
                ProcessManagement.util.Utils.loadScript(Ext.String.format('./locale/locale-{0}.js', appParams.lang), function(err, callback){
                    if (!err) {
                        me.set({ txtFields: txtFields });
                    }
                });
            } else {
                console.log("Locale cargado: " + Ext.String.format('./locale/locale-{0}.js', appParams.lang));
                me.set({ txtFields: txtFields });
            }
        }
    }
})