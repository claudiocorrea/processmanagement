/**
 * Created by Antonio on 8/31/2017.
 */
Ext.define('ProcessManagement.view.formIntegrado.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.formMainController',

    init: function () {
        console.log(loginUser)
        console.log(globalParams)
        if(Ext.getCmp(globalParams.lang)) Ext.getCmp(globalParams.lang).checked = true
    },

    logout: function() {
        Ext.MessageBox.confirm(txtFields.words.confirm, txtFields.messages.confirmLogout, function(btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: 'session/invalidate',
                    method: 'GET',
                    callback: function () {
                        var lang = ProcessManagement.util.Utils.getQueryVariable('lang');
                        if(lang) lang = '?lang=' + lang;
                        else lang = ''
                        window.location.href = 'http://' + window.location.hostname + ':' +
                            window.location.port + lang
                    }
                });
            }
        })
    },

    changeLanguage: function(bt) {
        var paramsStr = "";
        globalParams.lang = bt.id;

        if (appParams != null && typeof appParams != undefined) {
            Ext.each(Object.keys(appParams), function (key) {
                if (key != "lang") {
                    paramsStr += Ext.String.format("{0}={1}&", key, appParams[key]);
                }
            })
        }

        paramsStr += "lang=" + bt.id;

        window.location.href = window.location.pathname + '?' + paramsStr;
    },

    openNewRequest: function() {
        var win = this.lookupReference('newRequestPopup');
        if (!win) {
            win = new ProcessManagement.view.popup.Request();
            this.getView().add(win);
        }
        win.show()
    },

    openTheme: function() {
        var win = this.lookupReference('themePopup');
        if (!win) {
            win = new ProcessManagement.view.popup.Theme();
            this.getView().add(win);
        }
        win.show()
    },

    openUploadFile: function(data) {
        var win = this.lookupReference('uploadPopup')
        if (!win) {
            win = new ProcessManagement.view.popup.Upload({
                fileData: data
            });
            this.getView().add(win);
        }
        win.show()
    },

    openModalPopup: function(url) {
        console.log(url)
        var me = this,
            win = me.lookupReference('modalPopup');
        if (!win) {
            win = new ProcessManagement.view.popup.Modal();
            me.getView().add(win);
            console.log(me.lookupReference('componentURL'))
            me.lookupReference('componentURL').autoEl.src = url
        }
        win.show()
    },

    openRejectForm: function(data) {
        var win = this.lookupReference('rejectPopup')
        if (!win) {
            win = new ProcessManagement.view.popup.RejectForm({
                fileData: data
            });
            this.getView().add(win);
        }
        win.show()
    }

});