/**
 * Created by Antonio on 8/11/2017.
 */
Ext.define('ProcessManagement.view.popup.ChildFormController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.childForm',

    isWorkflowTerminated: function () {
        var flag = false,
            finishedStatesIds = ['3', '4', '8'];

        if (finishedStatesIds.includes(this.getView().getViewModel().get('IdState'))) {
            flag = true;
        }

        return flag;
    },

    afterRender: function () {
        var finishedStatesIds = ['3', '4', '8'];

        if (this.isWorkflowTerminated() || this.getView().getViewModel().get('formEsReadOnly')) {
            this.lookup('btnNewDetail').setHidden(true);
            this.lookup('btnAcceptChildForm').setHidden(true);
        } else {
            this.lookup('btnNewDetail').setHidden(false);
            this.lookup('btnAcceptChildForm').setHidden(false);
        }
    },

    loadChildFormValues: function () {
        var me = this;

        this.lookup('gridCamposFormHijo').mask(txtFields.messages.wait);
        Ext.Ajax.request({
            url: 'form/getChildFormFieldValues',
            params: {
                workflowId: this.getView().getViewModel().get('WorkflowId'),
                idForm: this.getView().getViewModel().get('idForm')
            },
            method: 'GET'
        }).then(
            function (response, opts) {
                var grid = me.lookup('gridCamposFormHijo'),
                    tempResponse = [];

                grid.unmask();

                response = Ext.JSON.decode(response.responseText);

                Ext.each(response.result, function (result) {
                    Ext.each(Object.keys(result), function (key) {
                        var tempVal;

                        try {
                            tempVal = JSON.parse(result[key]);
                            if (Array.isArray(tempVal)) {
                                result[key] = tempVal;
                            }
                        } catch (e) {
                            tempVal = result[key];
                        }


                    });
                    try {
                        tempResponse.push(result);
                    } catch(e2) {
                    }
                });

                response.result = tempResponse;

                console.log("Response servicio detailForm: ", response);

                grid.getStore().loadData(response.result);
                grid.getStore().each(function (record) {
                    record.set('DELETEFIELD', 0);
                });
                grid.getView().refresh();
            },
            function (response, opts) {
                me.lookup('gridCamposFormHijo').unmask();
            }
        );
    },

    onAcceptChildForm: function (button, e, options) {
        var me = this,
            win = this.lookup('childForm'),
            store = this.lookup('gridCamposFormHijo').getStore(),
            values = [],
            jsonData = {},
            seq = 1,
            records = (store.isFiltered())?store.getData().getSource():store.getData();

        if (!me.__validateData()) return;

        records.each(function (record) {
            var obj = record.data;

            Ext.each(Object.keys(obj), function (key) {
                if (Array.isArray(obj[key])) {
                    obj[key] = JSON.stringify(obj[key]); //Se guarda el array como string para evitar modificar el servicio CaptureFormFromXml
                }
            });

            console.log("Record.data: ", obj);

            record.set('SYSTEMFORM', me.getView().getViewModel().get('idForm'));
            record.set('System_IdParentField', me.getView().getViewModel().get('idField'));
            values.push(obj);
            seq += 1;
        });
        console.log("Antes de cerrar: ", this.getView(), this.getViewModel());

        jsonData = {
            idForm: this.getView().getViewModel().get('idForm'),
            idFieldInParentForm: this.getView().getViewModel().get('idField'),
            rows: values
        }

        console.log("JsonData: ", jsonData);

        this.getView().close();

        Ext.getCmp('formPanel').getController().onAcceptChildForm(jsonData);

    },

    onCancelChildForm: function (button, e, options) {
        var store = this.lookup('gridCamposFormHijo').getStore();

        store.rejectChanges();

        this.getView().close();
    },

    onAddNewDetailRow: function () {
        var store = this.lookup('gridCamposFormHijo').getStore(),
            fields = store.getModel().getFields(),
            newRec = {},
            nextSeq = 0,
            records = (store.isFiltered())?store.getData().getSource():store.getData();

        records.each(function (record) {
            if (record.get('sequence') > nextSeq) {
                nextSeq = record.get('sequence');
            }
        });
        Ext.each(fields, function (field) {
            newRec[field.name] = null;
        });

        nextSeq += 1;
        newRec.sequence = nextSeq;
        newRec.DELETEFIELD = 0;
        store.add([newRec]);
    },

    onRemoveDetailRow: function (view, index) {
        var store = this.lookup('gridCamposFormHijo').getStore();

        if (this.isWorkflowTerminated()) return;
        
        store.getAt(index).set('DELETEFIELD', 1);
        store.filterBy(function (record) {
            return (record.get('DELETEFIELD') != 1)?true:false;
        });
    },

    __validateData: function () {
        var me = this,
            grid = this.lookup('gridCamposFormHijo'),
            columns = grid.getColumns(),
            store = grid.getStore(),
            valid = true,
            validationMessages = [],
            invalidRows = [],
            rowIndex = 0;


        store.each(function (record) {
            var data = record.getData();

            try {
                Ext.each(Object.keys(data), function (key) {
                    Ext.each(columns, function (column) {
                        if (column.dataIndex == key) {
                            var editor = column.getEditor();
                            //console.log("Column: ", column);

                            if (!editor.allowBlank) {
                                //console.log("Validacion !allowBlank: ", key, valid, data);
                                if (!me.onCheckAllowBlank(data, key)) {
                                    var msg = Ext.String.format(txtFields.messages.validation.fieldMustHaveValue, key);
                                    valid = false;
                                    if (!validationMessages.includes(msg)) validationMessages.push(msg);
                                    if (!invalidRows.includes(rowIndex)) invalidRows.push(rowIndex);
                                }
                            }

                            if (editor.campoMenorQue) {
                                //console.log("Validacion campoMenorQue: ", key, editor.campoMenorQue, valid, data);
                                if (!me.onCheckMenorQue(data, key, editor.campoMenorQue, editor.xtype)) {
                                    var msg = Ext.String.format(txtFields.messages.validation.fieldMustBeLessThan, key, editor.campoMenorQue);
                                    valid = false;
                                    if (!validationMessages.includes(msg)) validationMessages.push(msg);
                                    if (!invalidRows.includes(rowIndex)) invalidRows.push(rowIndex);
                                }
                            }
                        }
                    })
                });
            } catch (e) {
                //alert(e);
            }

            rowIndex += 1;
        });

        if (!valid) {
            var m = "";
            Ext.each(validationMessages, function (msg) {
                m += Ext.String.format("{0}<br>", msg);
            });

            var tempRows = [];
            Ext.each(invalidRows, function (index) {
                tempRows.push(store.getAt(index));
            });
            grid.getSelectionModel().select(tempRows);

            Ext.Msg.alert(txtFields.messages.validationErrors, m);
        }

        return valid;
    },

    onCheckMenorQue: function (record, key, keyCampoMayor, tipoValidacion) {
        var tempValMenor = record[key],
            tempValMayor = record[keyCampoMayor];
        try {

            if (tipoValidacion == "numberfield") {
                if (eval(record[key]) > eval(record[keyCampoMayor])) {
                    //console.log("Validando menor: ", record, key, keyCampoMayor, tipoValidacion)
                    return false;
                }
            } else {
                if (record[key] > record[keyCampoMayor]) {
                    //console.log("Validando menor: ", record, key, keyCampoMayor, tipoValidacion)
                    return false;
                }
            }
        } catch (e) { alert(e)}

        return true
    },

    onCheckAllowBlank: function (record, keyToValidate) {
        if (record[keyToValidate] == null || record[keyToValidate] == "") {
            //alert("key invalido: " + keyToValidate)
            return false;
        }

        return true;
    },

    onClose: function () {
        this.getView().close();
    }
});