Ext.define('ProcessManagement.view.main.WorkflowModelController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.workflowModel',

    config: {
        data: null
    },

    init: function () {
        console.log(this.getView())
        this.data = this.getView().config.dataModel
        console.log(this.data)
		this.getView().setTitle(this.data.WorkFlow + ' - ' + this.data.proceso)
    },

    initScreen: function () {
        var controller = this
		Ext.Ajax.request({
            url: globalParams.urlSPM + '/tareasPersonales/getStepsByWorkflowId',
            method: 'GET',
            params: { workflowId: this.data.WorkflowId },
            success: function (response) {
                console.log('SUCCESS: ', response)
                var steps = JSON.parse(response.responseText)
                controller.data.nodes = controller.stepsToJSON(steps)
                initModel(controller.data)
            },
            failure: function (response) {
                console.log('FAILURE: ', response)
				if(response.status != 0) Ext.MessageBox.alert('ERROR', response.statusText)
            }
        })

    },

    stepsToJSON: function(steps) {		
		var nodes = new Array()
		steps.forEach(function(step) {			
			step.key = step.StepId
			step.category = "step"
            step.text = step.Title
			step.parent = step.dependsON != null ? step.dependsON.toString() : "1"
			if(step.Groups != '') {
				var newGroups = new Array()
				var groups = step.Groups.split('|')
				groups.forEach(function(group) {
					if(group != '') newGroups.push({ name: group })
				})
				step.Groups = newGroups
			}
			if(step.Users != '') {
				var newUsers = new Array()
				var users = step.Users.split('|')
				users.forEach(function(user) {
					if(user != '') {
						var mandatory = false
						if(user.substring(0,1) == '*') {
							mandatory = true
							user = user.substring(1)
						}
						var approved = 0
						var date = ''
						if(user.indexOf('-Approved:') != -1 ) {
							approved = 1
							date = user.split('-Approved:')[1]
							user = user.substring(0, user.indexOf('-Approved:'))
						} else if(user.indexOf('-Rejected:') != -1 ) {
							approved = -1
							date = user.split('-Rejected:')[1]
							user = user.substring(0, user.indexOf('-Rejected:'))
						}					
						newUsers.push({ "name": user, "mandatory": mandatory, "approved": approved, date: date })
					}
				})
				step.Users = newUsers
			}
			if(step.UnionConCondicional) {					
				var newConditions = new Array()
				var conditions = step.Condicional.split('|')
				conditions.forEach(function(condition) {
					if(condition != '') newConditions.push({ condition: condition })
				})
				step.UnionConCondicional = true
				step.Conditions = newConditions
			}
			nodes.push(step)
		})
		return nodes
	}

})