Ext.define('ProcessManagement.view.main.RejectFormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.rejectForm',
	
	config: {
		data: null
	},

    init: function(obj) {
		this.data = this.getView().fileData
        console.log(this.data)
		this.getView().setTitle(this.data.title)
		this.getView().getViewModel().set('stepId',	this.data.stepId)		
	},
	
	initScreen: function() {
		this.getPreviousSteps()
		
		if(this.data.action == 'rechazo') {
			var rg = this.getView().down('radiogroup');
			rg.setValue({RtipoRechazo: 1});
			rg.setReadOnly(false);
			
			this.getView().down('combobox').setDisabled(true);
		} else {
			var rg = this.getView().down('radiogroup');
			rg.setValue({RtipoRechazo: 0});
			rg.setReadOnly(true);

			this.getView().down('combobox').setDisabled(true);

			rg.setReadOnly(false);
		}
	},
	
	getPreviousSteps: function() {
		var store = this.lookupReference('cbPreviousSteps').getStore()
		store.load({
            params: {
                workflowId: this.data.workflowId
            }
        })
	},
	
	onRechazar: function (button, e, options) {
        var me = this,
            url = 'pasosWorkflow/rechazarPaso',
            params = {
                stepId: this.getView().getViewModel().get('stepId'),
                userId: loginUser.idUser,
                motivoRechazo: this.getView().getViewModel().get('comentario'),
                tipoRechazo: this.getView().getViewModel().get('tipoRechazo').RtipoRechazo,
                pasoAnterior: this.getView().getViewModel().get('pasoAnterior'),
                token: loginUser.token
            };
        console.log(params);
        this.getView().mask(txtFields.messages.wait + '...');
        Ext.Ajax.request({
            url: url,
            method: 'POST',
            params: params,
            success: function(conn, r, opts) {
                me.getView().unmask();
                conn = Ext.JSON.decode(conn.responseText, true);
                if (conn == null) {
                    conn = {
                        success: false,
                        error: txtFields.messages.stepRejectedUnsuccessfull
                    }
                }
                if (conn.success) {
                    Ext.Msg.show({
                        title: txtFields.words.notice,
                        msg: txtFields.messages.stepRejectedSuccessfully,
                        //icon: Ext.Msg.INFO,
                        buttons: Ext.Msg.OK,
                        fn: function() {
                            me.getView().close();
                            Ext.getCmp('requestScreen').controller.returnTab()
                        }
                    });
                } else {
                    Ext.Msg.show({
                        title: 'Error',
                        msg: conn.error,
                        //icon: Ext.Msg.ERROR,
                        buttons: Ext.Msg.OK
                    });

                }
            },
            failure: function(conn, r, opts) {
                me.getView().unmask();
				console.log(conn.responseText)
                conn = Ext.JSON.decode(conn.responseText, true);
                Ext.Msg.show({
                    title: 'Error',
                    msg: conn.error,
                    //icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
        });
    },
	
	close: function (button, e, options) {
        this.getView().destroy()
    }
	
})
