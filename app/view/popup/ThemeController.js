Ext.define('ProcessManagement.view.main.ThemeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.theme',
	
	config: {
		ready: null
	},

    init: function() {
		this.ready = true
	},
	
	onChangeHeader: function(obj, value) {
		console.log('OnChangeHeader: ')
		if(this.ready) {
			//this.getView().getHeader().setStyle('backgroundColor', '#' + value)
			//this.getView().getHeader().setStyle('borderColor', '#' + value)
			//this.getView().setStyle('borderColor', '#' + value)

			Ext.util.CSS.updateRule('.x-panel-header-default', 'background-color', '#' + value)

			Ext.util.CSS.updateRule('.x-window-default', 'border-color', '#' + value)
			Ext.util.CSS.updateRule('.x-window-header-default', 'border-color', '#' + value)
			Ext.util.CSS.updateRule('.x-window-header-default-top', 'background-color', '#' + value)
			Ext.util.CSS.updateRule('.x-window-body-default', 'background-color', '#' + value)

			Ext.util.CSS.updateRule('.x-btn-default-small', 'background-color', '#' + value)
			Ext.util.CSS.updateRule('.x-btn-default-small', 'border', 'none')

			Ext.util.CSS.updateRule('.x-panel-default', 'border-color', '#' + value)
			Ext.util.CSS.updateRule('.x-panel-default-framed', 'border-color', '#' + value)
			Ext.util.CSS.updateRule('.x-panel-header-default', 'background-color', '#' + value)
			Ext.util.CSS.updateRule('.x-treelist-item-tool', 'background-color', '#' + value)
			Ext.util.CSS.updateRule('.treelist-with-nav .x-panel-body', 'background-color', '#' + value)

			var panels = Ext.ComponentQuery.query('panel')
			panels.forEach(panel => {
				if(panel.xtype == 'panel') {
					if(panel.getHeader()) panel.getHeader().setStyle('backgroundColor', '#' + value)
				}
			})
			console.log(panels)
			
		}
	},
	
	onChangeTbar: function(obj, value) {
		if(this.ready)
			Ext.getCmp('toolbar').setStyle('backgroundColor', '#' + value)
	},
	
	onChangeMenu: function(obj, value) {
		if(this.ready) {
			if (appParams["verForm"]) {
				var scr = Ext.ComponentQuery.query('.mainToolbar')[0];
				scr.setStyle('backgroundColor', '#' + value);
			} else {
				//Ext.getCmp('navTreeList').setStyle('backgroundColor', '#' + value);
			}
		}
	},
	
	saveTheme: function() {
		this.getView().close()
	},
	
	resetTheme: function() {
		this.getView().getHeader().setStyle('backgroundColor', '#1E90FF')
		this.getView().getHeader().setStyle('borderColor', '#1E90FF')
		this.getView().setStyle('borderColor', '#1E90FF')
		Ext.getCmp('toolbar').setStyle('backgroundColor', 'white')
		Ext.getCmp('navigator').getTabBar().setStyle('backgroundColor', '#1E90FF')
		this.getView().close()
	}
	
})