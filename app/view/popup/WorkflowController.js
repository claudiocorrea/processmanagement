Ext.define('ProcessManagement.view.main.WorkflowController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.workflow',

    config: {
      selection: null
    },

    init: function(){
        this.selection = {}
        this.getWorkflows()
    },

    selectionWorkflow: function (obj, row) {
        this.selection =  (row[0]) ? row[0].data : {}
    },

    filterList: function(o) {
        var store = this.lookupReference('gridWorkflows').getStore()
        var filters = store.getFilters();
        filters.add({
            property: 'Workflow', value: o.value,
            anyMatch: true, caseSensitive: false
        });
    },

    getWorkflows: function() {
        var store = this.lookupReference('gridWorkflows').getStore();

        store.getWorkflows(loginUser, function(error, result) {
            if(error) {
                if(result.error == 401) Ext.MessageBox.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.MessageBox.alert('ERROR', callback.message)
            }
        });
    },

    openRequest: function() {
        var data = this.selection,
            controller = this;

        this.startRequest(data.WorkflowID, function(error, result) {
            if(!error) {
                console.log("WorkflowController openRequest - result: ", result);
                data.StepId = result.stepId;
                data.WorkflowIDoriginal = data.WorkflowID;
                data.WorkflowID = result.workflowId;                
				data.IdForm = result.formId;
                data.AllowDocs = (result.allowDocs == 1)?true:false;
                data.IdState = '2'; //Pendiente
                data.proceso = result.proceso;
                data.StepTxt = result.StepTxt;
                data.back = 'dashboard'
                if(data.WorkflowIDoriginal == "7235") data.back = 'dashboardCMPC'
				Ext.getCmp('main').controller.setCurrentView('request', data)
                controller.close()
            }
        });
    },

    startRequest: function(workflowId, callback) {
        var controller = this,
            store = this.lookupReference('gridWorkflows').getStore();

        store.startWorkflow(loginUser, { workflowId: workflowId }, function(error, result) {
            if(!error) {
                callback(false, {stepId: result.response.StepID, workflowId: result.response.WorkFlowId, formId: result.response.formId, allowDocs: result.response.allowDocs, proceso: result.response.proceso, StepTxt: result.response.StepTxt});
            }  else {
                if(result.error == 401) Ext.MessageBox.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.MessageBox.alert('ERROR', result.message)
                controller.close()
            }
        });
    },

    close: function() {
        var store = this.lookupReference('gridWorkflows').getStore()
        store.getFilters().removeAll()
        this.getView().destroy()
    }
});