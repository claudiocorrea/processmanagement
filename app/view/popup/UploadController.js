Ext.define('ProcessManagement.view.main.UploadController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.upload',

    config: {
        url: null,
        data: null,
        filename: null
    },

    init: function () {
        this.url = globalParams.urlServices + 'upload'
        this.data = this.getView().fileData
        console.log(this.data)

        this.lookupReference('componentURL').autoEl.src = "http://" + globalParams.urlExternServer + 
            ":8080/SPM/SPMScan/DWT_Scan_Upload_Demo.html?idWorkflow=" + this.data.id + "&idUser=" + loginUser.idUser +
            "&token=" + loginUser.token + "&fileName=" + this.data.fileName

    },

    extractFilename: function (path) {
        if (path.substr(0, 12) == "C:\\fakepath\\")
            return path.substr(12); // modern browser
        var x;
        x = path.lastIndexOf('/');
        if (x >= 0) // Unix-based path
            return path.substr(x+1);
        x = path.lastIndexOf('\\');
        if (x >= 0) // Windows-based path
            return path.substr(x+1);
        return path; // just the filename
    },

    upload: function(obj, path) {
        this.filename = path.replace(/C:\\fakepath\\/g, "");

        this.filename = this.extractFilename(this.filename);

        console.log('Upload file: ' + this.filename)

        var controller = this;
        var form = this.lookupReference('uploadForm');
        if(form.isValid()){
            form.submit({
                scope: this,
                url: this.url,
                waitMsg: txtFields.messages.uploading,
                params: {
                    name: this.filename
                },
                success: function(fp, o) {
                    controller.isSuccess()
                },
                failure: function(fp, o) {
                    if(o.response.status != 400) {
                        Ext.MessageBox.alert('ERROR', o.response.status + ': ' + o.response.statusText)
                        controller.close()
                    } else controller.isSuccess()

                }
            });
        }
    },

    isSuccess: function() {
        var controller = this
        var grid = this.lookupReference('gridFiles'),
            store = new ProcessManagement.store.Files()
        store.uploadedFile(loginUser, this.data.id, this.data.stepId, this.filename, grid, function(error, result) {
            console.log(result)
            if(error) {
                if(result.error == 401) Ext.MessageBox.alert('ERROR', txtFields.messages.errors.sessionError)
                else if(result.error != 0) Ext.MessageBox.alert('ERROR', result.message)
            } else {
                Ext.getCmp('requestScreen').controller.getFiles()
                controller.close()
            }
        })
    },

    close: function() {
        this.getView().destroy()
    }

})