Ext.define('ProcessManagement.view.main.NotificationsController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.notifications',

    init: function () {
        this.getNotifications();
    },

    getNotifications: function () {
        var me = this,
            store = this.lookupReference('gridNotifications').getStore();

        store.getPendingsByUser(loginUser.idUser, function (error, result) {
            if(error) {
                if(result.error == 401) Ext.MessageBox.alert('ERROR', txtFields.messages.errors.loginError);
                else if(result.error != 0) Ext.MessageBox.alert('ERROR', callback.message);

                return;
            }

            var response = Ext.JSON.decode(result.getResponse().responseText, true),
                btnMark = me.lookupReference('markSelectedNotificationsAsRead'),
                btnMarkAll = me.lookupReference('markAllNotificationsAsRead');

            btnMarkAll.setDisabled(response.data && response.data.length <= 0);

            console.log("Result getNotifications: ", result, error);
        });
    },

    clearGridSelection: function () {
        var grid = this.lookupReference('gridNotifications');

        grid.getSelectionModel().deselectAll();
    },

    gridHasSelectedRows: function () {
        var grid = this.lookupReference('gridNotifications'),
            flag = false;

        return grid.getSelection().length > 0;
    },

    markSelectionAsRead: function () {
        if (this.gridHasSelectedRows()) {
            this.process();
        }
    },

    markAllAsRead: function () {
        var grid = this.lookupReference('gridNotifications');

        grid.getSelectionModel().selectAll();

        this.process();
    },

    process: function () {
        var me = this,
            grid = this.lookupReference('gridNotifications'),
            selectedNotifications = [],
            win = Ext.ComponentQuery.query('window[reference=notificationsPopup]')[0];

        Ext.each(grid.getSelection(), function (selection) {
            var data = selection.getData();

            console.log("Procesando Notificacion: ", data.Idmensaje);

            selectedNotifications.push(data.Idmensaje);

        });

        win.mask(txtFields.messages.savingData + '....');
        Ext.Ajax.request({
            url: 'notification/markAsRead',
            method: 'POST',
            jsonData: {notifications: selectedNotifications},
            success: function (response) {
                win.unmask();
                console.log("Respuesta: ", response);
                response = Ext.JSON.decode(response.responseText, true);

                if (response == null) {
                    response = {
                        success: false,
                        error: txtFields.messages.saveUnsuccessful
                    };
                }

                Ext.Msg.show({
                    title:(response.success)?txtFields.words.confirm:'Error',
                    msg: (response.success)?txtFields.messages.saveSuccessfull:response.error,
                    buttons: Ext.Msg.YES,
                    fn: function() {
                        if (response.success) {
                            me.getNotifications();
                        }
                    }
                });
                ProcessManagement.app.getQtyNotificationsByUser();

            },
            failure: function (response) {
                win.unmask();
                Ext.Msg.show({
                    title:'Error', msg: txtFields.messages.saveUnsuccessful,
                    buttons: Ext.Msg.YES
                });
            }
        });
    },

    onBeforeclose: function () {
        this.clearGridSelection();
    },

    closeWindow: function () {
        var win = this.lookupReference('gridNotifications').up('window');

        if (win) {
            this.onBeforeclose();
            win.close();
        }
    }
});
