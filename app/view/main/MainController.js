Ext.define('ProcessManagement.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',

    init: function () {
        console.log(loginUser)
		console.log(globalParams)
		if(Ext.getCmp(globalParams.lang)) Ext.getCmp(globalParams.lang).checked = true
		this.findAvatar()
    },

    initMain: function() {
        console.log('Iniciando Main...')
        this.setThemeByEnterprise()
        var screen = 'dashboard'
        switch(loginUser.enterpriseSelected) {
            case '48': 
                screen = 'dashboardCMPC';
                Ext.getCmp('cmpc-menu-treelist').setHidden(false);            
                break;
            default:
                Ext.getCmp('menu-treelist').setHidden(false);  
                screen = 'dashboard'
        }
        this.setCurrentView(screen, {})        
    },
	
	findAvatar: function() {
		var obj = this.lookupReference('imageUser')
		var btnUser = this.lookupReference('btnUser')
        try {
            Ext.Ajax.request({
                url: 'resources/images/users/IMG' + loginUser.idUser + '.png',
            }).then(function () { 
				obj.setSrc('resources/images/users/IMG' + loginUser.idUser + '.png')
				btnUser.setHtml('<table class="btnUser"><tr><td>' + loginUser.name + '</td><td><img src="resources/images/users/IMG' + loginUser.idUser + '.png" /></td></tr></table>')
            }, function () { 
				obj.setSrc('resources/images/users/HSA0.png')
				btnUser.setHtml('<table class="btnUser"><tr><td>' + loginUser.name + '</td><td><img src="resources/images/users/HSA0.png" /></td></tr></table>')
			}); //ERROR
        } catch (Exception) {
            console.log(Exception)
            obj.setSrc('resources/images/users/HSA0.png')
			btnUser.setHtml('<table class="btnUser"><tr><td><td>' + loginUser.name + '</td><img src="resources/images/users/HSA0.png" /></td></tr></table>')
        }
    },

    setThemeByEnterprise: function() {
        var color = '#1E90FF', image = 'Logo', favicon = 'favicon';
        console.log('setThemeByEnterprise: ' + loginUser.enterpriseSelected)
        switch(loginUser.enterpriseSelected) {
            case '25': 
                color = '#3A9AA9';  //color = '#19481A'; 
                image = 'Logo AMSA'; //image = 'Logo CPBA'; 
                favicon = 'favicon AMSA'; //favicon = 'favicon CPBA'; 
                break;
            case '49': 
                color = '#3A9AA9'; 
                image = 'Logo AMSA'; 
                favicon = 'favicon AMSA';
                break; 
            case '48': 
                color = '#3A9AA9';  //color = '#3AAA35'; 
                image = 'Logo AMSA'; //image = 'Logo CMPC'; 
                favicon = 'favicon AMSA'; //favicon = 'favicon CMPC'; 
                break;
        }
        this.lookupReference('imageLogo').setHtml('<a href="."><div><img src="resources/images/logo/' + image + '.png"></div></a>')

        Ext.util.CSS.updateRule('.x-window-default', 'border-color', color)
        Ext.util.CSS.updateRule('.x-window-header-default', 'border-color', color)
        Ext.util.CSS.updateRule('.x-window-header-default-top', 'background-color', color)
        Ext.util.CSS.updateRule('.x-window-body-default', 'background-color', color)

        Ext.util.CSS.updateRule('.x-btn-default-small', 'background-color', color)
        Ext.util.CSS.updateRule('.x-btn-default-small', 'border', 'none')

        Ext.util.CSS.updateRule('.x-panel-default', 'border-color', color)
        Ext.util.CSS.updateRule('.x-panel-default-framed', 'border-color', color)
        Ext.util.CSS.updateRule('.x-panel-header-default', 'background-color', color)
        Ext.util.CSS.updateRule('.x-treelist-item-tool', 'background-color', color)
        Ext.util.CSS.updateRule('.treelist-with-nav .x-panel-body', 'background-color', color) 
        
        document.getElementById('favicon').setAttribute('href', 'resources/images/favicon/' + favicon + '.png')
    },
    
    setTheme: function(screen) {
        var color = '#1E90FF'
        switch(loginUser.enterpriseSelected) {
            case '25': color = '#19481A'; break;
            case '49': color = '#3A9AA9'; break;
            case '48': color = '#3A9AA9'; //color = '#3AAA35'; 
                break;            
        }
        console.log('setTheme: ', screen, color)
        var panels = Ext.ComponentQuery.query('panel')
        panels.forEach(panel => {            
            try {
                if(panel.title) {
                    //console.log('Se modifico...', panel)
                    panel.getHeader().setStyle('backgroundColor', color)                    
                } else {
                    //console.log('NO se modifico...', panel)
                    if(panel.layout.config.type == 'border') {
                        //console.log('Es tipo border: ', panel)                        
                        panel.items.items.forEach(subpanel =>{
                            //console.log('Subpanel: ', subpanel)
                            Ext.applyIf(subpanel, { header: { style: 'background-color: ' + color + ';' }})
                        })
                    }
                }
            } catch(e) { /*console.log('No se pudo modificar el componente: ', panel, e)*/ }
        })
    },

    logout: function() {
        Ext.MessageBox.confirm(txtFields.words.confirm, txtFields.messages.confirmLogout, function(btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: 'session/invalidate',
                    method: 'GET',
                    callback: function () {
                        var lang = ProcessManagement.util.Utils.getQueryVariable('lang');
                        if(lang) lang = '?lang=' + lang;
                        else lang = ''
                        window.location.href = 'http://' + window.location.hostname + ':' +
                                window.location.port + lang
                    }
                });
            }
        })
    },
	
	changeLanguage: function(bt) {
		var paramsStr = "";
        globalParams.lang = bt.id;

        if (appParams != null && typeof appParams != undefined) {
            Ext.each(Object.keys(appParams), function (key) {
                if (key != "lang") {
                    paramsStr += Ext.String.format("{0}={1}&", key, appParams[key]);
                }
            })
        }
        paramsStr += "lang=" + bt.id;

        window.location.href = window.location.pathname + '?' + paramsStr;
    },
	
	onNavTreeSelectionChange: function (tree, node) {
        var to = node && node.get('routeId'), 
		p_data = node && node.get('data'), data = {};
		if(p_data) data = p_data
        if (to) { this.setCurrentView(to, data); }
    },
	
	setCurrentView: function(screen, data) {
        console.log('setCurrentView: ', screen, data)
		var panel = this.lookupReference('mainMenuPanel')		
		if(panel) {
            panel.removeAll()
            try {
                panel.add(Ext.create({ xtype: screen + 'Screen', data: data }))
            } catch(e) {
                switch(screen) {
                    case 'dashboard':
                        scn = new ProcessManagement.view.screen.Dashboard({ data: data }); break;
                    case 'dashboardCMPC':
                        scn = new ProcessManagement.view.cmpc.screen.DashboardCMPC({ data: data }); 
                        //panel.add(Ext.create({ xtype: 'cmpc-dashboardScreen', data: data }));
                        break;
                                                
                }
            }	
		}
	},
	
	openPopup: function(title, data) {
        var win = this.lookupReference(title + 'Popup')
        if (!win) {
			switch(title) {
				case 'newRequest':
					win = new ProcessManagement.view.popup.Request({data: data }); break;
				case 'theme':
					win = new ProcessManagement.view.popup.Theme(); break;
				case 'workflowModel':
					win = new ProcessManagement.view.popup.WorkflowModel( {dataModel: data } ); break;
				case 'notifications':
					win = new ProcessManagement.view.popup.Notifications(); break;
			}
            
            this.getView().add(win);
        }
        win.show()
    },

    openNewRequest: function() {
        var win = this.lookupReference('newRequestPopup');
        if (!win) {
            win = new ProcessManagement.view.popup.Request();
            this.getView().add(win);
        }
        win.show()
    },
	
	openTheme: function() {
        var win = this.lookupReference('themePopup');
        if (!win) {
            win = new ProcessManagement.view.popup.Theme();
            this.getView().add(win);
        }
        win.show()
    },
	
	openModalPopup: function(url) {
		console.log(url)
        var me = this,
            win = me.lookupReference('modalPopup');
        if (!win) {
            win = new ProcessManagement.view.popup.Modal();
            me.getView().add(win);
            console.log(me.lookupReference('componentURL'))
            me.lookupReference('componentURL').autoEl.src = url
        }
        win.show();
    },

    openWorkflowModel: function(data) {
        var win = this.lookupReference('workflowModelPopup');
        if (!win) {
            win = new ProcessManagement.view.popup.WorkflowModel( {dataModel: data } );
            this.getView().add(win);
        }
        win.show();
    },

    showNotificationsPopup: function () {
        var win = this.lookupReference('notificationsPopup');
        if (!win) {
            win = new ProcessManagement.view.popup.Notifications();
            this.getView().add(win);
        }
        win.show();
    }

});