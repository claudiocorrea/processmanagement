Ext.define('ProcessManagement.util.Date', {
    singleton: true,

    stringToDate: function(str) {
        var parts = str.split('/')
        return new Date(parts[2], parts[1] - 1, parts[0])
    },

    getToday: function () {
        var hoy = new Date(Date.now());
        var day =  ((hoy.getDate() + '').length == 1) ? '0' + hoy.getDate() : hoy.getDate()
        var month = (((hoy.getMonth() +1) +'').length == 1) ? '0' + (hoy.getMonth() +1) : (hoy.getMonth() +1)
        return day + '/' + month + '/' + hoy.getFullYear()
    },

    getYesterday: function () {
        var ayer = new Date(Date.now() - 24*60*60*1000);
        var day =  ((ayer.getDate() + '').length == 1) ? '0' + ayer.getDate() : ayer.getDate()
        var month = (((ayer.getMonth() +1) +'').length == 1) ? '0' + (ayer.getMonth() +1) : (ayer.getMonth() +1)
        return day + '/' + month + '/' + ayer.getFullYear()
    },
	
	getWeek: function() {
        var semana = new Date(Date.now() - (24*60*60*1000)*1);
        var initSem = new Date(semana.getFullYear(), semana.getMonth(), semana.getDate() - (semana.getUTCDay() - 1))
        var finSem = new Date(semana.getFullYear(), semana.getMonth(), semana.getDate() - (semana.getUTCDay() - 1) +4)
        return [ initSem, finSem ]
    },

    getLastWeek: function() {
        var semPasada = new Date(Date.now() - (24*60*60*1000)*7);
        var initSem = new Date(semPasada.getFullYear(), semPasada.getMonth(), semPasada.getDate() - (semPasada.getUTCDay() - 1))
        var finSem = new Date(semPasada.getFullYear(), semPasada.getMonth(), semPasada.getDate() - (semPasada.getUTCDay() - 1) +4)
        return [ initSem, finSem ]
    },

    getMonth: function() {
        var today = new Date(Date.now())
        var initMon = new Date(today.getFullYear(), today.getMonth(),  1)
        var finMon = new Date(today.getFullYear(), today.getMonth(),  new Date(today.getFullYear(), today.getMonth(), 0).getDate()-1)
        return [ initMon, finMon ]
    },

    getLastMonth: function() {
        var today = new Date(Date.now())
        var initMon =new Date(today.getFullYear(), today.getMonth() -1,  1)
        var finMon = new Date(today.getFullYear(), today.getMonth() -1,  new Date(today.getFullYear(), today.getMonth(), 0).getDate()-1)
        return [ initMon, finMon ]
    }

})