/**
 * Created by Antonio on 2/16/2016.
 */
Ext.define('ProcessManagement.util.cmpc.HttpMethods', {
    singleton: true,

    delete: 'DELETE',
    get:    'GET',
    post:   'POST',
    put:    'PUT'
});