/**
 * Created by Antonio on 2/1/2016.
 */
Ext.define('ProcessManagement.util.cmpc.ServiceEndPoint', {
    singleton: true,

    _parseParams: function (params) {
        var str = "";
        if (params) {
            for (property in params) {

            }
        }

        return str;
    },
    getDomain: function () {
        return 'http://sgp.simbius.com:5555'; //Local
    },
    getProyecto: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/proyecto'
    },
    getTipoProyecto: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/tipoProyecto';
    },
    getProyectosByComuna: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/tipoProyectoByComuna';
    },
    getMarcoLogico: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/proyectoXML';
    },
    getObjetivoGeneral: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/objetivoGeneral';
    },
    getIndicadorObjGral: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/indicador';
    },
    getMediosVerificacion: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/medioVerificacion';
    },
    getComponente: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/componente';
    },
    getObjetivoEspecifico: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/objetivoEspecifico';
    },
    getTipoActividad: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/tipoActividad';
    },
    getCategoria: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/categoria';
    },
    getArea: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/area';
    },
    getNivel: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/nivel';
    },
    getTiposParticipantes: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/tipoParticipante';
    },
    getPeriodo: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/periodo';
    },
    getComuna: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/comuna';
    },
    getEstablecimiento: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/establecimiento';
    },
    getEstablecimientosComuna: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/establecimientosComuna';
    },
    getPlanta: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/planta';
    },
    getPersona: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/persona';
    },
    getProcesoGenerarMarcoLogico: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/ProcesoGenerarMarcoLogico';
    },
    getProcesoGenerarProyecto: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/ProcesoGenerarProyecto';
    },
    getSesion: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/sesion';
    },
    getPonderacion: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/ponderacion';
    },
    getEvaluacion: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/evaluacion';
    },
    getValidarMarcoLogico: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/validarMarcoLogico';
    },
    getPerfilPersona: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/perfilPersona';
    },
    getEstablecimientosDocente: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/establecimientosDocente';
    },
    getNivelesDocente: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/nivelesDocente';
    },
    getDocentesPorComuna: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/docentesPorComuna';
    },
    getDocentesPorComunaAndNivel: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/docentesPorCoumnaAndNivel';
    },
    getPlantasPorComuna: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/plantasPorComuna';
    },
    getSubtipoPerfil: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/subtipoPerfil';
    },
    getComunasPersona: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/comunasPersona';
    },
    getCursos: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/cursos';
    },
    getParticipantesSesion: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/participantesSesion';
    },
    getMetasRealesAnioProyecto: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/metasRealesAnioProyecto';
    },
    getPersonaPorPerfilAndComuna: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/personaPorPerfilAndComuna';
    },
    getPersonaTieneSesionesAsignadas: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/personaTieneSesionesACargo';
    },
    getContactoByPerfil: function () {
        return this.getDomain() + '/rest/CMPCGoogleAPI/fcmpc/contactoByPerfil';
    }
})