/**
 * Created by Antonio on 2/11/2016.
 */
Ext.define('ProcessManagement.util.cmpc.AjaxRequest', {
    singleton: true,

    request: function (url, method, jsonData, scope, successCbk, failureCbk, async, ptimeout) {
        Ext.Ajax.useDefaultXhrHeader = false;
        Ext.Ajax.cors = true;


        Ext.Ajax.request({
            url: url,
            method: method,
            jsonData: jsonData,
            scope: scope,
            async: (async)?false:true,
            timeout: (ptimeout)?ptimeout:30000,
            success: function (response, options) {
                if (successCbk) {
                    successCbk(response, options);
                }
            },
            failure: function (response, options) {
                if (failureCbk) {
                    failureCbk(response, options);
                }
            }
        });
    },

    requestWithParams: function (url, method, params, scope, successCbk, failureCbk, async) {
        Ext.Ajax.request({
            url: url,
            method: method,
            params: params,
            scope: scope,
            async: (async)?false:true,
            success: function (response, options) {
                if (successCbk) {
                    successCbk(response, options);
                }
            },
            failure: function (response, options) {
                if (failureCbk) {
                    failureCbk(response, options);
                }
            }
        });

    }
});