/**
 * Created by Antonio on 3/3/2016.
 */
Ext.define('ProcessManagement.util.cmpc.Messages', {
    singleton: true,

    error: function (msg, title, scope, callback) {
        Ext.Msg.show({
            msg: msg,
            title: title,
            scope: scope,
            icon: Ext.Msg.ERROR,
            buttons: Ext.Msg.OK,
            fn: function (buttonId, text, options) {
                if (callback) {
                    callback(buttonId, text, options)
                }
            }
        });
    },

    warning: function (msg, title, scope, callback) {
        Ext.Msg.show({
            msg: msg,
            title: title,
            scope: scope,
            icon: Ext.Msg.WARNING,
            buttons: Ext.Msg.OK,
            fn: function (buttonId, text, options) {
                if (callback) {
                    callback(buttonId, text, options)
                }
            }
        });
    },

    question: function (msg, title, scope, callback) {
        Ext.Msg.show({
            msg: msg,
            title: title,
            scope: scope,
            icon: Ext.Msg.QUESTION,
            buttons: Ext.Msg.YESNO,
            fn: function (buttonId, text, options) {
                if (callback) {
                    callback(buttonId, text, options)
                }
            }
        });
    },

    info: function (msg, title, scope, callback) {
        Ext.Msg.show({
            msg: msg,
            title: title,
            scope: scope,
            icon: Ext.Msg.INFO,
            buttons: Ext.Msg.OK,
            fn: function (buttonId, text, options) {
                if (callback) {
                    callback(buttonId, text, options)
                }
            }
        });
    }
});