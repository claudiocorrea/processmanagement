var cron = require('node-cron'),
    cronConfig = require('./nodejs/config/cronEmailJobs').getCronConfigEmailJobs(),
    alertsController = require('./nodejs/controller/alerts'),
    smtpConfig = require('./nodejs/config/mailConfig').getSMTPConfig(),
    nodemailer = require('nodemailer'),
    transporter = nodemailer.createTransport(smtpConfig),
    moment = require('moment');

    console.log("Cron config: " + JSON.stringify(cronConfig));

    global.DB = 'SPM';

    cron.schedule(cronConfig, function () {
        console.log("\n\nEjecutando cron a las " + new Date() + "\nPresione Ctrl+C para terminar");
        sendAlerts();
    });

    function sendAlerts() {
        //Busca las alertas que estan pendientes de envio
        alertsController.getAllPendingAlerts()
            .then(function (result) {

                if (result.success !== true) {
                    //console.log("Resultado getAllPendingAlerts: " + JSON.stringify(result.data));
                    result.data.forEach(function (alert) {
                        //console.log("\n\nAlerta: " + JSON.stringify(alert));
                        var message = {
                            from: smtpConfig.auth.user,
                            to: alert.ToEmail,
                            //to: smtpConfig.auth.user,
                            subject: alert.Subject,
                            text: alert.Body,
                            html: alert.Body
                        };

                        transporter.sendMail(message, function (error, info) {
                            if (error) {
                                console.log("Error al enviar el mensaje " + JSON.stringify(message) + "\n\t\t" + error);
                            } else {
                                var fechaStr = moment().format('DD/MM/YYYY'),
                                    horaStr = moment().format('HH:mm:ss'),
                                    res = {
                                        alertId: alert.IdJob,
                                        dateOut: fechaStr,
                                        timeOut: horaStr,
                                        result: 'ok'
                                    };

                                alertsController.markAlertAsRead(res);

                                //console.log("Mensaje enviado a las " + fechaStr + " " + horaStr);
                            }
                        });
                    });
                }
            })
            .catch(function (error) {
                console.log("Error al mandar alerta: " + error);
            });

    }
