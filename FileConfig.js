try {
	var server = null 
	try { server = window.location.hostname
	} catch (exp) {}
	if(!server) server = 'localhost'
	var urlExternServer = 'spm.simbius.cloud'
	var globalParams = {
		appName: 'Document Management',
		//appName: 'Fundación CMPC',
		lang: 'es',
		testing: false,

		uploadPathAdjuntos: 'C:/temp',

		urlSPM: 'http://' + server,
		urlServices: 'http://' + server + ':8081/',
		urlExternServer: urlExternServer,
		urlForms: 'http://' + urlExternServer + ':8080/form.html',
		urlEndPointQueriesAsmx: 'http://' + urlExternServer + ':8080/spm/webservice/queries.asmx?wsdl',
		urlKPI: 'http://' + urlExternServer + ':8082/#/app/',
		urlFileStore: 'http://' + urlExternServer + ':8080/SPM/SPMScan/action/vbnet-db.aspx?imgID='
	}
	module.exports = globalParams
} catch (exp) {}

var loginUser = null
var msgBox = null

function getAppParams () {
	var query = location.search.substring(1),
		values = {
			'false': false,
			'true': true,
			'null': null
		},
		paramRe = /([^&=]+)(=([^&]*))?/g,
		plusRe = /\+/g,  // Regex for replacing addition symbol with a space
		params = {}, match, key, val;

	while (match = paramRe.exec(query)) {
		key = decodeURIComponent(match[1].replace(plusRe, ' '));
		if (match[2]) {
			val = decodeURIComponent(match[3].replace(plusRe, ' '));
			if (val in values) {
				val = values[val];
			} else if (!isNaN(+val)) { val = +val; }
		} else { val = true; }
		params[key] = val;
	}
	return params;
}