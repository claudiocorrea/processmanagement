var $ = null
var myDiagram
var data

var configSize = 20
var eventSize = 30
var gatewaySize = 40
var nodeWidth = 180
var nodeHeight = 100

var nodeSpacing = 150

var baseColor = '#1E90FF'
var backgroundColor = '#f6f6f6'
var defaultFont = "12pt 'Open Sans', 'Helvetica Neue', helvetica, arial, verdana, sans-serif"

var infoTooltip = true

function initModel(p_data, p_readOnly) {
	data = p_data
	console.log('Iniciando modelo....', data)
	
	if ($ == null) $ = go.GraphObject.make;
	
	// DIAGRAMA
    myDiagram = $(go.Diagram, "myDiagram", {
        initialContentAlignment: go.Spot.TopCenter,
		layout: $(go.LayeredDigraphLayout,
                  { direction: 90, layeringOption: go.LayeredDigraphLayout.LayerLongestPathSource, columnSpacing: 50}),
		maxSelectionCount: 1,
		"SelectionDeleting": function (e) {
              //console.log("SelectionDeleting: ", e.subject.first())
			  if(e.subject.first().data.category != undefined)
			  deleteNode(e.subject.first())
		},
		"SelectionDeleted": function (e) { },
		"LinkDrawn": function (e) { },
		"toolManager.hoverDelay": 100,
		isReadOnly: true
	})
	
	//  NODOS
    var nodeTemplateMap = new go.Map("string", go.Node);
	nodeTemplateMap.add("step", createStepTemplate());
	nodeTemplateMap.add("event", createEventTemplate());
	nodeTemplateMap.add("gateway", createGatewayTemplate());
	myDiagram.nodeTemplateMap =  nodeTemplateMap;
	
	// LINKS
    myDiagram.linkTemplate = $(go.Link,
        {   routing: go.Link.AvoidsNodes, curve: go.Link.JumpGap, corner: 10, resegmentable: true,
            reshapable: true, relinkableFrom: true, relinkableTo: true, toEndSegmentLength: 10
        }, new go.Binding("points").makeTwoWay(),
        $(go.Shape, { stroke: "gray", strokeWidth: 3 }),
		$(go.Shape, "Circle",
			{   alignment: new go.Spot(100, 0, 150, 15),
				width: 18, height: 18, visible: false, cursor: 'pointer',
				strokeWidth: 1, stroke: 'gray', fill: 'darkgray',
				mouseEnter: function(e, obj) { obj.fill = 'lightgray'},
                mouseLeave: function(e, obj) { obj.fill = 'darkgray' },
				toolTip: infoTooltip ? createTooltip('conditions'): null
			}, new go.Binding("visible", "condition")
		),
        $(go.Shape, { toArrow: "Triangle", scale: 1.2, fill: "black", stroke: null }),
        $(go.TextBlock,
            {   name: "Label", editable: true, text: "label", segmentOffset: new go.Point(-10, -10), visible: false },
            new go.Binding("text", "text").makeTwoWay(),
            new go.Binding("visible", "visible").makeTwoWay())
    );
	
	createInfo()
	
	setModel(data)
	
}

//------------------------------------------   STEPS   -------------------------------------------------------------------

function createStepTemplate() {
	return $(go.Node, "Spot", {
			selectionAdorned: false,
			toolTip: infoTooltip ? createTooltip('groups'): null 
		}, new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Panel, "Auto",
            {   name: "panel", minSize: new go.Size(nodeWidth, nodeHeight),
                desiredSize: new go.Size(nodeWidth, nodeHeight)
            }, $(go.Panel, "Spot",
                $(go.Shape, "RoundedRectangle",
                    {   name: "shape", strokeWidth: 2, stroke: 'white',
                        parameter1: 0, cursor: !myDiagram.isReadOnly ? 'pointer': '',
                        portId: "", fromLinkable: true, toLinkable: true,
                        fromSpot: go.Spot.AllSides, toSpot: go.Spot.NotBottomSide
                    }, new go.Binding("fill", "IdState", setFillByState)
				), $(go.Shape, "Pointer",
                    {   alignment: new go.Spot(0, 0, nodeWidth - 15, 15),
						width: 18, height: 18, strokeWidth: 1, stroke: 'gray', fill: 'red'
					}, new go.Binding("visible", "RechazoEnFormularios")
                ), $(go.TextBlock,
                    {   alignment: go.Spot.Center, font: defaultFont,
                        textAlign: "start", margin: 0, stroke: 'white',
                        editable: false, cursor: 'pointer'
                    }, new go.Binding("text").makeTwoWay(),
					new go.Binding("stroke", "IdState", setStrokeByState)
                )
			))
		)
}

//------   PROPERTIES

function setFillByState(c) {
	if(c == 9) return 'lightgray'
	var colors = [ baseColor, "yellow", "green", "red", "orange" ]
    return c < colors.length ?  colors[c-1] : baseColor
}

function setStrokeByState(c) {
	var colors = [ baseColor, "yellow", "green", "red", "orange" ]
    return colors[c-1] != "yellow" && c != 9 ?  'white' : 'dimgray'
}

//------------------------------------------   EVENTS   ----------------------------------------------------------------

function createEventTemplate() {
    return $(go.Node, "Vertical",
        { 	locationSpot: go.Spot.Center,
            selectionAdorned: false
        }, new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
		$(go.Panel, "Spot",
            $(go.Shape, "Circle",
                {   name: "shape", fill: 'limegreen',
                    strokeWidth: 2, stroke: 'white',
                    desiredSize: new go.Size(eventSize, eventSize),
                    cursor: "pointer",
                    portId: "", fromLinkable: false, toLinkable: true,
                    fromSpot: go.Spot.AllSides, toSpot: go.Spot.NotBottomSide
                }, new go.Binding("fill", "type", function(s) { return s == 1 ? 'limegreen' : 'red' })
            )
		)
	)
}

//------------------------------------------   GATEWAY   ---------------------------------------------------------------

function nodeGatewaySymbolTypeConverter(s) {
    var tasks =  ["NotAllowed",
                  "ThinCross",      // 1 - Parallel
                  "Circle",         // 2 - Inclusive
                  "AsteriskLine",   // 3 - Complex
                  "ThinX",          // 4 - Exclusive  (exclusive can also be no symbol, just bind to visible=false for no symbol)
                  "Pentagon",       // 5 - double cicle event based gateway
                  "Pentagon",       // 6 - exclusive event gateway to start a process (single circle)
                  "ThickCross"]     // 7 - parallel event gateway to start a process (single circle)
    if (s < tasks.length) return tasks[s];
    return "NotAllowed"; // error
  }

function createGatewayTemplate() {
    return $(go.Node, "Vertical",
        { 	locationSpot: go.Spot.Center,
            selectionAdorned: false
		}, new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Panel, "Spot",
            $(go.Shape, "Diamond",
                {   name: "shape", fill: 'orange',
                    strokeWidth: 1, stroke: 'white',                    
                    desiredSize: new go.Size(gatewaySize, gatewaySize),
                    portId: "", fromLinkable: true, toLinkable: true, cursor: "pointer",
                    fromSpot: go.Spot.AllSides, toSpot: go.Spot.TopLeftSides
                }
            ), $(go.Shape, "NotAllowed",
				{ 	alignment: go.Spot.Center, stroke: 'white', fill: 'white', cursor: "pointer",
					desiredSize: new go.Size(gatewaySize -22, gatewaySize -22)},
                new go.Binding("figure", "type", nodeGatewaySymbolTypeConverter),
				new go.Binding("strokeWidth", "type", function(s) { return (s <=4 ) ? 3 : 1; }),
				new go.Binding("fill", "type", function(s) { return s == 2 ? 'orange' : 'white'; })
			)
		))
}
//------------------------------------------   OTHER THiNGS   ------------------------------------------------------------

function createTooltip(type) {
	var fieldTemplate = 
		$(go.Panel, "TableRow",
			$(go.TextBlock, {
				margin: new go.Margin(0, 2), column: 1, font: defaultFont,
				alignment: new go.Spot(0, 1)
			}, new go.Binding("text", "", function(o) {  
				if(type == 'groups') return o.approved && o.approved != 0 ? o.name + ' - ' + o.date : o.name 
				else return o.condition }),
			new go.Binding("stroke", "", function(o) { 
				if(o.approved) {
					if(o.approved == 1) return 'green'
					else if(o.approved == -1) return 'red'					
				} else if(o.mandatory && o.mandatory) return 'red'
				return 'black' 
			})
		));
		
	if(type == 'groups') {
		return $(go.Adornment, "Auto",
			new go.Binding("location", "loctooltip", go.Point.parse).makeTwoWay(go.Point.stringify),
			new go.Binding("visible", "", function() { return readOnly }),
			$(go.Shape, { fill: "#f6f6f6", strokeWidth: 2, stroke: 'white' }), //FONDO
				$(go.Panel, "Vertical",
					$(go.Panel, "Auto",
						{ 	stretch: go.GraphObject.Horizontal,
							minSize: new go.Size(200, 20)},
						$(go.Shape, { fill: baseColor, stroke: null }), //TITULO
						$(go.TextBlock, "GRUPOS", {
							alignment: go.Spot.Left, margin: 5,
							stroke: "white", font: defaultFont }
					)), $(go.Panel, "Table",
						{ 	padding: 1,
							minSize: new go.Size(150, 10),
							defaultStretch: go.GraphObject.Horizontal,
							itemTemplate: fieldTemplate 
						}, new go.Binding("itemArray", "Groups")
					), $(go.Panel, "Auto",
						{ stretch: go.GraphObject.Horizontal, minSize: new go.Size(200, 20) },
						$(go.Shape, { fill: baseColor, stroke: null }), //TITULO
						$(go.TextBlock, "APROBADORES", {
							alignment: go.Spot.Left, margin: 5,
							stroke: "white", font: defaultFont }
					)), $(go.Panel, "Table",
						{ padding: 1,
							minSize: new go.Size(150, 10),
							defaultStretch: go.GraphObject.Horizontal,
							itemTemplate: fieldTemplate },
						new go.Binding("itemArray", "Users"))
				)
			)
	} else if(type == 'conditions') {
		return $(go.Adornment, "Auto",
			new go.Binding("location", "loctooltip", go.Point.parse).makeTwoWay(go.Point.stringify),
			new go.Binding("visible", "", function() { return readOnly }),
			$(go.Shape, { fill: "#f6f6f6", strokeWidth: 2, stroke: 'white' }), //FONDO
				$(go.Panel, "Vertical",
					$(go.Panel, "Auto",
						{ 	stretch: go.GraphObject.Horizontal,
							minSize: new go.Size(200, 20)},
						$(go.Shape, { fill: baseColor, stroke: null }), //TITULO
						$(go.TextBlock, "CONDICIONES", {
							alignment: go.Spot.Left, margin: 5,
							stroke: "white", font: defaultFont }
					)), $(go.Panel, "Table",
						{ 	padding: 1,
							minSize: new go.Size(150, 10),
							defaultStretch: go.GraphObject.Horizontal,
							itemTemplate: fieldTemplate 
						}, new go.Binding("itemArray", "Conditions")
					)
				)
			)
	}
}

//------------------------------------------   INFO COLORS

function infoFigure() { return { figure: 'Circle', stroke: 'white', width: 18, height: 18, margin: 5 } }
function infoText() { return { font: defaultFont, alignment: go.Spot.Left, margin: 5, stroke: 'dimgray' } }
function createInfo() {	
		myDiagram.add( $(go.Part, { layerName: "Grid", _viewPosition: new go.Point(10,100) },
			 $(go.Panel, "Table",
				$(go.Shape, { row: 1, column: 0, fill: 'blue' }, infoFigure()),
				$(go.TextBlock, "No habilitado", { row: 1, column: 1 }, infoText()),
				$(go.Shape, { row: 2, column: 0, fill: 'yellow' }, infoFigure()),
				$(go.TextBlock, "Pendiente", { row: 2, column: 1 }, infoText()),
				$(go.Shape, { row: 3, column: 0, fill: 'green' }, infoFigure()),
				$(go.TextBlock, "Aprobado", { row: 3, column: 1 }, infoText()),
				$(go.Shape, { row: 4, column: 0, fill: 'red' }, infoFigure()),
				$(go.TextBlock, "Rechazado", { row: 4, column: 1 }, infoText()),
				$(go.Shape, { row: 5, column: 0, fill: 'lightgray' }, infoFigure()),
				$(go.TextBlock, "Ignorado", { row: 5, column: 1 }, infoText())
			 )
		));
		  
		  
		  
		myDiagram.addDiagramListener("ViewportBoundsChanged", function(e) {
			var diagram = e.diagram;
			diagram.startTransaction("fix Parts");
			diagram.parts.each(function(part) {
			  if(part._viewPosition) {
				part.position = diagram.transformViewToDoc(part._viewPosition);
				part.scale = 1/diagram.scale;
			  }
			});
			diagram.commitTransaction("fix Parts");
		});
}

//------------------------------------------   METHODS   -------------------------------------------------------------------

function setModel(data) {
	var model = createModel(data.nodes)
	myDiagram.model = new go.GraphLinksModel(model.nodesArray, model.linksArray)
}

function createModel(nodes) {
	var nodesArray =  [ { key: "1", category: "event", type: "1" } ]
	var linksArray = []
	if(nodes.length > 0) {
		nodes.forEach(function(step) { 	
			if(!existNode(nodesArray, step.key)) {
				//console.log(step)
				nodesArray.push(step)
				if(step.UltimoPaso) {
					nodesArray.push(createNode('end', 'event', '0', step.key))
					linksArray.push(createLink(step.key, 'end'))				
				}
			}
			linksArray.push(createLink(step.parent, step.key, step.UnionConCondicional ? true : false, step.Conditions))	
		})
	} else { 
		nodesArray.push(createEmptyNode(0,1)) 
		linksArray.push(createLink('1', 0))
		nodesArray.push(createNode('end', 'event', '0', 0))
		linksArray.push(createLink(0, 'end'))
	}
	return { "nodesArray": nodesArray, "linksArray": linksArray }
}

function existNode(list, key) {
	var exist = false
	list.forEach(function(obj) {
		if(obj.key == key)  { exist = true }
	})
	//console.log('Existe key: ' + key, exist)
	return exist
}

function createNode(key, category, type, parent) {
	return {
        key : key,
        category: category,
		type: type,
		parent: parent
	}
}

function createLink(from, to, condition, listConditions) { 
	condition = condition == undefined ? false : condition
	return { from: from, to: to, condition: condition, Conditions: listConditions} 
}